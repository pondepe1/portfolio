/**
 * Created by Wiedzmin on 29. 4. 2017.
 */

/* Funkce přepínající mezi HTML strukturami primární nabídky pro malá a velká rozlišení */
$(document).ready(function(){

    var x = $(window).width();
    if(x < 1075){
        $("#pull").html("<button>Menu <span class='fa fa-bars'></span></button>");
        $(".min-navigation-content").html(
            "<ul class='min-navigation'>"+
                "<li><a href='aktuality.html' title='Kategorie Aktuality - najdete zde spoustu novinek, akcí, soutěží, ...'>Aktuality</a></li>"+
                "<li class='actual'>O divadle</li>"+
                "<li><a href='vystoupeni.html' title='Kategorie Vystoupení - můžete zde zakoupit vstupenky, zjistit program'>Vystoupení</a></li>"+
                "<li class='inactive'>Služby</li>"+
                "<li><a href='kontakty.html' title='Kategorie Kontakty - Naleznete na nás jakýkoli kontakt'>Kontakty</a></li>"+
            "</ul>"
        );
        $(".full-navigation-wrapper").text("");
    }

    $(window).resize(function(){
        x = $(window).width();
        if(x < 1075){
            $("#pull").html("<button>Menu <span class='fa fa-bars'></span></button>");
            $(".min-navigation-content").html(
                "<ul class='min-navigation'>"+
                    "<li><a href='aktuality.html' title='Kategorie Aktuality - najdete zde spoustu novinek, akcí, soutěží, ...'>Aktuality</a></li>"+
                    "<li class='actual'>O divadle</li>"+
                    "<li><a href='vystoupeni.html' title='Kategorie Vystoupení - můžete zde zakoupit vstupenky, zjistit program'>Vystoupení</a></li>"+
                    "<li class='inactive'>Služby</li>"+
                    "<li><a href='kontakty.html' title='Kategorie Kontakty - Naleznete na nás jakýkoli kontakt'>Kontakty</a></li>"+
                "</ul>"
            );
            $(".full-navigation-wrapper").text("");
        }
        else{
            $("#pull").text("");
            $(".min-navigation-content").text("");
            $(".full-navigation-wrapper").html(
                "<ul class='full-navigation'>"+
                    "<li><a href='aktuality.html' title='Aktuální dění v RockOpeře'>Aktuality</a></li>"+
                    "<li class='actual'>O divadle</li>"+
                    "<li><a href='vystoupeni.html' title='Nákup vstupenek, program a archiv našich vystoupení'>Vystoupení</a></li>"+
                    "<li class='inactive'>Služby</li>"+
                    "<li><a href='kontakty.html' title='Kontakty na Rockoperu včetně spojů, mapy, ...'>Kontakty</a></li>"+
                "</ul>"
            );
        }
    });

});
