/**
 * Created by Wiedzmin on 29. 4. 2017.
 */

/**
 * Created by Wiedzmin on 29. 4. 2017.
 */

/* Funkce přepínající mezi HTML strukturami primární nabídky pro malá a velká rozlišení */
$(document).ready(function(){

    var x = $(window).width();
    if(x < 1075){
        $("#pull").html("<button>Menu <span class='fa fa-bars'></span></button>");
        $(".min-navigation-content").html(
            "<ul class='min-navigation'>"+
                "<li><a href='aktuality.html' title='Kategorie Aktuality - najdete zde spoustu novinek, akcí, soutěží, ...'>Aktuality</a></li>"+
                "<li><a href='odivadle.html' title='Kategorie O divadle - dozvíte se o nás spoustu zajímavostí'>O divadle</a></li>"+
                "<li><a href='vystoupeni.html' title='Kategorie Vystoupení - můžete zde zakoupit vstupenky, zjistit program'>Vystoupení</a></li>"+
                "<li class='inactive'>Služby</li>"+
                "<li class='actual'>Kontakty</li>"+
            "</ul>"
        );
        $(".full-navigation-wrapper").text("");
    }

    $(window).resize(function(){
        x = $(window).width();
        if(x < 1075){
            $("#pull").html("<button>Menu <span class='fa fa-bars'></span></button>");
            $(".min-navigation-content").html(
                "<ul class='min-navigation'>"+
                    "<li><a href='aktuality.html' title='Kategorie Aktuality - najdete zde spoustu novinek, akcí, soutěží, ...'>Aktuality</a></li>"+
                    "<li><a href='odivadle.html' title='Kategorie O divadle - dozvíte se o nás spoustu zajímavostí'>O divadle</a></li>"+
                    "<li><a href='vystoupeni.html' title='Kategorie Vystoupení - můžete zde zakoupit vstupenky, zjistit program'>Vystoupení</a></li>"+
                    "<li class='inactive'>Služby</li>"+
                    "<li class='actual'>Kontakty</li>"+
                "</ul>"
            );
            $(".full-navigation-wrapper").text("");
        }
        else{
            $("#pull").text("");
            $(".min-navigation-content").text("");
            $(".full-navigation-wrapper").html(
                "<ul class='full-navigation'>"+
                    "<li><a href='aktuality.html' title='Sekce vyhrazená pro informace o aktuálním dění'>Aktuality</a></li>"+
                    "<li><a href='odivadle.html' title='Sekce vyhrazená pro naše představení'>O divadle</a></li>"+
                    "<li><a href='vystoupeni.html' title='Sekce určená pro zakoupení vstupenek a přehled vystoupení'>Vystoupení</a></li>"+
                    "<li class='inactive'>Služby</li>"+
                    "<li class='actual'>Kontakty</li>"+
                "</ul>"
            );
        }
    });

});