/**
 * Created by Wiedzmin on 29. 4. 2017.
 */

/**
 * Created by Wiedzmin on 29. 4. 2017.
 */

/* Funkce přepínající mezi HTML strukturami primární nabídky pro malá a velká rozlišení */
$(document).ready(function(){

    var x = $(window).width();
    if(x < 1075){
        $("#pull").html("<button>Menu <span class='fa fa-bars'></span></button>");
        $(".min-navigation-content").html(
            "<ul class='min-navigation'>"+
                "<li class='actual'>Aktuality</li>"+
                "<li><a href='odivadle.html' title='Kategorie O divadle - dozvíte se o nás spoustu zajímavostí'>O divadle</a></li>"+
                "<li><a href='vystoupeni.html' title='Kategorie Vystoupení - můžete zde zakoupit vstupenky, zjistit program'>Vystoupení</a></li>"+
                "<li class='inactive'>Služby</li>"+
                "<li><a href='kontakty.html' title='Kategorie Kontakty - Naleznete na nás jakýkoli kontakt'>Kontakty</a></li>"+
            "</ul>"
        );
        $(".full-navigation-wrapper").text("");
    }

    $(window).resize(function(){
        x = $(window).width();
        if(x < 1075){
            $("#pull").html("<button>Menu <span class='fa fa-bars'></span></button>");
            $(".min-navigation-content").html(
                "<ul class='min-navigation'>"+
                    "<li class='actual'>Aktuality</li>"+
                    "<li><a href='odivadle.html' title='Kategorie O divadle - dozvíte se o nás spoustu zajímavostí'>O divadle</a></li>"+
                    "<li><a href='vystoupeni.html' title='Kategorie Vystoupení - můžete zde zakoupit vstupenky, zjistit program'>Vystoupení</a></li>"+
                    "<li class='inactive'>Služby</li>"+
                    "<li><a href='kontakty.html' title='Kategorie Kontakty - Naleznete na nás jakýkoli kontakt'>Kontakty</a></li>"+
                "</ul>"
            );
            $(".full-navigation-wrapper").text("");
        }
        else{
            $("#pull").text("");
            $(".min-navigation-content").text("");
            $(".full-navigation-wrapper").html(
                "<ul>"+
                    "<li class='actual'>Aktuality</li>"+
                    "<li><a href='odivadle.html' title='Informace o divadle - kdo za tím vším stojí'>O divadle</a></li>"+
                    "<li><a href='vystoupeni.html' title='Vstupenky, program vystoupení, archiv'>Vystoupení</a></li>"+
                    "<li class='inactive'>Služby</li>"+
                    "<li><a href='kontakty.html' title='Kontakty na nás včetně mapy'>Kontakty</a></li>"+
                "</ul>"
            );
        }
    });

});