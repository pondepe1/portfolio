/**
 * Created by Wiedzmin on 27. 4. 2017.
 */

/* Funkce ovládající zobrazení primární nabídky */
$(document).ready(function(){
    $("#pull").click(function(){
        if($(".min-navigation-wrapper ul").css("display") == "none")
            $(".min-navigation-wrapper ul").slideDown();
        else
            $(".min-navigation-wrapper ul").slideUp();
    }) ;
});