<?php

namespace Blog\Exceptions;


use Throwable;

class PostNotRemovedException extends BlogPersistenceException
{

    protected $code;
    protected $message;

    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->code = $code;
        $this->$message = $message;
    }

}