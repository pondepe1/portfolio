<?php

namespace Blog\Model;

class Attachment extends Model
{

    protected static $idSeq;

    protected $type;
    protected $name;
    protected $description;
    protected $location;

    use SerializableTrait;

    public function __construct(string $type, string $name, string $description)
    {
        $this->id = ++self::$idSeq;
        $this->type = $type;
        $this->name = $name;
        $this->description = $description;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getLocation(): string
    {
        return $this->location;
    }

    public function setName($name): Attachment
    {
        $this->name = $name;
        return $this;
    }

    public function setDescription($description): Attachment
    {
        $this->description = $description;
        return $this;
    }

    public function setLocation($location): Attachment
    {
        $this->location = $location;
        return $this;
    }

    public function preSerialize(array $properties)
    {
        return $properties;
    }

    public function postUnserialize()
    {
        // TODO: Implement postUnserialize() method.
    }

}