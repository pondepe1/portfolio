<?php

namespace Blog\Model;

class User extends Model
{

    static protected $idSeq = 0;

    protected $name;
    protected $email;

    use SerializableTrait;

    public function __construct()
    {
        $this->id = ++self::$idSeq;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setName(string $name): User
    {
        $this->name = $name;
        return $this;
    }

    public function setEmail(string $email): User
    {
        $this->email = $email;
        return $this;
    }

    public function preSerialize(array $properties)
    {
        return $properties;
    }

    public function postUnserialize()
    {
        // TODO: Implement postUnserialize() method.
    }

}