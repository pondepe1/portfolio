<?php

namespace Blog\Model;


class Text extends Attachment
{

    public function __construct(string $type, string $name, string $description)
    {
        parent::__construct($type, $name, $description);
    }

}