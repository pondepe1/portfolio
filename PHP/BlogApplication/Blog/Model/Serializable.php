<?php

namespace Blog\Model;


interface Serializable
{

    public function serialize(): string;
    public function unserialize(string $unserialized);

}