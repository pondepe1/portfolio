<?php

namespace Blog\Model;


class Blog extends Model
{

    protected static $idSeq = 0;

    protected $title;
    protected $author;
    protected $posts = array();

    use SerializableTrait;

    public function __construct(){
        $this->id = ++self::$idSeq;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getAuthor(): User
    {
        return $this->author;
    }

    public function getPosts(): array
    {
        return $this->posts;
    }

    public function setTitle(string $title): Blog
    {
        $this->title = $title;
        return $this;
    }

    public function setAuthor(User $author): Blog
    {
        $this->author = $author;
        return $this;
    }

    public function addPost(Post $post): Blog
    {
        $post->setBlog($this);
        $this->posts[$post->getId()] = $post;
        return $this;
    }

    public function removePost(Post $post): bool
    {
        if (key_exists($post->getId(), $this->posts)) {
            unset($this->posts[$post->getId()]);
            return true;
        }
        return false;
    }

    public function preSerialize(array $properties)
    {
        $posts = $properties["posts"];
        foreach($posts as $post){
            $properties["posts"][$post->getId()] = $post->getId();
        }
        return $properties;
    }

    public function postUnserialize()
    {
        $postsIds = $this->getPosts();
        foreach($postsIds as $postsId){
            $this->posts[$postsId] = Post::find($postsId);
        }
    }

}