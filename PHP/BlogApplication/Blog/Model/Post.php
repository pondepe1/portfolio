<?php

namespace Blog\Model;

use \dateTime;

class Post extends Model
{

    protected static $idSeq = 0;

    protected $blog;
    protected $title;
    protected $summary;
    protected $content;
    protected $modified;
    protected $published;

    protected $attachments = array();

    use SerializableTrait;

    /*public function __construct(string $title, string $content, $publish){
        $this->id = ++self::$idSeq;
        $this->title = $title;
        $this->content = $content;
        $this->modified = new dateTime();
        if($publish == null){ $this->published = new dateTime(); }
        else{ $this->published = new dateTime($publish); }
    }*/

    public function __construct()
    {
        $this->id = ++self::$idSeq;
        $this->modified = new dateTime();
    }

    public function getBlog(): Blog{
        return $this->blog;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getSummary(): string
    {
        return $this->summary;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function getModified(): string
    {
        return $this->modified;
    }

    public function getPublished(): string
    {
        return $this->published;
    }

    public function getAttachments(): array
    {
        return $this->attachments;
    }

    public function setBlog(Blog $blog): Post
    {
        $this->blog = $blog;
        $this->modified = date("d.m.Y H:i:s");
        return $this;
    }

    public function setTitle(string $title): Post
    {
        $this->title = $title;
        $this->modified = date("d.m.Y H:i:s");
        return $this;
    }

    public function setSummary(string $summary): Post
    {
        $this->summary = $summary;
        $this->modified = date("d.m.Y H:i:s");
        return $this;
    }

    public function setContent(string $content): Post
    {
        $this->content = $content;
        $this->modified = date("d.m.Y H:i:s");
        return $this;
    }

    public function setPublished(dateTime $published){
        $this->published = $published;
        return $this;
    }

    public function addAttachment(Attachment $att): Post
    {
        $att->setLocation($this);
        $this->attachments[$att->getId()] = $att;
        return $this;
    }

    public function removeAttachment(Attachment $att): Post
    {
        unset($this->attachments[$att->getId()]);
        return $this;
    }

    public function preSerialize(array $properties)
    {
        $properties["blog"] = $properties["blog"]->getId();
        return $properties;
    }

    public function postUnserialize()
    {
        $this->blog = Blog::find($this->blog);
    }

}