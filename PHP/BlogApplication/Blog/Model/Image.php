<?php

namespace Blog\Model;

abstract class Image extends Attachment
{

    public function __construct(string $type, string $name, string $description)
    {
        parent::__construct($type, $name, $description);
    }

}