<?php

namespace Blog\Model;


use Blog\Exceptions\NoBlogsException;
use Blog\Exceptions\NoSuchBlogException;
use Blog\Exceptions\NoSuchPostException;
use Blog\Service\StorageService;
use League\Flysystem\Exception;
use League\Flysystem\FileNotFoundException;
use League\Flysystem\Filesystem;
use League\Flysystem\FilesystemNotFoundException;

abstract class Model implements Serializable
{

    static protected $identityMap = array();

    protected $id;

    public function getId()
    {
        return $this->id;
    }

    private static function getStorage(): Filesystem
    {
        $fs = StorageService::getInstance()->getFileSystem();
        if(!$fs)
            throw new FilesystemNotFoundException("FileSystem was not set in StorageService! Please set the FileSystem first.".PHP_EOL);
        return $fs;
    }

    public static function getObjectPath($id = NULL)
    {
        $name = strtolower(static::class);
        $name = str_replace("\\", "/", $name);
        $name = $id ? $name."/".$id.".ser" : $name;
        //print_r($name);
        //echo PHP_EOL;
        return $name;
    }

    protected function nextId(): int
    {
        $fs = self::getStorage();
        $path = self::getObjectPath();
        $maxId = 1;
        foreach($fs->listContents($path) as $folderMeta){
            if($folderMeta["filename"] > $maxId)
                $maxId = $folderMeta["filename"];
        }
        return $maxId+1;
    }

    public static function walk()
    {
        $fs = self::getStorage();
        $collection = array();

        $objectType = self::getObjectPath();

        //print_r($objectType);

        foreach($fs->listContents($objectType) as $fileMeta){

            if(
                array_key_exists(self::getObjectPath(), self::$identityMap) &&
                array_key_exists($fileMeta["filename"], self::$identityMap[self::getObjectPath()])
            ){
                $collection[] = self::$identityMap[self::getObjectPath()][$fileMeta["filename"]];
                continue;
            }

            $instance = new static();
            try{
                $instance->unserialize($fs->read($fileMeta["path"]));
            }
            catch(Exception $e){

            }
            $collection[] = $instance;

            self::$identityMap[self::getObjectPath()][$fileMeta["filename"]] = $instance;

        }

        if(count($collection) === 0){
            if(self::getObjectPath() === Blog::getObjectPath())
                throw new NoBlogsException("There are no blogs saved.");
        }

        return $collection;
    }

    public static function find($id = 0)
    {
        if(
            array_key_exists(self::getObjectPath(), self::$identityMap) &&
            array_key_exists($id, self::$identityMap[self::getObjectPath()])
        ){
            return self::$identityMap[self::getObjectPath()][$id];
        }

        $fs = self::getStorage();
        $path = self::getObjectPath($id);

        $data = "";

        try{
            $data = $fs->read($path);
        }
        catch(FileNotFoundException $e){
            if(self::getObjectPath() === Blog::getObjectPath())
                throw new NoSuchBlogException("Blog could not be read during the storage find.".PHP_EOL, $id, $e);
            if(self::getObjectPath() === Post::getObjectPath())
                throw new NoSuchPostException("Post could not be read during the storage find.".PHP_EOL, $id, $e);
        }

        $object = new static();
        self::$identityMap[self::getObjectPath()][$id] = &$object;

        $object->unserialize($data);
        if(!($object instanceof static)){
            throw new \RuntimeException('Given $object ' . get_class($object) . " is not instance of " . static::class . PHP_EOL);
        }

        return $object;
    }

    public function save(){
        if(empty($this->getId()))
            $this->id = $this->nextId();

        $fs = self::getStorage();
        $data = $this->serialize();
        //print_r($data);
        $path = self::getObjectPath($this->getId());

        try{
            $fs->put($path, $data);
        }
        catch(Exception $e){
            throw new Exception("File could not be saved".PHP_EOL);
        }

        self::$identityMap[self::getObjectPath()][$this->getId()] = $this;
    }

    public function delete(){
        $fs = self::getStorage();
        $path = self::getObjectPath($this->getId());
        try{
            $fs->delete($path);
        }
        catch(Exception $e){
            throw new Exception("Data file of " . static::class . " with id = " . $this->getId() . " could not be deleted.".PHP_EOL);
        }
        unset(self::$identityMap[self::getObjectPath()][$this->getId()]);
    }

    public abstract function preSerialize(array $properties);

    public abstract function postUnserialize();

}