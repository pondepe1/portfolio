<?php

namespace Blog\Model;


trait SerializableTrait
{

    public function setProperties(array $valuesArr)
    {
        foreach($valuesArr as $key => $val){
            if(property_exists($this, $key)){
                $this->$key = $val;
            }
        }
    }

    public function serialize(): string
    {
        $properties = get_object_vars($this);
        $properties = $this->preSerialize($properties);
        return serialize($properties);
    }

    public function unserialize(string $serialized)
    {
        $properties = unserialize($serialized);
        $this->setProperties($properties);
        $this->postUnserialize();
        //print_r($this);
    }

}