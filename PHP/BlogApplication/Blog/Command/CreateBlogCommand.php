<?php

namespace Blog\Command;

use Blog\Model\User;
use Blog\Service\BlogService;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use \Symfony\Component\Console\Output\OutputInterface;


class CreateBlogCommand extends Command{

    protected function configure()
    {
        $this
            ->setName("blog:create")
            ->setDescription("Creates a new blog.")
            ->setHelp("This command allows you to create a new blog.")
            ->addArgument("title", InputArgument::REQUIRED, "Title of the blog.")
            ->addArgument("author", InputArgument::OPTIONAL, "Author of the blog.");

    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $output->writeln("You're going to create a blog.");
        $output->writeln("Title: ".$input->getArgument("title"));
        $output->writeln("Author: ".$input->getArgument("author"));

        $blogSrv = BlogService::getInstance();

        $blogSrv->create($input->getArgument("title"), User::find($input->getArgument("author")));

    }

}