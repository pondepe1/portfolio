<?php

namespace Blog\Command;

use Blog\Model\Blog;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use \Symfony\Component\Console\Output\OutputInterface;

class FindBlogCommand extends Command
{

    protected function configure()
    {
        $this
        ->setName("blog:find")
        ->setDescription("Finds blog with typed id.")
        ->setHelp("This command allows you to find a blog based on its.")
        ->addArgument("id", InputArgument::REQUIRED, "Id of the blog.");
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("You're looking for a blog with id ".$input->getArgument("id").".");

        $foundBlog = Blog::find($input->getArgument("id"));

        $output->writeln("Blog title: ".$foundBlog->getTitle());

    }

}