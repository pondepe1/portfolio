<?php

namespace Blog\Command;

use League\Flysystem\FileNotFoundException;

use Blog\Model\Blog;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use \Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;

class RemoveBlogCommand extends Command
{

    protected function configure()
    {
        $this
            ->setName("blog:remove")
            ->setDescription("Allows you to remove blogs.")
            ->setHelp("This command allows you to remove blogs.");
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("Blogs list:");
        $blogs = Blog::walk();
        $blogsIds = array();
        foreach($blogs as $blog){
            $blogsIds[$blog->getId()] = ($blog->getId());
            $output->writeln("#".$blog->getId().": ".$blog->getTitle());
        }
        $output->writeln("");

        $helper = $this->getHelper("question");
        $question = new Question("Please enter the index of blog to be removed. (:q)".PHP_EOL);

        $blogId = $helper->ask($input, $output, $question);

        if($blogId === ":q") return;

        $blog = Blog::find($blogId);
        $blog->delete();
    }

}