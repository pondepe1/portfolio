<?php

namespace Blog\Command;


use Blog\Model\Blog;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use \Symfony\Component\Console\Output\OutputInterface;

class WalkBlogsCommand extends Command
{

    protected function configure()
    {
        $this
            ->setName("blog:walk")
            ->setDescription("Walks all the blogs.")
            ->setHelp("This command allows you to walk all the blogs managed by the application.");
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("Blogs list:");
        $blogs = Blog::walk();
        foreach($blogs as $key => $blog){
            $output->writeln("#".$key.": ".$blog->getTitle());
        }

    }

}