<?php

namespace Blog\Service;


use League\Flysystem\Filesystem;

class StorageService
{

    protected $filesystem;

    use SingletonTrait;

    public function getFileSystem(): Filesystem
    {
        return $this->filesystem;
    }

    public function setFileSystem(Filesystem $filesystem)
    {
        $this->filesystem = $filesystem;
    }

}