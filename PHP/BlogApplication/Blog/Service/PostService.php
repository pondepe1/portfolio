<?php

namespace Blog\Service;

use Blog\Exceptions\NoSuchPostException;
use Blog\Model\Blog;
use Blog\Model\Post;

class PostService
{

    use SingletonTrait;

    public function create(Blog $blog, string $title, string $content, $publish, $attachments): PostService
    {
        $post = new Post();
        $post->setTitle($title)->setContent($content);
        if($publish == null)
            $post->setPublished(new \dateTime());
        else
            $post->setPublished(new \dateTime($publish));
        if($attachments != null){
            foreach($attachments as $att){
                $post->addAttachment($att);
            }
        }
        $blog->addPost($post);
        $post->save();
        return $this;
    }

    public function find(Blog $blog, int $id): Post
    {
        if(array_key_exists($id, $blog->getPosts())){
            return $blog->getPosts()[$id];
        }
        throw new NoSuchPostException("Non-existing post with id ".$id." in blog ".$blog->getId().".\n");
    }

    public function walk(Blog $blog): array
    {
        return $blog->getPosts();
    }

    public function remove(Blog $blog, Post $post): bool
    {
        if($blog->removePost($post))
            return true;
        return false;
    }

}