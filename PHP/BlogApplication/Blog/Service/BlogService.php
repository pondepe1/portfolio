<?php

namespace Blog\Service;

use Blog\Exceptions\BlogNotRemovedException;
use Blog\Exceptions\NoSuchBlogException;
use Blog\Model\Blog;
use Blog\Model\User;

class BlogService
{

    protected $blogs = array();

    use SingletonTrait;

    public function addBlog(Blog $blog): BlogService
    {
        $this->blogs[$blog->getId()] = $blog;
        return $this;
    }

    public function create(string $title, User $author){
        $newBlog = new Blog();
        $newBlog->setTitle($title)->setAuthor($author);
        $this->addBlog($newBlog);
        $newBlog->save();
        return $this;
    }

    public function findBlog(int $id): Blog
    {
        if(array_key_exists($id, $this->blogs))
            return $this->blogs[$id];
        throw new NoSuchBlogException("Non-existing blog with id ".$id." during find.\n");
    }

    public function walk(): array
    {
        return $this->blogs;
    }

    public function removeBlog(Blog $blog): bool
    {
        if(array_key_exists($blog->getId(), $this->blogs)){
            unset($this->blogs[$blog->getId()]);
            return true;
        }
        throw new BlogNotRemovedException("Blog ".$blog->getId()." not removed.");
    }

}