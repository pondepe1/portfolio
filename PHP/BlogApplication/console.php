<?php

require_once 'vendor/autoload.php';

use Symfony\Component\Console\Application;
use Blog\Service\BlogService;
use Blog\Service\PostService;
use Blog\Service\StorageService;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\ErrorHandler;

$log = new Logger("my_logger");
$log->pushHandler(new StreamHandler("var/data/logs.log", LOGGER::ERROR));

ErrorHandler::register($log);

$application = new Application();
$application->setCatchExceptions(false);

$adapter = new \League\Flysystem\Adapter\Local(__DIR__.'/var/data');
$filesystem = new \League\Flysystem\Filesystem($adapter);

$storage = StorageService::getInstance();
$blogService = BlogService::getInstance();
$postService = PostService::getInstance();

$storage->setFileSystem($filesystem);

// ... register commands
$application->add(new \Blog\Command\CreateBlogCommand());
$application->add(new \Blog\Command\FindBlogCommand());
$application->add(new \Blog\Command\WalkBlogsCommand());
$application->add(new \Blog\Command\RemoveBlogCommand());

$application->run();