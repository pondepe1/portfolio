<?php

$contentsArr = array();
$contentsArr[0] = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. In laoreet, magna id viverra tincidunt, sem odio bibendum justo, vel imperdiet sapien wisi sed libero. Nunc dapibus tortor vel mi dapibus sollicitudin. Praesent id justo in neque elementum ultrices. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Duis risus. Praesent id justo in neque elementum ultrices. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Aenean placerat. Nunc dapibus tortor vel mi dapibus sollicitudin. Et harum quidem rerum facilis est et expedita distinctio.";
$contentsArr[1] = "Curabitur vitae diam non enim vestibulum interdum. Aenean id metus id velit ullamcorper pulvinar. Duis ante orci, molestie vitae vehicula venenatis, tincidunt ac pede. Etiam dui sem, fermentum vitae, sagittis id, malesuada in, quam. Maecenas lorem. Proin mattis lacinia justo. Maecenas fermentum, sem in pharetra pellentesque, velit turpis volutpat ante, in pharetra metus odio a lectus. Fusce nibh. Pellentesque arcu. Nullam dapibus fermentum ipsum. In laoreet, magna id viverra tincidunt, sem odio bibendum justo, vel imperdiet sapien wisi sed libero.";
$contentsArr[2] = "Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Aenean fermentum risus id tortor. Phasellus et lorem id felis nonummy placerat. Integer tempor. In rutrum. Et harum quidem rerum facilis est et expedita distinctio. Sed vel lectus. Donec odio tempus molestie, porttitor ut, iaculis quis, sem. Cras pede libero, dapibus nec, pretium sit amet, tempor quis. Etiam dictum tincidunt diam. Mauris elementum mauris vitae tortor. Fusce tellus odio, dapibus id fermentum quis, suscipit id erat. Aliquam erat volutpat. Sed convallis magna eu sem. Nunc dapibus tortor vel mi dapibus sollicitudin. Nullam sit amet magna in magna gravida vehicula.";
$contentsArr[3] = "Quisque porta. Mauris elementum mauris vitae tortor. Morbi scelerisque luctus velit. Pellentesque arcu. Phasellus enim erat, vestibulum vel, aliquam a, posuere eu, velit. Praesent id justo in neque elementum ultrices. Aliquam erat volutpat. Praesent vitae arcu tempor neque lacinia pretium. Aliquam erat volutpat. Nullam faucibus mi quis velit. Phasellus faucibus molestie nisl. Nullam eget nisl. Aliquam erat volutpat.";
$contentsArr[4] = "Integer in sapien. Suspendisse nisl. Phasellus faucibus molestie nisl. Integer vulputate sem a nibh rutrum consequat. Fusce tellus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Maecenas aliquet accumsan leo. Aliquam in lorem sit amet leo accumsan lacinia. Quisque porta. Aenean fermentum risus id tortor. Aenean id metus id velit ullamcorper pulvinar. Duis risus. Aliquam ante. Mauris elementum mauris vitae tortor. Morbi imperdiet, mauris ac auctor dictum, nisl ligula egestas nulla, et sollicitudin sem purus in lacus. Nulla turpis magna, cursus sit amet, suscipit a, interdum id, felis. Etiam ligula pede, sagittis quis, interdum ultricies, scelerisque eu. Phasellus enim erat, vestibulum vel, aliquam a, posuere eu, velit. Ut tempus purus at lorem. Sed ac dolor sit amet purus malesuada congue.";
$contentsArr[5] = "In rutrum. Proin mattis lacinia justo. Vestibulum erat nulla, ullamcorper nec, rutrum non, nonummy ac, erat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Vestibulum fermentum tortor id mi. Etiam bibendum elit eget erat. In enim a arcu imperdiet malesuada. Nunc auctor. Vivamus luctus egestas leo. Ut tempus purus at lorem. Aliquam in lorem sit amet leo accumsan lacinia. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Mauris dictum facilisis augue. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Integer rutrum, orci vestibulum ullamcorper ultricies, lacus quam ultricies odio, vitae placerat pede sem sit amet enim. Nulla non arcu lacinia neque faucibus fringilla. Phasellus et lorem id felis nonummy placerat. Cras elementum.";



/*function autoloader(string $class){
    $file = str_replace('\\', '/', $class);
    $file .= ".php";
    if(file_exists($file))
        include $file;
}

spl_autoload_register("autoloader");*/

require 'vendor/autoload.php';

use Blog\Model\User;
use Blog\Model\Blog;
use Blog\Model\Post;
use Blog\Model\Text;
use Blog\Service\BlogService;
use Blog\Service\PostService;
use Blog\Service\StorageService;

$adapter = new \League\Flysystem\Adapter\Local(__DIR__.'/var/data');
$filesystem = new \League\Flysystem\Filesystem($adapter);

$storage = StorageService::getInstance();
$blogService = BlogService::getInstance();
$postService = PostService::getInstance();

$storage->setFileSystem($filesystem);

$userOne = new User();
$userOne->setName("User One")->setEmail("user1@example.org");
$userOne->save();
$userTwo = new User();
$userTwo->setName("User Two")->setEmail("user2@example.org");
$userTwo->save();

//$userOne = User::find(1);
//$userTwo = User::find(2);

//print_r(User::walk());
/*
try{
    Blog::walk();
}
catch(NoBlogsException $e){
    echo $e->getMessage();
}

try {
    $blogService->addBlog(Blog::find(1));
    $blogService->addBlog(Blog::find(2));
    $blogService->addBlog(Blog::find(3));
}
catch(NoSuchBlogException $e){
    echo $e->getMessage();
}*/

$blogService->create("One Random Blog", $userOne)->create("Notes", $userOne)->create("Shopping Lists", $userTwo);

$postService->create($blogService->findBlog(1), "First post", $contentsArr[0], null, array(new Text("Text", "First text attachment", "This is the first text attachment content.")));
$postService->create($blogService->findBlog(1), "Second post", $contentsArr[1], "7.12.2017 15:00", null);
$postService->create($blogService->findBlog(2), "Third post", $contentsArr[2], null, null);
$postService->create($blogService->findBlog(2), "Fourth post", $contentsArr[3], "8.12.2017 15:50", null);
$postService->create($blogService->findBlog(3), "Fifth post", $contentsArr[4], null, null);

$postService->find($blogService->findBlog(1), 1)->save();
$postService->find($blogService->findBlog(1), 2)->save();
$postService->find($blogService->findBlog(2), 3)->save();
$postService->find($blogService->findBlog(2), 4)->save();
$postService->find($blogService->findBlog(3), 5)->save();

$blogService->findBlog(1)->save();
$blogService->findBlog(2)->save();
$blogService->findBlog(3)->save();

print_r(Blog::walk());

//$blogService->findBlog(3)->delete();

//print_r(Blog::walk());