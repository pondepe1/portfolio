<?php

//Funkce naplňující pole náhodnými hodnotami
function randFillArr(&$arr, $size){

    for($i = 0; $i < $size; $i++){
        $arr[$i] = rand(0,9);
        if(rand(0,9) > 5) $arr[$i] = "non!num";
    }

}

//Funkce pro odstranění nečíselných hodnot z pole a jeho seřazení
function sortNumArr(&$arr){

    foreach($arr as $key => $item){
        if(!is_numeric($item)){
            unset($arr[$key]);
        }
    }

    sort($arr);

    $i = 0;
    $tmp = array();
    foreach($arr as $key => $item) {
        $tmp[$i++] = $arr[$key];
    }

    $arr = $tmp;

}

//Funkce pro seřazení pole od sudých po lichá čísla
function evenOddSortArr(&$arr){

    $nl = "<br/>";

    for($i = 0; $i < count($arr); $i++){
        for($j = 1; $j < count($arr)-$i; $j++){
            if(($arr[$j-1] % 2) !== 0){
                $tmp = $arr[$j-1];
                $arr[$j-1] = $arr[$j];
                $arr[$j] = $tmp;
            }
        }
    }

}