<?php

//Include připravené knihovny funkcí
include 'function_lib.php';

$nl = "\n";

$arr = array();
randFillArr($arr, 20);

echo "Original array: $nl";
print_r($arr);

sortNumArr($arr);

echo $nl.$nl;

print_r($arr);

evenOddSortArr($arr);

echo $nl.$nl;

echo "Sorted numeric array: $nl";

print_r($arr);

/*echo "Zadejte posloupnost čísel ukončenou znakem 0: $nl";

$fp = fopen("php://stdin", "r");

$arr = array();

do{

    $item = trim(fgets($fp, 1000));
    if(((boolean)$item) !== false) array_push($arr, $item);

} while(((boolean)$item) !== false);

print_r($arr);

$arr = sortNumArr($arr);

echo $nl.$nl;

echo "Seřazená číselná posloupnost: $nl";

print_r($arr);

fclose($fp);*/