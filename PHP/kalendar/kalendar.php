<!DOCTYPE html>
<html lang='cs'>
  <head>
    <title></title>
    <meta charset='Windows'>
    <meta name='description' content=''>
    <meta name='keywords' content=''>
    <meta name='author' content=''>
    <meta name='robots' content='all'>
    <!-- <meta http-equiv='X-UA-Compatible' content='IE=edge'> -->
    <link href='/favicon.png' rel='shortcut icon' type='image/png'>
    
    <style type="text/css">
    table{
      background-color: #EEE;
      box-shadow: 0px 0px 30px #BBBCCC;
      margin-left: 10%;
      border-radius: 2%;
    }
    
    td{
      padding: 2%;
      width: 6%;
      color: #600CAC;
    }
    
    #days{
      font-weight: bold;
      color: #3C0470;
    }
    
    </style>
    
  </head>
  <body>
  
  <?php

function pocetDnu($mesic,$rok){
  $pocet = cal_days_in_month(CAL_GREGORIAN,$mesic,$rok);
  return $pocet;  
}

function prvniDen($mesic,$rok){
  $prvni = date("N", mktime(0,0,0,$mesic,1,$rok));
  return $prvni;
}

function pocetTydnu($mesic,$rok){
  $pocet = pocetDnu($mesic,$rok);
  $tydny = date("W", mktime(0,0,0,$mesic,$pocet,$rok))
    - date("W", mktime(0,0,0,$mesic,1,$rok))+1;
  return $tydny;  
}

function kalendar($mesic,$rok){
$zacatek = 1;
$den = 1;

$pocet_dnu = pocetDnu($mesic,$rok);
$prvni_den = prvniDen($mesic,$rok);
$pocet_tydnu = pocetTydnu($mesic,$rok);

$dny_v_tydnu = array("Po","�t","St","�t","P�","So","Ne");

echo "<table>";
echo "<tr>";

for($i=0;$i<count($dny_v_tydnu);$i++){
  echo "<td id='days'>".$dny_v_tydnu[$i]."</td>";  
}

echo "</tr>";

for($i=0;$i<$pocet_tydnu;$i++){
    echo "<tr>";
      for($y=1;$y<=7;$y++){
          if($zacatek<$prvni_den || $den>$pocet_dnu){
            echo "<td>&nbsp;</td>";
            $zacatek++;
          }
          else{
            echo "<td>".$den++."</td>";
          }
        echo "</td>";
      }
    echo "</tr>";
  }   
}

echo "</table>";

kalendar(2,2015);
?>

  </body>
</html>