<?php

//Counter implementation with static scope variable
function counter1(){
    static $cnt = 0;
    $cnt++;
    return $cnt;
}

$cntTo = 5;

for($i = 0; $i < $cntTo; $i++) echo counter1()." ";


//Counter implementation with closure
function counter2($base = 0) {
  return function() use (&$base) {
      return ++$base;
  };
}

echo "\n";

$counter2Ins = counter2();
$counter2Ins2 = counter2();

echo $counter2Ins()." ";
echo $counter2Ins()." ";
echo $counter2Ins()." ";
echo "\n";
echo $counter2Ins2()." ";
echo $counter2Ins2()." ";

