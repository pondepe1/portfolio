<?php

function matchMacAddr($address){

    preg_match('/^[a-f0-9]{2}:[a-f0-9]{2}:[a-f0-9]{2}:[a-f0-9]{2}:[a-f0-9]{2}:[a-f0-9]{2}$/i', $address, $matches);
    print_r($matches);

    if($matches){
        $octets = preg_split('[:]', $address);
        return $octets;
    }

    return array();

}

print_r(matchMacAddr("a0:a0:b1:b1:c2:c2"));