<?php

function avgCalc(){

    $sum = 0;
    $cnt = 0;
    $tmpArr = array();

    foreach(func_get_args() as $item){
        if(is_numeric($item)){
            $tmpArr[$cnt++] = $item;
            $sum += $item;
        }
    }

    $arAvg = $sum/$cnt;

    usort($tmpArr, function($a, $b) {
        if ($a < $b) return  -1;
        if ($a > $b) return 1;
        return 0;
    });

    if(($cnt % 2) == 1){
        $medAvg = $tmpArr[(int)floor($cnt/2)];
    }
    else{
        $medAvg = (($tmpArr[($cnt/2)-1] + $tmpArr[$cnt/2])/2);
    }

    return array($arAvg, $medAvg);

}

$resArr =  avgCalc(4,2,3,100);

foreach($resArr as $item) echo $item."\n";