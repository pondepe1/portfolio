<!DOCTYPE html>
<html lang='cs'>
  <head>
    <title></title>
    <meta charset='utf-8'>
    <meta name='description' content=''>
    <meta name='keywords' content=''>
    <meta name='author' content=''>
    <meta name='robots' content='all'>
    <!-- <meta http-equiv='X-UA-Compatible' content='IE=edge'> -->
    <link href='/favicon.png' rel='shortcut icon' type='image/png'>
    <style type="text/css">
      body{
        margin: auto;
      }
      table{
        border-collapse: collapse;
        width: 100%;
      }
      tr:first-child{
        font-weight: bold;
        text-transform: uppercase;
      }
      td{
        border: 1px solid silver;
        width: 20%;
        padding: 1%;
      }
      
      
    </style>
  </head>
  
  <body>
  
  <?php
   
    $kurzy = file("http://www.cnb.cz/cs/financni_trhy/devizovy_trh/kurzy_devizoveho_trhu/denni_kurz.txt?date=17.03.2015"); 
    $kurzy_vel = count($kurzy);
    
    echo "<table>";
    
    for($i=1;$i<$kurzy_vel;$i++){
      $radka = explode("|",$kurzy[$i]);
      echo "<tr>";
        foreach($radka as $udaj){
          echo "<td>$udaj</td>";
        }
      echo "</tr>";  
    }
    
    echo "</table>";
    
  ?>

  </body>
</html>