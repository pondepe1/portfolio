<!DOCTYPE html>
<html lang='cs'>
  <head>
    <title></title>
    <meta charset='utf-8'>
    <meta name='description' content=''>
    <meta name='keywords' content=''>
    <meta name='author' content=''>
    <meta name='robots' content='all'>
    <!-- <meta http-equiv='X-UA-Compatible' content='IE=edge'> -->
    <link href='/favicon.png' rel='shortcut icon' type='image/png'>
    <style type="text/css">
      body{
        margin: auto;
      }
      table{
        border-collapse: collapse;
        width: 100%;
      }
      td{
        border: 1px solid silver;
        padding: 0.5%;
        width: 30%;
      }
      tr:first-child td{
        font-weight: bold;
      }
    </style>
    
  </head>
  
  <body>
  
  <?php
   
   $vzkaz = $_POST["vzkaz"];
   $jmeno = $_POST["jmeno"];
   $cas = date("H:i:s");
   $den = date("j");
   $mesic = date("n");
   $rok = date("Y");
   
   $fp = fopen("zapis.txt",'a');
   fwrite($fp,$cas." ".$den.".".$mesic.".".$rok."|".$vzkaz."|".$jmeno."\n");
   fclose($fp);
   
   $kniha = file("zapis.txt");
   
   echo "<table>";
   
   foreach($kniha as $retezec){
    $radka = explode("|",$retezec);
    echo "<tr>";
      foreach($radka as $polozka){
        echo "<td>$polozka</td>";
      }
    echo "</tr>";
   }
   
   echo "</table>";
   
   
   
  ?>

  </body>
</html>