<?php

echo "<h2>Na�ten� souboru do indexovan�ho pole pomoc� funkce file()</h2>";
foreach(file("text.txt") as $key => $item){
    echo "[".$key."] ".$item."<br/>";
}

echo "<h2>Na�ten� souboru znak po znaku pomoc� funkce fgetc()</h2>";
$file = fopen("text.txt","r");
while($character = fgetc($file)){
    echo $character;
}
fclose($file);

echo "<h2>Na�ten� souboru znak po znaku pomoc� funkc� fgetc() a feof()</h2>";
$file = fopen("text.txt","r");
while(!feof($file)){
    echo fgetc($file);
}
echo fgetc($file);
fclose($file);

echo "<h2>Na�ten� souboru ��dek po ��dku pomoc� funkce fgets()</h2>";
$file = fopen("text.txt","r");
while($row = fgets($file,4096)){
    echo $row;
}
fclose($file);

echo "<h2>Na�ten� souboru ��dek po ��dce pomoc� funkc� fgets() a feof()</h2>";
$file = fopen("text.txt","r");
while(feof($file) != true){
    echo fgets($file,4096);
}
fclose($file);

echo "<h2>Na�ten� cel�ho souboru pomoc� funkce fread()</h2>";
$file = fopen("text.txt","r");
echo fread($file,filesize("text.txt"));
fclose($file);

echo "<h2>Z�pis do souboru v re�imu p�id�v�n� pomoc� funkce fwrite()</h2>";
$file = fopen("text.txt","a");
fwrite($file,"\nPosledn� ��dek");
fclose($file);

echo "<h2Vytvo�en� souboru v p��pad�, �e neexistuje</h2>";
if(!file_exists("text2.txt")){
    fopen("text2.txt","x+");                          
}