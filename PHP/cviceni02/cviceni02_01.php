<?php

$nl = "\n";

function readToEof(){

    $fp = fopen("php://stdin", "r");
    $resArr = array();

    while( $line = fgets($fp, 1000) ){
        if($line != $GLOBALS['nl'])
            array_push($resArr, $line);
    }

    fclose($fp);

    return $resArr;

}

$arr = readToEof();

echo $nl;

print_r($arr);