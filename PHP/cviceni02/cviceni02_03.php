<?php

$nl = "\n";

function minMax(){
    echo "Max: ".PHP_INT_MAX;
    echo $GLOBALS['nl'];
    echo "Min: ".PHP_INT_MIN;
}

//Funkce přijímající libovolný počet argumentů
function printEvenArgs(){

    foreach(func_get_args() as $item){
        if($item %2 == 0) echo $item."\n";
    }

}

//Funkce přijímající libovolný počet argumentů
//zobrazující číselný hodnoty hex, dec, oct
function printArgsHexDecOct(){

    foreach(func_get_args() as $item){
        if(is_numeric($item)){
            echo "Dec: ".$item.$GLOBALS['nl'];
            echo "Hex: ".dechex($item).$GLOBALS['nl'];
            echo "Oct: ".decoct($item).$GLOBALS['nl'];
        }
    }

}

//Funkce zjišťující, zda je číslo zadané jako argument prvočíslo
function isPrime($num){

    $isPrime = true;

    if(($num % 1) != 0) return false;
    if(($num <= 0) || ($num == 1)) return false;

    for($i = 2; $i <= sqrt($num); $i++){
        if(($num % $i) == 0) $isPrime = false;
    }

    return $isPrime;

}

//Funkce počítající a zobrazující prvočísla menší než limit zadaný jako argument
function displayCountPrimes($lim){

    $arr = array();
    $arr[0] = false;
    $arr[1] = false;
    for($i = 2; $i < $lim; $i++) $arr[$i] = true;

    for($i = 2; $i < sqrt($lim); $i++){
        for($j = $i+1; $j < $lim; $j++){
            if(($j % $i) == 0)
                $arr[$j] = false;
        }
    }

    echo "Prime numbers:".$GLOBALS['nl'];

    $count = 0;
    foreach($arr as $key => $item){
        if($item){
            echo $key.$GLOBALS['nl'];
            $count++;
        }
    }

    echo "Prime numbers count: ".$count.$GLOBALS['nl'];

}


//minMax();

//echo $nl.$nl;

//printEvenArgs(1,2,3,4,5,6);

//echo $nl;

//printArgsHexDecOct(0b1111);

//echo $nl;

//echo isPrime(5);

echo $nl.$nl;

displayCountPrimes(100);