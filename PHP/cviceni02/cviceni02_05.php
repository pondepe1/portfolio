<?php

//Funkce pro výpis obsahu pole (na způsob iterátorů)
function printArray($arr){

    for(reset($arr); current($arr); next($arr)){
        echo current($arr);
    }

}

function printArray2($arr){

    $keys_arr = array_keys($arr);

    for($i = 0; $i < count($arr); $i++) echo $arr[$keys_arr[$i]
    ];


}

$arr = array('a' => 1, 2, 3, 4, 5);

//printArray($arr);

printArray2($arr);