<?php

$nl = "\n";

//Funkce porovnávající dvě čísla s plovoucí řádkou na určitou přesnost
function compareFloats($num1, $num2, $precision){

    if(abs($num1 - $num2) < $precision) return true;

    return false;

}

//echo compareFloats(1.111111, 1.111221, 0.0001);

//Funkce pro bezpečné porovnání dvou řetězců
function safeCompare($str1, $str2){

    $len = min(strlen($str1), strlen($str2));
    if(!($res = strncmp($str1, $str2, $len))){
        if(strlen($str1) < strlen($str2))
            return -1;
        else
            if(strlen($str1) > strlen($str2))
                return 1;
    }
    else return $res;

}

echo safeCompare("aaa", "aaa");

//Funkce pro sekvenční výpis řetězce
function secStringPrint($str){

    echo "Řetězec sekvenčně: ".$GLOBALS['nl'];

    for($i = 0; $i < strlen($str); $i++){
        echo $str[$i].$GLOBALS['nl'];
    }

    echo "Délka řetězce: ".strlen($str);

}

//Funkce určující, zda je řetězec palindrom
function isPalindrom($str){
    for($i = 0; $i < strlen($str); $i++) if($str[$i] != $str[strlen($str)-($i+1)]) return false;
    return true;
}

//Funkce pro serializaci hodnot do stringu
function serializeString($x, $y, $z, $t){

    $resStr = "";

    $resStr .= $x;
    $resStr .= $y;
    $resStr .= $z;

    for($i = 0; $i < 3; $i++) $resStr .= $t[$i];
    foreach($t['a'] as $item) $resStr .= $item;

    echo "Složený string: ".$resStr;

}

secStringPrint("asdf");

echo $nl;

echo isPalindrom("abba");

$s = 'this is a string';
$f = '3.3333e3';
$i = '3.333e3';
$a = [0, 1, 2, 'a' => [3, 'b' => 4, '5']];

//echo $nl;

//serializeString($s, $f, $i, $a);