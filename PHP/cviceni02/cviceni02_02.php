<?php

$nl = "\n";

function boolEqualTo($x){

    if($x == false){
        $res = 0;
        return $res;
    }
    else{
        if($x == true){
            $res = 1;
            return $res;
        }
        else{
            $res = 2;
            return $res;
        }
    }

}

function boolIdentityTo($x){

    if($x === false){
        $res = 0;
        return $res;
    }
    else{
        if($x === true){
            $res = 1;
            return $res;
        }
        else{
            $res = 2;
            return $res;
        }
    }

}

function printRes($x, $y){

    switch($x){
        case 0: echo "Rovno false.";
                break;
        case 1: echo "Rovno true.";
                break;
        default: echo "Nerovno true/false.";
    }

    echo $GLOBALS['nl'];

    switch($y){
        case 0: echo "Identické s false.";
            break;
        case 1: echo "Identické s true.";
            break;
        default: echo "Neidentické true/false.";
    }

}

printRes(boolEqualTo(""), boolIdentityTo(""));