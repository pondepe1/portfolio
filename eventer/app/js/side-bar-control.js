$(document).ready(function(){

    $(document).on('click', '#toggle-events-list', function(){
        $('.side-list').removeClass('side-list-hidden');
        $('.side-list').addClass('side-list-shown');
        if($(window).width() > 768){
            $('.side-list').css('width', '40%');
            $('#homepage-wrapper').css('marginRight', '40%');
        }
        else{
            $('.side-list').css('width', '100%');
            $('#homepage-wrapper').css('marginRight', '100%');
        }
        $('.side-list').css('right', '0%');
    });

    $(document).on('click', '#close-btn', function(){
        $('.side-list').removeClass('side-list-shown');
        $('.side-list').addClass('side-list-hidden');
        if($(window).width() > 768){
            $('.side-list').css('width', '40%');
            $('.side-list').css('right', '-40%');
        }
        else{
            $('.side-list').css('width', '100%');
            $('.side-list').css('right', '-100%');
        }
        $('#homepage-wrapper').css('marginRight', '0');
    });

    $(window).resize(function() {
        if($(window).width() > 768){
            if($('.side-list').hasClass('side-list-hidden')){
                $('.side-list').css('right', '-40%');
            }
            $('.side-list').css('width', '40%');
        }
        else{
            if($('.side-list').hasClass('side-list-hidden')){
                $('.side-list').css('right', '-100%');
            }
            if($('.side-list').hasClass('side-list-shown')){
                $('#homepage-wrapper').css('marginRight', '40%');
            }
            $('.side-list').css('width', '100%');
        }
    });

});