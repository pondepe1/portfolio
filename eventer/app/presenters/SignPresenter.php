<?php

namespace App\Presenters;

use Nette;
use Nette\Application\UI\Form;

class SignPresenter extends BasePresenter
{
    /** @var \App\Components\SignInForm @inject */
    public $signInFormFactory;

    protected function createComponentSignInForm()
    {
        $form = $this->signInFormFactory->create();
        $form->onSuccess[] = [$this, 'processSignInForm'];
        return $form;
    }

    public function processSignInForm(Form $form, Nette\Utils\ArrayHash $values)
    {
        if(!empty($values->email) && !empty($values->password)){
            if(!filter_var($values->email, FILTER_VALIDATE_EMAIL)){
                $this->flashMessage('Wrong email address.');
                $this->handleModal('signIn');
                return;
            }
            if($this->userManager->login($values->email, $values->password)){
                $this->redirect('Homepage:');
            }
            else{
                if($this->isAjax()){
                    $this->flashMessage('Wrong username or password.');
                    $this->handleModal('signIn');
                }
            }
        }
        else{
            if(empty($values->email))
                $this->flashMessage('Email required.');
            if(empty($values->password))
                $this->flashMessage('Password required.');
            $this->handleModal('signIn');
        }
    }

    public function handleLogout()
    {
        $this->userManager->logout();
    }
}