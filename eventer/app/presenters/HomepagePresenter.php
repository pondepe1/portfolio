<?php

namespace App\Presenters;

use Nette;
use Nette\Forms\Form;

class HomepagePresenter extends SignPresenter
{
    /** @var \App\Components\EventForm @inject */
    public $eventFormFactory;

    protected $eventAddMsg = '';

    protected $events = [];
    protected $closeEvents = [];
    protected $lastEventId;

    protected function createComponentEventForm()
    {
        $form = $this->eventFormFactory->create();
        $form->onSuccess[] = [$this, 'handleEventForm'];
        return $form;
    }

    public function handleEventForm(Form $form, Nette\Utils\ArrayHash $values)
    {
        if(!empty($values->title) && !empty($values->start) && !empty($values->end)){
            try {
                $this->storageManager->getEvents()->insert(
                    [
                        'id' => $this->storageManager->getLastEventId() + 1,
                        'title' => $values->title,
                        'start' => $values->start,
                        'end' => $values->end,
                        'user_id' => $this->userManager->getUserId()
                    ]
                );
                $this->eventAddMsg = "Event successfully added.";
            } catch (\PDOException $e) {
                $this->eventAddMsg = "Something went wrong. Event couldn't be added.";
            }

            $this->events = $this->storageManager->getUserEvents($this->userManager->getUserId());
            $this->closeEvents = $this->storageManager->getCloseEvents($this->userManager->getUserId(), 604800000);
            $events = $this->events->fetchAll();
            $this->lastEventId = end($events)['id'];
            if ($this->lastEventId == null)
                $this->lastEventId = 0;
            $this->redrawControl('closeEventsSnippet');
        }
        else{
            if(empty($values->title))
                $this->flashMessage('Title required.');
            if(empty($values->start))
                $this->flashMessage('Start date required.');
            if(empty($values->end))
                $this->flashMessage('End date required.');
            $this->redrawControl('eventAddFormSnippet');
        }
    }

    public function handleRemoveEvent($eventId)
    {
        $this->storageManager->getEvents()->where('id', $eventId)->delete();
        $this->events = $this->storageManager->getUserEvents($this->userManager->getUserId());
        $events = $this->events->fetchAll();
        $this->lastEventId = end($events)['id'];
        if($this->lastEventId == null)
            $this->lastEventId = 0;
        $this->payload->lastEventId = $this->lastEventId;
        $this->redrawControl('closeEventsSnippet');
    }

    public function renderDefault()
    {
        $this->events = $this->storageManager->getUserEvents($this->userManager->getUserId())->fetchAll();
        $this->closeEvents = $this->storageManager->getCloseEvents($this->userManager->getUserId(), 604800000);
        $this->lastEventId = $this->storageManager->getLastEventId();
        if($this->lastEventId == null)
            $this->lastEventId = 0;
        $this->template->lastEventId = $this->lastEventId;
        $this->template->events = $this->events;
        $this->template->closeEvents = $this->closeEvents;
        $this->template->eventAddMsg = $this->eventAddMsg;
    }
}
