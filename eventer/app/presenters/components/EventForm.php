<?php

namespace App\Components;

use Nette\Application\UI\Form;

class EventForm extends BaseForm
{
    public function create()
    {
        $form = parent::create();
        $form->addText('title')
            ->setHtmlId('event-title')
            ->setHtmlAttribute('class', 'form-control')
            ->setHtmlAttribute('placeholder', 'Title');
        $form->addText('start')
            ->setType('date')
            ->setHtmlAttribute('class', 'form-control')
            ->setHtmlId('event-start');
        $form->addText('end')
            ->setType('date')
            ->setHtmlAttribute('class', 'form-control')
            ->setHtmlId('event-end');
        $form->addSubmit('add_event', 'Add event')
            ->setHtmlAttribute('class', 'btn btn-primary')
            ->setHtmlId('event-submit');

        $form->getElementPrototype()->id = "event-form";

        return $form;
    }
}