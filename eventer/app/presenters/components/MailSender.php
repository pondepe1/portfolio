<?php

namespace App\Components;

use Nette\Bridges\ApplicationLatte\ILatteFactory;
use Nette\Bridges\ApplicationLatte\UIMacros;
use Nette\Mail\IMailer;
use Nette\Mail\Message;
use Nette\Application\LinkGenerator;

class MailSender
{
    /** @var \Nette\Bridges\ApplicationLatte\ILatteFactory */
    private $latteFactory;

    /** @var \Nette\Mail\IMailer */
    private $mailer;

    /** @var \Nette\Application\LinkGenerator */
    private $linkGenerator;

    public function __construct(ILatteFactory $latteFactory, IMailer $mailer, LinkGenerator $linkGenerator)
    {
        $this->latteFactory = $latteFactory;
        $this->mailer = $mailer;
        $this->linkGenerator = $linkGenerator;
    }

    public function sendEmail($email, $activationCode, $userId)
    {
        $latte = $this->latteFactory->create();
        UIMacros::install($latte->getCompiler());
        $latte->addProvider("uiControl", $this->linkGenerator);

        $params = [
            'activationCode' => $activationCode,
            'userId' => $userId
        ];

        $mail = new Message();
        $mail->setFrom("Eventer <eventer@wiedzminn.4fan.cz>")
            ->addTo($email)
            ->setSubject("Eventer Activation")
            ->setHtmlBody($latte->renderToString(__DIR__ . '/../templates/email.latte', $params));

        $myMailer = new \Nette\Mail\SmtpMailer([
            'host' => 'smtp.endora.cz',
            'username' => 'eventer@wiedzmin.4fan.cz',
            'password' => '6ysaz7dt',
            'secure' => 'ssl',
        ]);
        $myMailer->send($mail);
    }
}