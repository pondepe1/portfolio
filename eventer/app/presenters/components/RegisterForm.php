<?php

namespace App\Components;

use Nette\Application\UI\Form;

class RegisterForm extends BaseForm
{
    public function create()
    {
        $form = parent::create();
        $form->addText('email', 'Email')
            ->setRequired(false)
            ->setHtmlAttribute('class', 'form-control')
            ->setHtmlAttribute('placeholder','Email');
        $form->addPassword('password', 'Password')
            ->setRequired(false)
            ->setHtmlAttribute('class', 'form-control')
            ->setHtmlAttribute('placeholder', 'Password (At least 8 characters)');
        $form->addPassword('password_check', 'Password check')
            ->setRequired(false)
            ->setHtmlAttribute('class', 'form-control')
            ->setHtmlAttribute('placeholder', 'Password again (At least 8 characters)');
        $form->addSubmit('register', 'Register')
             ->setHtmlAttribute('class', 'btn btn-lg btn-block btn-primary');

        return $form;
    }
}