<?php

namespace App\Components;

use Nette\Application\UI\Form;

class SignInForm extends BaseForm
{
    public function create()
    {
        $form = parent::create();
        $form->addText('email', 'Email')
             ->setRequired(false)
             ->setHtmlAttribute('class', 'form-control')
             ->setHtmlAttribute('id', 'email')
             ->setHtmlAttribute('placeholder', 'Email');
        $form->addPassword('password', 'Password')
             ->setRequired(false)
             ->setHtmlAttribute('class', 'form-control')
             ->setHtmlAttribute('id', 'password')
             ->setHtmlAttribute('placeholder', 'Password');
        $form->addSubmit('sign', 'Sign in')
             ->setHtmlAttribute('class', 'btn btn-lg btn-block btn-primary')
             ->setHtmlAttribute('id', 'sign-submit');
        return $form;
    }
}