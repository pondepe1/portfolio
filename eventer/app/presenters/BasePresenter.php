<?php

namespace App\Presenters;

use App\Model\StorageManager;
use App\Model\UserManager;
use App\Components\MailSender;
use Nette;

class BasePresenter extends Nette\Application\UI\Presenter
{
    /** @var \App\Model\StorageManager  */
    protected $storageManager;

    /** @var \App\Model\UserManager */
    protected $userManager;

    /** @var \App\Components\MailSender */
    protected $mailSender;

    public function __construct(StorageManager $storageManager, UserManager $userManager, MailSender $mailSender)
    {
        parent::__construct();
        $this->storageManager = $storageManager;
        $this->userManager = $userManager;
        $this->mailSender = $mailSender;
    }

    public function handleModal($modalId)
    {
        $this->template->modal = $modalId;
        $this->redrawControl('modal');
    }
}