<?php

namespace App\Presenters;

use Nette;
use Nette\Application\UI\Form;
use Nette\Utils\Random;
use Nette\Security\Passwords;

class RegisterPresenter extends BasePresenter
{
    /** @var \App\Components\RegisterForm @inject */
    public $registerFormFactory;

    protected $values = [];
    protected $registerMsg = "";

    protected function createComponentRegisterForm()
    {
        $form = $this->registerFormFactory->create();
        $form->onSuccess[] = [$this, 'processRegisterForm'];
        return $form;
    }

    public function processRegisterForm(Form $form, Nette\Utils\ArrayHash $values)
    {
        if($this->isAjax()){

            if(!empty($values->email) && !empty($values->password) && !empty($values->password_check)){

                $error = false;

                if(strlen($values->password) < 8 || strlen($values->password_check) < 8){
                    $this->flashMessage('Password must have at least 8 characters.');
                    $error = true;
                }

                if($values->password !== $values->password_check){
                    $this->flashMessage("Passwords don't match. Please try again.");
                    $error = true;
                }

                if(!filter_var($values->email, FILTER_VALIDATE_EMAIL)){
                    $this->flashMessage('Wrong email address.');
                    $error = true;
                }

                if($error){
                    $this->handleModal('registration');
                    return;
                }

                $row = $this->storageManager->getUsers()->where('email', $values->email)->fetch();

                if ($row) {
                    $this->flashMessage("E-mail address already registered.");
                    $this->handleModal('registration');
                    return;
                }

                $this->storageManager->getUsers()->insert(
                    [
                        'email' => $values->email,
                        'password' => Passwords::hash($values->password)
                    ]
                );

                $row = $this->storageManager->getUsers()->where('email', $values->email)->fetch();

                if ($row) {
                    $activationCode = Random::generate(10);
                    $this->storageManager->getConfirmations()->insert(
                        [
                            'code' => $activationCode,
                            'user_id' => $row->id
                        ]
                    );
                    $this->mailSender->sendEmail($values->email, $activationCode, $row->id);
                    $this->flashMessage("Confirmation e-mail has been sent to your address. Please, activate your account.");
                    $this->handleModal('registration');
                } else {
                    $this->storageManager->getUsers()->where('email', $values->email)->delete();
                    $this->flashMessage("Something went wrong.");
                    $this->handleModal('registration');
                    return;
                }
            }
            else{
                if(empty($values->email))
                    $this->flashMessage('Email address required');
                if(empty($values->password) || empty($values->password_check))
                    $this->flashMessage('Password required.');
                $this->handleModal('registration');
            }
        }
    }

    public function renderDefault()
    {
        $this->template->registerMsg = $this->registerMsg;
    }
}