<?php

namespace App\Presenters;

class ActivationPresenter extends BasePresenter
{
    public function renderDefault($activationCode, $userId)
    {
        $row = $this->storageManager->getConfirmations()->where('code = ? AND user_id = ?', $activationCode, $userId)->fetch();
        if($row)
        {
            $this->storageManager->getUsers()->where('id', $userId)->update(
                [
                    'confirmed' => 1
                ]
            );
            $this->storageManager->getConfirmations()->where('user_id', $userId)->delete();
            $this->redirect('Homepage:');
        }
    }
}