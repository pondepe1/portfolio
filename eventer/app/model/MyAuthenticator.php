<?php

use Nette\Security as NS;

class MyAuthenticator implements NS\IAuthenticator
{
    public $database;

    function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    function authenticate(array $credentials)
    {
        list($username, $password) = $credentials;
        $row = $this->database->table('users')
                              ->where('email', $username)
                              ->fetch();

        if(!$row){
            throw new NS\AuthenticationException('User not found.');
        }

        if(!NS\Passwords::verify($password, $row->password)){
            throw new NS\AuthenticationException('Invalid password.');
        }

        if($row->confirmed){
            return new NS\Identity($row->id, 'confirmed', $row->email);
        }
        return new NS\Identity($row->id, 'unconfirmed', $row->email);
    }
}