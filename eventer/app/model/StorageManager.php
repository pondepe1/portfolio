<?php

namespace App\Model;

use Nette;
use Nette\Database\Context;

class StorageManager
{
    /** @var Nette\Database\Context */
    private $database;

    public function __construct(Context $database)
    {
        $this->database = $database;
    }

    public function getDatabase()
    {
        return $this->database;
    }

    public function getUsers()
    {
        return $this->database->table('users');
    }

    public function getConfirmations()
    {
        return $this->database->table('confirmation');
    }

    public function getEvents(){
        return $this->database->table('events');
    }

    public function getUserEvents($userId){
        return $this->database->table('events')->where('user_id', $userId);
    }

    public function getCloseEvents($userId, $limit){
        $events = $this->getUserEvents($userId)->fetchAll();
        $closeEvents = [];
        foreach($events as $event){
            if((strtotime($event['start']) - time()) <= $limit){
                array_push($closeEvents, $event);
            }
        }
        return $closeEvents;
    }

    public function getLastEventId()
    {
        $events = $this->getEvents()->fetchAll();
        return end($events)['id'];
    }
}