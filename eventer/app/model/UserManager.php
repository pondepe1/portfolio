<?php

namespace App\Model;

use Nette;
use Nette\Security\User;

class UserManager
{
    /** @var Nette\Security\User */
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function getUserId()
    {
        return $this->user->getId();
    }

    public function login($email, $password)
    {
        try{
            $this->user->login($email, $password);
        }
        catch(Nette\Security\AuthenticationException $e){
            return false;
        }
        return true;
    }

    public function logout()
    {
        $this->user->logout();
    }
}