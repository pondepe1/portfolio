<?php

$server = 'localhost';
$user = 'root';
$password = '';
$dbname = 'eventer_db';

$conn = new mysqli($server, $user, $password, $dbname);

if($conn->connect_error){
    die('Connection failed: '.$conn->connect_error);
}

$sql = "SET NAMES utf8";
$conn->query($sql);

$userId = $_GET['userId'];

$sql = "SELECT * FROM events WHERE user_id = $userId";

$event_array = [];
$result = $conn->query($sql);

if($result->num_rows > 0){
    while($row = $result->fetch_assoc()){
        array_push($event_array, $row);
    }
}

echo json_encode($event_array);

$conn->close();