# Portfolio

## BI-VWM Semestrální práce
Práce zaměřená na efektivní vyhledávání v souborové databázi.
Realizace pomocí technologií Angular5, NodeJS a Python.

## BI-ZWU Semestrální práce
Práce zaměřená na vytvoření přístupného a použitelného webu.
Realizace pomocí technologií HTML a CSS + jQuery.

## PHP
Soubor obsahující jednu větší a několik menších úloh realizovaných pomocí PHP.

## IAN Photography
Webové stránky vytvořené pro amatérského fotografa v rámci dlouhodobé maturitní práce.
Realizace pomocí HTML, CSS, PHP, JavaScriptu, AJAXu a šablonovacího systému Smarty.

### Odkaz na web
http://ianphoto.16mb.com/

## Eventer
Webová aplikace vytvořená za pomoci PHP frameworku Nette, jQuery, Bootstrapu.  
Implementuje přihlášení a registraci uživatele včetně ověření emailu.  
Obsahuje kalendář (plugin FullCalendar) s klikatelnými dny. Kliknutí na den v kalendáři vyvolá formulář s možností vytvoření události.  
Vytvořená událost je ihned zobrazena v kalendáři. Rovněž obsahuje vysouvací seznam událostí daného uživatele s možností zrušení události.  
Formuláře jsou realizované za využití Nette Forms, pro obsah ověřovacího emailu je využita latte šablona.  
Aplikace obsahuje ajaxové požadavky řešené s využitím nette.ajax.js pluginu.  
Front-end aplikace je stylizován převážně za využítí bootstrapu. Pro zobrazení upozornění a formuláře pro vytvoření události byl využit Bootstrap modal.

### Odkaz na web
http://wiedzmin.4fan.cz/