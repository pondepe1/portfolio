<?php

require_once "php_solutions/database_manipulation.php";
require "libs/Smarty.class.php";

$smarty = new Smarty();

if(isset($_GET['currentPage']) && !empty($_GET['currentPage'])){
    $currentPage = $_GET['currentPage'];
}
else $currentPage = 1;
//print_r($currentPage);

$articleId = $_GET['articleId'];

$blogArticle = $dbDataMan->getDbData($con,"SELECT articles.id,articles.time_stamp,articles.article_title,articles.content,articles.title_photo,categories.category_title,users.login
        FROM articles INNER JOIN categories ON articles.category = categories.id
        INNER JOIN users ON articles.author = users.id
        WHERE articles.id = $articleId");
//print_r($blogArticle);

if($blogArticle != false){
    $articleInnerPhotos = $dbDataMan->getDbData($con,"SELECT in_article_photo_path FROM in_article_photos
                                                      WHERE article = ".$blogArticle[0]['id']);
    //print_r($articleInnerPhotos);
}

$smarty->assign("articleInnerPhotos",$articleInnerPhotos);
$smarty->assign("blogArticle",$blogArticle);
$smarty->assign("articleId",$articleId);
$smarty->assign("currentPage",$currentPage);
$smarty->assign("userContent",$userContent);
$smarty->assign("mainMenuGaleryFiles",$mainMenuGaleryFiles);
$smarty->assign("mainMenuCategoryFiles",$mainMenuCategoryFiles);
$smarty->display("article.tpl");