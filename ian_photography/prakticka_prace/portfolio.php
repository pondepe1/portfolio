<?php

require_once "php_solutions/database_manipulation.php";
require "libs/Smarty.class.php";

$smarty = new Smarty();

$portfolioPhotos = $dbDataMan->getDbData($con,"SELECT photo_title,photo_path FROM photos
                                               ORDER BY id DESC");
//print_r($portfolioPhotos);

$smarty->assign("portfolioPhotos",$portfolioPhotos);
$smarty->assign("userContent",$userContent);
$smarty->assign("mainMenuGaleryFiles",$mainMenuGaleryFiles);
$smarty->assign("mainMenuCategoryFiles",$mainMenuCategoryFiles);
$smarty->display("portfolio.tpl");