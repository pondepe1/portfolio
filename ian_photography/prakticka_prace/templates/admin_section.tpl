<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title>IAN Photography</title>

    <!-- PŘIPOJENÍ CSS STYLŮ -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css_styles/general_styles.css"/>
    <link rel="stylesheet" type="text/css" href="css_styles/loading_screens.css"/>    
    <link rel="stylesheet" type="text/css" href="css_styles/header_styles.css"/>
    <link rel="stylesheet" type="text/css" href="css_styles/footer_styles.css"/>
    <link rel="stylesheet" type="text/css" href="css_styles/admin_section_styles.css"/>
    <link rel="stylesheet" type="text/css" href="css_styles/responsive_styles.css"/>
</head>



<body>



    <!-- DEFINICE LOADING SCREENU PRO LOADING DOM STRUKTURY DOKUMENTU -->
    <div class="cssloader-wrapper">

        <div class="cssload-loader">
            <div class="cssload-flipper">
                <div class="cssload-front"></div>
                <div class="cssload-back"></div>
            </div>
            <h4>Loading...</h4>
        </div>

    </div>
    
    
    <!-- DEFINICE LOADING SCREENU PRO ZPRACOVÁNÍ PHP APLIKACE V RÁMCI AJAXU -->
    <div id="floatingCirclesG-wrapper">
        <div id="floatingCirclesG">
            <div class="f_circleG" id="frotateG_01"></div>
            <div class="f_circleG" id="frotateG_02"></div>
            <div class="f_circleG" id="frotateG_03"></div>
            <div class="f_circleG" id="frotateG_04"></div>
            <div class="f_circleG" id="frotateG_05"></div>
            <div class="f_circleG" id="frotateG_06"></div>
            <div class="f_circleG" id="frotateG_07"></div>
            <div class="f_circleG" id="frotateG_08"></div>
        </div>
    </div>
    
    
    <!-- Výpis HTML struktury administračního rozhraní pouze v případě úspěšného přihlášení, viz. login.php -->
    {if isset($smarty.session['logged']) && $smarty.session['logged'] == true}

    <!-- DEFINICE WRAPPERU DOKUMENTU -->
    <div class="content-wrapper">


    <!-- DEFINICE WRAPPERU HLAVIČKY WEBU -->
    <header>

        <!-- DEFINICE UŽIVATESLKÉHO ADMINISTRAČNÍHO PANELU (pouze v případě úspěšného přihlášení uživatele -> $_SESSION['logged'] == true, viz. login.php) -->
        <div class='user_admin_bar'>
            <!-- Uvítací titulek uživateslkého panelu -> obsahuje uživatelské jméno, viz. login.php -->
            <p>Uživatel {$smarty.session['userData']['login']}</p>
            <div class='full_resolution_links'>  
                <a href='php_solutions/logout.php?backupFile=../index.php'>ODHLÁSIT SE</a>
                <a href='{$backupFile}'>ZPĚT</a>  
            </div>
            <div class='dropdown'>
                <button class='btn btn-default dropdown-toggle' type='button' id='dropdownMenu1' data-toggle='dropdown' aria-haspopup='true' aria-expanded='true'>
                    MOŽNOSTI
                    <span class='glyphicon glyphicon-triangle-bottom'></span>
                    <span class='glyphicon glyphicon-triangle-top'></span>            
                </button>
                <ul class='dropdown-menu' aria-labelledby='dropdownMenu1'>
                    <li><a href='{$backupFile}'>ZPĚT</a></li>
                    <li><a href='php_solutions/logout.php?backupFile=../index.php'>ODHLÁSIT SE</a></li>
                </ul>
            </div>
        </div>

    </header>


    <!-- DEFINICE WRAPPERU ADMINISTRAČNÍ ČÁSTI -->    
    <div class='admin-section-wrapper'>

        <!-- DEFINICE NAVIGACE MEZI KATEGORIEMI UŽIVATELSKÝCH NÁSTROJŮ REDAKČNÍHO SYSTÉMU -->
        <nav class='rstools-categories-wrapper'>
            <!-- Navigace mezi kategoriemi nástrojů realizována za využití bootstrap pills -->
            <ul class="nav nav-pills nav-stacked" role="tablist">
                <!-- Bootstrap pills realizuje navigaci za využití záložek v dokumentu -> href="#zalozkaId" -->
                <li role="presentation" class="active"><a href="#photo-tools" aria-controls="photo-tools" role="tab" data-toggle="tab">NÁSTROJE SPRÁVY FOTOGALERIE</a></li>
                <li role="presentation"><a href="#blog-tools" aria-controls="blog-tools" role="tab" data-toggle="tab">NÁSTROJE SPRÁVY BLOGU</a></li>
                <li role="presentation"><a href="#user-content-tools" aria-controls="user-content-tools" role="tab" data-toggle="tab">NÁSTROJE UŽIVATELSKÉ EDITACE</a></li>
            </ul>
        </nav>

        <div class="tab-content tab-content-wrapper">
            <!-- DEFINICE KATEGORIE NÁSTROJE SPRÁVY FOTOGALERIE -->
            <div role="tabpanel" class="tab-pane active" id="photo-tools">
                <h2>NÁSTROJE SPRÁVY FOTOGALERIE</h2>
                <!-- Navigace mezi jednotlivými nástroji v rámci jedné kategorie realizováná za využití bootstrap tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <!-- Bootstrap tabs rovněž realizuje navigaci za využití záložek v dokumentu -->
                    <li role="presentation" class="active"><a href="#photo-add" aria-controls="home" role="tab" data-toggle="tab">PŘIDAT FOTOGRAFII</a></li>
                    <li role="presentation"><a href="#photo-edit" aria-controls="profile" role="tab" data-toggle="tab">EDITOVAT/ODEBRAT FOTOGRAFII</a></li>
                    <li role="presentation"><a href="#galery-add" aria-controls="settings" role="tab" data-toggle="tab">ZALOŽIT GALERII</a></li>
                    <li role="presentation"><a href="#galery-edit" aria-controls="settings" role="tab" data-toggle="tab">EDITOVAT/ODEBRAT GALERII</a></li>
                </ul>
                <div class="tab-content">
                    <!-- DEFINICE NÁSTROJE PRO PŘIDÁNÍ FOTOGRAFIE -->
                    <div role="tabpanel" class="tab-pane active" id="photo-add">
                        <h3>Přidat fotografii</h3>
                        <!-- DEFINICE FORMULÁŘE PRO PŘIDÁNÍ FOTOGRAFIE -> multipart/form-data -->
                        <form id="photo-add-form" role="form" method="post" action="php_solutions/redaction_system.php?tool=photoAdd&userId={$smarty.session['userData']['id']}" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="photo-add-title">Zadejte název fotografie</label>
                                <input class="form-control" type="text" name="photo-add-title" id="photo-add-title"/>
                                <label for="photo-add-galery">Zvolte galerii</label>
                                <select name="photo-add-galery" id="photo-add-galery" class="form-control">
                                    <!-- Jednotlivé galerie jako možnosti elementu select načítány z databáze, viz. database_load.php -->
                                    {foreach $galeryFiles as $galeryFile}
                                    <option>{$galeryFile.galery_title}</option>
                                    {/foreach}
                                </select>
                                <input id="photo-add-file" class="input-file" type="file" name="photo-add-file"/>
                                <label id="photo-add-label" class="btn btn-default" for="photo-add-file"><span class="glyphicon glyphicon-upload"></span><span>Vyberte soubor</span></label>
                                <input class="btn btn-lg btn-info" type="submit" value="Nahrát fotografii"/>
                            </div>
                        </form>
                        <!-- DEFINICE PROGRESS BARU -->
                        <div class="progress">
                            <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%"></div>
                        </div>
                        <!-- DEFINICE INFORMAČNÍHO MODALU PRO NÁSTROJ PŘIDÁNÍ FOTOGRAFIE -->
                        <div class="modal fade" id="photo-add-info-modal" tabindex="-1" role="dialog" aria-labelledby="photo-add-info-modal-label">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h3 class="modal-title" id="photo-add-info-modal-label">ZPRÁVA O STAVU UPLOADU</h3>
                                    </div>
                                    <div class="modal-footer">
                                        <h4></h4>
                                        <div class="image"></div>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">ZAVŘÍT</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- DEFINICE NÁSTROJE PRO EDITACI A ODSTANĚNÍ FOTOGRAFIE -->
                    <div role="tabpanel" class="tab-pane" id="photo-edit">
                        <h3>Editovat/odebrat fotografii</h3>
                        <!-- DEFINICE TABULKY OBSAHUJÍCÍ EDITOVATELNÉ FOTOGRAFIE -->
                        <table class="table table-hover photo-edit-table">
                            <!-- Jednotlivé záznamy fotografií načítány z databáze, viz. admin_section.php -->
                            {foreach $photosEditArray as $photo}
                            <tr id="photo-row{$photo.id}">
                                <!-- WRAPPEREM KAŽDÉ FOTOGRAFIE BUŇKA DANÉ ŘÁDKY TABULKY -->
                                <td class="photo-edit-wrapper">
                                    <div class="photo-edit-thumbnail-underlay">
                                        <div class="photo-edit-thumbnail" style="background-image: url({$photo.photo_path})"></div>
                                    </div>
                                    <div class="photo-edit-full-wrapper">
                                        <div class="photo-edit-full-underlay">
                                            <div class="photo-edit-full" style="background-image: url({$photo.photo_path})"></div>
                                        </div>
                                        <h3>{$photo.photo_title}</h3>
                                        <p>{$photo.time_stamp} | By: {$photo.login} | Galerie: {$photo.galery_title}</p>
                                        <div class="photo-edit-buttons">
                                            <a class="btn btn-lg btn-info edit-button" data-toggle="modal" data-target="#photo-edit-modal{$photo.id}">Upravit fotografii</a><button class="btn btn-lg btn-danger delete-button" data-toggle="modal" data-target="#photo-remove-modal{$photo.id}"><span class="glyphicon glyphicon-remove"></span>Odebrat fotografii</button>
                                        </div>
                                    </div>
                                </td>
                                <!-- DEFINICE MODALU S FORMULÁŘEM PRO ODSTRANĚNÍ FOTOGRAFIE -->
                                <div class="modal fade photo-remove-modal" id="photo-remove-modal{$photo.id}" tabindex="-1" role="dialog" aria-labelledby="photo-remove-modal{$photo.id}-label">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="close"><span aria-hidden="true">&times;</span></button>
                                                <h3 class="modal-title" id="photo-remove-modal{$photo.id}-label">Odstranit fotografii {$photo.photo_title} z {$photo.time_stamp}?</h3>  
                                            </div>
                                            <div class="modal-footer">
                                                <!-- DEFINICE FORMULÁŘE PRO ODSTRANĚNÍ FOTOGRAFIE -->
                                                <form class="photo-remove-form" method="post" role="form" action="php_solutions/redaction_system.php?tool=photoRemove&id={$photo.id}&photoFile={$photo.photo_path}&photoTitle={$photo.photo_title}">
                                                      <button type="button" class="btn btn-default" data-dismiss="modal">ZPĚT</button>
                                                      <button type="submit" class="btn btn-danger">POTVRDIT</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- DEFINICE MODALU S FORMULÁŘEM PRO EDITACI FOTOGRAFIE -->
                                <div class="modal fade photo-edit-modal" id="photo-edit-modal{$photo.id}" tabindex="-1" role="dialog" aria-labelledby="photo-edit-modal{$photo.id}-label">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="close"><span aria-hidden="true">&times;</span></button>
                                                <h3 class="modal-title" id="photo-edit-modal{$photo.id}-label">Editace fotografie {$photo.photo_title} z {$photo.time_stamp}</h3>  
                                            </div>
                                            <div class="modal-footer">
                                                <!-- DEFINICE FORMULÁŘE PRO EDITACI FOTOGRAFIE -->
                                                <form class="photo-edit-form" method="post" role="form" action="php_solutions/redaction_system.php?tool=photoEdit&id={$photo.id}&timeStamp={$photo.time_stamp}&author={$photo.login}">
                                                    <div class="form-group">
                                                        <label for="photo{$photo.id}-edit-title">Titulek fotografie</label>
                                                        <input class="form-control" id="photo{$photo.id}-edit-title" name="photo{$photo.id}-edit-title" type="text" value="{$photo.photo_title}"/>
                                                        <label for="photo{$photo.id}-edit-galery">Galerie</label>
                                                        <select class="form-control" id="photo{$photo.id}-edit-galery" name="photo{$photo.id}-edit-galery">
                                                            <!-- Výchozí položkou nabídky elementu select příslušná galerii dané fotografie -->
                                                            <option>{$photo.galery_title}</option>
                                                                <!-- Následující položky tvoří zbývající galerie -->
                                                            {foreach $galeryFiles as $galeryFile}
                                                                {if $galeryFile.galery_title != $photo.galery_title}
                                                            <option>{$galeryFile.galery_title}</option>
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                    </div>      
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">ZPĚT</button>
                                                    <button type="submit" class="btn btn-info">POTVRDIT</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </tr>
                            {/foreach}
                        </table>
                        <!-- DEFINICE INFORMAČNÍHO MODALU O ODSTRANĚNÍ FOTOGRAFIE -->
                        <div class="modal fade" id="photo-remove-info-modal" tabindex="-1" role="dialog" aria-labelledby="photo-remove-info-modal-label">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h3 class="modal-title" id="photo-remove-info-modal-label">ZPRÁVA O STAVU ODEBRÁNÍ FOTOGRAFIE</h3>
                                    </div>
                                    <div class="modal-footer">
                                        <h4></h4>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">ZAVŘÍT</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- DEFINICE INFORMAČNÍHO MODALU O EDITACI FOTOGRAFIE -->
                        <div class="modal fade" id="photo-edit-info-modal" tabindex="-1" role="dialog" aria-labelledby="photo-edit-info-modal-label">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h3 class="modal-title" id="photo-edit-info-modal-label">ZPRÁVA O STAVU EDITACE FOTOGRAFIE</h3>
                                    </div>
                                    <div class="modal-footer">
                                        <h4></h4>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">ZAVŘÍT</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- DEFINICE NÁSTROJE PRO ZALOŽENÍ GALERIE -->
                    <div role="tabpanel" class="tab-pane" id="galery-add">
                        <h3>Založit galerii</h3>
                        <!-- DEFINICE FORMULÁŘE PRO ZALOŽENÍ GALERIE -->
                        <form id="galery-add-form" role="form" method="post" action="php_solutions/redaction_system.php?tool=galeryAdd&authorId={$smarty.session['userData']['id']}">
                            <div class="form-group">
                                <label for="galery-add-title">Zadejte název galerie</label>
                                <input class="form-control" type="text" name="galery-add-title" id="galery-add-title"/>
                                <input type="checkbox" name="galery-add-display" class="galery-display" id="galery-add-display" value="1"/>
                                <label class="display-label" for="galery-add-display">Zobrazit galerii v hlavní nabídce</label>
                            </div>
                            <input class="btn btn-lg btn-info" type="submit" value="Založit galerii"/>                          
                        </form>
                        <!-- DEFINICE MODALU PRO INFORMACE O ZALOŽENÍ GALERIE -->
                        <div class="modal fade" id="galery-add-info-modal" tabindex="-1" role="dialog" aria-labelledby="galery-add-info-modal-label">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h3 class="modal-title" id="galery-add-info-modal-label">ZPRÁVA O STAVU ZALOŽENÍ GALERIE</h3>
                                    </div>
                                    <div class="modal-footer">
                                        <h4></h4>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">ZAVŘÍT</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- DEFINICE NÁSTROJE PRO EDITACI A ODSTANĚNÍ GALERIE -->
                    <div role="tabpanel" class="tab-pane" id="galery-edit">
                        <h3>Editovat/odebrat galerii</h3>
                        <!-- DEFINICE TABULKY OBSAHUJÍCÍ EDITOVATELNÉ GALERIE -->
                        <table class="table table-hover galery-edit-table">
                            <!-- Jednotlivé záznamy galerií načítány z databáze, viz. admin_section.php -->
                        {foreach $galeriesEditArray as $galery}
                            <tr id="galery-row{$galery.id}">
                                <td>
                                    <div class="galery-edit-description">
                                        <h3>{$galery.galery_title}</h3>
                                        <p>{$galery.time_stamp} | By: {$galery.login}</p>
                                    </div>
                                    <div class="galery-edit-buttons">
                                        <button class="btn btn-info edit-button" data-toggle="modal" data-target="#galery-edit-modal{$galery.id}">Upravit galerii</button>
                                        <button class="btn btn-danger delete-button" data-toggle="modal" data-target="#galery-remove-modal{$galery.id}"><span class="glyphicon glyphicon-remove"></span>Odebrat galerii</button>
                                    </div>
                                </td>
                                <!-- DEFINICE MODALU S FORMULÁŘEM PRO ODSTRANĚNÍ GALERIE -->
                                <div class="modal fade galery-remove-modal" id="galery-remove-modal{$galery.id}" tabindex="-1" role="dialog" aria-labelledby="galery-remove-modal{$galery.id}">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="close"><span aria-hidden="true">&times;</span></button>
                                                <h3 class="modal-title" id="galery-remove-modal{$galery.id}-label">Odstranit galerii {$galery.galery_title} z {$galery.time_stamp}?</h3>  
                                            </div>
                                            <div class="modal-footer">
                                                <form class="galery-remove-form" method="post" role="form" action="php_solutions/redaction_system.php?tool=galeryRemove&galeryId={$galery.id}&galeryTitle={$galery.galery_title}">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">ZPĚT</button>
                                                        <button type="submit" class="btn btn-danger">POTVRDIT</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- DEFINICE MODALU S FORMULÁŘEM PRO EDITACI GALERIE -->
                                <div class="modal fade galery-edit-modal" id="galery-edit-modal{$galery.id}" tabindex="-1" role="dialog" aria-labelledby="galery-edit-modal{$galery.id}">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="close"><span aria-hidden="true">&times;</span></button>
                                                <h3 class="modal-title" id="galery-edit-modal{$galery.id}-label">Editace galerie {$galery.galery_title} z {$galery.time_stamp}</h3>  
                                            </div>
                                            <div class="modal-footer">
                                                <form class="galery-edit-form" method="post" role="form" action="php_solutions/redaction_system.php?tool=galeryEdit&id={$galery.id}&timeStamp={$galery.time_stamp}&authorId={$galery.login}">
                                                    <div class="form-group">
                                                        <label for="galery{$galery.id}-edit-title">Titulek galerie</label>
                                                        <input class="form-control" id="galery{$galery.id}-edit-title" name="galery{$galery.id}-edit-title" type="text" value="{$galery.galery_title}"/>
                                                       <input type="checkbox" name="galery{$galery.id}-edit-display" class="galery-display" id="galery{$galery.id}-edit-display" value="1" {if $galery.display == 1}checked{/if}/>
                                                       <label class="display-label" for="galery{$galery.id}-edit-display">Zobrazit galerii v hlavní nabídce</label>
                                                    </div>      
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">ZPĚT</button>
                                                    <button type="submit" class="btn btn-info">POTVRDIT</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </tr>
                        {/foreach}
                        </table>
                        <!-- DEFINICE INFORMAČNÍHO MODALU O ODSTRANĚNÍ GALERIE -->
                        <div class="modal fade" id="galery-remove-info-modal" tabindex="-1" role="dialog" aria-labelledby="galery-remove-info-modal-label">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h3 class="modal-title" id="galery-remove-info-modal-label">ZPRÁVA O STAVU ODEBRÁNÍ GALERIE</h3>
                                    </div>
                                    <div class="modal-footer">
                                        <h4></h4>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">ZAVŘÍT</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- DEFINICE INFORMAČNÍHO MODALU O EDITACI GALERIE -->
                        <div class="modal fade" id="galery-edit-info-modal" tabindex="-1" role="dialog" aria-labelledby="galery-edit-info-modal-label">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h3 class="modal-title" id="galery-edit-info-modal-label">ZPRÁVA O STAVU EDITACE GALERIE</h3>
                                    </div>
                                    <div class="modal-footer">
                                        <h4></h4>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">ZAVŘÍT</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>    
            </div>
            <!-- DEFINICE KATEGORIE NÁSTROJE SPRÁVY BLOGU -->  
            <div role="tabpanel" class="tab-pane" id="blog-tools">
                <h2>NÁSTROJE SPRÁVY BLOGU</h2>
                <ul class="nav nav-tabs" role="tablist">
                    <!-- Bootstrap tabs rovněž realizuje navigaci za využití záložek v dokumentu -->
                    <li role="presentation" class="active"><a href="#article-add" aria-controls="home" role="tab" data-toggle="tab">PŘIDAT ČLÁNEK</a></li>
                    <li role="presentation"><a href="#article-edit" aria-controls="profile" role="tab" data-toggle="tab">EDITOVAT/ODEBRAT ČLÁNEK</a></li>
                    <li role="presentation"><a href="#category-add" aria-controls="settings" role="tab" data-toggle="tab">ZALOŽIT KATEGORII ČLÁNKŮ</a></li>
                    <li role="presentation"><a href="#category-edit" aria-controls="settings" role="tab" data-toggle="tab">EDITOVAT/ODEBRAT KATEGORII ČLÁNKŮ</a></li>
                </ul>
                <div class="tab-content">
                    <!-- DEFINICE NÁSTROJE PRO VYTVOŘENÍ NOVÉHO ČLÁNKU -->
                    <div role="tabpanel" class="tab-pane active" id="article-add">
                        <h3>Vytvořit článek</h3>
                        <!-- DEFINICE FORMULÁŘE PRO VYTVOŘENÍ ČLÁNKU -->
                        <form id="article-add-form" role="form" method="post" action="php_solutions/redaction_system.php?tool=articleAdd&authorId={$smarty.session['userData']['id']}" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="article-add-title">Zadejte titulek článku</label>
                                <input class="form-control" type="text" name="article-add-title" id="article-add-title"/>
                                <label for="article-add-content">Vlastní obsah článku</label>
                                <textarea class="form-control" id="article-add-content" name="article-add-content"></textarea>
                                <label for="article-add-category">Zvolte kategorii</label>
                                <select name="article-add-category" id="article-add-category" class="form-control">
                                    <!-- Jednotlivé kategorie jako možnosti elementu select načítány z databáze, viz. database_load.php -->
                                    {foreach $categoryFiles as $categoryFile}
                                    <option>{$categoryFile.category_title}</option>
                                    {/foreach}
                                </select>
                                <input id="article-add-title-photo-file" class="input-file" type="file" name="article-add-title-photo-file"/>
                                <label class="article-add-title-photo-label1" for="article-add-title-photo-file">Zvolte titulní fotografii</label>
                                <label class="btn btn-default article-add-title-photo-label2" id="file-write-1" for="article-add-title-photo-file"><span class="glyphicon glyphicon-upload"></span><span>Vyberte soubor</span></label>
                                <input id="article-add-photos-files" class="input-file" type="file" name="article-add-photos-files[]" multiple/>
                                <label class="article-add-title-photo-label1" for="article-add-photos-file">Zvolte fotografie uvnitř článku</label>
                                <label class="btn btn-default article-add-title-photo-label2" id="file-write-2" for="article-add-photos-files"><span class="glyphicon glyphicon-upload"></span><span>Vyberte soubor(y)</span></label>
                                <input class="btn btn-lg btn-info" type="submit" value="Přidat článek"/>
                            </div>
                        </form>
                        <!-- DEFINICE INFORMAČNÍHO MODALU PRO NÁSTROJ PŘIDÁNÍ ČLÁNKU -->
                        <div class="modal fade" id="article-add-info-modal" tabindex="-1" role="dialog" aria-labelledby="article-add-info-modal-label">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h3 class="modal-title" id="article-add-info-modal-label">ZPRÁVA O STAVU UPLOADU</h3>
                                    </div>
                                    <div class="modal-footer">
                                        <h4></h4>
                                        <div class="image"></div>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">ZAVŘÍT</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- DEFINICE NÁSTROJE PRO EDITACI A ODEBRÁNÍ ČLÁNKU -->
                    <div role="tabpanel" class="tab-pane" id="article-edit">
                        <h3>Editovat/odebrat článek</h3>
                        <!-- DEFINICE TABULKY OBSAHUJÍCÍ EDITOVATELNÉ ČLÁNKY -->
                        <table class="table table-hover article-edit-table">
                            <!-- Jednotlivé záznamy článků načítány z databáze, viz. admin_section.php -->
                        {foreach $articlesEditArray as $article}
                            <tr id="article-row{$article.id}">
                                <td>
                                    <div class="article-edit-bg" style="background-image: url({$article.title_photo})"></div>
                                    <div class="article-edit-description">
                                        <h3>{$article.article_title}</h3>
                                        <p>{$article.time_stamp} | By: {$article.login} | Kategorie: {$article.category_title}</p>
                                    </div>
                                    <div class="article-edit-buttons">
                                        <button class="btn btn-info edit-button" data-toggle="modal" data-target="#article-edit-modal{$article.id}">Upravit článek</button>
                                        <button class="btn btn-danger delete-button" data-toggle="modal" data-target="#article-remove-modal{$article.id}"><span class="glyphicon glyphicon-remove"></span>Odebrat článek</button>
                                    </div>
                                </td>
                                <!-- DEFINICE MODALU S FORMULÁŘEM PRO ODSTRANĚNÍ ČLÁNKU -->
                                <div class="modal fade article-remove-modal" id="article-remove-modal{$article.id}" tabindex="-1" role="dialog" aria-labelledby="article-remove-modal{$article.id}">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="close"><span aria-hidden="true">&times;</span></button>
                                                <h3 class="modal-title" id="article-remove-modal{$article.id}-label">Odstranit článek {$article.article_title} z {$article.time_stamp}?</h3>  
                                            </div>
                                            <div class="modal-footer">
                                                <form class="article-remove-form" method="post" role="form" action="php_solutions/redaction_system.php?tool=articleRemove&articleId={$article.id}&articleTitle={$article.article_title}&articleTitlePhoto={$article.title_photo}">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">ZPĚT</button>
                                                    <button type="submit" class="btn btn-danger">POTVRDIT</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- DEFINICE MODALU S FORMULÁŘEM PRO EDITACI ČLÁNKU -->
                                <div class="modal fade article-edit-modal" id="article-edit-modal{$article.id}" tabindex="-1" role="dialog" aria-labelledby="article-edit-modal{$article.id}">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="close"><span aria-hidden="true">&times;</span></button>
                                                <h3 class="modal-title" id="article-edit-modal{$article.id}-label">Editace článku {$article.article_title} z {$article.time_stamp}</h3>  
                                            </div>
                                            <div class="modal-footer">
                                                <form class="article-edit-form" method="post" role="form" action="php_solutions/redaction_system.php?tool=articleEdit&id={$article.id}&timeStamp={$article.time_stamp}&authorId={$article.login}">
                                                    <div class="form-group">
                                                        <label for="article{$article.id}-edit-title">Titulek článku</label>
                                                        <input class="form-control" id="article{$article.id}-edit-title" name="article{$article.id}-edit-title" type="text" value="{$article.article_title}"/>
                                                        <label for="article{$article.id}-edit-content">Obsah článku</label>
                                                        <textarea class="form-control" id="article{$article.id}-edit-content" name="article{$article.id}-edit-content">{$article.content}</textarea>
                                                        <label for="article{$article.id}-edit-category">Kategorie článku</label>
                                                        <select class="form-control" id="article{$article.id}-edit-category" name="article{$article.id}-edit-category">    
                                                            <!-- Výchozí položkou nabídky elementu select příslušná kategorie daného článku -->
                                                            <option>{$article.category_title}</option>
                                                                <!-- Následující položky tvoří zbývající kategorie -->
                                                            {foreach $categoryFiles as $categoryFile}
                                                                {if $categoryFile.category_title != $article.category_title}
                                                            <option>{$categoryFile.category_title}</option>
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">ZPĚT</button>
                                                    <button type="submit" class="btn btn-info">POTVRDIT</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </tr>
                        {/foreach}
                        </table>
                        <!-- DEFINICE INFORMAČNÍHO MODALU O ODSTRANĚNÍ ČLÁNKU -->
                        <div class="modal fade" id="article-remove-info-modal" tabindex="-1" role="dialog" aria-labelledby="article-remove-info-modal-label">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h3 class="modal-title" id="article-remove-info-modal-label">ZPRÁVA O STAVU ODEBRÁNÍ ČLÁNKU</h3>
                                    </div>
                                    <div class="modal-footer">
                                        <h4></h4>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">ZAVŘÍT</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- DEFINICE INFORMAČNÍHO MODALU O EDITACI ČLÁNKU -->
                        <div class="modal fade" id="article-edit-info-modal" tabindex="-1" role="dialog" aria-labelledby="article-edit-info-modal-label">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h3 class="modal-title" id="article-edit-info-modal-label">ZPRÁVA O STAVU EDITACE ČLÁNKU</h3>
                                    </div>
                                    <div class="modal-footer">
                                        <h4></h4>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">ZAVŘÍT</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- DEFINICE NÁSTROJE PRO ZALOŽENÍ KATEGORIE ČLÁNKŮ -->
                    <div role="tabpanel" class="tab-pane" id="category-add">
                        <h3>Založit kategorii článků</h3>
                        <!-- DEFINICE FORMULÁŘE PRO ZALOŽENÍ KATEGORIE -->
                        <form id="category-add-form" role="form" method="post" action="php_solutions/redaction_system.php?tool=categoryAdd&authorId={$smarty.session['userData']['id']}">
                            <div class="form-group">
                                <label for="category-add-title">Zadejte název kategorie</label>
                                <input class="form-control" type="text" name="category-add-title" id="category-add-title"/>
                                <input type="checkbox" name="category-add-display" class="category-display" id="category-add-display" value="1"/>
                                <label class="display-label" for="category-add-display">Zobrazit kategorii v hlavní nabídce</label>
                            </div>
                            <input class="btn btn-lg btn-info" id="article-title-photo-file" type="submit" value="Založit kategorii"/>
                        </form>
                        <!-- DEFINICE MODALU PRO INFORMACE O ZALOŽENÍ KATEGORIE -->
                        <div class="modal fade" id="category-add-info-modal" tabindex="-1" role="dialog" aria-labelledby="category-add-info-modal-label">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h3 class="modal-title" id="category-add-info-modal-label">ZPRÁVA O STAVU ZALOŽENÍ KATEGORIE</h3>
                                    </div>
                                    <div class="modal-footer">
                                        <h4></h4>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">ZAVŘÍT</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- DEFINICE NÁSTROJE PRO EDITACI A ODEBRÁNÍ KATEGORIÍ ČLÁNKŮ -->
                    <div role="tabpanel" class="tab-pane" id="category-edit">
                        <h3>Editovat/odebrat kategorii článků</h3>
                        <!-- DEFINICE TABULKY OBSAHUJÍCÍ EDITOVATELNÉ KATEGORIE -->
                        <table class="table table-hover category-edit-table">
                            <!-- Jednotlivé záznamy kategorií načítány z databáze, viz. admin_section.php -->
                        {foreach $categoriesEditArray as $category}
                            <tr id="category-row{$category.id}">
                                <td>
                                    <div class="category-edit-description">
                                        <h3>{$category.category_title}</h3>
                                        <p>{$category.time_stamp} | By: {$category.login}</p>
                                    </div>
                                    <div class="category-edit-buttons">
                                        <button class="btn btn-info edit-button" data-toggle="modal" data-target="#category-edit-modal{$category.id}">Upravit kategorii</button>
                                        <button class="btn btn-danger delete-button" data-toggle="modal" data-target="#category-remove-modal{$category.id}"><span class="glyphicon glyphicon-remove"></span>Odebrat kategorii</button>
                                    </div>
                                </td>
                                <!-- DEFINICE MODALU S FORMULÁŘEM PRO ODSTRANĚNÍ KATEGORIE -->
                                <div class="modal fade category-remove-modal" id="category-remove-modal{$category.id}" tabindex="-1" role="dialog" aria-labelledby="category-remove-modal{$category.id}">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="close"><span aria-hidden="true">&times;</span></button>
                                                <h3 class="modal-title" id="category-remove-modal{$category.id}-label">Odstranit kategorii {$category.category_title} z {$category.time_stamp}?</h3>  
                                            </div>
                                            <div class="modal-footer">
                                                <form class="category-remove-form" method="post" role="form" action="php_solutions/redaction_system.php?tool=categoryRemove&categoryId={$category.id}&categoryTitle={$category.category_title}">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">ZPĚT</button>
                                                    <button type="submit" class="btn btn-danger">POTVRDIT</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- DEFINICE MODALU S FORMULÁŘEM PRO EDITACI KATEGORIE -->
                                <div class="modal fade category-edit-modal" id="category-edit-modal{$category.id}" tabindex="-1" role="dialog" aria-labelledby="category-edit-modal{$category.id}">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="close"><span aria-hidden="true">&times;</span></button>
                                                <h3 class="modal-title" id="category-edit-modal{$category.id}-label">Editace kategorie {$category.category_title} z {$category.time_stamp}</h3>  
                                            </div>
                                            <div class="modal-footer">
                                                <form class="category-edit-form" method="post" role="form" action="php_solutions/redaction_system.php?tool=categoryEdit&id={$category.id}&timeStamp={$category.time_stamp}&authorId={$category.login}">
                                                    <div class="form-group">
                                                        <label for="category{$category.id}-edit-title">Titulek kategorie</label>
                                                        <input class="form-control" id="category{$category.id}-edit-title" name="category{$category.id}-edit-title" type="text" value="{$category.category_title}"/>
                                                       <input type="checkbox" name="category{$category.id}-edit-display" class="category-display" id="category{$category.id}-edit-display" value="1" {if $category.display == 1}checked{/if}/>
                                                       <label class="display-label" for="category{$category.id}-edit-display">Zobrazit kategorii v hlavní nabídce</label>
                                                    </div>      
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">ZPĚT</button>
                                                    <button type="submit" class="btn btn-info">POTVRDIT</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </tr>
                        {/foreach}
                        </table>
                        <!-- DEFINICE INFORMAČNÍHO MODALU O ODSTRANĚNÍ KATEGORIE -->
                        <div class="modal fade" id="category-remove-info-modal" tabindex="-1" role="dialog" aria-labelledby="category-remove-info-modal-label">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h3 class="modal-title" id="category-remove-info-modal-label">ZPRÁVA O STAVU ODEBRÁNÍ KATEGORIE</h3>
                                    </div>
                                    <div class="modal-footer">
                                        <h4></h4>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">ZAVŘÍT</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- DEFINICE INFORMAČNÍHO MODALU O EDITACI KATEGORIE -->
                        <div class="modal fade" id="category-edit-info-modal" tabindex="-1" role="dialog" aria-labelledby="category-edit-info-modal-label">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h3 class="modal-title" id="category-edit-info-modal-label">ZPRÁVA O STAVU EDITACE KATEGORIE</h3>
                                    </div>
                                    <div class="modal-footer">
                                        <h4></h4>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">ZAVŘÍT</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="user-content-tools">
                <h2>NÁSTROJE UŽIVATELSKÉ EDITACE</h2>
                <!-- Navigace mezi jednotlivými nástroji v rámci jedné kategorie realizována za využití bootstrap tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <!-- Bootstrap tabs rovněž realizuje navigaci za využití záložek v dokumentu -->
                    <li role="presentation" class="active"><a href="#main-menu-titles" aria-controls="main-menu-titles" role="tab" data-toggle="tab">EDITOVAT HLAVIČKU</a></li>
                    <li role="presentation"><a href="#opening-layer-modification" aria-controls="opening-layer-modification" role="tab" data-toggle="tab">EDITOVAT ÚVODNÍ VRSTVU</a></li>
                    <li role="presentation"><a href="#footer-user-variable-content" aria-controls="footer-user-variable-content" role="tab" data-toggle="tab">EDITOVAT PATIČKU</a></li>
                    <li role="presentation"><a href="#contacts-editation" aria-controls="contacts-editation" role="tab" data-toggle="tab">EDITOVAT KONTAKTY</a></li>
                </ul>
                <div class="tab-content">
                    <!-- DEFINICE NÁSTROJE PRO EDITACI POLOŽEK HLAVIČKY -->
                    <div role="tabpanel" class="tab-pane active" id="main-menu-titles">
                        <h3>Editovat hlavičku</h3>
                        <!-- DEFINICE FORMULÁŘE PRO EDITACI POLOŽEK HLAVIČKY -->
                        <form id="header-edit-form" role="form" method="post" action="php_solutions/redaction_system.php?tool=headerEdit">
                            <div class="form-group">
                                <label for="logo-title">Titulek loga webové stránky</label>
                                <input class="form-control" type="text" name="logo-title" id="logo-title" value="{$userContent[0]['content']}"/>
                                <label for="portfolio-title">Titulek portfolia</label>
                                <input class="form-control" type="text" name="portfolio-title" id="portfolio-title" value="{$userContent[1]['content']}"/>
                                <label for="galery-title">Titulek fotogalerie</label>
                                <input class="form-control" type="text" name="galery-title" id="galery-title" value="{$userContent[2]['content']}"/>
                                <label for="blog-title">Titulek blogu</label>
                                <input class="form-control" type="text" name="blog-title" id="blog-title" value="{$userContent[3]['content']}"/>
                                <label for="contacts-title">Titulek kontaktů</label>
                                <input class="form-control" type="text" name="contacts-title" id="contacts-title" value="{$userContent[4]['content']}"/>
                                <div class="header-edit-buttons">
                                    <input class="btn btn-lg btn-info" type="submit" value="Potvrdit"/>
                                    <input class="btn btn-lg btn-default" id="header-edit-default" type="button" value="Obnovit výchozí"/>
                                </div>
                            </div>
                        </form>
                        <!-- DEFINICE INFORMAČNÍHO MODALU PRO NÁSTROJ EDITACE POLOŽEK HLAVIČKY -->
                        <div class="modal fade" id="header-edit-info-modal" tabindex="-1" role="dialog" aria-labelledby="header-edit-info-modal-label">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h3 class="modal-title" id="header-edit-info-modal-label">ZPRÁVA O STAVU EDITACE HLAVIČKY</h3>
                                    </div>
                                    <div class="modal-footer">
                                        <h4></h4>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">ZAVŘÍT</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- DEFINICE NÁSTROJE PRO EDITACI ÚVODNÍ VRSTVY -->
                    <div role="tabpanel" class="tab-pane" id="opening-layer-modification">
                        <h3>Editovat úvodní vrstvu</h3>
                        <!-- DEFINICE FORMULÁŘE PRO EDITACI POLOŽEK HLAVIČKY -->
                        <form id="opening-layer-edit-form" role="form" method="post" action="php_solutions/redaction_system.php?tool=openingLayerEdit">
                            <div class="form-group">
                                <label for="opening-layer-title">Titulek úvodní vrstvy</label>
                                <input class="form-control" type="text" name="opening-layer-title" id="opening-layer-title" value="{$userContent[5]['content']}"/>
                                <label for="opening-layer-content">Článek úvodní vrstvy</label>
                                <textarea class="form-control" name="opening-layer-content" id="opening-layer-content">{$userContent[6]['content']}</textarea>
                                <label for="opening-layer-background">Pozadí uvítací vrstvy</label>
                                <div class="opening-layer-background-underlay">
                                    <div class="opening-layer-background" data-toggle="modal" data-target="#opening-layer-bg-edit-modal" style="background-image: url({$openingLayerCurrentBg[0]['bg_path']})"></div>
                                    <input name="opening-layer-background" id="opening-layer-background" value="{$openingLayerCurrentBg[0]['bg_path']}"/>
                                </div>
                                <div class="opening-layer-edit-button">
                                    <input class="btn btn-lg btn-info" type="submit" value="Potvrdit"/>
                                </div>
                            </div>
                        </form>
                        <!-- DEFINICE MODALU PRO EDITACI POZADÍ ÚVODNÍ VRSTVY -->
                        <div class="modal fade" id="opening-layer-bg-edit-modal" tabindex="-1" role="dialog" aria-labelledby="opening-layer-bg-edit-modal-label">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h3 class="modal-title" id="opening-layer-bg-edit-modal-label">EDITACE POZADÍ UVÍTACÍ VRSTVY</h3>
                                    </div>
                                    <div class="modal-footer">
                                        <label>Zvolit pozadí úvodní vrstvy</label>
                                        {foreach $photosEditArray as $photo}
                                            <div class="opening-layer-background-underlay">
                                                <div class="opening-layer-bg-edit-list" style="background-image: url({$photo.photo_path})"></div>
                                                <p>{$photo.photo_path}</p>
                                            </div>
                                        {/foreach}
                                    <div class="opening-layer-bg-edit-buttons">
                                        <button type="button" class="btn btn-info" data-dismiss="modal">POTVRDIT</button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">ZAVŘÍT</button>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- DEFINICE INFORMAČNÍHO MODALU PRO NÁSTROJ EDITACE ÚVODNÍ VRSTVY -->
                        <div class="modal fade" id="opening-layer-edit-info-modal" tabindex="-1" role="dialog" aria-labelledby="opening-layer-edit-info-modal-label">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h3 class="modal-title" id="opening-layer-edit-info-modal-label">ZPRÁVA O STAVU EDITACE ÚVODNÍ VRSTVY</h3>
                                    </div>
                                    <div class="modal-footer">
                                        <h4></h4>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">ZAVŘÍT</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- DEFINICE NÁSTROJE PRO EDITACI POLOŽEK PATIČKY -->
                    <div role="tabpanel" class="tab-pane" id="footer-user-variable-content">
                        <h3>Editovat patičku</h3>
                        <!-- DEFINICE FORMULÁŘE PRO EDITACI POLOŽEK PATIČKY -->
                        <form id="footer-edit-form" role="form" method="post" action="php_solutions/redaction_system.php?tool=footerEdit">
                            <div class="form-group">
                                <label for="first-section-title">Titulek prvního oddílu</label>
                                <input class="form-control" type="text" name="first-section-title" id="first-section-title" value="{$userContent[7]['title']}"/>
                                <label for="first-section-content">Obsah článku prvního oddílu</label>
                                <textarea class="form-control" id="firt-section-content" name="first-section-content">{$userContent[7]['content']}</textarea>
                                <label for="second-section-title">Titulek druhého oddílu</label>
                                <input class="form-control" type="text" name="second-section-title" id="second-section-title" value="{$userContent[8]['title']}"/>
                                <label for="second-section-content">Obsah článku druhého oddílu</label>
                                <textarea class="form-control" id="second-section-content" name="second-section-content">{$userContent[8]['content']}</textarea>
                                <label for="third-section-title">Titulek třetího oddílu</label>
                                <input class="form-control" type="text" name="third-section-title" id="third-section-title" value="{$userContent[9]['title']}"/>
                                <label for="third-section-content">Obsah článku třetího oddílu</label>
                                <textarea class="form-control" id="third-section-content" name="third-section-content">{$userContent[9]['content']}</textarea>
                                
                                <div class="footer-edit-buttons">
                                    <input class="btn btn-lg btn-info" type="submit" value="Potvrdit"/>
                                    <input class="btn btn-lg btn-default" id="footer-edit-default" type="button" value="Obnovit výchozí titulky"/>
                                </div>
                            </div>
                        </form>
                        <!-- DEFINICE INFORMAČNÍHO MODALU PRO NÁSTROJ EDITACE POLOŽEK PATIČKY -->
                        <div class="modal fade" id="footer-edit-info-modal" tabindex="-1" role="dialog" aria-labelledby="footer-edit-info-modal-label">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h3 class="modal-title" id="footer-edit-info-modal-label">ZPRÁVA O STAVU EDITACE PATIČKY</h3>
                                    </div>
                                    <div class="modal-footer">
                                        <h4></h4>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">ZAVŘÍT</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- DEFINICE NÁSTROJE PRO EDITACI KONTAKTŮ -->
                    <div role="tabpanel" class="tab-pane" id="contacts-editation">
                        <h3>Editovat kontakty</h3>
                        <!-- DEFINICE FORMULÁŘE PRO EDITACI POLOŽEK KONTAKTŮ -->
                        <form id="contacts-edit-form" role="form" method="post" action="php_solutions/redaction_system.php?tool=contactsEdit">
                            <div class="form-group">
                                <h4 class="contacts-section">Základní údaje</h4>
                                {for $i=10 to 13}
                                <label for="item-{$i}-content">{$userContent[$i]['title']}</label>
                                <input class="form-control" type="text" name="item-{$i}-content" id="item-{$i}-content" value="{$userContent[$i]['content']}"/>
                                {/for}
                                <h4 class="contacts-section">Sociální sítě</h4>
                                {for $i=14 to 18}
                                <label for="item-{$i}-content">{$userContent[$i]['title']}</label>
                                <input class="form-control" type="text" name="item-{$i}-content" id="item-{$i}-content" value="{$userContent[$i]['content']}"/>
                                <label for="item-{$i}-link">Adresa profilu</label>
                                <input class="form-control" type="text" name="item-{$i}-link" id="item-{$i}-link" value="{$userContent[$i]['link']}"/>
                                {/for}
                                <div class="footer-edit-buttons">
                                    <input class="btn btn-lg btn-info" type="submit" value="Potvrdit"/>
                                </div>
                            </div>
                        </form>
                        <!-- DEFINICE INFORMAČNÍHO MODALU PRO NÁSTROJ EDITACE KONTAKTŮ -->
                        <div class="modal fade" id="contacts-edit-info-modal" tabindex="-1" role="dialog" aria-labelledby="contacts-edit-info-modal-label">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h3 class="modal-title" id="contacts-edit-info-modal-label">ZPRÁVA O STAVU EDITACE KONTAKTŮ</h3>
                                    </div>
                                    <div class="modal-footer">
                                        <h4></h4>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">ZAVŘÍT</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <footer>
        <h3>CREATED BY &copy; PETR PONDĚLÍK 2016</h3>
    </footer>


    <!-- PŘIPOJENÍ JQUERY -->
    <script src="https://code.jquery.com/jquery-2.1.1-rc2.min.js"></script>
        
    <!-- PŘIPOJENÍ PLUGINU PRO AJAX -->
    <script src="http://malsup.github.com/jquery.form.js"></script>

    <!-- PŘIPOJENÍ BOOTSTRAP JS PLUGINŮ -->
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    <!-- PŘIPOJENÍ SCRIPTU OVLÁDAJÍCÍHO PRŮHLEDNOST HLAVNÍ NABÍDKY A NABÍDKU PRO MALÁ ROZLIŠENÍ-->
    <script src="js_solutions/header_functions.js"></script>
    
    <!-- PŘIPOJENÍ SCRIPTU OVLÁDAJÍCÍHO AJAX UDÁLOSTI NA STRANĚ KLIENTA -->
    <script src="js_solutions/redaction_system_ajax.js"></script>
        
    <!-- PŘIPOJENÍ SCRIPTU OVLÁDAJÍCÍHO DYNAMICKÉ UDÁLOSTI FRONTENDU -->
    <script src="js_solutions/redaction_system_frontend_dynamic.js"></script>
    
    <!-- SCRIPT OVLÁDAJÍCÍ LOADING SCREEN PRO DOM STRUKTURU DOKUMENTU -->
    <script>
        $(window).load(function(){
            $(".cssloader-wrapper").remove();
            $("html").css("overflow","visible");
        });
    </script>

    {else}
        
    <p>PRO PŘÍSTUP DO ADMINISTRAČNÍHO ROZHRANÍ SE MUSÍTE PŘIHLÁSIT</p>

    {/if}



</body>
</html>