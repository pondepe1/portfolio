<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title>IAN Photography</title>

    <!-- PŘIPOJENÍ CSS STYLŮ -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css_styles/general_styles.css"/>
    <link rel="stylesheet" type="text/css" href="css_styles/loading_screens.css"/>
    <link rel="stylesheet" type="text/css" href="css_styles/gallery_styles.css"/>
    <link  href="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css" rel="stylesheet">
</head>

    
    
<body>
    
    <a href="photogalery.php" class="btn btn-lg btn-info backup-button">ZPĚT</a>
    
    
    <!-- DEFINICE LOADING SCREENU PRO LOADING DOM STRUKTURY DOKUMENTU -->
    <div class="cssloader-wrapper">

        <div class="cssload-loader">
            <div class="cssload-flipper">
                <div class="cssload-front"></div>
                <div class="cssload-back"></div>
            </div>
            <h4>Loading...</h4>
        </div>

    </div>


    <!-- DEFINICE LOADING SCREENU PRO ZPRACOVÁNÍ PHP APLIKACE V RÁMCI AJAXU -->
    <div id="floatingCirclesG-wrapper">
        
        <div id="floatingCirclesG">
            <div class="f_circleG" id="frotateG_01"></div>
            <div class="f_circleG" id="frotateG_02"></div>
            <div class="f_circleG" id="frotateG_03"></div>
            <div class="f_circleG" id="frotateG_04"></div>
            <div class="f_circleG" id="frotateG_05"></div>
            <div class="f_circleG" id="frotateG_06"></div>
            <div class="f_circleG" id="frotateG_07"></div>
            <div class="f_circleG" id="frotateG_08"></div>
        </div>
        
    </div>   

    
    <!-- DEFINICE OBSAHU GALERIE -->
    <div class="fotorama" data-width="100%" data-height="100%" data-ratio="4/3" data-nav="thumbs" data-thumbwidth="96px" data-thumbheight="64px" data-allowfullscreen="native">
        
        {if $photosInGallery != false}
        
        {foreach $photosInGallery as $galeryPhoto}
        <div data-img="{$galeryPhoto.photo_path}">
            <h2 class="photo-title">{$galeryPhoto.photo_title}</h2>
            <div class="photo-description"><h3 class="galery">{$currentGallery[0].galery_title} | By: {$currentGallery[0].login}</h3><h3 class="author">Photo by: Jan Růžička</h3></div>
        </div>
        {/foreach}
        {else}
        <div>
            <h2 class="photo-title">Nenalezena žádná fotografie</h2>
            <div class="photo-description"><h3 class="galery">{$currentGallery[0].galery_title} | By: {$currentGallery[0].login}</h3></div>
        </div>
        
        {/if}
        
    </div>
    
        
    <!-- PŘIPOJENÍ JQUERY -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    
    <!-- PŘIPOJENÍ SCRIPTU OVLÁDAJÍCÍHO PRŮHLEDNOST HLAVNÍ NABÍDKY A NABÍDKU PRO MALÁ ROZLIŠENÍ-->
    <script src="js_solutions/header_functions.js"></script>
    
    <!-- PŘIPOJENÍ SCRIPTU PRO MD5 HASHOVÁNÍ -->
    <script src="js_solutions/md5_hashing.js"></script>
    
    <!-- PŘIPOJENÍ SCRIPTU OVLÁDAJÍCÍHO PŘIHLAŠOVACÍ FORMULÁŘ -->
    <script src="js_solutions/login_form_control.js"></script>
    
    <script src="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js"></script>

        
    </script>
        
    <!-- SCRIPT OVLÁDAJÍCÍ LOADING SCREEN PRO DOM STRUKTURU DOKUMENTU -->
    <script type="text/javascript">
        $(window).load(function(){
            $(".cssloader-wrapper").remove();
            $("html").css("overflow","visible");
        });
        $("html").mouseover(function(){
            $(".backup-button").css("opacity",0.9);
            $(".backup-button").css("right","4%");
        });
        $("html").mouseout(function(){
            $(".backup-button").css("opacity",0);
            $(".backup-button").css("right","0px");
        });
    </script>    

       
    
</body>
</html>