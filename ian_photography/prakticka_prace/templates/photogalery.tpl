<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title>IAN Photography</title>

    <!-- PŘIPOJENÍ CSS STYLŮ -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css_styles/general_styles.css"/>
    <link rel="stylesheet" type="text/css" href="css_styles/loading_screens.css"/>
    <link rel="stylesheet" type="text/css" href="css_styles/header_styles.css"/>
    <link rel="stylesheet" type="text/css" href="css_styles/collapsed_navigation_styles.css"/>
    <link rel="stylesheet" type="text/css" href="css_styles/footer_styles.css"/>
    <link rel="stylesheet" type="text/css" href="css_styles/photogalery_styles.css"/>
    <link rel="stylesheet" type="text/css" href="css_styles/responsive_styles.css"/>
    
</head>

    
    
<body>
    
    
        <!-- DEFINICE LOADING SCREENU PRO LOADING DOM STRUKTURY DOKUMENTU -->
        <div class="cssloader-wrapper">
            
            <div class="cssload-loader">
                <div class="cssload-flipper">
                    <div class="cssload-front"></div>
                    <div class="cssload-back"></div>
                </div>
                <h4>Loading...</h4>
            </div>
            
        </div>
        
        
        <!-- DEFINICE LOADING SCREENU PRO ZPRACOVÁNÍ PHP APLIKACE V RÁMCI AJAXU -->
        <div id="floatingCirclesG-wrapper">
            <div id="floatingCirclesG">
                <div class="f_circleG" id="frotateG_01"></div>
                <div class="f_circleG" id="frotateG_02"></div>
                <div class="f_circleG" id="frotateG_03"></div>
                <div class="f_circleG" id="frotateG_04"></div>
                <div class="f_circleG" id="frotateG_05"></div>
                <div class="f_circleG" id="frotateG_06"></div>
                <div class="f_circleG" id="frotateG_07"></div>
                <div class="f_circleG" id="frotateG_08"></div>
            </div>
        </div>   
        
        
        <!-- DEFINICE WRAPPERU HLAVIČKY WEBU -->
        <header>
            
            {if isset($smarty.session.logged) && $smarty.session.logged == true}
            <!-- DEFINICE UŽIVATESLKÉHO ADMINISTRAČNÍHO PANELU (pouze v případě úspěšného přihlášení uživatele -> $_SESSION['logged'] == true, viz. login.php) -->
            <div class="user_admin_bar">
                <!-- Uvítací titulek uživateslkého panelu -> obsahuje uživatelské jméno, viz. login.php -->
                <p>Uživatel {$smarty.session['userData']['login']}</p>
                <div class="full_resolution_links">  
                    <a href="php_solutions/logout.php?backupFile=../photogalery.php">ODHLÁSIT SE</a>
                    <a href="admin_section.php?backupFile=photogalery.php">ADMINISTRACE</a>  
                </div>
                <div class="dropdown">
                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        MOŽNOSTI
                        <span class="glyphicon glyphicon-triangle-bottom"></span>
                        <span class="glyphicon glyphicon-triangle-top"></span>            
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                        <li><a href="admin_section.php?backupFile=photogalery.php">ADMINISTRACE</a></li>
                        <li><a href="php_solutions/logout.php?backupFile=../photogalery.php">ODHLÁSIT SE</a></li>
                    </ul>
                </div>
           </div>
           {/if}
           
            <!-- DEFINICE HLAVIČKY WEBOVÉ STRÁNKY (obsah je načítán z databáze, viz. database_manipulation.php) -->
            <h1><a href="index.php">{$userContent[0].content}</a></h1>

            <!-- DEFINICE DVOUÚROVŇOVÉ NABÍDKY WEBU PRO VELKÁ ROZLIŠENÍ -->
            <nav class="primary_navigation">
                <ul class="main_menu_primary">
                    <!-- Obsah položek první úrovně hlavní nabídky načítán z databáze, viz. database_manipulation.php -->
                    <li><a href="portfolio.php">{$userContent[1].content}</a></li>
                    <li><a href="photogalery.php">{$userContent[2].content}</a>
                        <ul>
                            <!-- Položky druhé úrovně hlavní nabídky načítány z databáze, viz. database_manipulation.php -->
                            {foreach $mainMenuGaleryFiles as $galeryFile}
                            <li><a href="gallery.php?galleryId={$galeryFile.id}">{$galeryFile.galery_title}</a></li>
                            {/foreach}
                        </ul>
                    </li>
                    <li><a href="blog.php">{$userContent[3].content}</a>
                        <ul>
                            {foreach $mainMenuCategoryFiles as $categoryFile}
                            <li><a href="blog.php?categoryId={$categoryFile.id}">{$categoryFile.category_title}</a></li>
                            {/foreach}
                        </ul>
                    </li>
                    <li><a href="contacts.php">{$userContent[4].content}</a></li>
                </ul>
            </nav>

            <!-- DEFINICE DVOUÚROVŇOVÉ NABÍDKY WEBU PRO MALÁ ROZLIŠENÍ -->
            <nav class="collapsed_navigation">
                <a href="#" id="pull"><span>MENU</span></a>
                <ul class="main_menu_collapsed">
                    <!-- DEFINICE WRAPPERU POLOŽEK NABÍDKY PRO ZNEVIDITELNĚNÍ SCROLLBARU -->
                    <li id="scroller">
                        <ul>
                            <li><a href="portfolio.php">{$userContent[1].content}</a></li>
                            <li><a href="photogalery.php">{$userContent[2].content}</a>
                                <ul>
                                    {foreach $mainMenuGaleryFiles as $galeryFile}
                                    <li><a href="gallery.php?galleryId={$galeryFile.id}">{$galeryFile.galery_title}</a></li>
                                    {/foreach}
                                </ul>
                            </li>
                            <li><a href="blog.php">{$userContent[3].content}</a>
                                <ul>
                                    {foreach $mainMenuCategoryFiles as $categoryFile}
                                    <li><a href="blog.php?categoryId={$categoryFile.id}">{$categoryFile.category_title}</a></li>
                                    {/foreach}
                                </ul>
                            </li>
                            <li><a href="contacts.php">{$userContent[4].content}</a></li>
                        </ul>
                    </li>
                </ul>
            </nav>
            
        </header>
    
        <!-- DEFINICE WRAPPERU OBSAHU FOTOGALERIE -->
        <div class="photogalery-content-wrapper">
            <h2 class="photogalery-title" {if isset($smarty.session['logged']) && $smarty.session['logged'] == true} style="padding-top: 110px;" {/if}>FOTOGALERIE</h2>
            {if isset($photogaleryFiles) && !empty($photogaleryFiles)}
            {foreach $photogaleryFiles as $galeryFile}
                <div class="galery-container">
                    <a href="gallery.php?galleryId={$galeryFile.id}" class="galery-block-link" style="background-image: url({$galeryFile.actual_photo_path})">
                    </a>
                    <h2 class="galery-title"><a href="gallery.php?galleryId={$galeryFile.id}">{$galeryFile.galery_title}</a></h2>
                </div>
            {/foreach}
            {else}
                <h2 class="empty-galery-msg">Nenalezena žádná galerie</h2>
                <!-- DEFINICE INFORMAČNÍHO MODALU PŘI NEEXISTENCI ŽÁDNÉ GALERIE -->
                <div class="modal fade" id="empty-galery-info-modal" tabindex="-1" role="dialog" aria-labelledby="empty-galery-info-modal-label">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h3 class="modal-title" id="empty-galery-modal-label">ZPRÁVA O STAVU FOTOGALERIE</h3>
                            </div>
                            <div class="modal-footer">
                                <h4>Nenalezena žádná galerie</h4>
                                <button type="button" class="btn btn-default" data-dismiss="modal">ZAVŘÍT</button>
                            </div>
                        </div>
                    </div>
                </div>
            {/if}
        </div>

                
        <!-- DEFINICE WRAPPERU PATIČKY WEBU -->
        <footer>
            <div class="block-backup-link"><a href="index.php">ZPĚT NA ÚVODNÍ STRÁNKU</a></div>
            <!-- DEFINICE WRAPPERU OBSAHOVÉ ČÁSTI PATIČKY WEBU -->
            <div class='footer_content_wrapper'>
                <!-- Patička webu se skládá ze 3 obsahových bloků -->
                {for $i=7 to 9}
                <div class='footer_content'>
                    <!-- Obsah patičky načítán z databáze, viz. database_load.php -->
                    <h2>{$userContent[$i]['title']}</h2>
                    <p>{$userContent[$i]['content']}</p>
                </div>
                {/for}
            </div>
            <!-- Výpis HTML struktury přihlašovacího formuláře proběhne pouze v případě zadání parametru v URL adrese a v případě, že uživatel není momentálně úspěšně přihlášený -->
            {if isset($smarty.get.user) && empty($smarty.get.user)}
                {if !isset($smarty.session.logged) || $smarty.session.logged != true}
            <!-- DEFINICE WRAPPERU PŘIHLAŠOVACÍHO FORMULÁŘE -->
            <div class='form_wrapper'>
                <form method='POST'>
                    <div class='form_group'>
                        <label for='login'>PŘIHLAŠOVACÍ JMÉNO</label>
                        <input type='text' id='login' name='login' placeholder='ZADEJTE PŘIHLAŠOVACÍ JMÉNO'/>
                    </div>
                    <div class='form_group'>
                        <label for='password'>HESLO</label>
                        <input type='password' id='password' name='password' placeholder='ZADEJTE HESLO'/>
                    </div>
                    <input id='login_btn' class='submit_btn' type='button' name='submit' value='PŘIHLÁSIT SE'/>
                    <p id='login_msg'></p>
                </form>
            </div>    
                {/if}
            {/if}
            <h3>CREATED BY &copy; PETR PONDĚLÍK 2016</h3>
            
        </footer>
    
        
    <!-- PŘIPOJENÍ JQUERY -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    
    <!-- PŘIPOJENÍ BOOTSTRAP JS PLUGINŮ -->
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    
    <!-- PŘIPOJENÍ SCRIPTU OVLÁDAJÍCÍHO PRŮHLEDNOST HLAVNÍ NABÍDKY A NABÍDKU PRO MALÁ ROZLIŠENÍ-->
    <script src="js_solutions/header_functions.js"></script>
    
    <!-- PŘIPOJENÍ SCRIPTU PRO MD5 HASHOVÁNÍ -->
    <script src="js_solutions/md5_hashing.js"></script>
    
    <!-- PŘIPOJENÍ SCRIPTU OVLÁDAJÍCÍHO PŘIHLAŠOVACÍ FORMULÁŘ -->
    <script src="js_solutions/login_form_control.js"></script>
    
    <!-- SCRIPT OVLÁDAJÍCÍ LOADING SCREEN PRO DOM STRUKTURU DOKUMENTU -->
    <script type="text/javascript">
        $(window).load(function(){
            $(".cssloader-wrapper").remove();
            $("html").css("overflow","visible");
        });
    </script>
    
    {if !isset($photogaleryFiles) || empty($photogaleryFiles)}
    <script type="text/javascript">
        $(document).ready(function(){
            $("#empty-galery-info-modal").modal("show");
        });
    </script>
    {/if}
    
</body>
</html>