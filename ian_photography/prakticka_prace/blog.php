<?php

require_once "php_solutions/database_manipulation.php";
require "libs/Smarty.class.php";
require "libs/SmartyPaginate.class.php";

$smarty = new Smarty();

//AKTIVACE PLUGINU SmartyPaginate A NASTAVENÍ HODNOTY ATRIBUTU LIMIT DANÉ TŘÍDY
SmartyPaginate::connect();
SmartyPaginate::setUrlVar('pageStart');
SmartyPaginate::setPageLimit(5);
SmartyPaginate::setLimit(2);

if(isset($_POST['category-select-reset'])){
    unset($_SESSION['categoryId']);
    SmartyPaginate::setCurrentItem(1);
}

if(isset($_POST['category-select']) || isset($_SESSION['categoryId']) || (isset($_GET['categoryId']) && !empty($_GET['categoryId']))){
    if(isset($_POST['category-select'])){
    $categoryTitle = $_POST['category-title'];
        //print_r($categoryTitle);
        foreach($categoryFiles as $file){
            if($categoryTitle == $file['category_title']){
                $_SESSION['categoryId'] = $file['id'];
            }
        }
        SmartyPaginate::setCurrentItem(1);
    }
    if(isset($_GET['categoryId']) && !empty($_GET['categoryId'])){
        $_SESSION['categoryId'] = $_GET['categoryId'];
        SmartyPaginate::setCurrentItem(1);
    }
    //print_r($_SESSION['categoryId']);
    $sql = "SELECT articles.id,articles.time_stamp,articles.article_title,articles.content,articles.title_photo,categories.category_title,users.login
            FROM articles INNER JOIN categories ON articles.category = categories.id
            INNER JOIN users ON articles.author = users.id
            WHERE articles.category = ".$_SESSION['categoryId']."
            ORDER BY articles.id DESC";
}
else{
    $sql = "SELECT articles.id,articles.time_stamp,articles.article_title,articles.content,articles.title_photo,categories.category_title,users.login
        FROM articles INNER JOIN categories ON articles.category = categories.id
        INNER JOIN users ON articles.author = users.id
        ORDER BY articles.id DESC";
}

$blogArticleFilesRes = $con->query($sql);

function get_db_results($result){
    
    if($result->num_rows > 0){
        while($row = $result->fetch_assoc()){
            $resultArray[] = $row;
        }
        SmartyPaginate::setTotal(count($resultArray));
        return array_slice($resultArray,SmartyPaginate::getCurrentIndex(),SmartyPaginate::getLimit()); 
    }
    else return false;
    
}
//print_r(get_db_results($result));

if(isset($_GET['pageStart']) && !empty($_GET['pageStart'])){
    $currentPage = $_GET['pageStart'];
}
else $currentPage = 1;
//print_r($currentPage);

$smarty->assign("blogArticleFiles",get_db_results($blogArticleFilesRes));
SmartyPaginate::assign($smarty);
$smarty->assign("currentPage",$currentPage);
$smarty->assign("userContent",$userContent);
$smarty->assign("categoryFiles",$categoryFiles);
$smarty->assign("mainMenuGaleryFiles",$mainMenuGaleryFiles);
$smarty->assign("mainMenuCategoryFiles",$mainMenuCategoryFiles);
$smarty->display("blog.tpl");