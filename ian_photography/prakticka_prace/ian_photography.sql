-- phpMyAdmin SQL Dump
-- version 4.1.4
-- http://www.phpmyadmin.net
--
-- Počítač: 127.0.0.1
-- Vytvořeno: Čtv 10. bře 2016, 22:11
-- Verze serveru: 5.6.15-log
-- Verze PHP: 5.5.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databáze: `ian_photography`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `articles`
--

CREATE TABLE IF NOT EXISTS `articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `article_title` varchar(100) COLLATE utf16_czech_ci NOT NULL,
  `content` text COLLATE utf16_czech_ci NOT NULL,
  `author` int(11) NOT NULL,
  `category` int(11) NOT NULL,
  `title_photo` varchar(100) COLLATE utf16_czech_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `article_title` (`article_title`),
  KEY `author` (`author`,`category`),
  KEY `category` (`category`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf16 COLLATE=utf16_czech_ci AUTO_INCREMENT=90 ;

--
-- Vypisuji data pro tabulku `articles`
--

INSERT INTO `articles` (`id`, `time_stamp`, `article_title`, `content`, `author`, `category`, `title_photo`) VALUES
(1, '2015-12-21 07:30:41', 'Test article 1', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. In enim a arcu imperdiet malesuada. Fusce consectetuer risus a nunc. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 2, 1, ''),
(2, '2016-01-22 17:13:13', 'Test article 2', 'Fusce aliquam vestibulum ipsum. Praesent dapibus. Et harum quidem rerum facilis est et expedita distinctio. Integer rutrum, orci vestibulum ullamcorper ultricies, lacus quam ultricies odio, vitae placerat pede sem sit amet enim. In dapibus augue non sapien. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Ut tempus purus at lorem. Morbi scelerisque luctus velit. Vivamus porttitor turpis ac leo. Duis pulvinar. Phasellus faucibus molestie nisl. Fusce consectetuer risus a nunc. Fusce suscipit libero eget elit. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Nullam sit amet magna in magna gravida vehicula. Nullam justo enim, consectetuer nec, ullamcorper ac, vestibulum in, elit. Etiam quis quam. In dapibus augue non sapien.\r\n', 2, 2, 'in_article_photos/_DSC0645.NEF-min.jpg'),
(3, '2016-01-22 17:13:13', 'Test article 3', 'Aliquam ornare wisi eu metus. Aenean placerat. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Aliquam erat volutpat. Vivamus porttitor turpis ac leo. Sed ac dolor sit amet purus malesuada congue. Nullam justo enim, consectetuer nec, ullamcorper ac, vestibulum in, elit. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Aliquam erat volutpat. Nullam faucibus mi quis velit.', 1, 2, 'in_article_photos/_DSC0295.NEF-001-min.jpg'),
(6, '2016-01-22 17:13:13', 'Test article 4', 'Maecenas ipsum velit, consectetuer eu lobortis ut, dictum at dui. In rutrum. Nunc auctor. Duis condimentum augue id magna semper rutrum. Integer in sapien. Nulla pulvinar eleifend sem. Vivamus ac leo pretium faucibus. Vivamus porttitor turpis ac leo. Nullam sit amet magna in magna gravida vehicula. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Et harum quidem rerum facilis est et expedita distinctio. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Pellentesque sapien. Phasellus faucibus molestie nisl. Donec iaculis gravida nulla. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.\r\n\r\nNullam justo enim, consectetuer nec, ullamcorper ac, vestibulum in, elit. Praesent in mauris eu tortor porttitor accumsan. Mauris dictum facilisis augue. Duis ante orci, molestie vitae vehicula venenatis, tincidunt ac pede. Aliquam erat volutpat. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Proin pede metus, vulputate nec, fermentum fringilla, vehicula vitae, justo. Mauris elementum mauris vitae tortor. Duis bibendum, lectus ut viverra rhoncus, dolor nunc faucibus libero, eget facilisis enim ipsum id lacus. Nam quis nulla. Mauris metus. Nulla est. Nulla accumsan, elit sit amet varius semper, nulla mauris mollis quam, tempor suscipit diam nulla vel leo.', 2, 2, 'in_article_photos/_DSC0530-001-min.JPG'),
(80, '2016-02-01 16:39:26', 'Test article 5', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas sollicitudin. Morbi imperdiet, mauris ac auctor dictum, nisl ligula egestas nulla, et sollicitudin sem purus in lacus. Sed ac dolor sit amet purus malesuada congue. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Duis sapien nunc, commodo et, interdum suscipit, sollicitudin et, dolor. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat. Pellentesque ipsum. Integer pellentesque quam vel velit. Ut tempus purus at lorem. Nunc tincidunt ante vitae massa. In convallis. Nullam justo enim, consectetuer nec, ullamcorper ac, vestibulum in, elit. Sed vel lectus. Donec odio tempus molestie, porttitor ut, iaculis quis, sem. Etiam dictum tincidunt diam. Phasellus et lorem id felis nonummy placerat. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Integer imperdiet lectus quis justo.', 1, 1, 'in_article_photos/IMG_20140928_181836-min.jpg'),
(86, '2016-02-02 12:17:39', 'A New Article', 'Fusce wisi. Proin pede metus, vulputate nec, fermentum fringilla, vehicula vitae, justo. Etiam ligula pede, sagittis quis, interdum ultricies, scelerisque eu. Aenean id metus id velit ullamcorper pulvinar. Integer in sapien. Duis bibendum, lectus ut viverra rhoncus, dolor nunc faucibus libero, eget facilisis enim ipsum id lacus. Duis condimentum augue id magna semper rutrum. Fusce aliquam vestibulum ipsum. Suspendisse nisl. Morbi scelerisque luctus velit. Etiam commodo dui eget wisi. Aliquam erat volutpat.', 1, 12, 'in_article_photos/_DSC0411-min.JPG'),
(87, '2016-02-07 12:31:41', 'Test article 6', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Fusce wisi. Praesent dapibus. Fusce wisi. Mauris tincidunt sem sed arcu. Sed elit dui, pellentesque a, faucibus vel, interdum nec, diam. Nullam sit amet magna in magna gravida vehicula. Nunc auctor. Proin pede metus, vulputate nec, fermentum fringilla, vehicula vitae, justo. Fusce wisi. Curabitur ligula sapien, pulvinar a vestibulum quis, facilisis vel sapien. Nullam dapibus fermentum ipsum.', 1, 13, 'in_article_photos/_DSC0727-001-min.jpg'),
(88, '2016-02-07 12:33:42', 'Test article 7', 'Cras elementum. Curabitur bibendum justo non orci. Sed convallis magna eu sem. Maecenas fermentum, sem in pharetra pellentesque, velit turpis volutpat ante, in pharetra metus odio a lectus. Ut tempus purus at lorem. Vestibulum fermentum tortor id mi. Etiam commodo dui eget wisi. Etiam dui sem, fermentum vitae, sagittis id, malesuada in, quam. Sed convallis magna eu sem. Mauris dictum facilisis augue. Etiam ligula pede, sagittis quis, interdum ultricies, scelerisque eu. Mauris dictum facilisis augue. Proin pede metus, vulputate nec, fermentum fringilla, vehicula vitae, justo. Aliquam ante. Nulla accumsan, elit sit amet varius semper, nulla mauris mollis quam, tempor suscipit diam nulla vel leo. Duis ante orci, molestie vitae vehicula venenatis, tincidunt ac pede. Nulla non arcu lacinia neque faucibus fringilla. Nullam dapibus fermentum ipsum.', 1, 12, 'in_article_photos/_DSC0026-min.JPG'),
(89, '2016-02-07 12:35:28', 'Test article 8', 'Vivamus ac leo pretium faucibus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Aenean id metus id velit ullamcorper pulvinar. Curabitur sagittis hendrerit ante. Maecenas aliquet accumsan leo. Curabitur sagittis hendrerit ante. Suspendisse sagittis ultrices augue. Integer tempor. Vivamus ac leo pretium faucibus. Donec vitae arcu. Duis pulvinar. Morbi leo mi, nonummy eget tristique non, rhoncus non leo. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nulla accumsan, elit sit amet varius semper, nulla mauris mollis quam, tempor suscipit diam nulla vel leo.', 1, 1, 'in_article_photos/_DSC1201-min.JPG');

-- --------------------------------------------------------

--
-- Struktura tabulky `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `category_title` varchar(100) COLLATE utf16_czech_ci NOT NULL,
  `author` int(11) NOT NULL,
  `display` bit(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `category_title` (`category_title`),
  KEY `author` (`author`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf16 COLLATE=utf16_czech_ci AUTO_INCREMENT=14 ;

--
-- Vypisuji data pro tabulku `categories`
--

INSERT INTO `categories` (`id`, `time_stamp`, `category_title`, `author`, `display`) VALUES
(1, '2015-11-12 16:26:41', 'My life', 2, b'1'),
(2, '2015-11-12 16:26:41', 'Work again', 2, b'1'),
(12, '2016-02-02 12:16:52', 'A New Category', 1, b'1'),
(13, '2016-02-02 12:17:01', 'Second New Category', 1, b'0');

-- --------------------------------------------------------

--
-- Struktura tabulky `galeries`
--

CREATE TABLE IF NOT EXISTS `galeries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `galery_title` varchar(100) COLLATE utf16_czech_ci NOT NULL,
  `author` int(11) NOT NULL,
  `display` bit(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `galery_title` (`galery_title`),
  KEY `author` (`author`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf16 COLLATE=utf16_czech_ci AUTO_INCREMENT=6 ;

--
-- Vypisuji data pro tabulku `galeries`
--

INSERT INTO `galeries` (`id`, `time_stamp`, `galery_title`, `author`, `display`) VALUES
(1, '2015-11-11 22:28:29', 'Moody', 2, b'1'),
(2, '2015-11-11 22:28:29', 'Nature', 2, b'1'),
(3, '2015-11-11 22:28:29', 'Models', 2, b'1'),
(4, '2015-11-11 22:28:29', 'Ian', 2, b'1'),
(5, '2015-11-15 17:12:16', 'Croatia', 2, b'1');

-- --------------------------------------------------------

--
-- Struktura tabulky `in_article_photos`
--

CREATE TABLE IF NOT EXISTS `in_article_photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `in_article_photo_path` varchar(100) COLLATE utf16_czech_ci NOT NULL,
  `article` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `article` (`article`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf16 COLLATE=utf16_czech_ci AUTO_INCREMENT=94 ;

--
-- Vypisuji data pro tabulku `in_article_photos`
--

INSERT INTO `in_article_photos` (`id`, `time_stamp`, `in_article_photo_path`, `article`) VALUES
(3, '2015-12-13 14:04:17', 'in_article_photos/_DSC0292.NEF-min.jpg', 3),
(75, '2016-01-31 19:30:36', 'in_article_photos/_DSC0770-min.jpg', 80),
(76, '2016-01-31 19:30:38', 'in_article_photos/_DSC0773-min.jpg', 80),
(77, '2016-01-31 19:30:38', 'in_article_photos/_DSC0850-min.jpg', 80),
(88, '2016-02-02 12:17:39', 'in_article_photos/_DSC0356-min.JPG', 86),
(89, '2016-02-02 12:17:40', 'in_article_photos/_DSC0362.NEF-001-min.jpg', 86),
(90, '2016-02-07 12:33:43', 'in_article_photos/_DSC0037-min.JPG', 88),
(91, '2016-02-07 12:33:43', 'in_article_photos/IMG_20140509_153859-min.jpg', 88),
(92, '2016-02-07 12:33:43', 'in_article_photos/JanRuzickaPhotography.JPG', 88),
(93, '2016-02-07 12:35:28', 'in_article_photos/_DSC0730-min.jpg', 89);

-- --------------------------------------------------------

--
-- Struktura tabulky `photos`
--

CREATE TABLE IF NOT EXISTS `photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `photo_title` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `photo_path` varchar(200) COLLATE utf8_czech_ci NOT NULL,
  `author` int(11) NOT NULL,
  `galery` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `photo_title` (`photo_title`,`photo_path`),
  KEY `author` (`author`,`galery`),
  KEY `author_2` (`author`),
  KEY `galery` (`galery`),
  KEY `photo_path` (`photo_path`),
  KEY `photo_path_2` (`photo_path`),
  KEY `photo_path_3` (`photo_path`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=99 ;

--
-- Vypisuji data pro tabulku `photos`
--

INSERT INTO `photos` (`id`, `time_stamp`, `photo_title`, `photo_path`, `author`, `galery`) VALUES
(75, '2016-02-03 22:54:37', 'Croatia Shore', 'photos/_DSC0362.NEF-001-min.jpg', 1, 5),
(76, '2016-02-03 22:56:37', 'Apple', 'photos/_DSC0770-min.jpg', 1, 2),
(77, '2016-02-03 22:57:20', 'Flower', 'photos/_DSC0730-min.jpg', 1, 2),
(78, '2016-02-03 22:58:51', 'Branch', 'photos/IMG_20140509_153859-min.jpg', 1, 2),
(82, '2016-02-03 23:04:02', 'Ladybug', 'photos/JanRuzickaPhotography.JPG', 1, 2),
(83, '2016-02-03 23:05:24', 'A Dark Path', 'photos/_DSC0026-min.JPG', 1, 1),
(84, '2016-02-03 23:06:07', 'Forest Beauty', 'photos/_DSC0292.NEF-min.jpg', 1, 3),
(85, '2016-02-03 23:06:43', 'A Plant', 'photos/_DSC0356-min.JPG', 1, 2),
(86, '2016-02-03 23:07:47', 'Linda', 'photos/_DSC0649.NEF-001-min.jpg', 1, 3),
(87, '2016-02-03 23:08:45', 'A Long Way', 'photos/_DSC0011-min.JPG', 1, 1),
(88, '2016-02-03 23:09:21', 'Forest Beauty Second', 'photos/_DSC0295.NEF-001-min.jpg', 1, 3),
(89, '2016-02-03 23:10:19', 'A Bug', 'photos/_DSC0037-min.JPG', 1, 2),
(90, '2016-02-03 23:10:49', 'A Cat', 'photos/_DSC0773-min.jpg', 1, 2),
(91, '2016-02-03 23:11:32', 'Black and White Street', 'photos/IMG_20140928_181836-min.jpg', 1, 1),
(92, '2016-02-03 23:12:10', 'Ian In The Forest', 'photos/_DSC0727-001-min.jpg', 1, 4),
(93, '2016-02-03 23:12:57', 'Autumn', 'photos/_DSC0645.NEF-min.jpg', 1, 2),
(94, '2016-02-03 23:13:49', 'Forest', 'photos/_DSC0530-001-min.JPG', 1, 2),
(95, '2016-02-03 23:14:18', 'Leafs in the Wind', 'photos/_DSC0850-min.jpg', 1, 1),
(96, '2016-02-03 23:14:49', 'Forest Path', 'photos/_DSC1201-min.JPG', 1, 2),
(98, '2016-02-07 12:25:26', 'Station', 'photos/_DSC0411-min.JPG', 1, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `login` varchar(100) COLLATE utf16_czech_ci NOT NULL,
  `password` varchar(100) COLLATE utf16_czech_ci NOT NULL,
  `email` varchar(100) COLLATE utf16_czech_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login` (`login`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf16 COLLATE=utf16_czech_ci AUTO_INCREMENT=3 ;

--
-- Vypisuji data pro tabulku `users`
--

INSERT INTO `users` (`id`, `time_stamp`, `login`, `password`, `email`) VALUES
(1, '2015-11-11 22:32:12', 'wiedzminn', '6dbc06e4e10c57b5b639ab0d0060d3c0', 'wiedzminn@seznam.cz'),
(2, '2015-11-11 22:32:12', 'ian', '911e39a924e7f51db564665a48822072', 'ianphoto@seznam.cz');

-- --------------------------------------------------------

--
-- Struktura tabulky `user_backgrounds`
--

CREATE TABLE IF NOT EXISTS `user_backgrounds` (
  `id` int(11) NOT NULL,
  `bg_title` varchar(100) COLLATE utf16_czech_ci NOT NULL,
  `bg_path` varchar(100) COLLATE utf16_czech_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `bg_title` (`bg_title`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_czech_ci;

--
-- Vypisuji data pro tabulku `user_backgrounds`
--

INSERT INTO `user_backgrounds` (`id`, `bg_title`, `bg_path`) VALUES
(1, 'opening_layer_bg', 'photos/_DSC0356-min.JPG');

-- --------------------------------------------------------

--
-- Struktura tabulky `user_content`
--

CREATE TABLE IF NOT EXISTS `user_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) COLLATE utf16_czech_ci NOT NULL,
  `content` varchar(1024) COLLATE utf16_czech_ci NOT NULL,
  `icon` varchar(100) COLLATE utf16_czech_ci NOT NULL,
  `link` varchar(100) COLLATE utf16_czech_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf16 COLLATE=utf16_czech_ci AUTO_INCREMENT=20 ;

--
-- Vypisuji data pro tabulku `user_content`
--

INSERT INTO `user_content` (`id`, `title`, `content`, `icon`, `link`) VALUES
(1, 'logo_title', 'IAN PHOTOGRAPHY', '', ''),
(2, 'menu_1', 'PORTFOLIO', '', ''),
(3, 'menu_2', 'FOTO', '', ''),
(4, 'menu_3', 'BLOG', '', ''),
(5, 'menu_4', 'KONTAKTY', '', ''),
(6, 'basic_inf_title', 'Welcome to my page!', '', ''),
(7, 'basic_inf_content', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. In enim a arcu imperdiet malesuada. Fusce consectetuer risus a nunc. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '', ''),
(8, 'NĚCO MÁLO O MNĚ', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Vestibulum erat nulla, ullamcorper nec, rutrum non, nonummy ac, erat. Sed elit dui, pellentesque a, faucibus vel, interdum nec, diam. Cras pede libero, dapibus nec, pretium sit amet, tempor quis. Integer lacinia. Integer rutrum, orci vestibulum ullamcorper ultricies, lacus quam ultricies odio, vitae placerat pede sem sit amet enim. Duis bibendum, lectus ut viverra rhoncus, dolor nunc faucibus libero, eget facilisis enim ipsum id lacus. Aliquam ornare wisi eu metus. Etiam dictum tincidunt diam. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', '', ''),
(9, 'O MÉ ZÁLIBĚ', 'Cras elementum. Vestibulum fermentum tortor id mi. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Nulla turpis magna, cursus sit amet, suscipit a, interdum id, felis. Integer pellentesque quam vel velit. Aenean fermentum risus id tortor. Mauris tincidunt sem sed arcu. Nullam faucibus mi quis velit. Pellentesque pretium lectus id turpis. Vestibulum erat nulla, ullamcorper nec, rutrum non, nonummy ac, erat. Maecenas lorem. Etiam commodo dui eget wisi. Nullam justo enim, consectetuer nec, ullamcorper ac, vestibulum in, elit. In enim a arcu imperdiet malesuada.', '', ''),
(10, 'KONTAKT', 'Maecenas libero. Quisque tincidunt scelerisque libero. Aenean id metus id velit ullamcorper pulvinar. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Ut tempus purus at lorem. Aliquam ornare wisi eu metus. Aliquam erat volutpat. Integer tempor. Nullam at arcu a est sollicitudin euismod. Maecenas ipsum velit, consectetuer eu lobortis ut, dictum at dui. Mauris metus. Nullam sapien sem, ornare ac, nonummy non.', '', ''),
(11, 'Jméno:', 'Jan', '', ''),
(12, 'Příjmení: ', 'Růžička', '', ''),
(13, 'Telefonní číslo:', '+420 111 111 111', '', ''),
(14, 'E-mail:', 'myemail@example.com', '', ''),
(15, 'Facebook', 'IAN Photo', '<span class="fa fa-facebook-f"></span>', 'https://www.facebook.com/IAN-Photo-195578203931469/?fref=ts'),
(16, 'Twitter', '- / -', '<span class="fa fa-twitter"></span>', ''),
(17, 'Google+', '- / -', '<span class="fa fa-google-plus"></span>', ''),
(18, 'Pinterest', '- / -', '<span class="fa fa-pinterest"></span>', ''),
(19, 'Tumblr', '- / -', '<span class="fa fa-tumblr"></span>', '');

--
-- Omezení pro exportované tabulky
--

--
-- Omezení pro tabulku `articles`
--
ALTER TABLE `articles`
  ADD CONSTRAINT `articles_ibfk_1` FOREIGN KEY (`category`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `articles_ibfk_2` FOREIGN KEY (`author`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Omezení pro tabulku `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_ibfk_1` FOREIGN KEY (`author`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Omezení pro tabulku `galeries`
--
ALTER TABLE `galeries`
  ADD CONSTRAINT `galeries_ibfk_1` FOREIGN KEY (`author`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Omezení pro tabulku `in_article_photos`
--
ALTER TABLE `in_article_photos`
  ADD CONSTRAINT `in_article_photos_ibfk_1` FOREIGN KEY (`article`) REFERENCES `articles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Omezení pro tabulku `photos`
--
ALTER TABLE `photos`
  ADD CONSTRAINT `photos_ibfk_3` FOREIGN KEY (`galery`) REFERENCES `galeries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `photos_ibfk_4` FOREIGN KEY (`author`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
