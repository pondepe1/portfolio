<?php

require "php_solutions/database_manipulation.php";
require "libs/Smarty.class.php";

$smarty = new Smarty();

//VYKONÁNÍ SQL DOTAZU NAD DATABÁZÍ A ZÍSKÁNÍ POLE $userBackgrounds (obsah: uživatelsky proměnlivá pozadí prvků webu)
$userBackgrounds = $dbDataMan->getDbData($con,'SELECT bg_title,bg_path FROM user_backgrounds');
//print_r($userBackgrounds);

//VYKONÁNÍ SQL DOTAZU NAD DATABÁZÍ A ZÍSKÁNÍ POLE $actualPhotos
$actualPhotos = $dbDataMan->getDbData($con,'SELECT photos.photo_path,photos.time_stamp,photos.photo_title,galeries.galery_title,users.login
/* Vybíráme pole time_stamp, photo_title, photo_path z tabulky photos, pole galery_title z tabulky galeries a pole login z tabulky users */
                                                FROM photos INNER JOIN galeries ON photos.galery = galeries.id
/* Pole galery z tabulky photos propojíme s polem id z tabulky galeries pomocí INNER JOIN. -> Na místě cizího klíče (galeries.id) v poli galery vypisujeme název galerie (výběr galeries.galery_title) */
                                                INNER JOIN users ON photos.author = users.id /* Pole author z tabulky photos propojíme s poldem id z tabulky users pomocí INNER JOIN. -> Na místě cizího klíče (users.id) v poli author tabulky photos vypisuje jméno autora (users.login) */
                                                ORDER BY photos.id DESC LIMIT 3 /* Seřazení sestupně podle id fotografie. Výběr pouze prvních 2 záznamů z výstupu -> dvou nejnovějších fotografií. */');
//print_r($actualPhotos);

//VYKONÁNÍ SQL DOTAZU NAD DATABÁZÍ A ZÍSKÁNÍ POLE $actualArticles
$actualArticles = $dbDataMan->getDbData($con,'SELECT articles.id,articles.time_stamp,articles.article_title,articles.title_photo,articles.content,categories.category_title,users.login
                                                        FROM articles INNER JOIN categories ON articles.category = categories.id
                                                        INNER JOIN users ON articles.author = users.id
                                                        ORDER BY articles.id DESC LIMIT 2');

//print_r($actualArticles);

$smarty->assign("userContent",$userContent);
$smarty->assign("userBackgrounds",$userBackgrounds);
$smarty->assign("mainMenuGaleryFiles",$mainMenuGaleryFiles);
$smarty->assign("mainMenuCategoryFiles",$mainMenuCategoryFiles);
$smarty->assign("actualPhotos",$actualPhotos);
$smarty->assign("actualArticles",$actualArticles);
$smarty->display("index.tpl");