<?php
require_once "php_solutions/database_manipulation.php";
require "libs/Smarty.class.php";

$smarty = new Smarty();

$galleryId = $_GET['galleryId'];

//ZÍSKÁNÍ POTŘEBNÝCH ÚDAJŮ O ZOBRAZOVANÉ GALERII
$currentGallery = $dbDataMan->getDbData($con,"SELECT galeries.galery_title,users.login
                                              FROM galeries INNER JOIN users ON galeries.author = users.id
                                              WHERE galeries.id = $galleryId");
//print_r($currentGallery);

//ZÍSKÁNÍ VŠECH FOTOGRAFIÍ PATŘÍCÍCH DO ZOBRAZOVANÉ GALERIE
$photosInGallery = $dbDataMan->getDbData($con,"SELECT photos.photo_title,photos.photo_path,users.login
                                               FROM photos INNER JOIN users ON photos.author = users.id
                                               WHERE photos.galery = $galleryId");
//print_r($photosInGallery);

$smarty->assign("currentGallery",$currentGallery);
$smarty->assign("photosInGallery",$photosInGallery);
$smarty->assign("userContent",$userContent);
$smarty->assign("mainMenuGaleryFiles",$mainMenuGaleryFiles);
$smarty->assign("mainMenuCategoryFiles",$mainMenuCategoryFiles);
$smarty->display("gallery.tpl");