<?php

require_once "php_solutions/database_manipulation.php";
require "libs/Smarty.class.php";

$smarty = new Smarty();

$smarty->assign("userContent",$userContent);
$smarty->assign("mainMenuGaleryFiles",$mainMenuGaleryFiles);
$smarty->assign("mainMenuCategoryFiles",$mainMenuCategoryFiles);
$smarty->display("contacts.tpl");