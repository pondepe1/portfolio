<?php

require_once "php_solutions/database_manipulation.php";
require "libs/Smarty.class.php";

$smarty = new Smarty();

//VYKONÁNÍ DOTAZU NAD DATABÁZÍ A ZÍSKÁNÍ DATABÁZOVÉHO OBJEKTU SE VŠEMI EXISTUJÍCÍMI FOTOGALERIEMI
$sql = "SELECT id,galery_title FROM galeries
        ORDER BY id DESC";
$photogaleryFilesRes = $con->query($sql);

//VYHODNOCENÍ EXISTENCE FOTOGALERIÍ
if($photogaleryFilesRes->num_rows > 0){
    
    //ZÍSKÁNÍ DVOUROZMĚRNÉHO POLE $photogaleryFiles S EXISTUJÍCÍMI FOTOGALERIEMI
    $i = 0;
    while($photogaleryFilesRow = $photogaleryFilesRes->fetch_assoc()){
        $photogaleryFiles[$i]['id'] = $photogaleryFilesRow['id'];
        $photogaleryFiles[$i]['galery_title'] = $photogaleryFilesRow['galery_title'];
        $photogaleryFiles[$i]['actual_photo_path'] = "";
        $i++;
    }
    //print_r($photogaleryFiles);

    //ZÍSKÁNÍ NEJAKTUÁLNĚJŠÍ FOTOGRAFIE KAŽDÉ Z EXISTUJÍCÍCH GALERIÍ
    foreach($photogaleryFiles as $galeryFile){
        $sql = "SELECT photo_path,galery FROM photos WHERE galery = ".$galeryFile['id']."
                ORDER BY id DESC
                LIMIT 1";
        $actualGaleryPhotosRes = $con->query($sql);
        $actualGaleryPhotos[] = $actualGaleryPhotosRes->fetch_assoc();
    }
    //print_r($actualGaleryPhotos);

    //PŘIŘAZENÍ NEJAKTULNĚJŠÍCH FOTOGRAFIÍ K PŘÍSLUŠNÝM GALERIÍM
    foreach($photogaleryFiles as $key => $galeryFile){
        foreach($actualGaleryPhotos as $galeryPhoto){
            if($galeryPhoto['galery'] == $galeryFile['id']){
                $photogaleryFiles[$key]['actual_photo_path'] = $galeryPhoto['photo_path'];
                //print_r($photogaleryGaleryFiles[$key]['actual_photo_path']);
            }
        }
    }
    //print_r($photogaleryGaleryFiles);
    
    $smarty->assign("photogaleryFiles",$photogaleryFiles);
    
}

$smarty->assign("userContent",$userContent);
$smarty->assign("mainMenuGaleryFiles",$mainMenuGaleryFiles);
$smarty->assign("mainMenuCategoryFiles",$mainMenuCategoryFiles);
$smarty->display("photogalery.tpl");