<?php

require_once "database_connection.php";

if(isset($_POST['submit-btn'])){
    
    //print_r($_POST);
    
    //INICIALIZACE ZÁKLADNÍCH ŘÍDÍCÍCH PROMĚNNÝCH INDIKUJÍCÍCH VÝSLEDEK APLIKACE
    $contactFormStatus = 1;
    $contactFormSigns = array();
    $signsString = "";
    
    //OŠETŘENÍ VSTUPU VŮČI NEPOVOLENÝM ZNAKŮM
    function test_input($data) {
        //Odstranění bílých znaků na začátku a na konci řetězce
        $data = trim($data);
        //Odstranění zpětných lomítek v řetězci
        $data = stripslashes($data);
        //Převod speciálních znaků v řetězci na znakové entity
        $data = htmlspecialchars($data);
        //print_r($data)
        return $data;
    }
    
    //VYHODNOCENÍ ZADÁNÍ JMÉNA PRODUKTORA
    if(isset($_POST['name']) && !empty($_POST['name'])){
        
        //ODSTRANĚNÍ NEPOVOLENÝCH ZNAKŮ ZE ZADANÉHO JMÉNA
        $name = test_input($_POST['name']);
        //print_r($name)
        
        if(!preg_match("/^[a-ŽA-Ž ]*$/",$name)){
            $contactFormStatus = 0;
            $contactFormSigns[] = "Povolena pouze písmena a mezery";
        }
        
    }
    else{
        //V případě nezadání jména produktora -> hlavní řídící proměnná nastavena na 0, vyplnění identifikátoru chyby
        $contactFormStatus = 0;
        $contactFormSigns[] = "Vyžadováno zadání jména";
    }
    
    //VYHODNOCENÍ ZADÁNÍ EMAILU
    if(isset($_POST['email']) && !empty($_POST['email'])){
        
        //ODSTRANĚNÍ NEPOVOLENÝCH ZNAKŮ ZE ZADANÉHO MAILU
        $email = test_input($_POST['email']);
        //print_r($mail);
        
        //OVĚŘENÍ FORMÁTU ZADANÉHO EMAILU
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
            $contactFormStatus = 0;
            $contactFormSigns[] = "Zadán neplatný formát e-mailu"; 
        }
        
    }
    else{
        //V případě nezadání emailu produktora -> hlavní řídící proměnná nastavena na 0, vyplnění identifikátoru chyby
        $contactFormStatus = 0;
        $contactFormSigns[] = "Vyžadováno zadání e-mailu";
    }
    
    //VYHODNOCENÍ ZADÁNÍ TITULKU VZKAZU
    if(isset($_POST['subject']) && !empty($_POST['subject'])){
        
        //ODSTRANĚNÍ NEPOVOLENÝCH ZNAKŮ ZE ZADANÉHO TITULKU
        $subject = test_input($_POST['subject']);
        //print_r($subject);
        
    }
    else{
        $contactFormStatus = 0;
        $contactFormSigns[] = "Vyžadováno zadání titulku";
    }
    
    //VYHODNOCENÍ ZADÁNÍ OBSAHU EMAILU
    if(isset($_POST['message']) && !empty($_POST['message'])){
        
        //ODSTRANĚNÍ NEPOVOLENÝCH ZNAKŮ ZE ZADANÉHO OBSAHU EMAILU
        $message = test_input($_POST['message']);
        //print_r($message);
        
    }
    else{
        $contactFormStatus = 0;
        $contactFormSigns[] = "Vyžadován obsah";
    }
    
    //OVĚŘENÍ BEZCHYBNÉHO PRŮBĚHU APLIKACE
    if($contactFormStatus == 1 && empty($contactFormSigns)){
        
        //NASTAVENÍ FORMÁTU OBSAHU
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        
        //PARAMETR FROM
        $headers .= "From: ".$email."\r\n";
        
        //E-mailová adresa adresáta
        $sql = "SELECT email FROM users WHERE id=2";
        $emailToRes = $con->query($sql);
        $emailTo = $emailToRes->fetch_assoc();
        $to = $emailTo['email'];
        //print_r($to);
        
        //SESTAVENÍ STRUKTURY EMAILU
        $names = explode(" ",$name);
        //print_r(count($names));
        if(count($names) > 1){
            $firstName = $names[0];
            $lastName = $names[1];
            $message = "
                        <html>
                        <head>
                        <title>$subject</title>
                        </head>
                        <body>
                        <h3>$subject</h3>
                        <p style='width: 60%; text-align: justify;'>$message</p>
                        <p><br /></p>
                        <p style='font-style: italic;'>$firstName<br />$lastName</p>
                        </body>
                        </html>
                       ";
        }
        else{
            $name = $names[0];
            $message = "
                        <html>
                        <head>
                        <title>$subject</title>
                        </head>
                        <body>
                        <h3>$subject</h3>
                        <p>$message</p>
                        <p></p>
                        <p><em>$name</em></p>
                        </body>
                        </html>
                       ";
        }
        
        //print_r($name);
        //print_r($email);
        //print_r($subject);
        //print_r($message);
        
        if(mail($to,$subject,$message,$headers) == false){
            $contactFormStatus = 0;
            $contactFormSigns[] = "Nepodařilo se odeslat e-mail";
        }
        else{
            $contactFormSigns[] = "Email úspěšně odeslán";
            $contactFormSigns[] = "Děkuji za kontaktování. Vaši korespondenci zodpovím co nejdříve.";
            $contactFormSigns[] = "Přeji hezký zbytek dne, Jan Růžička.";
        }
            
    }
    
    //print_r($contactFormStatus);
    //print_r($contactFormSigns);
    
    //SESTAVENÍ VÝSLEDNÉHO INDIKÁTORU VÝSLEDKU APLIKACE
    foreach($contactFormSigns as $key => $sign){
        if($key > 0){
            $signsString .= "|<p class=multiple-info-msg>".$contactFormSigns[$key]."</p>";
        }
        else $signsString .= "|".$contactFormSigns[$key];
    }
    
    echo $contactFormStatus.$signsString;
}