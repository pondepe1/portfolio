<?php

//TŘÍDA OBSAHUJÍCÍ PŘIPOJENÍ K DATABÁZI
class DbConnectionClass{

    //DEFINICE ATRIBUTŮ TŘÍDY JAKO PROTECTED (NÁZEV SERVERU, UŽIVATELSKÉ JMÉNO, HESLO, NÁZEV DATABÁZE)
    protected $host;
    protected $user;
    protected $password;
    protected $dbname;

    //DEFINICE KONSTRUKTORU PRO TVORBU INSTANCÍ TŘÍDY DbConnectionClass
    public function __construct($ht,$us,$passwd,$dbnm){
        $this->host = $ht;
        $this->user = $us;
        $this->password = $passwd;
        $this->dbname = $dbnm;
    }

    //DEFINICE GETTERŮ TŘÍDY (ZÍSKÁNÍ HODNOT ATRIBUTŮ MIMO TŘÍDU DbConnectionClass A JEJÍ POTOMKY)
    public function getHost(){
        return $this->host;
    }
    public function getUser(){
        return $this->user;
    }
    public function getPassword(){
        return $this->password;
    }
    public function getDbname(){
        return $this->dbname;
    }

    //DEFINICE SETTERŮ TŘÍDY (ZMĚNA HODNOT ATRIBUTŮ TŘÍDY)
    public function setHost($ht){
        $this->host=$ht;
    }
    public function setUser($us){
        $this->user=$us;
    }
    public function setPassword($passwd){
        $this->password=$passwd;
    }
    public function setDbname($dbnm){
        $this->dbname=$dbnm;
    }

    //DEFINICE METODY ZAJIŠŤUJÍCÍ PŘIPOJENÍ K DATABÁZI
    public function dbConnection(){
        //Zahájení připojení k databázi $con s údaji uloženými v atributech třídy
        $con = new mysqli($this->host,$this->user,$this->password,$this->dbname);
        //Chybová hláška v případě nezískání přístupu k databázi
        if($con->connect_error){
            die("Connection failed: ".$con->connect_error);
        }
        //print_r($con);
        //Nastavení kódování objektů načítaných z databáze
        $con->set_charset("UTF8");
        //Připojení k databázi jako návratová hodnota metody
        return $con;
    }
    
}


//VYTVOŘENÍ INSTANCE TŘÍDY DbConnectionClass A PŘIPOJENÍ SE K DATABÁZI
$dbCon = new DbConnectionClass('localhost','root','','ian_photography');
$con = $dbCon->dbConnection();