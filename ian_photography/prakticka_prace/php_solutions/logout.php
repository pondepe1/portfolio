<?php
session_start();

$backupFile = $_GET['backupFile'];

unset($_SESSION['logged']);
unset($_SESSION['userName']);


if(isset($_GET['articleId']) && !empty($_GET['articleId'])){
    $articleId = $_GET['articleId'];
}

if(isset($_GET['currentPage']) && !empty($_GET['currentPage'])){
    $currentPage = $_GET['currentPage'];
}

if(isset($currentPage) && !empty($currentPage) && !isset($articleId)){
    header('Location: '.$backupFile.'?pageStart='.$currentPage.'&user');
    exit;
}

if(isset($articleId) && !empty($articleId)){
    if(isset($currentPage) && !empty($currentPage)){
        header('Location: '.$backupFile.'?articleId='.$articleId.'&currentPage='.$currentPage.'&user');
    }
    else header('Location: '.$backupFile.'?articleId='.$articleId.'&user');
}
else header('Location: '.$backupFile.'?user');