<?php
/**
 * Created by PhpStorm.
 * User: Wiedzmin
 * Date: 9. 11. 2015
 * Time: 10:44
 */

session_start();

require "database_connection.php";


//TŘÍDA OBSAHUJÍCÍ MANIPULACI S DATABÁZOVÝMI DATY
class DbDataManClass{
    
    //DEFINICE METODY SLOUŽÍCÍ K NAČTENÍ DAT Z DATABÁZE NA ZÁKLADĚ SQL DOTAZU A JEJICH PŘEDÁNÍ JAKO NÁVRATOVOU HODNOTU
    public function getDbData($connection,$sql){
        //Vykonání SQL dotazu, získání databázového objektu
        $result = $connection->query($sql);
        //print_r($result);
        if($result->num_rows > 0){
            while($loadedTableRow = $result->fetch_assoc()){
                //Cyklus, který postupně ukládá záznamy tabulky ($loadedTableRow) do dvourozměrného pole $loadedDbData. Výsledná struktura pole jsou tedy jednotlivé záznnamy rozlišené číselným indexem. Každý ze záznamů dále představuje asociativní pole o položkách sloupců databázové tabulky
                 $loadedDbData[] = $loadedTableRow;
            }
            //print_r($loadedDbData);
            //Pole $loadedDbData jako návratová hodnota metody
            return $loadedDbData;
        }
        return false;
    }
    
    //DEFINICE METODY SLOUŽÍCÍ K ZALOŽENÍ GALERIE FOTOGRAFIÍ A KATEGORIE ČLÁNKŮ
    public function addDbGalCat($itemAddType,$con,$existingItemTitles){
        
        //KONTROLNÍ VÝPIS VSTUPNÍCH PARAMETRŮ
        //print_r($itemAddType);
        //print_r($_POST[$itemAddType.'-add-title']);
        
        //VYHODNOCENÍ ZAPISOVANÉ DATABÁZOVÉ TABULKY A ČÁSTI INFORMAČNÍHO ŘETĚZCE S TYPEM POLOŽKY
        switch($itemAddType){
            case "galery": $dbInsertTable = "galeries";
                           $resultInfoText = "galerie";
                           break;
            case "category": $dbInsertTable = "categories";
                             $resultInfoText = "kategorie";
                             break;
        }
        //print_r($dbInsertTable);
        //print_r($resultInfoText);
        
        //OVĚŘENÍ ZADÁNÍ NÁZVU POLOŽKY DO PŘÍSLUŠNÉHO INPUTU
        if(isset($_POST[$itemAddType.'-add-title']) && !empty($_POST[$itemAddType.'-add-title'])){
            
            //VYTVOŘENÍ PLNÝCH NÁZVŮ UŽÍVANÝCH PARAMETRŮ ZAPISOVANÉHO PRVKU
            $addTitleName = $itemAddType.'-add-title';
            $addDisplayName = $itemAddType.'-add-display';
            $dbTitleColumn = $itemAddType.'_title';
            //print_r($addTitleName);
            //print_r($addDisplayName);
            //print_r($dbTitleColumn);
            
            //ZÍSKÁNÍ POTŘEBNÝCH ÚDAJŮ O ZAPISOVANÉ POLOŽCE
            $itemAddAuthor = $_GET['authorId'];
            $itemAddTitle = htmlspecialchars($_POST[$addTitleName]);
            //print_r($itemAddAuthor);
            //print_r($itemAddTitle);
            
            //ZÍSKÁNÍ HODNOTY DISPLAY ZAPISOVANÉ POLOŽKY (v případě nezatrhnutí checkboxu -> display = 0)
            if(isset($_POST[$addDisplayName]) && !empty($_POST[$addDisplayName])){
                $itemAddDisplay = $_POST[$addDisplayName];
            }
            else $itemAddDisplay = 0;
            //print_r($itemAddDisplay);
            
            //OVĚŘENÍ NEKOLIZNOSTI NÁZVU ZAPISOVANÉ POLOŽKY S NÁZVY EXISTUJÍCÍCH POLOŽEK (v případě kolize -> ukončení scriptu)
            foreach($existingItemTitles as $existingItemTitle){
                if($existingItemTitle[$dbTitleColumn] == $itemAddTitle){
                    return "Zadaný název $resultInfoText již existuje.";
                }
            }
            
            $sql = "INSERT INTO $dbInsertTable ($dbTitleColumn,author,display)
                    VALUES ('$itemAddTitle',$itemAddAuthor,$itemAddDisplay)";
            if($con->query($sql) !== true){
                return "Chyba při vykonávání dotazu";
            }
            else return "$resultInfoText $itemAddTitle úspěšně založena";
            
        }
        else return "Nezadán název $resultInfoText";
        
    }
    
    //DEFINICE METODY SLOUŽÍCÍ K EDITACI FOTOGALERIÍ A KATEGORIÍ ČLÁNKŮ
    public function editDbGalCat($itemEditId,$itemEditType,$dbDataMan,$con){
        
        //KONTROLNÍ VÝPIS VSTUPNÍCH PARAMETRŮ
        //print_r($itemEditId);
        //print_r($itemEditType);
        //print_r($_POST["$itemEditType".$itemEditId."-edit-title"]);
        
        //VYHODNOCENÍ EDITOVANÉ DATABÁZOVÉ TABULKY A ČÁSTI INFORMAČNÍHO ŘETĚŽCE S TYPEM POLOŽKY
            switch($itemEditType){
                case "galery": $dbEditTable = "galeries";
                               $resultInfoText = "galerie";
                               break;
                case "category": $dbEditTable = "categories";
                                 $resultInfoText = "kategorie";
                                 break;
            }
            //print_r($dbEditTable);
            //print_r($resultInfoText);
        
        //OVĚŘENÍ ZADÁNÍ NÁZVU POLOŽKY DO PŘÍSLUŠNÉHO INPUTU
        if(isset($_POST["$itemEditType".$itemEditId."-edit-title"]) && !empty($_POST["$itemEditType".$itemEditId."-edit-title"])){
            
            //VYTVOŘENÍ PLNÝCH NÁZVŮ UŽÍVANÝCH PARAMETRŮ EDITOVANÉHO PRVKU
            $editTitleName = $itemEditType.$itemEditId."-edit-title";
            $editDisplayName = $itemEditType.$itemEditId."-edit-display";
            $dbTitleColumn = $itemEditType."_title";
            //print_r($editTitleName);
            //print_r($editDisplayName);
            //print_r($dbTitleColumn);
            
            //ZÍSKÁNÍ POTŘEBNÝCH ÚDAJŮ O POLOŽCE
            $itemEditTimeStamp = $_GET['timeStamp'];
            $itemEditAuthor = $_GET['authorId'];
            $itemEditTitle = htmlspecialchars($_POST[$editTitleName]);
            //print_r($itemEditTimeStamp);
            //print_r($itemEditAuthor);
            //print_r($itemEditTitle);
                                    
            //ZÍSKÁNÍ HODNOTY DISPLAY EDITOVANÉ POLOŽKY (v případě nezatrhnutí checkboxu -> 0)
            if(isset($_POST[$editDisplayName]) && !empty($_POST[$editDisplayName])){
                $itemEditDisplay = $_POST[$editDisplayName];
            }
            else $itemEditDisplay = 0;
            //print_r($itemEditDisplay);
                        
            //VYKONÁNÍ SQL DOTAZU NAD DATABÁZÍ A ZÍSKÁNÍ POLE $existingItemTitles (obsah: názvy všech položek kromě editované položky)
            $otherItemTitles = $dbDataMan->getDbData($con,"SELECT $dbTitleColumn FROM $dbEditTable WHERE id != $itemEditId");
            //print_r($existingItemTitles);
            
            //OVĚŘENÍ NEKOLIZNOSTI ZADANÉHO NÁZVU POLOŽKY S EXISTUJÍCÍMI NÁZVY POLOŽEK (v případě kolize -> ukončení scriptu)
            foreach($otherItemTitles as $otherItemTitle){
                if($otherItemTitle[$dbTitleColumn] == $itemEditTitle){
                    return "Zadaný název $resultInfoText již existuje.";
                }
            }

            //VYKONÁNÍ SQL DOTAZU NAD DATABÁZÍ PRO UPDATE PŘÍSLUŠNÉ POLOŽKY
            $sql = "UPDATE $dbEditTable
                    SET $dbTitleColumn='$itemEditTitle',display=$itemEditDisplay
                    WHERE id=$itemEditId";
            if($con->query($sql) !== true){
                return "Chyba při vykonávání dotazu.";
            }
            else return $itemEditId."|".$itemEditTitle."|".$itemEditTimeStamp."|".$itemEditAuthor;
            
        }
        else return "Nezadán název $resultInfoText.";    
    }
        

}


//VYTVOŘENÍ INSTANCE TŘÍDY DbDataManClass
$dbDataMan = new DbDataManClass();

//VYKONÁNÍ SQL DOTAZU NAD DATABÁZÍ A ZÍSKÁNÍ POLE $userContent (obsah: uživatelsky proměnlivé textové položky webu)
$userContent = $dbDataMan->getDbData($con,'SELECT title,content,icon,link FROM user_content');

//VYKONÁNÍ SQL DOTAZU NAD DATABÁZÍ A ZÍSKÁNÍ POLE $mainMenuGaleryFiles (obsah: položky galerií v hlavní nabídce)
$mainMenuGaleryFiles = $dbDataMan->getDbData($con,'SELECT id,galery_title FROM galeries WHERE display = 1');

//VYKONÁNÍ SQL DOTAZU NAD DATABÁZÍ A ZÍSKÁNÍ POLE $mainMenuCategoryFiles (obsah: položky kategorií v hlavní nabídce)
$mainMenuCategoryFiles = $dbDataMan->getDbData($con,'SELECT id,category_title FROM categories WHERE display = 1');

//VYKONÁNÍ SQL DOTAZU NAD DATABÁZÍ A ZÍSKÁNÍ POLE $galeryFiles (obsah: existující galerie fotografií)
$galeryFiles = $dbDataMan->getDbData($con,'SELECT id,galery_title FROM galeries ORDER BY id DESC');

//VYKONÁNÍ SQL DOTAZU NAD DATABÁZÍ A ZÍSKÁNÍ POLE $categoryFiles (obsah: existující kategorie článků)
$categoryFiles = $dbDataMan->getDbData($con,'SELECT id,category_title FROM categories ORDER BY id DESC');

//VYKONÁNÍ SQL DOTAZU NAD DATABÁZÍ A ZÍSKÁNÍ POLE $galeryFiles (obsah: existující uživatelé)
$users = $dbDataMan->getDbData($con,'SELECT id,login FROM users');