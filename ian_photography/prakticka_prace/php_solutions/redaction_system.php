<?php

//print_r($con);
//print_r($_FILES["photo-add-file"]);

require_once "database_manipulation.php";


if(isset($_GET['tool']) && !empty($_GET['tool'])){
    
    
    if($_GET['tool'] == "photoAdd"){
        
        //print_r($_FILES['photo-add-file']);
        //Vyhodnocení existence souboru pro upload
        if(isset($_FILES["photo-add-file"]) && !empty($_FILES['photo-add-file'])){
            
            if(isset($_POST['photo-add-title']) && !empty($_POST['photo-add-title'])){
                
                $allowedFormats = array("gif","jpeg","jpg","png","GIF","JPEG","JPG","PNG");
                $target_dir = "../photos/";
                $photoAddTitle = htmlspecialchars($_POST["photo-add-title"]);
                $target_file = $target_dir . basename($_FILES["photo-add-file"]["name"]);
                //print_r($target_file);
                $uploadStatus = 1;
                $uploadErrorType = "";
                $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
                //print_r($imageFileType);

                //Ověření, zda se skutečně jedná o obrázek
                if(getimagesize($_FILES["photo-add-file"]["tmp_name"]) == false){
                    echo "Zvolený soubor není obrázkem.";
                    exit;
                }

                //Ověření existence toho samého souboru
                if(file_exists($target_file)){
                    $uploadStatus = 0;
                    $uploadErrorType = "Soubor již existuje.";
                }

                //VYKONÁNÍ SQL DOTAZU NAD DATABÁZÍ A ZÍSKÁNÍ POLE $existingPhotoTitles (obsah: titulky všech fotografií)
                $existingPhotoTitles = $dbDataMan->getDbData($con,"SELECT photo_title FROM photos");

                //Ošetření kolize názvů fotografií
                if($existingPhotoTitle != false){
                    foreach($existingPhotoTitles as $existingPhotoTitle){
                        if($existingPhotoTitle['photo_title'] == $photoAddTitle){
                            $uploadStatus = 0;
                            $uploadErrorType = "Zadaný název fotografie již existuje.";
                        }
                    }
                }

                //Omezení velikosti obrázku na 2MB
                if ($_FILES["photo-add-file"]["size"] > 2097152){
                    $uploadStatus = 0;
                    $uploadErrorType = "Zvolený soubor je příliš velký.";
                }

                //Povolení pouze určitých formátů uvedených v poli $allowedFormats
                if(in_array($imageFileType,$allowedFormats) == false){
                    $uploadStatus = 0;
                    $uploadErrorType = "Povolené formáty: GIF, JPEG, JPG, PNG";
                }

                //Vyhodnocení proměnné $uploadStatus (v případě 0 nesplněny podmínky uploadu)
                if($uploadStatus == 0){
                    echo $uploadErrorType;
                //V případě splnění všech podmínek nastane upload
                }
                else{
                    if(move_uploaded_file($_FILES["photo-add-file"]["tmp_name"], $target_file)){
                        echo "photos/" . $_FILES["photo-add-file"]["name"];
                        $photoAddPath = "photos/" . $_FILES["photo-add-file"]["name"];
                        $author = $_GET["userId"];
                        foreach($galeryFiles as $galeryFile){
                            if($galeryFile["galery_title"] == $_POST["photo-add-galery"]){
                                $galery = $galeryFile["id"];
                            }
                        }
                        $sql = "INSERT INTO photos (photo_title,photo_path,author,galery)
                                VALUES ('$photoAddTitle','$photoAddPath',$author,$galery)";
                        $con->query($sql);
                    }
                    //Ošetření výskytu neočekávané chyby při uploadu (problém komunikace se serverem)
                    else{
                        $uploadErrorType = "Během uploadu nastala chyba.";
                        echo $uploadErrorType;
                    }
                }
                
            }
            else echo "Nezadán název fotografie.";
            
        }
        else echo "Nezvolena žádná fotografie.";
            
    }
    
    if($_GET['tool'] == "photoRemove"){
        
        $photoRemoveId = $_GET['id'];
        $photoRemoveFile = $_GET['photoFile'];
        $photoRemoveTitle = $_GET['photoTitle'];
        $photoToRemovePath = "../".$photoRemoveFile;
        
        $sql = "DELETE FROM photos WHERE id = $photoRemoveId";
        
        if($con->query($sql) != true){ 
            echo "Nepodařilo se odstranit fotografii ".$photoRemoveTitle." z databáze: ";
            exit;   
        }
        
        if(!unlink($photoToRemovePath)){
            echo "Nepodařilo se odstranit fotografii ".$photoRemoveTitle." z filesystému.";
        }
        else{
            echo $photoRemoveId."|Fotografie ".$photoRemoveTitle." úspěšně odstraněna.";
        }
        
    }
    
    if($_GET['tool'] == "photoEdit"){
        
        $photoEditId = $_GET['id'];
        $photoEditTimeStamp = $_GET['timeStamp'];
        $photoEditAuthor = $_GET['author'];
        $photoEditTitle = htmlspecialchars($_POST['photo'.$photoEditId.'-edit-title']);
        
        if(isset($photoEditTitle) && !empty($photoEditTitle)){
            
            //VYKONÁNÍ SQL DOTAZU NAD DATABÁZÍ A ZÍSKÁNÍ POLE $existingPhotoTitles (obsah: titulky všech fotografií kromě editované fotografie)
            $existingPhotoTitles = $dbDataMan->getDbData($con,"SELECT photo_title FROM photos WHERE id != $photoEditId");
            //print_r($existingPhotosTitles);

            if($existingPhotoTitles != false){
                foreach($existingPhotoTitles as $existingPhotoTitle){
                    if($existingPhotoTitle['photo_title'] == $photoEditTitle){
                        echo "Zadaný název fotografie již existuje.";
                        exit;
                    }
                }
            }

            foreach($galeryFiles as $galeryFile){
                if($galeryFile['galery_title'] == $_POST['photo'.$photoEditId.'-edit-galery']){
                    $photoEditGalery = $galeryFile['id'];
                }
            }

            $sql = "UPDATE photos
                    SET photo_title='$photoEditTitle',galery=$photoEditGalery
                    WHERE id=$photoEditId";
            $con->query($sql);

            echo $photoEditId."|".$photoEditTitle."|".$_POST['photo'.$photoEditId.'-edit-galery']."|".$photoEditTimeStamp."|".$photoEditAuthor;
            
        }
        else echo "Nezadán název fotografie.";
        
    }
    
    
    if($_GET['tool'] == "galeryAdd"){
        
       echo $dbDataMan->addDbGalCat("galery",$con,$galeryFiles);
        
    }
    
    if($_GET['tool'] == "galeryRemove"){
        
        $galeryRemoveId = $_GET["galeryId"];
        $galeryRemoveTitle = $_GET["galeryTitle"];
        
        $sql = "SELECT id,photo_title,photo_path,galery FROM photos";
        $photosEvery = $con->query($sql);
        //print_r($photosEvery);
        
        foreach($photosEvery as $photo){
            if($photo['galery'] == $galeryRemoveId){
                if(!unlink("../".$photo['photo_path'])){
                    echo "Nepodařilo se odstranit fotografii z filesystému.";
                    exit;
                }
                $infoMsgArray[] = "Fotografie ".$photo['photo_title']." úspěšně odstraněna.";
            }
        }
        
        $sql = "DELETE FROM galeries WHERE id = $galeryRemoveId";
        $con->query($sql);
        $infoMsgArray[] = "Galerie $galeryRemoveTitle úspěšně odstraněna.";
        
        for($i=0; $i<count($infoMsgArray); $i++){
            if($i == 0) echo "$infoMsgArray[$i]";
            else echo "<p class='multiple-info-msg'>$infoMsgArray[$i]<p>";
        }
        echo "|".$galeryRemoveId;
    }
    
    if($_GET['tool'] == "galeryEdit"){
        echo $dbDataMan->editDbGalCat($_GET['id'],"galery",$dbDataMan,$con);
    }
    
    if($_GET['tool'] == "articleAdd"){
        
        //INICIALIZACE ZÁKLADNÍCH PROMĚNNÝCH PRO INDIKACI VÝSLEDKU APLIKACE
        $articleAddStatus = 1;
        $articleTitlePhotoStatus = 1;
        $articleInnerPhotosStatus = 1;
        $articleInnerPhotosPath = array();
        $articleAddSigns = array();
        $signsString = "";
        
        //OŠETŘENÍ ZADÁNÍ TITULKU A OBSAHU ČLÁNKU
        if(isset($_POST['article-add-title']) && !empty($_POST['article-add-title']) && isset($_POST['article-add-content']) && !empty($_POST['article-add-content'])){
            
            //ZÍSKÁNÍ TITULKU ZAPISOVANÉHO ČLÁNKU
            $articleAddTitle = htmlspecialchars($_POST['article-add-title']);
            //print_r($articleAddTitle);
            
            //VYKONÁNÍ SQL DOTAZU NAD DATABÁZÍ A ZÍKÁNÍ POLE $existingArticleTitles (obsah: titulky všech již existujících článků)
            $sql = "SELECT article_title FROM articles";
            $existingArticleTitlesRes = $con->query($sql);
            while($existingArticleTitlesRow = $existingArticleTitlesRes->fetch_assoc()){
                $existingArticleTitles[] = $existingArticleTitlesRow['article_title'];
            };
            //print_r($existingArticleTitles);
            
            //OVĚŘENÍ NEKOLIZNOSTI NÁZVU ZAPISOVANÉHO ČLÁNKU S NÁZVY EXISTUJÍCH ČLÁNKŮ
            if(!in_array($articleAddTitle,$existingArticleTitles)){
                
                //VYHODNOCENÍ VOLBY TITULNÍ FOTOGRAFIE ČLÁNKU 
                if(isset($_FILES['article-add-title-photo-file']['name']) && $_FILES['article-add-title-photo-file']['error'] == 0){
                    $articleAddTitlePhotoPath = "in_article_photos/" . basename($_FILES["article-add-title-photo-file"]["name"]);
                }
                else{
                    //V případě, že titulní fotografie článku nebyla zvolena, cesta k ní zapisovaná do tabulky articles je prádný řetězec
                    $articleAddTitlePhotoPath = "";
                }
                //print_r($_FILES['article-add-title-photo-file']);
                //print_r($articleAddTitlePhotoPath);

                //VYHODNOCENÍ VOLBY TITULNÍ FOTOGRAFIE A JEJÍ UPLOAD DO ADRESÁŘE in_article_photos
                if($articleAddTitlePhotoPath != "" && $_FILES['article-add-title-photo-file']['error'] == 0){

                    //POVOLENÉ FORMÁTY VKLÁDANÝCH FOTOGRAFIÍ/OBRÁZKŮ
                    $allowedFormats = array("gif","jpeg","jpg","png","GIF","JPEG","JPG","PNG");
                    //CÍLOVÝ ADRESÁŘ UPLOADU VE FILESYSTÉMU
                    $target_dir = "../in_article_photos/";
                    //NÁZEV UPLOADOVANÉ FOTOGRAFIE
                    $target_file = $target_dir . basename($_FILES["article-add-title-photo-file"]["name"]);
                    //print_r($target_file);
                    //ZÍSKÁNÍ FORMÁTU FOTOGRAFIE
                    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
                    //print_r($imageFileType);

                    //VYHODNOCENÍ, ZDA SE SKUTEČNĚ JEDNÁ O OBRÁZEK
                    if(getimagesize($_FILES["article-add-title-photo-file"]["tmp_name"]) == false){
                        $articleAddStatus = 0;
                        $articleTitlePhotoStatus = 0;
                        $articleAddSigns[] =  "Soubor titulní fotografie není obrázkem.";
                    }

                    //OŠETŘENÍ EXISTENCE UPLOADOVANÉHO SOUBORU
                    if(file_exists($target_file)){
                        $articleAddStatus = 0;
                        $articleTitlePhotoStatus = 0;
                        $articleAddSigns[] = "Zvolená titulní fotografie již existuje.";
                    }

                    //OMEZENÍ VELIKOSTI OBRÁZKU NA 2MB
                    if ($_FILES["article-add-title-photo-file"]["size"] > 2097152){
                        $articleAddStatus = 0;
                        $articleTitlePhotoStatus = 0;
                        $articleAddSigns[] = "Soubor titulní fotografie je příliš velký.";
                    }

                    //POVOLENÍ POUZE URČITÝCH FORMÁTŮ UVEDENÝCH V POLE $allowedFormats
                    if(in_array($imageFileType,$allowedFormats) == false){
                        $articleAddStatus = 0;
                        $articleTitlePhotoStatus = 0;
                        $articleAddSigns[] = "Povolené formáty: GIF, JPEG, JPG, PNG";
                    }

                    //VYHODNOCENÍ PROMĚNNÉ $articleAddStatus (v případě 0 nesplněny podmínky uploadu)
                    if($articleAddStatus != 0){
                        if(!move_uploaded_file($_FILES["article-add-title-photo-file"]["tmp_name"], $target_file)){
                            //OŠETŘENÍ VÝSKYTU NEOČEKÁVANÉ CHYBY PŘI UPLOADU (problém komunikace se serverem)
                            $articleAddStatus = 0;
                            $articleTitlePhotoStatus = 0;
                            $articleAddSigns[] = "Během uploadu nastala chyba.";
                        }
                    }
                }
                
                //VYHODNOCENÍ VOLBY FOTOGRAFIÍ UVNITŘ ČLÁNKU A JEJICH UPLOAD DO ADRESÁŘE in_article_photos, OŠETŘENÍ VŮČI CHYBĚ PŘI UPLOADU TITULNÍ FOTOGRAFIE
                if(isset($_FILES['article-add-photos-files']['name'][0]) && !empty($_FILES['article-add-photos-files']['name'][0]) && $articleTitlePhotoStatus == 1){
                    
                    //POVOLENÉ FORMÁTY VKLÁDANÝCH FOTOGRAFIÍ/OBRÁZKŮ
                    $allowedFormats = array("gif","jpeg","jpg","png","GIF","JPEG","JPG","PNG");
                    //CÍLOVÝ ADRESÁŘ UPLOADU VE FILESYSTÉMU
                    $target_dir = "../in_article_photos/";
                    
                    foreach($_FILES['article-add-photos-files']['name'] as $key=>$name){
                        
                        //Vyhodnocování kritérií pro upload následující fotografie uvnitř článku umožněno, dokud nenastane chyba v rámci uploadu jakékoli předchozí fotografie z pole $_FILES['article-add-photos-files] -> v případě chyby, $articleInnerPhotosStatus = 0
                        if($articleInnerPhotosStatus == 1){
                            
                            //echo $key;

                            //NÁZEV UPLOADOVANÉ FOTOGRAFIE
                            $target_file = $target_dir . basename($_FILES["article-add-photos-files"]["name"][$key]);
                            //print_r($target_file);
                            //ZÍSKÁNÍ FORMÁTU FOTOGRAFIE
                            $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
                            //print_r($imageFileType);

                            //VYHODNOCENÍ, ZDA SE SKUTEČNĚ JEDNÁ O OBRÁZEK
                            if(getimagesize($_FILES["article-add-photos-files"]["tmp_name"][$key]) == false){
                                $articleAddStatus = 0;
                                $articleInnerPhotosStatus = 0;
                                $articleAddSigns[] =  "Soubor fotografie uvnitř článku není obrázkem.";
                            }

                            //OŠETŘENÍ EXISTENCE UPLOADOVANÉHO SOUBORU
                            if(file_exists($target_file)){
                                $articleAddStatus = 0;
                                $articleInnerPhotosStatus = 0;
                                $articleAddSigns[] = "Zvolená fotografie uvnitř článku již existuje.";
                            }

                            //OMEZENÍ VELIKOSTI OBRÁZKU NA 2MB
                            if($_FILES["article-add-photos-files"]["size"][$key] > 2000000){
                                $articleAddStatus = 0;
                                $articleInnerPhotosStatus = 0;
                                $articleAddSigns[] = "Soubor fotografie uvnitř článku je příliš velký.";
                            }

                            //POVOLENÍ POUZE URČITÝCH FORMÁTŮ UVEDENÝCH V POLI $allowedFormats
                            if(in_array($imageFileType,$allowedFormats) == false){
                                $articleAddStatus = 0;
                                $articleInnerPhotosStatus = 0;
                                $articleAddSigns[] = "Povolené formáty: GIF, JPEG, JPG, PNG";
                            }

                            //VYHODNOCENÍ PROMĚNNÉ $articleAddStatus (v případě 0 nesplněny podmínky uploadu)
                            if($articleAddStatus != 0){
                                if(!move_uploaded_file($_FILES["article-add-photos-files"]["tmp_name"][$key], $target_file)){
                                    //OŠETŘENÍ VÝSKYTU NEOČEKÁVANÉ CHYBY PŘI UPLOADU (problém komunikace se serverem)
                                    $articleAddStatus = 0;
                                    $articleInnerPhotosStatus = 0;
                                    $articleAddSigns[] = "Během uploadu nastala chyba.";
                                }
                                else{
                                    $articleInnerPhotosPath[] = "in_article_photos/" . basename($_FILES["article-add-photos-files"]["name"][$key]);
                                    //print_r($articleInnerPhotosPath);
                                }
                            }
                            
                        }        
                        
                    }
                }
                //print_r($_FILES['article-add-photos-files']);
                
                //VYHODNOCENÍ ÚSPĚŠNOSTI DOSAVADNÍHO PRŮBĚHU APLICE (BEZ UPLOADU || ÚSPĚŠNÝ UPLOAD)
                if($articleAddStatus == 1 && count($articleAddSigns) == 0){
                
                    //ZÍSKÁNÍ OSTATNÍCH POTŘEBNÝCH ÚDAJŮ O ZAPISOVANÉM ČLÁNKU
                    $articleAddContent = htmlspecialchars($_POST['article-add-content']);
                    $articleAddCategory = $_POST['article-add-category'];
                    $author = $_GET['authorId'];
                    //print_r($articleAddContent);
                    //print_r($articleAddCategory);
                    //print_r($author);

                    //ZÍSKÁNÍ HODNOTY ID ZVOLENÉ KATEGORIE ČLÁNKU
                    foreach($categoryFiles as $categoryFile){
                        if($categoryFile['category_title'] == $articleAddCategory){
                            $articleAddCategory = $categoryFile['id'];
                        }
                    }
                    //print_r($articleAddCategory);

                    //VYKONÁNÍ SQL DOTAZU NAD DATABÁZÍ A VYTVOŘENÍ NOVÉHO ČLÁNKU
                    $sql = "INSERT INTO articles (article_title,content,author,category,title_photo)
                            VALUES ('$articleAddTitle','$articleAddContent',$author,$articleAddCategory,'$articleAddTitlePhotoPath')";
                    $con->query($sql);

                    //VYHODNOCENÍ PŘÍTOMNOSTI UPLOADOVANÝCH FOTOGRAFIÍ (pro určení vykonání jejich zápisu do databáze)
                    if(isset($_FILES['article-add-photos-files']['name'][0]) && !empty($_FILES['article-add-photos-files']['name'][0])){
                    
                        //ZÍSKÁNÍ ID AKTUÁLNĚ VYTVOŘENÉHO ČLÁNKU V TABULCE articles
                        $sql = "SELECT id FROM articles ORDER BY id DESC LIMIT 1";
                        $articleAddRes = $con->query($sql);
                        $articleAdd = $articleAddRes->fetch_assoc();
                        $articleAddId = $articleAdd['id'];
                        //print_r($articleAddId);
                            
                            //VYKONÁNÍ SQL DOTAZU NAD DAZABÁZÍ A ZÁPIS FOTOGRAFIÍ UVNITŘ ČLÁNKU DO TABULKY in_articles_photos
                            //print_r($articleInnerPhotosPath);
                            foreach($_FILES['article-add-photos-files']['name'] as $key=>$name){
                                $sql = "INSERT INTO in_article_photos (in_article_photo_path,article)
                                        VALUES ('$articleInnerPhotosPath[$key]',$articleAddId)";
                                $con->query($sql);
                            }
                            
                    }
                        
                }
                else{
                    //VYMAZÁNÍ UPLOADOVANÝCH FOTOGRAFIÍ Z ULOŽIŠTĚ V PŘÍPADĚ NEÚSPĚŠNÉHO POKUSU O PŘIDÁNÍ ČLÁNKU
                    //Zpětné mazání fotografií pouze v případě neúspěšného uploadu fotografie uvnitř článku (viz. podmínka uploadu fotografií uvnitř článku)
                    if($articleTitlePhotoStatus == 1 && $articleInnerPhotosStatus == 0){
                        unlink("../".$articleAddTitlePhotoPath);
                        //V případě úspěšného uploadu určitých fotografií uvnitř článku před výskytem chyby -> odstranění těchto fotografií
                        if(count($articleInnerPhotosPath) > 0){
                            foreach($articleInnerPhotosPath as $innerPhotoPath){
                                unlink("../".$innerPhotoPath);
                            }
                        }
                    }
                }
                
            }
            else{
                //V případě kolize titulku článku s titulkem již existujícího článku -> nepovoleno přidat článek
                //Hlavní řídící proměnná nastavena na 0
                $articleAddStatus = 0;
                //Naplnění indentifikátoru chyb (pole $articleAddSigns)
                $articleAddSigns[] = "Zadaný titulek článku již existuje";
            }
               
        }
        else{
            //V případě absence titulku či obsahu zapisovaného článku -> nepovoleno přidat článek
            //Hlavní řídící proměnná aplikace nastavena na 0
            $articleAddStatus = 0;
            //KONKRÉTNÍ VYHODNOCENÍ NEVYPLNĚNÝCH INPUTŮ -> naplnění indentifikátoru chyb (pole $articleAddSigns)
            if(!isset($_POST['article-add-title']) || empty($_POST['article-add-title'])) $articleAddSigns[] = "Nezadán titulek článku.";
            if(!isset($_POST['article-add-content']) || empty($_POST['article-add-content'])) $articleAddSigns[] = "Nenalezen žádný obsah článku.";
        }
        
        //SESTAVENÍ VÝSLEDNÉHO INDIKÁTORU VÝSLEDKU APLIKACE PRO PŘEDÁNÍ JS ČÁSTI AJAXU
        foreach($articleAddSigns as $sign){
            $signsString .= "|".$sign;
        }
        //print_r($signsString);
        //print_r($articleAddStatus);
        
        if($articleAddStatus == 1){
            echo $articleAddStatus."|".$articleAddTitle.$signsString;
        }
        else{
            echo $articleAddStatus.$signsString;
        }
    
    }
    
    if($_GET['tool'] == "articleRemove"){
        
        //INICIALIZACE ZÁKLADNÍ PROMĚNNÝCH PRO INDIKACI VÝSLEDKU APLIKACE
        $articleRemoveStatus = 1;
        $articleRemoveSigns = array();
        $signsString = "";
        
        //ZÍSKÁNÍ POTŘEBNÝCH ÚDAJŮ O ČLÁNKU
        $articleRemoveId = $_GET['articleId'];
        $articleRemoveTitle = $_GET['articleTitle'];
        $articleRemoveTitlePhoto = $_GET['articleTitlePhoto'];
        //print_r($articleRemoveId);
        //print_r($articleRemoveTitle);
        //print_r($articleRemoveTitlePhoto);
        
        //ODSTRANĚNÍ SOUBORU TITULNÍ FOTOGRAFIE ČLÁNKU Z FILESYSTÉMU (ošetření vykonání pouze v případě existence titulní fotografie článku)
        if(!empty($articleRemoveTitlePhoto)){
            
            if(!unlink("../".$articleRemoveTitlePhoto)){
                //V případě nemožnosti odstanit titulní fotografii -> řídící proměnná indikující výsledek procesu nastavena na 0, vyplněn identifikátor chyby
                $articleRemoveStatus = 0;
                $articleRemoveSigns[] = "Nepodařilo se odstranit titulní fotografii z uložiště.";
            }
            
        }
        
        //VYKONÁNÍ SQL DOTAZU NAD DATABÁZÍ A ZÍSKÁNÍ FOTOGRAFIÍ VÁZANÝCH NA ČLÁNEK URČENÝ K ODSTRANĚNÍ
        $sql = "SELECT id,in_article_photo_path,article FROM in_article_photos WHERE article = $articleRemoveId";
        $photosToRemove = $con->query($sql);
        //print_r($photosToRemove);
        
        //OSTRANĚNÍ SOUBORŮ FOTOGRAFIÍ UVNITŘ ČLÁNKU Z FILESYSTÉMU (ošetření vykonání pouze v případě existince fotografií uvnitř článku a případného úspěšného odstranění titulní fotografie článku)
        if(!empty($photosToRemove) && $articleRemoveStatus == 1){
            
            foreach($photosToRemove as $photoToRemove){
                //Odstraňování následující fotografie uvnitř článku proběhne pouze v případě úspěšného odstranění předchozích fotografií
                if($articleRemoveStatus == 1){
                    if(!unlink("../".$photoToRemove['in_article_photo_path'])){
                        $articleRemoveStatus = 0;
                        $articleRemoveSigns[] = "Nepodařilo se odstranit fotografii z uložiště.";
                    }
                }
            }
            
        }
        
        //VYKONÁNÍ SQL DOTAZU NAD DATABÁZÍ A ODSTRANĚNÍ DANÉHO ČLÁNKU Z TABULKY articles (na základě kaskády relačního vztahu s tabulkou in_article_photos odstraněny také případné vnitřní fotografie článku)
        if($articleRemoveStatus == 1){
            $sql = "DELETE FROM articles WHERE id = $articleRemoveId";
            if($con->query($sql) == true) $articleRemoveSigns[] = "Článek ".$articleRemoveTitle." úspěšně odstraněn.";
            else $articleRemoveSigns[] = "Nepodařilo se odstranit článek ".$articleRemoveTitle;
        }
        
        //SESTAVENÍ VÝSLEDNÉHO INDIKÁTORU VÝSLEDKU APLIKACE PRO PŘEDÁNÍ JS ČÁSTI AJAXU
        foreach($articleRemoveSigns as $sign){
            $signsString .= "|".$sign;
        }
        //print_r($signsString);
        //print_r($articleRemoveStatus);
        
        echo $articleRemoveStatus."|".$articleRemoveId.$signsString;
        
    }
    
    if($_GET['tool'] == "articleEdit"){
        
        //INICIALIZACE ZÁKLADNÍCH ŘÍDÍCÍCH PROMĚNNÝCH APLIKACE PRO INDIKACE JEJÍHO VÝSLEDKU
        $articleEditStatus = 1;
        $articleEditSigns = array();
        $signsString = "";
        
        //ZÍSKÁNÍ ID EDITOVANÉHO ČLÁNKU
        $articleEditId = $_GET['id'];
        //print_r($articleEditId);
        
        //SESTAVENÍ NÁZVŮ ČASTO UŽÍVANÝCH PARAMETRŮ EDITOVANÉHO ČLÁNKU
        $editTitleName = 'article'.$articleEditId.'-edit-title';
        $editContentName = 'article'.$articleEditId.'-edit-content';
        $editCategoryName = 'article'.$articleEditId.'-edit-category';
        //print_r($editTitleName);
        //print_r($editContentName);
        //print_r($editCategoryName);
        
        //OVĚŘENÍ ZADÁNÍ TITULKU A OBSAHU EDITOVANÉHO ČLÁNKU
        if(isset($_POST[$editTitleName]) && !empty($_POST[$editTitleName]) && isset($_POST[$editContentName]) && !empty($_POST[$editContentName])){
        
            //ZÍSKÁNÍ POTŘEBNÝCH ÚDAJŮ O EDITOVANÉM ČLÁNKU
            $articleEditTimeStamp = $_GET['timeStamp'];
            $articleEditAuthor = $_GET['authorId'];
            $articleEditTitle = htmlspecialchars($_POST[$editTitleName]);
            $articleEditContent = htmlspecialchars($_POST[$editContentName]);
            $articleEditCategory = $_POST[$editCategoryName];
            //print_r($articleEditTimeStamp);
            //print_r($articleEditAuthor);
            //print_r($articleEditTitle);
            //print_r($articleEditContent);
            //print_r($articleEditCategory);
            
            //VYKONÁNÍ SQL DOTAZU NAD DATABÁZÍ A ZÍSKÁNÍ POLE $otherArtileTitles(obsah: titulky všech článků kromě editovaného článku)
            $sql = "SELECT article_title FROM articles WHERE id != $articleEditId";
            $otherArticleTitlesRes = $con->query($sql);
            while($otherArticleTitlesRow = $otherArticleTitlesRes->fetch_assoc()){
                $otherArticleTitles[] = $otherArticleTitlesRow['article_title'];
            }
            //print_r($otherArticleTitles);
            
            //OVĚŘENÍ NEKOLIZNOSTI ZADANÉHO TITULKU EDITOVANÉHO ČLÁNKU S TITULKY JIŽ EXISTUJÍCÍCH ČLÁNKŮ
            if(!in_array($articleEditTitle,$otherArticleTitles)){
                
                //ZÍSKÁNÍ HODNOTY ID KATEGORIE EDITOVANÉHO ČLÁNKU
                foreach($categoryFiles as $categoryFile){
                    if($categoryFile['category_title'] == $articleEditCategory){
                        $articleEditCategory = $categoryFile['id'];
                    }
                }
                
                //VYKONÁNÍ SQL DOTAZU NAD DATABÁZÍ A VYKONÁNÍ EDITACE ČLÁNKU
                $sql = "UPDATE articles
                        SET article_title='$articleEditTitle', content='$articleEditContent', category=$articleEditCategory
                        WHERE id=$articleEditId";
                if($con->query($sql) == true){
                    $articleEditSigns[] = "Titulek $articleEditTitle úspěšně editován";
                }
                else $articleEditSigns[] = "Chyba při vykonávání dotazu";
            }
            else{
                $articleEditStatus = 0;
                $articleEditSigns[] = "Zadaný titulek článku již existuje";
            }
            
        }
        else{
            //V případě nezadání titulku či obsahu editovaného článku -> indikátor výsledku nastaven na 0
            $articleEditStatus = 0;
            //VYHODNOCENÍ KONKRÉTNÍHO NEZADANÉHO PARAMETRU EDITOVANÉHO ČLÁNKU
            if(!isset($_POST[$editTitleName]) || empty($_POST[$editTitleName])) $articleEditSigns[] = "Nezadán titulek článku";
            if(!isset($_POST[$editContentName]) || empty($_POST[$editContentName])) $articleEditSigns[] = "Nenalezan žádný obsah článku";
        }
        
        //SESTAVENÍ VÝSLEDNÉHO INDIKÁTORU VÝSLEDKU APLIKACE PRO JS ČÁST AJAXU
        for($i=0; $i<count($articleEditSigns);$i++){
            //První chybové oznámení na řádce s informační ikonou -> další odřádkována s odsazením (multiple-info-msg)
            if($i>0){
                $signsString .= "|<p class='multiple-info-msg'>".$articleEditSigns[$i]."</p>";
            }
            else $signsString .= "|".$articleEditSigns[$i];
        }
        
        //V případě úspěchu editace je JS předán titulek editovaného článku
        if($articleEditStatus == 1) echo $articleEditStatus."|".$articleEditId."|".$articleEditTimeStamp."|".$articleEditAuthor."|".$_POST[$editCategoryName]."|".$articleEditTitle.$signsString;
        else echo $articleEditStatus.$signsString;
    }
    
    if($_GET['tool'] == "categoryAdd"){
        echo $dbDataMan->addDbGalCat("category",$con,$categoryFiles);
    }
    
    if($_GET['tool'] == "categoryRemove"){
        
        //INICIALIZACE ZÁKLADNÍCH ŘÍDÍCÍCH PROMĚNNÝCH INDIKUJÍCÍCH VÝSLEDEK APLIKACE
        $categoryRemoveStatus = 1;
        $categoryRemoveSigns = array();
        $signsString = "";
        
        //ZÍSKÁNÍ POTŘEBNÝCH ÚDAJŮ O ODEBÍRANÉ KATEGORII
        $categoryRemoveId = $_GET['categoryId'];
        $categoryRemoveTitle = $_GET['categoryTitle'];
        //print_r($categoryRemoveId);
        //print_r($categoryRemoveTitle);
        
        //VYKONÁNÍ SQL DOTAZU NAD DATABÁZÍ PRO ZÍSKÁNÍ VŠECH ČLÁNKŮ DANÉ KATEGORIE
        $sql = "SELECT id,article_title FROM articles WHERE category = $categoryRemoveId";
        
        //OŠETŘENÍ VÝSKYTU CHYBY PŘI VYKONÁVÁNÍ VÝBĚROVÉHO DOTAZU 
        if($con->query($sql) == false){
            
            $categoryRemoveStatus = 0;
            $categoryRemoveSigns[] = "Chyba při vykonávání dotazu: SELECT shodných článků";
            
        }
        else{
            
            //V případě úspěšného vykonání dotazu -> získání databázového objektu
            $articlesToRemoveRes = $con->query($sql);
            //print_r($articlesToRemoveRes);
            
            //VYHODNOCENÍ PŘÍTOMNOSTI ČLÁNKŮ V ODEBÍRANÉ KATEGORII
            if($articlesToRemoveRes->num_rows > 0){
                
                //ZÍSKÁNÍ POLE $articlesToRemove (obsah: id článků patřících do odebírané kategorie)
                while($articlesToRemoveRow = $articlesToRemoveRes->fetch_assoc()){
                    $articlesToRemove[] = $articlesToRemoveRow;
                }
                //print_r($articlesToRemove);
                
                //ZÍSKÁNÍ TITULNÍCH FOTOGRAFIÍ A FOTOGRAFIÍ UVNITŘ VŠECH ČLÁNKŮ DANÉ KATEGORIE A JEJICH ODSTRANĚNÍ Z ULOŽIŠTĚ
                foreach($articlesToRemove as $article){
                    
                    //Odebírání následující titulní fotografie a fotografií uvnitř článku z uložiště pouze za podmínky, že nenastala chyba při odebírání předchozích
                    if($categoryRemoveStatus == 1){
                        
                        //VYKONÁNÍ SQL DOTAZU NAD DATABÁZÍ A ZÍSKÁNÍ RELATIVNÍ CESTY K TITULNÍ FOTOGRAFII ($titlePhotoToRemove)
                        $sql = "SELECT title_photo FROM articles WHERE id = ".$article['id'];
                        if($con->query($sql) == false){
                            
                            $articleRemoveStatus = 0;
                            $articleRemoveSigns[] = "Chyba při vykonávání dotazu: SELECT titulní fotografie";
                            
                        }
                        else{
                            
                            $titlePhotoRemoveRes = $con->query($sql);
                            $titlePhotoToRemove = $titlePhotoRemoveRes->fetch_assoc();
                            //print_r($titlePhotoToRemove);

                            //VYHODNOCENÍ EXISTENCE TITULNÍ FOTOGRAFIE ČLÁNKU
                            if($titlePhotoToRemove != ""){
                                //V případě, že titulní fotografie existuje -> odstraněna z uložiště
                                if(!unlink("../".$titlePhotoToRemove['title_photo'])){
                                    $categoryRemoveStatus = 0;
                                    $categoryRemoveSigns[] = "Nepodařilo se odstranit titulní fotografii článku".$article['article_title'];
                                }
                                else $categoryRemoveSigns[] = "Titulní fotografie článku ".$article['article_title']." úspěšně odstraněna";
                            }
                            
                        }
                        
                        //VYKONÁNÍ SQL DOTAZU NAD DATABÁZÍ A ZÍSKÁNÍ RELATIVNÍCH CEST K FOTOGRAFIÍM UVNITŘ ČLÁNKU ($innerPhotosToRemove)
                        $sql = "SELECT in_article_photo_path FROM in_article_photos WHERE article = ".$article['id'];
                        $innerPhotosToRemoveRes = $con->query($sql);
                        //print_r($innerPhotosToRemoveRes);
                        
                        //VYHODNOCENÍ EXISTENCE FOTOGRAFIÍ UVNITŘ ČLÁNKU
                        if($innerPhotosToRemoveRes->num_rows > 0){
                            
                            //V případě, že existují fotografie uvnitř článku -> sestavení pole $innerPhotosToRemove
                            while($innerPhotosToRemoveRow = $innerPhotosToRemoveRes->fetch_assoc()){
                                $innerPhotosToRemove[] = $innerPhotosToRemoveRow['in_article_photo_path'];
                            }
                            //print_r($innerPhotosToRemove);
                            
                            //ODSTRANĚNÍ FOTOGRAFIÍ UVNITŘ ČLÁNKU Z ULOŽIŠTĚ
                            foreach($innerPhotosToRemove as $innerPhoto){
                                //print_r($innerPhoto);
                            
                                if(!unlink("../".$innerPhoto)){
                                    $categoryRemoveStatus = 0;
                                    $categoryRemoveSigns[] = "Nepodařilo se odstranit fotografii ".$innerPhoto;
                                }
                                else $categoryRemoveSigns[] = "Fotografie $innerPhoto úspěšně odstraněna";
                                    
                            }
                            
                        }
                        
                    }
                    
                }
                
                //VYKONÁNÍ SQL DOTAZU NAD DATABÁZÍ A ODSTRANĚNÍ KATEGORIE ČLÁNKŮ
                if($categoryRemoveStatus == 1){
                    $sql = "DELETE FROM categories WHERE id = $categoryRemoveId";
                    if($con->query($sql) == false){
                        $categoryRemoveStatus = 0;
                        $categoryRemoveSigns[] = "Chyba při vykonávání dotazu: DELETE kategorie";
                    }
                    else $categoryRemoveSigns[] = "Kategorie $categoryRemoveTitle úspěšně odebrána";
                }
            
            }
            else{
                //VYKONÁNÍ SQL DOTAZU NAD DATABÁZÍ A ODSTRANĚNÍ KATEGORIE (v případě, že neexistují žádné vázané články)
                $sql = "DELETE FROM categories WHERE id = $categoryRemoveId";
                if($con->query($sql) == false){
                    $categoryRemoveStatus = 0;
                    $categoryRemoveSigns[] = "Chyba při vykonávání dotazu: DELETE kategorie";
                }
                else $categoryRemoveSigns[] = "Kategorie $categoryRemoveTitle úspěšně odebrána";
            }
               
        }
        
        //SESTAVENÍ VÝSLEDNÉHO INDIKÁTORU VÝSLEDKU APLIKACE PRO JS ČÁST AJAXU
        for($i=0; $i<count($categoryRemoveSigns);$i++){
            //První chybové oznámení na řádce s informační ikonou -> další odřádkována s odsazením (multiple-info-msg)
            if($i>0){
                $signsString .= "|<p class='multiple-info-msg'>".$categoryRemoveSigns[$i]."</p>";
            }
            else $signsString .= "|".$categoryRemoveSigns[$i];
        }
        
        //V případě úspěchu odebrání je JS předán titulek odebrané katagorie
        if($categoryRemoveStatus == 1) echo $categoryRemoveStatus."|".$categoryRemoveId.$signsString;
        else echo $categoryRemoveStatus.$signsString;
        
    }
    
    if($_GET['tool'] == "categoryEdit"){
        echo $dbDataMan->editDbGalCat($_GET['id'],"category",$dbDataMan,$con);
    }
    
    if($_GET['tool'] == "headerEdit"){
        
        //INICIALIZACE ZÁKLADNÍCH ŘÍDÍCÍCH PROMĚNNÝCH PRO INDIKACI VÝSLEDKU APLIKACE
        $headerEditStatus = 1;
        $headerEditSigns = array();
        $signsString = "";
        
        //OVĚŘENÍ ZADÁNÍ TITULKŮ VŠECH POLOŽEK
        if(isset($_POST['logo-title']) && !empty($_POST['logo-title']) && isset($_POST['portfolio-title']) && !empty($_POST['portfolio-title']) && isset($_POST['galery-title']) && !empty($_POST['galery-title']) && isset($_POST['blog-title']) && !empty($_POST['blog-title']) && isset($_POST['contacts-title']) && !empty($_POST['contacts-title'])){
            
            //ZÍSKÁNÍ POTŘEBNÝCH ÚDAJŮ O EDITOVANÝCH TITULCÍCH
            $headerEditTitles[0] = htmlspecialchars($_POST['logo-title']);
            $headerEditTitles[1] = htmlspecialchars($_POST['portfolio-title']);
            $headerEditTitles[2] = htmlspecialchars($_POST['galery-title']);
            $headerEditTitles[3] = htmlspecialchars($_POST['blog-title']);
            $headerEditTitles[4] = htmlspecialchars($_POST['contacts-title']);
            //print_r($headerEditTitles);
            
            //VYKONÁNÍ SQL DOTAZU NAD DATABÁZÍ A EDITOVÁNÍ POLOŽEK HLAVIČKY
            for($i=0; $i<count($headerEditTitles); $i++){
                $sql = "UPDATE user_content
                        SET content = '$headerEditTitles[$i]'
                        WHERE id = ($i+1)";
                if($con->query($sql) == false){
                    $headerEditStatus = 0;
                    $headerEditSigns[] = "Nepodařilo se editovat titulek ".$userContent[$i]['title'];
                }
            }
            if($headerEditStatus == 1) $headerEditSigns[] = "Hlavička úspěšně editována";
            
        }
        else{
            
            $headerEditStatus = 0;
            if(!isset($_POST['logo-title']) || empty($_POST['logo-title'])) $headerEditSigns[] = "Nezadán titulek loga";
            if(!isset($_POST['portfolio-title']) || empty($_POST['portfolio-title'])) $headerEditSigns[] = "Nezadán titulek portfolia";
            if(!isset($_POST['galery-title']) || empty($_POST['galery-title'])) $headerEditSigns[] = "Nezadán titulek galerie";
            if(!isset($_POST['blog-title']) || empty($_POST['blog-title'])) $headerEditSigns[] = "Nezadán titulek blogu";
            if(!isset($_POST['contacts-title']) || empty($_POST['contacts-title']))$headerEditSigns[] = "Nezadán titulek kontaktů";
            
        }
        
        //SESTAVENÍ VÝSLEDNÉHO INDIKÁTORU VÝSLEDKU APLIKACE PRO JS ČÁST AJAXU
        for($i=0; $i<count($headerEditSigns);$i++){
            //První chybové oznámení na řádce s informační ikonou -> další odřádkována s odsazením (multiple-info-msg)
            if($i>0){
                $signsString .= "|<p class='multiple-info-msg'>".$headerEditSigns[$i]."</p>";
            }
            else $signsString .= "|".$headerEditSigns[$i];
        }
        
        echo $headerEditStatus.$signsString;
        
    }
    
    if($_GET['tool'] == "openingLayerEdit"){
        
        //INICIALIZACE ZÁKLADNÍCH ŘÍDÍCÍCH PROMĚNNÝCH PRO INDIKACI VÝSLEDKU APLIKACE
        $openingLayerEditStatus = 1;
        $openingLayerEditSigns = array();
        $signsString = "";
        
        //OŠETŘENÍ ZADÁNÍ VŠECH VSTUPNÍCH INPUTŮ FORMULÁŘE
        if(isset($_POST['opening-layer-title']) && !empty($_POST['opening-layer-title']) && isset($_POST['opening-layer-content']) && !empty($_POST['opening-layer-content'])){
            
            //ZÍSKÁNÍ POTŘEBNÝCH ÚDAJŮ O EDITOVANÉ UVÍTACÍ VRSTVĚ
            $openingLayerEditTitle = htmlspecialchars($_POST['opening-layer-title']);
            $openingLayerEditContent = htmlspecialchars($_POST['opening-layer-content']);
            $openingLayerBgPath = $_POST['opening-layer-background'];
            //print_r($openingLayerEditTitle);
            //print_r($openingLayerEditContent);
            //print_r($openingLayerBgPath);
            
            //VYKONÁNÍ SQL DOTAZŮ NAD DATABÁZÍ A EDITOVÁNÍ UVÍTACÍ VRSTVY
            $sql = "UPDATE user_content
                    SET content = '$openingLayerEditTitle'
                    WHERE id = 6";
            if($con->query($sql) == false){
                $openingLayerEditStatus = 0;
                $openingLayerEditSigns[] = "Chyba při vykonávání dotazu: UPDATE titulku";
            }
            $sql = "UPDATE user_content
                    SET content = '$openingLayerEditContent'
                    WHERE id = 7";
            if($con->query($sql) == false){
                $openingLayerEditStatus = 0;
                $openingLayerEditSigns[] = "Chyba při vykonávání dotazu: UPDATE obsahu článku";
            }
            $sql = "UPDATE user_backgrounds
                    SET bg_path = '$openingLayerBgPath'
                    WHERE id = 1";
            if($con->query($sql) == false){
                $openingLayerEditStatus = 0;
                $openingLayerEditSigns[] = "Chyba při vykonávání dotazu: UPDATE pozadí úvodní vrstvy";
            }
            
            if($openingLayerEditStatus == 1) $openingLayerEditSigns[] = "Úvodní vrstva úspěšně editována";
            
        }
        else{
            
            $openingLayerEditStatus = 0;
            if(!isset($_POST['opening-layer-title']) || empty($_POST['opening-layer-title'])) $openingLayerEditSigns[] = "Nezadán titulek úvodní vrstvy";
            if(!isset($_POST['opening-layer-content']) || empty($_POST['opening-layer-content'])) $openingLayerEditSigns[] = "Nezadán obsah článku úvodní vrstvy";
            
        }
        
        //SESTAVENÍ VÝSLEDNÉHO INDIKÁTORU VÝSLEDKU APLIKACE PRO JS ČÁST AJAXU
        for($i=0; $i<count($openingLayerEditSigns);$i++){
            //První chybové oznámení na řádce s informační ikonou -> další odřádkována s odsazením (multiple-info-msg)
            if($i>0){
                $signsString .= "|<p class='multiple-info-msg'>".$openingLayerEditSigns[$i]."</p>";
            }
            else $signsString .= "|".$openingLayerEditSigns[$i];
        }
        
        echo $openingLayerEditStatus.$signsString;
        
    }
    
    if($_GET['tool'] == "footerEdit"){
        
        //INICIALIZACE ZÁKLADNÍCH ŘÍDÍCÍCH PROMĚNNÝCH PRO INDIKACI VÝSLEDKU APLIKACE
        $footerEditStatus = 1;
        $footerEditSigns = array();
        $signsString = "";
                
        //OŠETŘENÍ ZADÁNÍ VŠECH VSTUPNÍCH INPUTŮ FORMULÁŘE
        if(isset($_POST['first-section-title']) && !empty($_POST['first-section-title']) && isset($_POST['first-section-content']) && !empty($_POST['first-section-content']) && isset($_POST['second-section-title']) && !empty($_POST['second-section-title']) && isset($_POST['second-section-content']) && !empty($_POST['second-section-content']) && isset($_POST['third-section-title']) && !empty($_POST['third-section-title']) && isset($_POST['third-section-content']) && !empty($_POST['third-section-content'])){
            
            //ZÍSKÁNÍ POTŘEBNÝCH ÚDAJŮ O EDITOVANÉ PATIČCE WEBU
            $footerEditItems[0]['title'] = htmlspecialchars($_POST['first-section-title']);
            $footerEditItems[0]['content'] = htmlspecialchars($_POST['first-section-content']);
            $footerEditItems[1]['title'] = htmlspecialchars($_POST['second-section-title']);
            $footerEditItems[1]['content'] = htmlspecialchars($_POST['second-section-content']);
            $footerEditItems[2]['title'] = htmlspecialchars($_POST['third-section-title']);
            $footerEditItems[2]['content'] = htmlspecialchars($_POST['third-section-content']);
            //print_r($footerEditItems);
            
            //VYKONÁNÍ SQL DOTAZU NAD DATABÁZÍ A EDITOVÁNÍ PATIČKY WEBU
            for($i=0; $i<count($footerEditItems);$i++){
                
                $sql = "UPDATE user_content
                        SET title = '".$footerEditItems[$i]['title']."', content = '".$footerEditItems[$i]['content']."'
                        WHERE id = ($i+8)";
                if($con->query($sql) == false){
                    $footerEditStatus = 0;
                    $footerEditSigns[] = "Chyba při vykonávání dotatu: UPDATE user_content";
                }
                
            }
            
            if($footerEditStatus == 1) $footerEditSigns[] = "Patička webu úspěšně editována";
            
        }
        else{
            
            $footerEditStatus = 0;
            if(!isset($_POST['first-section-title']) || empty($_POST['first-section-title'])) $footerEditSigns[] = "Nezadán titulek prvního oddílu";
            if(!isset($_POST['first-section-content']) || empty($_POST['first-section-content'])) $footerEditSigns[] = "Nezadán obsah článku prvního oddílu";
            if(!isset($_POST['second-section-title']) || empty($_POST['second-section-title'])) $footerEditSigns[] = "Nezadán titulek druhého oddílu";
            if(!isset($_POST['second-section-content']) || empty($_POST['second-section-content'])) $footerEditSigns[] = "Nezadán obsah článku druhého oddílu";
            if(!isset($_POST['third-section-title']) || empty($_POST['third-section-title'])) $footerEditSigns[] = "Nezadán titulek třetího oddílu";
            if(!isset($_POST['third-section-content']) || empty($_POST['third-section-content'])) $footerEditSigns[] = "Nezadán obsah článku třetího oddílu";
            
        }
        
        //SESTAVENÍ VÝSLEDNÉHO INDIKÁTORU VÝSLEDKU APLIKACE PRO JS ČÁST AJAXU
        for($i=0; $i<count($footerEditSigns);$i++){
            //První chybové oznámení na řádce s informační ikonou -> další odřádkována s odsazením (multiple-info-msg)
            if($i>0){
                $signsString .= "|<p class='multiple-info-msg'>".$footerEditSigns[$i]."</p>";
            }
            else $signsString .= "|".$footerEditSigns[$i];
        }
        
        echo $footerEditStatus.$signsString;
        
    }
    
    if($_GET['tool'] == "contactsEdit"){
        
        //INICIALIZACE ZÁKLADNÍCH ŘÍDÍCÍCH PROMĚNNÝCH PRO INDIKACI VÝSLEDKU APLIKACE
        $contactsEditStatus = 1;
        $contactsEditSigns = array();
        $signsString = "";
            
            //ZÍSKÁNÍ POTŘEBNÝCH ÚDAJŮ O EDITOVANÝCH ZÁKLADNÍCH KONTAKTNÍCH ÚDAJÍCH
            for($i=10; $i<14; $i++){
                $contactsBasicItems[$i] = htmlspecialchars($_POST['item-'.$i.'-content']);
                if(empty($contactsBasicItems[$i])){
                    $contactsBasicItems[$i] = "- / -";
                }
            }
            //print_r($contactsBasicItems);
            
            //ZÍSKÁNÍ POTŘEBNÝCH ÚDAJŮ O EDITOVANÝCH SOCIÁLNÍCH SÍTÍCH
            for($i=14;$i<19;$i++){
                $contactsSocialItems[$i]['content'] = htmlspecialchars($_POST['item-'.$i.'-content']);
                $contactsSocialItems[$i]['link'] = htmlspecialchars($_POST['item-'.$i.'-link']);
                if(empty($contactsSocialItems[$i]['content'])){
                    $contactsSocialItems[$i]['content'] = "- / -";
                }
            }
            //print_r($contactsSocialItems);
        
            //VYKONÁNÍ SQL DOTAZU NAD DATABÁZÍ A EDITOVÁNÍ ZÁKLADNÍCH KONTAKTNÍCH ÚDAJŮ
            for($i=10; $i<14; $i++){
                
                $sql = "UPDATE user_content
                        SET content = '".$contactsBasicItems[$i]."'
                        WHERE id = ($i+1)";
                
                if($con->query($sql) == false){
                    $contactsEditStatus = 0;
                    $contactsEditSigns[] = "Chyba při vykonávání dotatu: UPDATE základních údajů";
                }
                
            }
        
            //VYKONÁNÍ SQL DOTAZU NAD DATABÁZÍ A EDITOVÁNÍ SOCIÁLNÍCH SÍTÍ V KONTAKTECH
            if($contactsEditStatus == 1){
                for($i=14;$i<19;$i++){

                    $sql = "UPDATE user_content
                            SET content = '".$contactsSocialItems[$i]['content']."', link = '".$contactsSocialItems[$i]['link']."'
                            WHERE id = ($i+1)";

                    if($con->query($sql) == false){
                        $contactsEditStatus = 0;
                        $contactsEditSigns[] = "Chyba při vykonávání dotatu: UPDATE sociálních sítí";
                    }

                }
            }
            
            if($contactsEditStatus == 1) $contactsEditSigns[] = "Kontakty úspěšně editovány";
        
            //SESTAVENÍ VÝSLEDNÉHO INDIKÁTORU VÝSLEDKU APLIKACE PRO JS ČÁST AJAXU
            for($i=0; $i<count($contactsEditSigns);$i++){
                //První chybové oznámení na řádce s informační ikonou -> další odřádkována s odsazením (multiple-info-msg)
                if($i>0){
                    $signsString .= "|<p class='multiple-info-msg'>".$contactsEditSigns[$i]."</p>";
                }
                else $signsString .= "|".$contactsEditSigns[$i];
            }
        
            echo $contactsEditStatus.$signsString;
            
        }
        
    
}