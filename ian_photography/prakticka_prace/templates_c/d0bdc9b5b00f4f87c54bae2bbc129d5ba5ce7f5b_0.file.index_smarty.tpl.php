<?php /* Smarty version 3.1.27, created on 2016-01-20 17:33:11
         compiled from "G:\Programy\EasyPHP-DevServer-14.1VC11\data\localweb\ian_photography\templates\index_smarty.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:27661569fb6c7864e89_77079943%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd0bdc9b5b00f4f87c54bae2bbc129d5ba5ce7f5b' => 
    array (
      0 => 'G:\\Programy\\EasyPHP-DevServer-14.1VC11\\data\\localweb\\ian_photography\\templates\\index_smarty.tpl',
      1 => 1453306717,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '27661569fb6c7864e89_77079943',
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_569fb6c7e394e7_02262207',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_569fb6c7e394e7_02262207')) {
function content_569fb6c7e394e7_02262207 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '27661569fb6c7864e89_77079943';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title>IAN Photography</title>

    <!-- PŘIPOJENÍ CSS STYLŮ -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css_styles/general_styles.php"/>
    <link rel="stylesheet" type="text/css" href="css_styles/header_styles.php"/>
    <link rel="stylesheet" type="text/css" href="css_styles/collapsed_navigation_styles.php"/>
    <link rel="stylesheet" type="text/css" href="css_styles/content_styles.php"/>
    <link rel="stylesheet" type="text/css" href="css_styles/footer_styles.php"/>
    <link rel="stylesheet" type="text/css" href="css_styles/responsive_styles.css"/>
</head>
    
<body>
    <div class="content_wrapper">
        <header>
            <div class="user_admin_bar">
                <p>Vítejte, uživateli admin</p>
                <div class="full_resolution_links">
                    <a href="php_solutions/logout.php">ODHLÁSIT SE</a>
                    <a href="admin_section.php">ADMINISTRACE</a>
                </div>
                <div class="dropdown">
                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    MOŽNOSTI
                        <span class="glyphicon glyphicon-triangle-bottom"></span>
                        <span class="glyphicon glyphicon-triangle-top"></span>
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                        <li><a href="admin_section.php">ADMINISTRACE</a></li>
                        <li><a href="php_solutions/logout.php">ODHLÁSIT SE</a></li>
                    </ul>
                </div>
            </div>
<!-- DEFINICE HLAVIČKY WEBOVÉ STRÁNKY -->
            <h1><a href='index.php'>IAN PHOTOGRAPHY</a></h1>

            <!-- DVOUÚROVŇOVÁ NABÍDKA WEBU PRO VELKÁ ROZLIŠENÍ -->
            <nav class='primary_navigation'>
                <ul class='main_menu_primary'>
                    <li><a href='portfolio.php'>PORTFOLIO</a></li>
                    <li><a href='fotogalerie.php'>FOTO</a>
                        <ul>
                            <li><a href='fotogalerie.php?galery_title=moody'>moody</a></li>
                            <li><a href='fotogalerie.php?galery_title=nature'>nature</a></li>
                            <li><a href='fotogalerie.php?galery_title=models'>models</a></li>
                            <li><a href='fotogalerie.php?galery_title=ian'>ian</a></li>
                            <li><a href='fotogalerie.php?galery_title=croatia'>croatia</a></li>
                        </ul>
                    </li>
                    <li><a href='blog.php'>BLOG</a>
                        <ul>
                            <li><a href='blog.php?category_title=My life'>My life</a></li>
                            <li><a href='blog.php?category_title=Work again'>Work again</a></li>
                        </ul>
                    </li>
                    <li><a href='kontakty.php'>KONTAKTY</a></li>
                </ul>
            </nav>

            <!-- DVOUÚROVŇOVÁ NABÍDKA WEBU PRO MALÁ ROZLIŠENÍ -->
            <nav class='collapsed_navigation'>
                <a href='#' id='pull'><span>MENU</span></a>
                <ul class='main_menu_collapsed'>
                    <li id='scroller'>
                        <ul>
                            <li><a href='portfolio.php'>PORTFOLIO</a></li>
                            <li><a href='fotogalerie.php'>FOTO</a>
                                <ul>
                                    <li><a href='fotogalerie.php?galery_title=moody'>moody</a></li>
                                    <li><a href='fotogalerie.php?galery_title=nature'>nature</a></li>
                                    <li><a href='fotogalerie.php?galery_title=models'>models</a></li>
                                    <li><a href='fotogalerie.php?galery_title=ian'>ian</a></li>
                                    <li><a href='fotogalerie.php?galery_title=croatia'>croatia</a></li>
                                </ul>
                            </li>
                            <li><a href='blog.php'>BLOG</a>
                                <ul>
                                    <li><a href='blog.php?category_title=My life'>My life</a></li>
                                    <li><a href='blog.php?category_title=Work again'>Work again</a></li>
                                </ul>
                            </li>
                            <li><a href='kontakty.php'>KONTAKTY</a></li>
                        </ul>
                    </li>
                </ul>
            </nav>
        </header>
        <div class='parallax_wrapper'>
                    <!-- DEFINICE OBSAHOVÉ ČÁSTI WEBOVÉ STRÁNKY S PARALLAX EFEKTEM -->

            <div class='layer_underlay'>
                <div class='content_layer photo_layer parallax_layer'>
                    <div class='image_wrapper'></div>
                    <div class='text_area'>
                        <h2>Welcome to my page!</h2>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. In enim a arcu imperdiet malesuada. Fusce consectetuer risus a nunc. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    </div>
                </div>
            </div>
            <div class='layer_underlay'>
                <div class='content_layer photo_layer'>
                    <div class='image_wrapper'></div>
                    <div class='text_area'>
                        <a href='fotogalerie.php'>
                            <span>PŘEJÍT DO FOTOGALERIE</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class='layer_underlay'>
                <div class='content_layer photo_layer parallax_layer'>
                    <div class='image_wrapper'></div>
                        <div class='hidden_description_trigger'>
                            <span class='glyphicon glyphicon-info-sign' aria-hidden='true'></span>                            <div class='hidden_description text_area'>
                                <p>any_luck</p>
                                <div>
                                    <p>nature</p>
                                    <p>ian</p>
                                    <p>2015-11-15 18:08:19</p>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
            <div class='layer_underlay'>
                <div class='content_layer photo_layer'>
                    <div class='image_wrapper'></div>
                        <div class='hidden_description_trigger'>
                            <span class='glyphicon glyphicon-info-sign' aria-hidden='true'></span>                            <div class='hidden_description text_area'>
                                <p>ian_in_forrest</p>
                                <div>
                                    <p>ian</p>
                                    <p>ian</p>
                                    <p>2015-11-11 23:38:43</p>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
            <div class='layer_underlay'>
                <div class='content_layer blog_layer'>
                    <div class='text_area'>
                        <a href='blog.php'>
                            <span>PŘEJÍT DO BLOGU</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class='layer_underlay'>
                <div class='content_layer blog_layer'>
                    <div class='text_area'>
                        <div class='article_photo'>
                            <div class='article_photo_overlay'></div>
                        </div>
                        <h3><a class='article_title' href='#'>Test article 4</a></h3>
                        <p class='article_timestamp'>2015-12-25 13:17:39 | By: ian | Category: Work again</p>
                        <p>Maecenas ipsum velit, consectetuer eu lobortis ut, dictum at dui. In rutrum. Nunc auctor. Duis condimentum augue id magna semper rutrum. Integer in sapien. Nulla pulvinar eleifend sem. Vivamus ac leo pretium faucibus. Vivamus porttitor turpis ac leo. Nullam sit amet magna in magna gravida vehicula. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Et harum quidem rerum facilis est et expedita distinctio. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Pellentesque sapien. Phasellus faucibus molestie nisl. Donec iaculis gravida nulla. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.

Nullam justo enim, consectetuer nec, ullamcorper ac, vestibulum in, elit. Praesent in mauris  [...]</p>
                        <p><a href='blog.php?id=6'>PŘEJÍT DO BLOGU</a></p>
                    </div>
                </div>
            </div>
            <div class='layer_underlay'>
                <div class='content_layer blog_layer'>
                    <div class='text_area'>
                        <div class='article_photo'>
                            <div class='article_photo_overlay'></div>
                        </div>
                        <h3><a class='article_title' href='#'>Test article 3</a></h3>
                        <p class='article_timestamp'>2015-12-25 13:17:39 | By: admin | Category: Work again</p>
                        <p>Aliquam ornare wisi eu metus. Aenean placerat. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Aliquam erat volutpat. Vivamus porttitor turpis ac leo. Sed ac dolor sit amet purus malesuada congue. Nullam justo enim, consectetuer nec, ullamcorper ac, vestibulum in, elit. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Aliquam erat volutpat. Nullam faucibus mi quis velit.</p>
                        <p><a href='blog.php?id=3'>PŘEJÍT DO BLOGU</a></p>
                    </div>
                </div>
            </div>
            <div class="form_wrapper">
                <form method="post" action="php_solutions/data_transfering.php" id="contact_form">
                    <h2>KONTAKTUJTE MĚ</h2>
                    <div class="form_group">
                        <label for="email">VÁŠ E-MAIL</label>
                        <input type="text" id="email" name="em" placeholder="ZADEJTE SVŮJ E-MAIL" maxlength="32"/>
                        <p>(MAX. 32 ZNAKŮ)</p>
                    </div>
                    <div class="form_group">
                        <label for="title">TITULEK VZKAZU</label>
                        <input type="text" id="title" name="titl" placeholder="ZADEJTE TITULEK ZPRÁVY" maxlength="64"/>
                        <p>(MAX. 64 ZNAKŮ)</p>
                    </div>
                    <div class="form_group">
                        <label for="message">VÁŠ VZKAZ</label>
                        <textarea id="message" name="msg" placeholder="MÍSTO PRO VÁŠ TEXT..." form="contact_form" maxlength="1024"></textarea>
                        <p>(MAX. 1024 ZNAKŮ)</p>    
                    </div>
                    <input class="submit_btn" type="submit" value="ODESLAT"/>
                </form>
            </div>
            <footer>
                <div class='footer_content_wrapper'>
                    <div class='footer_content'>
                        <h2>NĚCO MÁLO O MNĚ</h2>
                            <p>
                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Vestibulum erat nulla, ullamcorper nec, rutrum non, nonummy ac, erat. Sed elit dui, pellentesque a, faucibus vel, interdum nec, diam. Cras pede libero, dapibus nec, pretium sit amet, tempor quis. Integer lacinia. Integer rutrum, orci vestibulum ullamcorper ultricies, lacus quam ultricies odio, vitae placerat pede sem sit amet enim. Duis bibendum, lectus ut viverra rhoncus, dolor nunc faucibus libero, eget facilisis enim ipsum id lacus. Aliquam ornare wisi eu metus. Etiam dictum tincidunt diam. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
                            </p>
                    </div>
                    <div class='footer_content'>
                        <h2>O MÉ ZÁLIBĚ</h2>
                            <p>
                                Cras elementum. Vestibulum fermentum tortor id mi. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Nulla turpis magna, cursus sit amet, suscipit a, interdum id, felis. Integer pellentesque quam vel velit. Aenean fermentum risus id tortor. Mauris tincidunt sem sed arcu. Nullam faucibus mi quis velit. Pellentesque pretium lectus id turpis. Vestibulum erat nulla, ullamcorper nec, rutrum non, nonummy ac, erat. Maecenas lorem. Etiam commodo dui eget wisi. Nullam justo enim, consectetuer nec, ullamcorper ac, vestibulum in, elit. In enim a arcu imperdiet malesuada.
                            </p>
                    </div>
                    <div class='footer_content'>
                        <h2>KONTAKT</h2>
                            <p>
                                Maecenas libero. Quisque tincidunt scelerisque libero. Aenean id metus id velit ullamcorper pulvinar. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Ut tempus purus at lorem. Aliquam ornare wisi eu metus. Aliquam erat volutpat. Integer tempor. Nullam at arcu a est sollicitudin euismod. Maecenas ipsum velit, consectetuer eu lobortis ut, dictum at dui. Mauris metus. Nullam sapien sem, ornare ac, nonummy non.
                            </p>
                    </div>
                </div>
                <h3>CREATED BY &copy; PETR PONDĚLÍK 2016</h3>
            </footer>
        </div>
    </div>
    
    <!-- PŘIPOJENÍ JQUERY -->
    <?php echo '<script'; ?>
 src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"><?php echo '</script'; ?>
>
    
    <!-- PŘIPOJENÍ BOOTSTRAP JS PLUGINŮ -->
    <?php echo '<script'; ?>
 src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"><?php echo '</script'; ?>
>
    
    <!-- PŘIPOJENÍ SCRIPTU PRO MD5 HASHOVÁNÍ -->
    <?php echo '<script'; ?>
 src="js_solutions/md5_hashing.js"><?php echo '</script'; ?>
>
    
    <!-- PŘIPOJENÍ SCRIPTU OVLÁDAJÍCÍHO PRŮHLEDNOST HLAVNÍ NABÍDKY A NABÍDKU PRO MALÁ ROZLIŠENÍ-->
    <?php echo '<script'; ?>
 src="js_solutions/header_functions.js"><?php echo '</script'; ?>
>
    
    <!-- PŘIPOJENÍ SCRIPTU OVLÁDAJÍCÍHO PARALLAX EFEKT -->
    <?php echo '<script'; ?>
 src="js_solutions/parallax_effect.js"><?php echo '</script'; ?>
>
    
    <!-- PŘIPOJENÍ SCRIPTU OVLÁDAJÍCÍHO PŘIHLAŠOVACÍ FORMULÁŘ -->
    <?php echo '<?php ';?>include "js_solutions/login_form_control.php" <?php echo '?>';?>
    
</body>
</html><?php }
}
?>