<?php /* Smarty version 3.1.27, created on 2016-02-22 13:54:55
         compiled from "G:\Programy\EasyPHP-DevServer-14.1VC11\data\localweb\ian_photography_soc\templates\contacts.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:211156cb051fa1c673_48203750%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2c7e97cb47065be9176f7588209934536cd614bd' => 
    array (
      0 => 'G:\\Programy\\EasyPHP-DevServer-14.1VC11\\data\\localweb\\ian_photography_soc\\templates\\contacts.tpl',
      1 => 1454871401,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '211156cb051fa1c673_48203750',
  'variables' => 
  array (
    'userContent' => 0,
    'mainMenuGaleryFiles' => 0,
    'galeryFile' => 0,
    'mainMenuCategoryFiles' => 0,
    'categoryFile' => 0,
    'i' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_56cb05200f5a99_97687082',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_56cb05200f5a99_97687082')) {
function content_56cb05200f5a99_97687082 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '211156cb051fa1c673_48203750';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title>IAN Photography</title>

    <!-- PŘIPOJENÍ CSS STYLŮ -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css_styles/general_styles.css"/>
    <link rel="stylesheet" type="text/css" href="css_styles/loading_screens.css"/>
    <link rel="stylesheet" type="text/css" href="css_styles/header_styles.css"/>
    <link rel="stylesheet" type="text/css" href="css_styles/collapsed_navigation_styles.css"/>
    <link rel="stylesheet" type="text/css" href="css_styles/footer_styles.css"/>
    <link rel="stylesheet" type="text/css" href="css_styles/contacts_styles.css"/>
    <link rel="stylesheet" type="text/css" href="css_styles/responsive_styles.css"/>
    
</head>

    
    
<body>
    
    
        <!-- DEFINICE LOADING SCREENU PRO LOADING DOM STRUKTURY DOKUMENTU -->
        <div class="cssloader-wrapper">
            
            <div class="cssload-loader">
                <div class="cssload-flipper">
                    <div class="cssload-front"></div>
                    <div class="cssload-back"></div>
                </div>
                <h4>Loading...</h4>
            </div>
            
        </div> 
        
        
        <!-- DEFINICE WRAPPERU HLAVIČKY WEBU -->
        <header>
            
            <?php if (isset($_SESSION['logged']) && $_SESSION['logged'] == true) {?>
            <!-- DEFINICE UŽIVATESLKÉHO ADMINISTRAČNÍHO PANELU (pouze v případě úspěšného přihlášení uživatele -> $_SESSION['logged'] == true, viz. login.php) -->
            <div class="user_admin_bar">
                <!-- Uvítací titulek uživateslkého panelu -> obsahuje uživatelské jméno, viz. login.php -->
                <p>Uživatel <?php echo $_SESSION['userData']['login'];?>
</p>
                <div class="full_resolution_links">  
                    <a href="php_solutions/logout.php?backupFile=../contacts.php">ODHLÁSIT SE</a>
                    <a href="admin_section.php?backupFile=contacts.php">ADMINISTRACE</a>  
                </div>
                <div class="dropdown">
                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        MOŽNOSTI
                        <span class="glyphicon glyphicon-triangle-bottom"></span>
                        <span class="glyphicon glyphicon-triangle-top"></span>            
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                        <li><a href="admin_section.php?backupFile=contacts.php">ADMINISTRACE</a></li>
                        <li><a href="php_solutions/logout.php?backupFile=../contacts.php">ODHLÁSIT SE</a></li>
                    </ul>
                </div>
           </div>
           <?php }?>
           
            <!-- DEFINICE HLAVIČKY WEBOVÉ STRÁNKY (obsah je načítán z databáze, viz. database_manipulation.php) -->
            <h1><a href="index.php"><?php echo $_smarty_tpl->tpl_vars['userContent']->value[0]['content'];?>
</a></h1>

            <!-- DEFINICE DVOUÚROVŇOVÉ NABÍDKY WEBU PRO VELKÁ ROZLIŠENÍ -->
            <nav class="primary_navigation">
                <ul class="main_menu_primary">
                    <!-- Obsah položek první úrovně hlavní nabídky načítán z databáze, viz. database_manipulation.php -->
                    <li><a href="portfolio.php"><?php echo $_smarty_tpl->tpl_vars['userContent']->value[1]['content'];?>
</a></li>
                    <li><a href="photogalery.php"><?php echo $_smarty_tpl->tpl_vars['userContent']->value[2]['content'];?>
</a>
                        <ul>
                            <!-- Položky druhé úrovně hlavní nabídky načítány z databáze, viz. database_manipulation.php -->
                            <?php
$_from = $_smarty_tpl->tpl_vars['mainMenuGaleryFiles']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['galeryFile'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['galeryFile']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['galeryFile']->value) {
$_smarty_tpl->tpl_vars['galeryFile']->_loop = true;
$foreach_galeryFile_Sav = $_smarty_tpl->tpl_vars['galeryFile'];
?>
                            <li><a href="gallery.php?galleryId=<?php echo $_smarty_tpl->tpl_vars['galeryFile']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['galeryFile']->value['galery_title'];?>
</a></li>
                            <?php
$_smarty_tpl->tpl_vars['galeryFile'] = $foreach_galeryFile_Sav;
}
?>
                        </ul>
                    </li>
                    <li><a href="blog.php"><?php echo $_smarty_tpl->tpl_vars['userContent']->value[3]['content'];?>
</a>
                        <ul>
                            <?php
$_from = $_smarty_tpl->tpl_vars['mainMenuCategoryFiles']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['categoryFile'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['categoryFile']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['categoryFile']->value) {
$_smarty_tpl->tpl_vars['categoryFile']->_loop = true;
$foreach_categoryFile_Sav = $_smarty_tpl->tpl_vars['categoryFile'];
?>
                            <li><a href="blog.php?categoryId=<?php echo $_smarty_tpl->tpl_vars['categoryFile']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['categoryFile']->value['category_title'];?>
</a></li>
                            <?php
$_smarty_tpl->tpl_vars['categoryFile'] = $foreach_categoryFile_Sav;
}
?>
                        </ul>
                    </li>
                    <li><a href="contacts.php"><?php echo $_smarty_tpl->tpl_vars['userContent']->value[4]['content'];?>
</a></li>
                </ul>
            </nav>

            <!-- DEFINICE DVOUÚROVŇOVÉ NABÍDKY WEBU PRO MALÁ ROZLIŠENÍ -->
            <nav class="collapsed_navigation">
                <a href="#" id="pull"><span>MENU</span></a>
                <ul class="main_menu_collapsed">
                    <!-- DEFINICE WRAPPERU POLOŽEK NABÍDKY PRO ZNEVIDITELNĚNÍ SCROLLBARU -->
                    <li id="scroller">
                        <ul>
                            <li><a href="portfolio.php"><?php echo $_smarty_tpl->tpl_vars['userContent']->value[1]['content'];?>
</a></li>
                            <li><a href="photogalery.php"><?php echo $_smarty_tpl->tpl_vars['userContent']->value[2]['content'];?>
</a>
                                <ul>
                                    <?php
$_from = $_smarty_tpl->tpl_vars['mainMenuGaleryFiles']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['galeryFile'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['galeryFile']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['galeryFile']->value) {
$_smarty_tpl->tpl_vars['galeryFile']->_loop = true;
$foreach_galeryFile_Sav = $_smarty_tpl->tpl_vars['galeryFile'];
?>
                                    <li><a href="gallery.php?galleryId=<?php echo $_smarty_tpl->tpl_vars['galeryFile']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['galeryFile']->value['galery_title'];?>
</a></li>
                                    <?php
$_smarty_tpl->tpl_vars['galeryFile'] = $foreach_galeryFile_Sav;
}
?>
                                </ul>
                            </li>
                            <li><a href="blog.php"><?php echo $_smarty_tpl->tpl_vars['userContent']->value[3]['content'];?>
</a>
                                <ul>
                                    <?php
$_from = $_smarty_tpl->tpl_vars['mainMenuCategoryFiles']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['categoryFile'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['categoryFile']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['categoryFile']->value) {
$_smarty_tpl->tpl_vars['categoryFile']->_loop = true;
$foreach_categoryFile_Sav = $_smarty_tpl->tpl_vars['categoryFile'];
?>
                                    <li><a href="blog.php?categoryId=<?php echo $_smarty_tpl->tpl_vars['categoryFile']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['categoryFile']->value['category_title'];?>
</a></li>
                                    <?php
$_smarty_tpl->tpl_vars['categoryFile'] = $foreach_categoryFile_Sav;
}
?>
                                </ul>
                            </li>
                            <li><a href="contacts.php"><?php echo $_smarty_tpl->tpl_vars['userContent']->value[4]['content'];?>
</a></li>
                        </ul>
                    </li>
                </ul>
            </nav>
            
        </header>
    
    
        <!-- DEFINICE WRAPPERU OBSAHU BLOGU -->
        <div class="contacts-content-wrapper">
            
            <h2 class="contacts-title" <?php if (isset($_SESSION['logged']) && $_SESSION['logged'] == true) {?> style="padding-top: 110px;" <?php }?>>KONTAKTY</h2>
            <div class="table-wrapper">
                <table class="table table-hover">
                    <thead>
                        <tr><td>Základní údaje</td><td></td></tr>
                    </thead>
                    <?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['i']->step = 1;$_smarty_tpl->tpl_vars['i']->total = (int) ceil(($_smarty_tpl->tpl_vars['i']->step > 0 ? 13+1 - (10) : 10-(13)+1)/abs($_smarty_tpl->tpl_vars['i']->step));
if ($_smarty_tpl->tpl_vars['i']->total > 0) {
for ($_smarty_tpl->tpl_vars['i']->value = 10, $_smarty_tpl->tpl_vars['i']->iteration = 1;$_smarty_tpl->tpl_vars['i']->iteration <= $_smarty_tpl->tpl_vars['i']->total;$_smarty_tpl->tpl_vars['i']->value += $_smarty_tpl->tpl_vars['i']->step, $_smarty_tpl->tpl_vars['i']->iteration++) {
$_smarty_tpl->tpl_vars['i']->first = $_smarty_tpl->tpl_vars['i']->iteration == 1;$_smarty_tpl->tpl_vars['i']->last = $_smarty_tpl->tpl_vars['i']->iteration == $_smarty_tpl->tpl_vars['i']->total;?>
                    <tr><td><?php echo $_smarty_tpl->tpl_vars['userContent']->value[$_smarty_tpl->tpl_vars['i']->value]['title'];?>
</td><td><?php echo $_smarty_tpl->tpl_vars['userContent']->value[$_smarty_tpl->tpl_vars['i']->value]['content'];?>
</td></tr>
                    <?php }} ?>
                </table>
                <table class="table table-hover">
                    <thead>
                        <tr><td>Sociální sítě</td><td></td></tr>
                    </thead>
                    <?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['i']->step = 1;$_smarty_tpl->tpl_vars['i']->total = (int) ceil(($_smarty_tpl->tpl_vars['i']->step > 0 ? 18+1 - (14) : 14-(18)+1)/abs($_smarty_tpl->tpl_vars['i']->step));
if ($_smarty_tpl->tpl_vars['i']->total > 0) {
for ($_smarty_tpl->tpl_vars['i']->value = 14, $_smarty_tpl->tpl_vars['i']->iteration = 1;$_smarty_tpl->tpl_vars['i']->iteration <= $_smarty_tpl->tpl_vars['i']->total;$_smarty_tpl->tpl_vars['i']->value += $_smarty_tpl->tpl_vars['i']->step, $_smarty_tpl->tpl_vars['i']->iteration++) {
$_smarty_tpl->tpl_vars['i']->first = $_smarty_tpl->tpl_vars['i']->iteration == 1;$_smarty_tpl->tpl_vars['i']->last = $_smarty_tpl->tpl_vars['i']->iteration == $_smarty_tpl->tpl_vars['i']->total;?>
                    <tr><td><?php echo $_smarty_tpl->tpl_vars['userContent']->value[$_smarty_tpl->tpl_vars['i']->value]['icon'];?>
</td><td><?php if ($_smarty_tpl->tpl_vars['userContent']->value[$_smarty_tpl->tpl_vars['i']->value]['content'] != "- / -" && !empty($_smarty_tpl->tpl_vars['userContent']->value[$_smarty_tpl->tpl_vars['i']->value]['link'])) {?> <a href="<?php echo $_smarty_tpl->tpl_vars['userContent']->value[$_smarty_tpl->tpl_vars['i']->value]['link'];?>
"><?php echo $_smarty_tpl->tpl_vars['userContent']->value[$_smarty_tpl->tpl_vars['i']->value]['content'];?>
</a> <?php } else { ?> <?php echo $_smarty_tpl->tpl_vars['userContent']->value[$_smarty_tpl->tpl_vars['i']->value]['content'];?>
 <?php }?></td></tr>
                    <?php }} ?>
                </table>
            </div>
    
        </div>
    
    
        <!-- DEFINICE WRAPPERU PATIČKY WEBU -->
        <footer>
            <div class="block-backup-link"><a href="index.php">ZPĚT NA ÚVODNÍ STRÁNKU</a></div>
            <!-- DEFINICE WRAPPERU OBSAHOVÉ ČÁSTI PATIČKY WEBU -->
            <div class='footer_content_wrapper'>
                <!-- Patička webu se skládá ze 3 obsahových bloků -->
                <?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['i']->step = 1;$_smarty_tpl->tpl_vars['i']->total = (int) ceil(($_smarty_tpl->tpl_vars['i']->step > 0 ? 9+1 - (7) : 7-(9)+1)/abs($_smarty_tpl->tpl_vars['i']->step));
if ($_smarty_tpl->tpl_vars['i']->total > 0) {
for ($_smarty_tpl->tpl_vars['i']->value = 7, $_smarty_tpl->tpl_vars['i']->iteration = 1;$_smarty_tpl->tpl_vars['i']->iteration <= $_smarty_tpl->tpl_vars['i']->total;$_smarty_tpl->tpl_vars['i']->value += $_smarty_tpl->tpl_vars['i']->step, $_smarty_tpl->tpl_vars['i']->iteration++) {
$_smarty_tpl->tpl_vars['i']->first = $_smarty_tpl->tpl_vars['i']->iteration == 1;$_smarty_tpl->tpl_vars['i']->last = $_smarty_tpl->tpl_vars['i']->iteration == $_smarty_tpl->tpl_vars['i']->total;?>
                <div class='footer_content'>
                    <!-- Obsah patičky načítán z databáze, viz. database_load.php -->
                    <h2><?php echo $_smarty_tpl->tpl_vars['userContent']->value[$_smarty_tpl->tpl_vars['i']->value]['title'];?>
</h2>
                    <p><?php echo $_smarty_tpl->tpl_vars['userContent']->value[$_smarty_tpl->tpl_vars['i']->value]['content'];?>
</p>
                </div>
                <?php }} ?>
            </div>
            <!-- Výpis HTML struktury přihlašovacího formuláře proběhne pouze v případě zadání parametru v URL adrese a v případě, že uživatel není momentálně úspěšně přihlášený -->
            <?php if (isset($_GET['user']) && empty($_GET['user'])) {?>
                <?php if (!isset($_SESSION['logged']) || $_SESSION['logged'] != true) {?>
            <!-- DEFINICE WRAPPERU PŘIHLAŠOVACÍHO FORMULÁŘE -->
            <div class='form_wrapper'>
                <form method='POST'>
                    <div class='form_group'>
                        <label for='login'>PŘIHLAŠOVACÍ JMÉNO</label>
                        <input type='text' id='login' name='login' placeholder='ZADEJTE PŘIHLAŠOVACÍ JMÉNO'/>
                    </div>
                    <div class='form_group'>
                        <label for='password'>HESLO</label>
                        <input type='password' id='password' name='password' placeholder='ZADEJTE HESLO'/>
                    </div>
                    <input id='login_btn' class='submit_btn' type='button' name='submit' value='PŘIHLÁSIT SE'/>
                    <p id='login_msg'></p>
                </form>
            </div>    
                <?php }?>
            <?php }?>
            <h3>CREATED BY &copy; PETR PONDĚLÍK 2016</h3>
            
        </footer>
    
        
    <!-- PŘIPOJENÍ JQUERY -->
    <?php echo '<script'; ?>
 src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"><?php echo '</script'; ?>
>
    
    <!-- PŘIPOJENÍ BOOTSTRAP JS PLUGINŮ -->
    <?php echo '<script'; ?>
 src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"><?php echo '</script'; ?>
>
    
    <!-- PŘIPOJENÍ SCRIPTU OVLÁDAJÍCÍHO PRŮHLEDNOST HLAVNÍ NABÍDKY A NABÍDKU PRO MALÁ ROZLIŠENÍ-->
    <?php echo '<script'; ?>
 src="js_solutions/header_functions.js"><?php echo '</script'; ?>
>
    
    <!-- PŘIPOJENÍ SCRIPTU PRO MD5 HASHOVÁNÍ -->
    <?php echo '<script'; ?>
 src="js_solutions/md5_hashing.js"><?php echo '</script'; ?>
>
    
    <!-- PŘIPOJENÍ SCRIPTU OVLÁDAJÍCÍHO PŘIHLAŠOVACÍ FORMULÁŘ -->
    <?php echo '<script'; ?>
 src="js_solutions/login_form_control.js"><?php echo '</script'; ?>
>
    
    <!-- SCRIPT OVLÁDAJÍCÍ LOADING SCREEN PRO DOM STRUKTURU DOKUMENTU -->
    <?php echo '<script'; ?>
 type="text/javascript">
        $(window).load(function(){
            $(".cssloader-wrapper").remove();
            $("html").css("overflow","visible");
        });
    <?php echo '</script'; ?>
>
    
</body>
</html><?php }
}
?>