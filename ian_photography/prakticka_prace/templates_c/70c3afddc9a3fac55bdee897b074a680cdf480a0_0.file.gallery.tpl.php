<?php /* Smarty version 3.1.27, created on 2016-03-10 22:08:29
         compiled from "G:\Programy\EasyPHP-DevServer-14.1VC11\data\localweb\ian_photography_soc\templates\gallery.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:806556e1e24de87453_03922251%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '70c3afddc9a3fac55bdee897b074a680cdf480a0' => 
    array (
      0 => 'G:\\Programy\\EasyPHP-DevServer-14.1VC11\\data\\localweb\\ian_photography_soc\\templates\\gallery.tpl',
      1 => 1455131097,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '806556e1e24de87453_03922251',
  'variables' => 
  array (
    'photosInGallery' => 0,
    'galeryPhoto' => 0,
    'currentGallery' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_56e1e24e1417a7_02060854',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_56e1e24e1417a7_02060854')) {
function content_56e1e24e1417a7_02060854 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '806556e1e24de87453_03922251';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title>IAN Photography</title>

    <!-- PŘIPOJENÍ CSS STYLŮ -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css_styles/general_styles.css"/>
    <link rel="stylesheet" type="text/css" href="css_styles/loading_screens.css"/>
    <link rel="stylesheet" type="text/css" href="css_styles/gallery_styles.css"/>
    <link  href="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css" rel="stylesheet">
</head>

    
    
<body>
    
    <a href="photogalery.php" class="btn btn-lg btn-info backup-button">ZPĚT</a>
    
    
    <!-- DEFINICE LOADING SCREENU PRO LOADING DOM STRUKTURY DOKUMENTU -->
    <div class="cssloader-wrapper">

        <div class="cssload-loader">
            <div class="cssload-flipper">
                <div class="cssload-front"></div>
                <div class="cssload-back"></div>
            </div>
            <h4>Loading...</h4>
        </div>

    </div>


    <!-- DEFINICE LOADING SCREENU PRO ZPRACOVÁNÍ PHP APLIKACE V RÁMCI AJAXU -->
    <div id="floatingCirclesG-wrapper">
        
        <div id="floatingCirclesG">
            <div class="f_circleG" id="frotateG_01"></div>
            <div class="f_circleG" id="frotateG_02"></div>
            <div class="f_circleG" id="frotateG_03"></div>
            <div class="f_circleG" id="frotateG_04"></div>
            <div class="f_circleG" id="frotateG_05"></div>
            <div class="f_circleG" id="frotateG_06"></div>
            <div class="f_circleG" id="frotateG_07"></div>
            <div class="f_circleG" id="frotateG_08"></div>
        </div>
        
    </div>   

    
    <!-- DEFINICE OBSAHU GALERIE -->
    <div class="fotorama" data-width="100%" data-height="100%" data-ratio="4/3" data-nav="thumbs" data-thumbwidth="96px" data-thumbheight="64px" data-allowfullscreen="native">
        
        <?php if ($_smarty_tpl->tpl_vars['photosInGallery']->value != false) {?>
        
        <?php
$_from = $_smarty_tpl->tpl_vars['photosInGallery']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['galeryPhoto'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['galeryPhoto']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['galeryPhoto']->value) {
$_smarty_tpl->tpl_vars['galeryPhoto']->_loop = true;
$foreach_galeryPhoto_Sav = $_smarty_tpl->tpl_vars['galeryPhoto'];
?>
        <div data-img="<?php echo $_smarty_tpl->tpl_vars['galeryPhoto']->value['photo_path'];?>
">
            <h2 class="photo-title"><?php echo $_smarty_tpl->tpl_vars['galeryPhoto']->value['photo_title'];?>
</h2>
            <div class="photo-description"><h3 class="galery"><?php echo $_smarty_tpl->tpl_vars['currentGallery']->value[0]['galery_title'];?>
 | By: <?php echo $_smarty_tpl->tpl_vars['currentGallery']->value[0]['login'];?>
</h3><h3 class="author">Photo by: Jan Růžička</h3></div>
        </div>
        <?php
$_smarty_tpl->tpl_vars['galeryPhoto'] = $foreach_galeryPhoto_Sav;
}
?>
        <?php } else { ?>
        <div>
            <h2 class="photo-title">Nenalezena žádná fotografie</h2>
            <div class="photo-description"><h3 class="galery"><?php echo $_smarty_tpl->tpl_vars['currentGallery']->value[0]['galery_title'];?>
 | By: <?php echo $_smarty_tpl->tpl_vars['currentGallery']->value[0]['login'];?>
</h3></div>
        </div>
        
        <?php }?>
        
    </div>
    
        
    <!-- PŘIPOJENÍ JQUERY -->
    <?php echo '<script'; ?>
 src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"><?php echo '</script'; ?>
>
    
    <!-- PŘIPOJENÍ SCRIPTU OVLÁDAJÍCÍHO PRŮHLEDNOST HLAVNÍ NABÍDKY A NABÍDKU PRO MALÁ ROZLIŠENÍ-->
    <?php echo '<script'; ?>
 src="js_solutions/header_functions.js"><?php echo '</script'; ?>
>
    
    <!-- PŘIPOJENÍ SCRIPTU PRO MD5 HASHOVÁNÍ -->
    <?php echo '<script'; ?>
 src="js_solutions/md5_hashing.js"><?php echo '</script'; ?>
>
    
    <!-- PŘIPOJENÍ SCRIPTU OVLÁDAJÍCÍHO PŘIHLAŠOVACÍ FORMULÁŘ -->
    <?php echo '<script'; ?>
 src="js_solutions/login_form_control.js"><?php echo '</script'; ?>
>
    
    <?php echo '<script'; ?>
 src="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js"><?php echo '</script'; ?>
>

        
    <?php echo '</script'; ?>
>
        
    <!-- SCRIPT OVLÁDAJÍCÍ LOADING SCREEN PRO DOM STRUKTURU DOKUMENTU -->
    <?php echo '<script'; ?>
 type="text/javascript">
        $(window).load(function(){
            $(".cssloader-wrapper").remove();
            $("html").css("overflow","visible");
        });
        $("html").mouseover(function(){
            $(".backup-button").css("opacity",0.9);
            $(".backup-button").css("right","4%");
        });
        $("html").mouseout(function(){
            $(".backup-button").css("opacity",0);
            $(".backup-button").css("right","0px");
        });
    <?php echo '</script'; ?>
>    

       
    
</body>
</html><?php }
}
?>