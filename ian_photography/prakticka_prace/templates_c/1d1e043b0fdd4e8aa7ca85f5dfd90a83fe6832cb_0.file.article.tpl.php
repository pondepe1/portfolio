<?php /* Smarty version 3.1.27, created on 2016-02-07 19:58:22
         compiled from "G:\Programy\EasyPHP-DevServer-14.1VC11\data\localweb\ian_photography\templates\article.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:2840556b793ce483b11_86791103%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1d1e043b0fdd4e8aa7ca85f5dfd90a83fe6832cb' => 
    array (
      0 => 'G:\\Programy\\EasyPHP-DevServer-14.1VC11\\data\\localweb\\ian_photography\\templates\\article.tpl',
      1 => 1454871383,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2840556b793ce483b11_86791103',
  'variables' => 
  array (
    'articleId' => 0,
    'currentPage' => 0,
    'userContent' => 0,
    'mainMenuGaleryFiles' => 0,
    'galeryFile' => 0,
    'mainMenuCategoryFiles' => 0,
    'categoryFile' => 0,
    'blogArticle' => 0,
    'articleInnerPhotos' => 0,
    'innerPhoto' => 0,
    'i' => 0,
    'photogaleryFiles' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_56b793ced611c2_22411393',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_56b793ced611c2_22411393')) {
function content_56b793ced611c2_22411393 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '2840556b793ce483b11_86791103';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title>IAN Photography</title>

    <!-- PŘIPOJENÍ CSS STYLŮ -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css_styles/general_styles.css"/>
    <link rel="stylesheet" type="text/css" href="css_styles/loading_screens.css"/>
    <link rel="stylesheet" type="text/css" href="css_styles/header_styles.css"/>
    <link rel="stylesheet" type="text/css" href="css_styles/collapsed_navigation_styles.css"/>
    <link rel="stylesheet" type="text/css" href="css_styles/footer_styles.css"/>
    <link rel="stylesheet" type="text/css" href="css_styles/blog_styles.css"/>
    <link rel="stylesheet" type="text/css" href="css_styles/responsive_styles.css"/>
    
    <!-- PŘIPOJENÍ STYLŮ LIGHTVIEW -->
    <link rel="stylesheet" type="text/css" href="lightview-3.5.0/css/lightview/lightview.css"/>
    
</head>

    
    
<body>
    
    
        <!-- DEFINICE LOADING SCREENU PRO LOADING DOM STRUKTURY DOKUMENTU -->
        <div class="cssloader-wrapper">
            
            <div class="cssload-loader">
                <div class="cssload-flipper">
                    <div class="cssload-front"></div>
                    <div class="cssload-back"></div>
                </div>
                <h4>Loading...</h4>
            </div>
            
        </div>
        
        
        <!-- DEFINICE LOADING SCREENU PRO ZPRACOVÁNÍ PHP APLIKACE V RÁMCI AJAXU -->
        <div id="floatingCirclesG-wrapper">
            <div id="floatingCirclesG">
                <div class="f_circleG" id="frotateG_01"></div>
                <div class="f_circleG" id="frotateG_02"></div>
                <div class="f_circleG" id="frotateG_03"></div>
                <div class="f_circleG" id="frotateG_04"></div>
                <div class="f_circleG" id="frotateG_05"></div>
                <div class="f_circleG" id="frotateG_06"></div>
                <div class="f_circleG" id="frotateG_07"></div>
                <div class="f_circleG" id="frotateG_08"></div>
            </div>
        </div>   
        
        
        <!-- DEFINICE WRAPPERU HLAVIČKY WEBU -->
        <header>
            
            <?php if (isset($_SESSION['logged']) && $_SESSION['logged'] == true) {?>
            <!-- DEFINICE UŽIVATESLKÉHO ADMINISTRAČNÍHO PANELU (pouze v případě úspěšného přihlášení uživatele -> $_SESSION['logged'] == true, viz. login.php) -->
            <div class="user_admin_bar">
                <!-- Uvítací titulek uživateslkého panelu -> obsahuje uživatelské jméno, viz. login.php -->
                <p>Uživatel <?php echo $_SESSION['userData']['login'];?>
</p>
                <div class="full_resolution_links">  
                    <a href="php_solutions/logout.php?backupFile=../article.php&articleId=<?php echo $_smarty_tpl->tpl_vars['articleId']->value;?>
&currentPage=<?php echo $_smarty_tpl->tpl_vars['currentPage']->value;?>
">ODHLÁSIT SE</a>
                    <a href="admin_section.php?backupFile=article.php&articleId=<?php echo $_smarty_tpl->tpl_vars['articleId']->value;?>
&currentPage=<?php echo $_smarty_tpl->tpl_vars['currentPage']->value;?>
">ADMINISTRACE</a>  
                </div>
                <div class="dropdown">
                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        MOŽNOSTI
                        <span class="glyphicon glyphicon-triangle-bottom"></span>
                        <span class="glyphicon glyphicon-triangle-top"></span>            
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                        <li><a href="admin_section.php?backupFile=article.php">ADMINISTRACE</a></li>
                        <li><a href="php_solutions/logout.php?backupFile=../article.php">ODHLÁSIT SE</a></li>
                    </ul>
                </div>
           </div>
           <?php }?>
           
            <!-- DEFINICE HLAVIČKY WEBOVÉ STRÁNKY (obsah je načítán z databáze, viz. database_manipulation.php) -->
            <h1><a href="index.php"><?php echo $_smarty_tpl->tpl_vars['userContent']->value[0]['content'];?>
</a></h1>

            <!-- DEFINICE DVOUÚROVŇOVÉ NABÍDKY WEBU PRO VELKÁ ROZLIŠENÍ -->
            <nav class="primary_navigation">
                <ul class="main_menu_primary">
                    <!-- Obsah položek první úrovně hlavní nabídky načítán z databáze, viz. database_manipulation.php -->
                    <li><a href="portfolio.php"><?php echo $_smarty_tpl->tpl_vars['userContent']->value[1]['content'];?>
</a></li>
                    <li><a href="photogalery.php"><?php echo $_smarty_tpl->tpl_vars['userContent']->value[2]['content'];?>
</a>
                        <ul>
                            <!-- Položky druhé úrovně hlavní nabídky načítány z databáze, viz. database_manipulation.php -->
                            <?php
$_from = $_smarty_tpl->tpl_vars['mainMenuGaleryFiles']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['galeryFile'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['galeryFile']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['galeryFile']->value) {
$_smarty_tpl->tpl_vars['galeryFile']->_loop = true;
$foreach_galeryFile_Sav = $_smarty_tpl->tpl_vars['galeryFile'];
?>
                            <li><a href="gallery.php?galleryId=<?php echo $_smarty_tpl->tpl_vars['galeryFile']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['galeryFile']->value['galery_title'];?>
</a></li>
                            <?php
$_smarty_tpl->tpl_vars['galeryFile'] = $foreach_galeryFile_Sav;
}
?>
                        </ul>
                    </li>
                    <li><a href="blog.php"><?php echo $_smarty_tpl->tpl_vars['userContent']->value[3]['content'];?>
</a>
                        <ul>
                            <?php
$_from = $_smarty_tpl->tpl_vars['mainMenuCategoryFiles']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['categoryFile'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['categoryFile']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['categoryFile']->value) {
$_smarty_tpl->tpl_vars['categoryFile']->_loop = true;
$foreach_categoryFile_Sav = $_smarty_tpl->tpl_vars['categoryFile'];
?>
                            <li><a href="blog.php?categoryId=<?php echo $_smarty_tpl->tpl_vars['categoryFile']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['categoryFile']->value['category_title'];?>
</a></li>
                            <?php
$_smarty_tpl->tpl_vars['categoryFile'] = $foreach_categoryFile_Sav;
}
?>
                        </ul>
                    </li>
                    <li><a href="contacts.php"><?php echo $_smarty_tpl->tpl_vars['userContent']->value[4]['content'];?>
</a></li>
                </ul>
            </nav>

            <!-- DEFINICE DVOUÚROVŇOVÉ NABÍDKY WEBU PRO MALÁ ROZLIŠENÍ -->
            <nav class="collapsed_navigation">
                <a href="#" id="pull"><span>MENU</span></a>
                <ul class="main_menu_collapsed">
                    <!-- DEFINICE WRAPPERU POLOŽEK NABÍDKY PRO ZNEVIDITELNĚNÍ SCROLLBARU -->
                    <li id="scroller">
                        <ul>
                            <li><a href="portfolio.php"><?php echo $_smarty_tpl->tpl_vars['userContent']->value[1]['content'];?>
</a></li>
                            <li><a href="photogalery.php"><?php echo $_smarty_tpl->tpl_vars['userContent']->value[2]['content'];?>
</a>
                                <ul>
                                    <?php
$_from = $_smarty_tpl->tpl_vars['mainMenuGaleryFiles']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['galeryFile'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['galeryFile']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['galeryFile']->value) {
$_smarty_tpl->tpl_vars['galeryFile']->_loop = true;
$foreach_galeryFile_Sav = $_smarty_tpl->tpl_vars['galeryFile'];
?>
                                    <li><a href="gallery.php?galleryId=<?php echo $_smarty_tpl->tpl_vars['galeryFile']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['galeryFile']->value['galery_title'];?>
</a></li>
                                    <?php
$_smarty_tpl->tpl_vars['galeryFile'] = $foreach_galeryFile_Sav;
}
?>
                                </ul>
                            </li>
                            <li><a href="blog.php"><?php echo $_smarty_tpl->tpl_vars['userContent']->value[3]['content'];?>
</a>
                                <ul>
                                    <?php
$_from = $_smarty_tpl->tpl_vars['mainMenuCategoryFiles']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['categoryFile'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['categoryFile']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['categoryFile']->value) {
$_smarty_tpl->tpl_vars['categoryFile']->_loop = true;
$foreach_categoryFile_Sav = $_smarty_tpl->tpl_vars['categoryFile'];
?>
                                    <li><a href="blog.php?categoryId=<?php echo $_smarty_tpl->tpl_vars['categoryFile']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['categoryFile']->value['category_title'];?>
</a></li>
                                    <?php
$_smarty_tpl->tpl_vars['categoryFile'] = $foreach_categoryFile_Sav;
}
?>
                                </ul>
                            </li>
                            <li><a href="contacts.php"><?php echo $_smarty_tpl->tpl_vars['userContent']->value[4]['content'];?>
</a></li>
                        </ul>
                    </li>
                </ul>
            </nav>
            
        </header>
    
        <!-- DEFINICE WRAPPERU OBSAHU BLOGU -->
        <div class="blog-content-wrapper">
            
            <?php if ($_smarty_tpl->tpl_vars['blogArticle']->value[0] != false) {?>
                
                <h2 class="article-title-top" <?php if (isset($_SESSION['logged']) && $_SESSION['logged'] == true) {?> style="padding-top: 110px;" <?php }?>><?php echo $_smarty_tpl->tpl_vars['blogArticle']->value[0]['article_title'];?>
</h2>
            
                <div class='article-wrapper single-article-wrapper'>
                    
                    <!-- Podmínka ošetřující přítomnost titulní fotografie článku -->
                    <?php if (isset($_smarty_tpl->tpl_vars['blogArticle']->value[0]['title_photo']) && !empty($_smarty_tpl->tpl_vars['blogArticle']->value[0]['title_photo'])) {?>
                    <div class='article-title-photo-underlay'>
                        <div class='article-title-photo single-article-title-photo' style="background-image: url(<?php echo $_smarty_tpl->tpl_vars['blogArticle']->value[0]['title_photo'];?>
)"></div>
                    </div>
                    <?php }?>
                    <!-- Obsah jednotlivých článků načítán z databáze, viz. article.php -->
                    <h3><span class="article-title single-article-title"><?php echo $_smarty_tpl->tpl_vars['blogArticle']->value[0]['article_title'];?>
</span></h3>
                    <p class='article-info'><?php echo $_smarty_tpl->tpl_vars['blogArticle']->value[0]['time_stamp'];?>
 | By: <?php echo $_smarty_tpl->tpl_vars['blogArticle']->value[0]['login'];?>
 | Kategorie: <?php echo $_smarty_tpl->tpl_vars['blogArticle']->value[0]['category_title'];?>
</p>
                    <p><?php echo $_smarty_tpl->tpl_vars['blogArticle']->value[0]['content'];?>
</p>
                    
                    <?php if (isset($_smarty_tpl->tpl_vars['articleInnerPhotos']->value) && !empty($_smarty_tpl->tpl_vars['articleInnerPhotos']->value) && $_smarty_tpl->tpl_vars['articleInnerPhotos']->value != false) {?>
                    <div class="article-inner-gallery-wrapper">
                    <?php
$_from = $_smarty_tpl->tpl_vars['articleInnerPhotos']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['innerPhoto'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['innerPhoto']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['innerPhoto']->value) {
$_smarty_tpl->tpl_vars['innerPhoto']->_loop = true;
$foreach_innerPhoto_Sav = $_smarty_tpl->tpl_vars['innerPhoto'];
?>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['innerPhoto']->value['in_article_photo_path'];?>
"
                           class="lightview"
                           data-lightview-group="articleInnerGalery"
                           data-lightview-title="Photo by: Jan Růžička">
                            <img class="inner-photo-thumbnail" src="<?php echo $_smarty_tpl->tpl_vars['innerPhoto']->value['in_article_photo_path'];?>
" alt=""/>
                        </a>
                    <?php
$_smarty_tpl->tpl_vars['innerPhoto'] = $foreach_innerPhoto_Sav;
}
?>
                    </div>
                    <?php }?>
                    
                </div>
            
            <?php } else { ?>
                <h2 class="empty-blog-msg">Nenalezen žádný článek</h2>
                <!-- DEFINICE INFORMAČNÍHO MODALU PŘI NEEXISTENCI ŽÁDNÉHO ČLÁNKU -->
                <div class="modal fade" id="empty-blog-info-modal" tabindex="-1" role="dialog" aria-labelledby="empty-blog-info-modal-label">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h3 class="modal-title" id="empty-blog-modal-label">ZPRÁVA O STAVU BLOGU</h3>
                            </div>
                            <div class="modal-footer">
                                <h4>Nenalezen žádný článek</h4>
                                <button type="button" class="btn btn-default" data-dismiss="modal">ZAVŘÍT</button>
                            </div>
                        </div>
                    </div>
                </div>
            <?php }?>
        </div>

                
        <!-- DEFINICE WRAPPERU PATIČKY WEBU -->
        <footer>
            <div class="block-backup-link"><a href="blog.php?pageStart=<?php echo $_smarty_tpl->tpl_vars['currentPage']->value;?>
">ZPĚT DO BLOGU</a></div>
            <!-- DEFINICE WRAPPERU OBSAHOVÉ ČÁSTI PATIČKY WEBU -->
            <div class='footer_content_wrapper'>
                <!-- Patička webu se skládá ze 3 obsahových bloků -->
                <?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['i']->step = 1;$_smarty_tpl->tpl_vars['i']->total = (int) ceil(($_smarty_tpl->tpl_vars['i']->step > 0 ? 9+1 - (7) : 7-(9)+1)/abs($_smarty_tpl->tpl_vars['i']->step));
if ($_smarty_tpl->tpl_vars['i']->total > 0) {
for ($_smarty_tpl->tpl_vars['i']->value = 7, $_smarty_tpl->tpl_vars['i']->iteration = 1;$_smarty_tpl->tpl_vars['i']->iteration <= $_smarty_tpl->tpl_vars['i']->total;$_smarty_tpl->tpl_vars['i']->value += $_smarty_tpl->tpl_vars['i']->step, $_smarty_tpl->tpl_vars['i']->iteration++) {
$_smarty_tpl->tpl_vars['i']->first = $_smarty_tpl->tpl_vars['i']->iteration == 1;$_smarty_tpl->tpl_vars['i']->last = $_smarty_tpl->tpl_vars['i']->iteration == $_smarty_tpl->tpl_vars['i']->total;?>
                <div class='footer_content'>
                    <!-- Obsah patičky načítán z databáze, viz. database_load.php -->
                    <h2><?php echo $_smarty_tpl->tpl_vars['userContent']->value[$_smarty_tpl->tpl_vars['i']->value]['title'];?>
</h2>
                    <p><?php echo $_smarty_tpl->tpl_vars['userContent']->value[$_smarty_tpl->tpl_vars['i']->value]['content'];?>
</p>
                </div>
                <?php }} ?>
            </div>
            <!-- Výpis HTML struktury přihlašovacího formuláře proběhne pouze v případě zadání parametru v URL adrese a v případě, že uživatel není momentálně úspěšně přihlášený -->
            <?php if (isset($_GET['user']) && empty($_GET['user'])) {?>
                <?php if (!isset($_SESSION['logged']) || $_SESSION['logged'] != true) {?>
            <!-- DEFINICE WRAPPERU PŘIHLAŠOVACÍHO FORMULÁŘE -->
            <div class='form_wrapper'>
                <form method='POST'>
                    <div class='form_group'>
                        <label for='login'>PŘIHLAŠOVACÍ JMÉNO</label>
                        <input type='text' id='login' name='login' placeholder='ZADEJTE PŘIHLAŠOVACÍ JMÉNO'/>
                    </div>
                    <div class='form_group'>
                        <label for='password'>HESLO</label>
                        <input type='password' id='password' name='password' placeholder='ZADEJTE HESLO'/>
                    </div>
                    <input id='login_btn' class='submit_btn' type='button' name='submit' value='PŘIHLÁSIT SE'/>
                    <p id='login_msg'></p>
                </form>
            </div>    
                <?php }?>
            <?php }?>
            <h3>CREATED BY &copy; PETR PONDĚLÍK 2016</h3>
            
        </footer>
    
        
    <!-- PŘIPOJENÍ JQUERY -->
    <?php echo '<script'; ?>
 src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"><?php echo '</script'; ?>
>
    
    <!-- PŘIPOJENÍ JS SOUČÁSTÍ LIGHTVIEW -->
    <?php echo '<script'; ?>
 type="text/javascript" src="lightview-3.5.0/js/excanvas/excanvas.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 type="text/javascript" src="lightview-3.5.0/js/spinners/spinners.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 type="text/javascript" src="lightview-3.5.0/js/lightview/lightview.js"><?php echo '</script'; ?>
>
    
    <!-- PŘIPOJENÍ BOOTSTRAP JS PLUGINŮ -->
    <?php echo '<script'; ?>
 src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"><?php echo '</script'; ?>
>
    
    <!-- PŘIPOJENÍ SCRIPTU OVLÁDAJÍCÍHO PRŮHLEDNOST HLAVNÍ NABÍDKY A NABÍDKU PRO MALÁ ROZLIŠENÍ-->
    <?php echo '<script'; ?>
 src="js_solutions/header_functions.js"><?php echo '</script'; ?>
>
    
    <!-- PŘIPOJENÍ SCRIPTU PRO MD5 HASHOVÁNÍ -->
    <?php echo '<script'; ?>
 src="js_solutions/md5_hashing.js"><?php echo '</script'; ?>
>
    
    <!-- PŘIPOJENÍ SCRIPTU OVLÁDAJÍCÍHO PŘIHLAŠOVACÍ FORMULÁŘ -->
    <?php echo '<script'; ?>
 src="js_solutions/login_form_control.js"><?php echo '</script'; ?>
>
        
    <!-- SCRIPT OVLÁDAJÍCÍ LOADING SCREEN PRO DOM STRUKTURU DOKUMENTU -->
    <?php echo '<script'; ?>
 type="text/javascript">
        $(window).load(function(){
            $(".cssloader-wrapper").remove();
            $("html").css("overflow","visible");
        });
    <?php echo '</script'; ?>
>
    
    <?php if (!isset($_smarty_tpl->tpl_vars['photogaleryFiles']->value) || empty($_smarty_tpl->tpl_vars['photogaleryFiles']->value) || $_smarty_tpl->tpl_vars['photogaleryFiles']->value != false) {?>
    <?php echo '<script'; ?>
 type="text/javascript">
        $(document).ready(function(){
            $("#empty-blog-info-modal").modal("show");
        });
    <?php echo '</script'; ?>
>
    <?php }?>
    
</body>
</html><?php }
}
?>