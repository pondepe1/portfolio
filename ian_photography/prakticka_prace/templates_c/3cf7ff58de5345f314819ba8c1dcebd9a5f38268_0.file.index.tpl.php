<?php /* Smarty version 3.1.27, created on 2016-02-22 13:51:45
         compiled from "G:\Programy\EasyPHP-DevServer-14.1VC11\data\localweb\ian_photography_soc\templates\index.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:704756cb046101f404_45390443%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3cf7ff58de5345f314819ba8c1dcebd9a5f38268' => 
    array (
      0 => 'G:\\Programy\\EasyPHP-DevServer-14.1VC11\\data\\localweb\\ian_photography_soc\\templates\\index.tpl',
      1 => 1454870508,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '704756cb046101f404_45390443',
  'variables' => 
  array (
    'userContent' => 0,
    'mainMenuGaleryFiles' => 0,
    'galeryFile' => 0,
    'mainMenuCategoryFiles' => 0,
    'categoryFile' => 0,
    'userBackgrounds' => 0,
    'i' => 0,
    'actualPhotos' => 0,
    'actualArticles' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_56cb046182d0e6_38927237',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_56cb046182d0e6_38927237')) {
function content_56cb046182d0e6_38927237 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '704756cb046101f404_45390443';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title>IAN Photography</title>

    <!-- PŘIPOJENÍ CSS STYLŮ -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css_styles/general_styles.css"/>
    <link rel="stylesheet" type="text/css" href="css_styles/loading_screens.css"/>
    <link rel="stylesheet" type="text/css" href="css_styles/header_styles.css"/>
    <link rel="stylesheet" type="text/css" href="css_styles/collapsed_navigation_styles.css"/>
    <link rel="stylesheet" type="text/css" href="css_styles/index_content_styles.css"/>
    <link rel="stylesheet" type="text/css" href="css_styles/footer_styles.css"/>
    <link rel="stylesheet" type="text/css" href="css_styles/responsive_styles.css"/>
</head>


<body>
    
    
    
    <!-- DEFINICE GLOBÁLNÍHO WRAPPERU OBSAHU ÚVODNÍ STRÁNKY -->
    <div class="content_wrapper"> 
        
        
        <!-- DEFINICE LOADING SCREENU PRO LOADING DOM STRUKTURY DOKUMENTU -->
        <div class="cssloader-wrapper">
            
            <div class="cssload-loader">
                <div class="cssload-flipper">
                    <div class="cssload-front"></div>
                    <div class="cssload-back"></div>
                </div>
                <h4>Loading...</h4>
            </div>
            
        </div>
        
        
        <!-- DEFINICE LOADING SCREENU PRO ZPRACOVÁNÍ PHP APLIKACE V RÁMCI AJAXU -->
        <div id="floatingCirclesG-wrapper">
            <div id="floatingCirclesG">
                <div class="f_circleG" id="frotateG_01"></div>
                <div class="f_circleG" id="frotateG_02"></div>
                <div class="f_circleG" id="frotateG_03"></div>
                <div class="f_circleG" id="frotateG_04"></div>
                <div class="f_circleG" id="frotateG_05"></div>
                <div class="f_circleG" id="frotateG_06"></div>
                <div class="f_circleG" id="frotateG_07"></div>
                <div class="f_circleG" id="frotateG_08"></div>
            </div>
        </div>   
        
        
        <!-- DEFINICE WRAPPERU HLAVIČKY WEBU -->
        <header>
            
            <?php if (isset($_SESSION['logged']) && $_SESSION['logged'] == true) {?>
            <!-- DEFINICE UŽIVATESLKÉHO ADMINISTRAČNÍHO PANELU (pouze v případě úspěšného přihlášení uživatele -> $_SESSION['logged'] == true, viz. login.php) -->
            <div class="user_admin_bar">
                <!-- Uvítací titulek uživateslkého panelu -> obsahuje uživatelské jméno, viz. login.php -->
                <p>Uživatel <?php echo $_SESSION['userData']['login'];?>
</p>
                <div class="full_resolution_links">  
                    <a href="php_solutions/logout.php?backupFile=../index.php">ODHLÁSIT SE</a>
                    <a href="admin_section.php?backupFile=index.php">ADMINISTRACE</a>  
                </div>
                <div class="dropdown">
                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        MOŽNOSTI
                        <span class="glyphicon glyphicon-triangle-bottom"></span>
                        <span class="glyphicon glyphicon-triangle-top"></span>            
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                        <li><a href="admin_section.php?backupFile=index.php">ADMINISTRACE</a></li>
                        <li><a href="php_solutions/logout.php?backupFile=../index.php">ODHLÁSIT SE</a></li>
                    </ul>
                </div>
           </div>
           <?php }?>
           
            <!-- DEFINICE HLAVIČKY WEBOVÉ STRÁNKY (obsah je načítán z databáze, viz. database_manipulation.php) -->
            <h1><a href="index.php"><?php echo $_smarty_tpl->tpl_vars['userContent']->value[0]['content'];?>
</a></h1>

            <!-- DEFINICE DVOUÚROVŇOVÉ NABÍDKY WEBU PRO VELKÁ ROZLIŠENÍ -->
            <nav class="primary_navigation">
                <ul class="main_menu_primary">
                    <!-- Obsah položek první úrovně hlavní nabídky načítán z databáze, viz. database_manipulation.php -->
                    <li><a href="portfolio.php"><?php echo $_smarty_tpl->tpl_vars['userContent']->value[1]['content'];?>
</a></li>
                    <li><a href="photogalery.php"><?php echo $_smarty_tpl->tpl_vars['userContent']->value[2]['content'];?>
</a>
                        <ul>
                            <!-- Položky druhé úrovně hlavní nabídky načítány z databáze, viz. database_manipulation.php -->
                            <?php
$_from = $_smarty_tpl->tpl_vars['mainMenuGaleryFiles']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['galeryFile'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['galeryFile']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['galeryFile']->value) {
$_smarty_tpl->tpl_vars['galeryFile']->_loop = true;
$foreach_galeryFile_Sav = $_smarty_tpl->tpl_vars['galeryFile'];
?>
                            <li><a href="gallery.php?galleryId=<?php echo $_smarty_tpl->tpl_vars['galeryFile']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['galeryFile']->value['galery_title'];?>
</a></li>
                            <?php
$_smarty_tpl->tpl_vars['galeryFile'] = $foreach_galeryFile_Sav;
}
?>
                        </ul>
                    </li>
                    <li><a href="blog.php"><?php echo $_smarty_tpl->tpl_vars['userContent']->value[3]['content'];?>
</a>
                        <ul>
                            <?php
$_from = $_smarty_tpl->tpl_vars['mainMenuCategoryFiles']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['categoryFile'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['categoryFile']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['categoryFile']->value) {
$_smarty_tpl->tpl_vars['categoryFile']->_loop = true;
$foreach_categoryFile_Sav = $_smarty_tpl->tpl_vars['categoryFile'];
?>
                            <li><a href="blog.php?categoryId=<?php echo $_smarty_tpl->tpl_vars['categoryFile']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['categoryFile']->value['category_title'];?>
</a></li>
                            <?php
$_smarty_tpl->tpl_vars['categoryFile'] = $foreach_categoryFile_Sav;
}
?>
                        </ul>
                    </li>
                    <li><a href="contacts.php"><?php echo $_smarty_tpl->tpl_vars['userContent']->value[4]['content'];?>
</a></li>
                </ul>
            </nav>

            <!-- DEFINICE DVOUÚROVŇOVÉ NABÍDKY WEBU PRO MALÁ ROZLIŠENÍ -->
            <nav class="collapsed_navigation">
                <a href="#" id="pull"><span>MENU</span></a>
                <ul class="main_menu_collapsed">
                    <!-- DEFINICE WRAPPERU POLOŽEK NABÍDKY PRO ZNEVIDITELNĚNÍ SCROLLBARU -->
                    <li id="scroller">
                        <ul>
                            <li><a href="portfolio.php"><?php echo $_smarty_tpl->tpl_vars['userContent']->value[1]['content'];?>
</a></li>
                            <li><a href="photogalery.php"><?php echo $_smarty_tpl->tpl_vars['userContent']->value[2]['content'];?>
</a>
                                <ul>
                                    <?php
$_from = $_smarty_tpl->tpl_vars['mainMenuGaleryFiles']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['galeryFile'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['galeryFile']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['galeryFile']->value) {
$_smarty_tpl->tpl_vars['galeryFile']->_loop = true;
$foreach_galeryFile_Sav = $_smarty_tpl->tpl_vars['galeryFile'];
?>
                                    <li><a href="gallery.php?galleryId=<?php echo $_smarty_tpl->tpl_vars['galeryFile']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['galeryFile']->value['galery_title'];?>
</a></li>
                                    <?php
$_smarty_tpl->tpl_vars['galeryFile'] = $foreach_galeryFile_Sav;
}
?>
                                </ul>
                            </li>
                            <li><a href="blog.php"><?php echo $_smarty_tpl->tpl_vars['userContent']->value[3]['content'];?>
</a>
                                <ul>
                                    <?php
$_from = $_smarty_tpl->tpl_vars['mainMenuCategoryFiles']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['categoryFile'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['categoryFile']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['categoryFile']->value) {
$_smarty_tpl->tpl_vars['categoryFile']->_loop = true;
$foreach_categoryFile_Sav = $_smarty_tpl->tpl_vars['categoryFile'];
?>
                                    <li><a href="blog.php?categoryId=<?php echo $_smarty_tpl->tpl_vars['categoryFile']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['categoryFile']->value['category_title'];?>
</a></li>
                                    <?php
$_smarty_tpl->tpl_vars['categoryFile'] = $foreach_categoryFile_Sav;
}
?>
                                </ul>
                            </li>
                            <li><a href="contacts.php"><?php echo $_smarty_tpl->tpl_vars['userContent']->value[4]['content'];?>
</a></li>
                        </ul>
                    </li>
                </ul>
            </nav>
            
        </header>
        
        
        <!-- DEFINICE OBSAHOVÉ ČÁSTI WEBOVÉ STRÁNKY S PARALLAX EFEKTEM -->
        <div class="parallax_wrapper">
            
            <!-- DEFINICE UVÍTACÍ VRSTY ÚVODNÍ STRÁNKY -->
            <div class="layer_underlay">
                <!-- Na uvítací vrstvu úvodní stránky aplikován parallax efekt (parallay_layer) -->
                <div class="content_layer photo_layer parallax_layer">
                    <div class="image_wrapper" style="background-image: url(<?php echo $_smarty_tpl->tpl_vars['userBackgrounds']->value[0]['bg_path'];?>
)"></div>
                    <div class="text_area">
                        <!-- Obsah uvítací vrstvy načítán z databáze , viz. database_load.php -->
                        <h2><?php echo $_smarty_tpl->tpl_vars['userContent']->value[5]['content'];?>
</h2>
                        <p><?php echo $_smarty_tpl->tpl_vars['userContent']->value[6]['content'];?>
</p>
                    </div>
                </div>
            </div>
            
            <!-- DEFINICE SEKCE ÚVODNÍ STRÁNKY VYHRAZENÉ PRO NÁHLED AKTUÁLNÍCH FOTOGRAFIÍ V PARALLAXU -> 3 fotografie, viz. index.php -->
            <?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['i']->step = 1;$_smarty_tpl->tpl_vars['i']->total = (int) ceil(($_smarty_tpl->tpl_vars['i']->step > 0 ? 2+1 - (0) : 0-(2)+1)/abs($_smarty_tpl->tpl_vars['i']->step));
if ($_smarty_tpl->tpl_vars['i']->total > 0) {
for ($_smarty_tpl->tpl_vars['i']->value = 0, $_smarty_tpl->tpl_vars['i']->iteration = 1;$_smarty_tpl->tpl_vars['i']->iteration <= $_smarty_tpl->tpl_vars['i']->total;$_smarty_tpl->tpl_vars['i']->value += $_smarty_tpl->tpl_vars['i']->step, $_smarty_tpl->tpl_vars['i']->iteration++) {
$_smarty_tpl->tpl_vars['i']->first = $_smarty_tpl->tpl_vars['i']->iteration == 1;$_smarty_tpl->tpl_vars['i']->last = $_smarty_tpl->tpl_vars['i']->iteration == $_smarty_tpl->tpl_vars['i']->total;?>
            <!-- DEFINICE WRAPPERU JEDNOTLIVÝCH VRSTEV S BAREVNÝM PODKLADEM -->
            <div class="layer_underlay">
                <?php if ($_smarty_tpl->tpl_vars['i']->value == 1) {?>
                <!-- Na druhou fotografii v náhledu (třetí vrstva) aplikován parallax efekt -->
                <div class="content_layer photo_layer parallax_layer">
                <?php } else { ?>
                <!-- Ostatní vrstvy bez parallaxu -->    
                <div class="content_layer photo_layer">
                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['i']->value == 0) {?>
                    <!-- Vrstva s náhledem první fotografie určena pro blokový odkaz do sekce webu s fotogalerií -->
                    <div class='image_wrapper' style="background-image: url(<?php echo $_smarty_tpl->tpl_vars['actualPhotos']->value[$_smarty_tpl->tpl_vars['i']->value]['photo_path'];?>
)"></div>
                    <div class='text_area'>
                        <a href='photogalery.php'>
                            <span>PŘEJÍT DO FOTOGALERIE</span>
                        </a>
                    </div>
                <?php } else { ?>
                    <!-- Ostatní vrstvy s náhledy pouze zobrazují příslušnou fotografii -->
                    <div class='image_wrapper' style="background-image: url(<?php echo $_smarty_tpl->tpl_vars['actualPhotos']->value[$_smarty_tpl->tpl_vars['i']->value]['photo_path'];?>
)"></div>
                        <!-- DEFINICE STRUKTURY SKRYTÉHO POPISKU FOTOGRAFIE V NÁHLEDOVÉ VRSTVĚ -->
                        <div class='hidden_description_trigger'>
                            <span class='glyphicon glyphicon-info-sign' aria-hidden='true'></span>
                            <div class='hidden_description text_area'>
                                <!-- Příslušné informace k fotografii načítány z databáze, viz. index.php -->
                                <p><?php echo $_smarty_tpl->tpl_vars['actualPhotos']->value[$_smarty_tpl->tpl_vars['i']->value]['photo_title'];?>
</p>
                                <div>
                                    <p><?php echo $_smarty_tpl->tpl_vars['actualPhotos']->value[$_smarty_tpl->tpl_vars['i']->value]['galery_title'];?>
</p>
                                    <p><?php echo $_smarty_tpl->tpl_vars['actualPhotos']->value[$_smarty_tpl->tpl_vars['i']->value]['login'];?>
</p>
                                    <p><?php echo $_smarty_tpl->tpl_vars['actualPhotos']->value[$_smarty_tpl->tpl_vars['i']->value]['time_stamp'];?>
</p>
                                </div>
                            </div>
                        </div>        
                <?php }?>
                <!-- UZAVÍRACÍ ČÁST WRAPPERU VLASTNÍHO NÁHLEDU -->   
                </div>
            <!-- UZAVÍRACÍ ČÁST WRAPPERU VRSTVY S BAREVNÝM PODKLADEM -->        
            </div>
            <?php }} ?>
            <!-- První blogová vrstva určena pro blokový odkaz do sekve webu s blogem -->
            <div class='layer_underlay'>
                <div class='content_layer blog_layer'>
                    <div class='text_area'>
                        <a href='blog.php'>
                            <span>PŘEJÍT DO BLOGU</span>
                        </a>
                    </div>
                </div>
            </div>
            <!-- DEFINICE SEKCE ÚVODNÍ STRÁNKY VYHRAZENÉ PRO NÁHLED AKTUÁLNÍCH ČLÁNKŮ Z BLOGU -> 2 články, viz. index.php -->    
            <?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['i']->step = 1;$_smarty_tpl->tpl_vars['i']->total = (int) ceil(($_smarty_tpl->tpl_vars['i']->step > 0 ? 1+1 - (0) : 0-(1)+1)/abs($_smarty_tpl->tpl_vars['i']->step));
if ($_smarty_tpl->tpl_vars['i']->total > 0) {
for ($_smarty_tpl->tpl_vars['i']->value = 0, $_smarty_tpl->tpl_vars['i']->iteration = 1;$_smarty_tpl->tpl_vars['i']->iteration <= $_smarty_tpl->tpl_vars['i']->total;$_smarty_tpl->tpl_vars['i']->value += $_smarty_tpl->tpl_vars['i']->step, $_smarty_tpl->tpl_vars['i']->iteration++) {
$_smarty_tpl->tpl_vars['i']->first = $_smarty_tpl->tpl_vars['i']->iteration == 1;$_smarty_tpl->tpl_vars['i']->last = $_smarty_tpl->tpl_vars['i']->iteration == $_smarty_tpl->tpl_vars['i']->total;?>
                <div class='layer_underlay'>   
                    <div class='content_layer blog_layer'>
                        <div class='text_area'>
                        <!-- Podmínka ošetřující přítomnost titulní fotografie článku -->    
                <?php if (!empty($_smarty_tpl->tpl_vars['actualArticles']->value[$_smarty_tpl->tpl_vars['i']->value]['title_photo'])) {?>
                            <div class='article_photo_underlay'>
                                <a href="article.php?articleId=<?php echo $_smarty_tpl->tpl_vars['actualArticles']->value[$_smarty_tpl->tpl_vars['i']->value]['id'];?>
" class='article_photo' style="background-image: url(<?php echo $_smarty_tpl->tpl_vars['actualArticles']->value[$_smarty_tpl->tpl_vars['i']->value]['title_photo'];?>
)"></a>
                            </div>    
                <?php }?>
                            <!-- Obsah náhledů jednotlivých článků načítán z databáze, viz. index.php -->
                            <h3><a class='article_title' href="article.php?articleId=<?php echo $_smarty_tpl->tpl_vars['actualArticles']->value[$_smarty_tpl->tpl_vars['i']->value]['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['actualArticles']->value[$_smarty_tpl->tpl_vars['i']->value]['article_title'];?>
</a></h3>
                            <p class='article_timestamp'><?php echo $_smarty_tpl->tpl_vars['actualArticles']->value[$_smarty_tpl->tpl_vars['i']->value]['time_stamp'];?>
 | By: <?php echo $_smarty_tpl->tpl_vars['actualArticles']->value[$_smarty_tpl->tpl_vars['i']->value]['login'];?>
 | Category: <?php echo $_smarty_tpl->tpl_vars['actualArticles']->value[$_smarty_tpl->tpl_vars['i']->value]['category_title'];?>
</p>
                            <!-- Podmínka omezující výpis na limit 1024 znaků -->
                <?php if ((strlen($_smarty_tpl->tpl_vars['actualArticles']->value[$_smarty_tpl->tpl_vars['i']->value]['content']) > 1024)) {?>
                            <p><?php echo substr($_smarty_tpl->tpl_vars['actualArticles']->value[$_smarty_tpl->tpl_vars['i']->value]['content'],0,1024);?>
 [...]</p>
                <?php } else { ?>
                            <p><?php echo $_smarty_tpl->tpl_vars['actualArticles']->value[$_smarty_tpl->tpl_vars['i']->value]['content'];?>
</p>
                <?php }?>
                            <p><a href="article.php?articleId=<?php echo $_smarty_tpl->tpl_vars['actualArticles']->value[$_smarty_tpl->tpl_vars['i']->value]['id'];?>
">PŘEJÍT NA ČLÁNEK</a></p>
                        </div>
                    </div>
                </div>
            <?php }} ?>
                
            <!-- DEFINICE WRAPPERU KONTAKTNÍHO FORMULÁŘE -->
            <div class="form_wrapper">
                <form id="contact_form" method="post" action="php_solutions/contact_form.php">
                    <h2>KONTAKTUJTE MĚ</h2>
                    <div class="form_group">
                        <label for="name">VÁŠE JMÉNO A PŘÍJMENÍ</label>
                        <input type="text" id="name" name="name" placeholder="ZADEJTE SVÉ JMÉNO A PŘÍJMENÍ" maxlength="64" autocomplete="off"/>
                        <p>(MAX. 64 ZNAKŮ)</p>
                    </div>
                    <div class="form_group">
                        <label for="email">VÁŠ E-MAIL</label>
                        <input type="text" id="email" name="email" placeholder="ZADEJTE SVŮJ E-MAIL" maxlength="64" autocomplete="off"/>
                        <p>(MAX. 64 ZNAKŮ)</p>
                    </div>
                    <div class="form_group">
                        <label for="subject">PŘEDMĚT VZKAZU</label>
                        <input type="text" id="subject" name="subject" placeholder="ZADEJTE PŘEDMĚT ZPRÁVY" maxlength="64" autocomplete="off"/>
                        <p>(MAX. 64 ZNAKŮ)</p>
                    </div>
                    <div class="form_group">
                        <label for="message">VÁŠ VZKAZ</label>
                        <textarea id="message" name="message" placeholder="MÍSTO PRO VÁŠ TEXT..." form="contact_form" maxlength="1024" autocomplete="off"></textarea>
                        <p>(MAX. 1024 ZNAKŮ)</p>    
                    </div>
                    <input class="reset_btn" type="reset" value="RESETOVAT"/>
                    <input class="submit_btn" name="submit-btn" type="submit" value="ODESLAT"/>
                </form>
            </div>
                
            <!-- DEFINICE INFORMAČNÍHO MODALU PRO KONTAKTNÍ FORMULÁŘ -->
            <div class="modal fade" id="contact-form-info-modal" tabindex="-1" role="dialog" aria-labelledby="contact-form-info-modal-label">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h3 class="modal-title" id="contact-form-modal-label">ZPRÁVA O STAVU ZASLÁNÍ EMAILU</h3>
                        </div>
                        <div class="modal-footer">
                            <h4></h4>
                            <button type="button" class="btn btn-default" data-dismiss="modal">ZAVŘÍT</button>
                        </div>
                    </div>
                </div>
            </div>
                
            <!-- DEFINICE WRAPPERU PATIČKY WEBU -->
            <footer>
                <!-- DEFINICE WRAPPERU OBSAHOVÉ ČÁSTI PATIČKY WEBU -->
                <div class='footer_content_wrapper'>
                    <!-- Patička webu se skládá ze 3 obsahových bloků -->
                    <?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['i']->step = 1;$_smarty_tpl->tpl_vars['i']->total = (int) ceil(($_smarty_tpl->tpl_vars['i']->step > 0 ? 9+1 - (7) : 7-(9)+1)/abs($_smarty_tpl->tpl_vars['i']->step));
if ($_smarty_tpl->tpl_vars['i']->total > 0) {
for ($_smarty_tpl->tpl_vars['i']->value = 7, $_smarty_tpl->tpl_vars['i']->iteration = 1;$_smarty_tpl->tpl_vars['i']->iteration <= $_smarty_tpl->tpl_vars['i']->total;$_smarty_tpl->tpl_vars['i']->value += $_smarty_tpl->tpl_vars['i']->step, $_smarty_tpl->tpl_vars['i']->iteration++) {
$_smarty_tpl->tpl_vars['i']->first = $_smarty_tpl->tpl_vars['i']->iteration == 1;$_smarty_tpl->tpl_vars['i']->last = $_smarty_tpl->tpl_vars['i']->iteration == $_smarty_tpl->tpl_vars['i']->total;?>
                    <div class='footer_content'>
                        <!-- Obsah patičky načítán z databáze, viz. database_load.php -->
                        <h2><?php echo $_smarty_tpl->tpl_vars['userContent']->value[$_smarty_tpl->tpl_vars['i']->value]['title'];?>
</h2>
                        <p><?php echo $_smarty_tpl->tpl_vars['userContent']->value[$_smarty_tpl->tpl_vars['i']->value]['content'];?>
</p>
                    </div>
                    <?php }} ?>
                </div>
                <!-- Výpis HTML struktury přihlašovacího formuláře proběhne pouze v případě zadání parametru v URL adrese a v případě, že uživatel není momentálně úspěšně přihlášený -->
                <?php if (isset($_GET['user']) && empty($_GET['user'])) {?>
                    <?php if (!isset($_SESSION['logged']) || $_SESSION['logged'] != true) {?>
                <!-- DEFINICE WRAPPERU PŘIHLAŠOVACÍHO FORMULÁŘE -->
                <div class='form_wrapper'>
                    <form method='POST'>
                        <div class='form_group'>
                            <label for='login'>PŘIHLAŠOVACÍ JMÉNO</label>
                            <input type='text' id='login' name='login' placeholder='ZADEJTE PŘIHLAŠOVACÍ JMÉNO'/>
                        </div>
                        <div class='form_group'>
                            <label for='password'>HESLO</label>
                            <input type='password' id='password' name='password' placeholder='ZADEJTE HESLO'/>
                        </div>
                        <input id='login_btn' class='submit_btn' type='button' name='submit' value='PŘIHLÁSIT SE'/>
                        <p id='login_msg'></p>
                    </form>
                </div>    
                    <?php }?>
                <?php }?>
                <h3>CREATED BY &copy; PETR PONDĚLÍK 2016</h3>
            </footer>
                
        </div>
    
        
    </div>
    
        
    <!-- PŘIPOJENÍ JQUERY -->
    <?php echo '<script'; ?>
 src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"><?php echo '</script'; ?>
>
                
    <!-- PŘIPOJENÍ PLUGINU PRO AJAX -->    
    <?php echo '<script'; ?>
 src="http://malsup.github.com/jquery.form.js"><?php echo '</script'; ?>
>
    
    <!-- PŘIPOJENÍ BOOTSTRAP JS PLUGINŮ -->
    <?php echo '<script'; ?>
 src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"><?php echo '</script'; ?>
>
    
    <!-- PŘIPOJENÍ SCRIPTU PRO MD5 HASHOVÁNÍ -->
    <?php echo '<script'; ?>
 src="js_solutions/md5_hashing.js"><?php echo '</script'; ?>
>
    
    <!-- PŘIPOJENÍ SCRIPTU OVLÁDAJÍCÍHO PRŮHLEDNOST HLAVNÍ NABÍDKY A NABÍDKU PRO MALÁ ROZLIŠENÍ-->
    <?php echo '<script'; ?>
 src="js_solutions/header_functions.js"><?php echo '</script'; ?>
>
        
    <!-- PŘIPOJENÍ SCRIPTU PRO AKTIVACI AJAXOVÉ KOMUNIKACE KONTAKTNÍHO FORMULÁŘE -->
    <?php echo '<script'; ?>
 src="js_solutions/contact_form_ajax.js"><?php echo '</script'; ?>
>
    
    <!-- PŘIPOJENÍ SCRIPTU OVLÁDAJÍCÍHO PŘIHLAŠOVACÍ FORMULÁŘ -->
    <?php echo '<script'; ?>
 src="js_solutions/login_form_control.js"><?php echo '</script'; ?>
>
        
    <!-- SCRIPT OVLÁDAJÍCÍ LOADING SCREEN PRO DOM STRUKTURU DOKUMENTU -->
    <?php echo '<script'; ?>
 type="text/javascript">
        $(window).load(function(){
            $(".cssloader-wrapper").remove();
            $("html").css("overflow","visible");
        });
    <?php echo '</script'; ?>
>    

    <!-- PŘIPOJENÍ SCRIPTU OVLÁDAJÍCÍHO PARALLAX EFEKT -->
    <?php echo '<script'; ?>
 src="js_solutions/parallax_effect.js"><?php echo '</script'; ?>
>
    
</body>
</html><?php }
}
?>