<?php /* Smarty version 3.1.27, created on 2016-02-06 18:51:08
         compiled from "G:\Programy\EasyPHP-DevServer-14.1VC11\data\localweb\ian_photography\js_solutions\login_form_control.php" */ ?>
<?php
/*%%SmartyHeaderCode:2167356b6328c08e685_23511814%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1dbabedd61c97a900d7f43981bd6229bef9670a0' => 
    array (
      0 => 'G:\\Programy\\EasyPHP-DevServer-14.1VC11\\data\\localweb\\ian_photography\\js_solutions\\login_form_control.php',
      1 => 1454781065,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2167356b6328c08e685_23511814',
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_56b6328c0fb264_46468792',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_56b6328c0fb264_46468792')) {
function content_56b6328c0fb264_46468792 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '2167356b6328c08e685_23511814';
?>
<?php echo '<script'; ?>
 type="text/javascript">
    
    //SCRIPT PRO PŘIHLÁŠENÍ POMOCÍ TECHNOLOGIE AJAX
    $(document).ready(function(){
        $('#login_btn').click(function(){
            var login = $('#login').val(),
                password = $('#password').val(),
                loginInput = $('#login'),
                passwordInput = $('#password');

            if(login !== '' && password !== ''){
                if(window.XMLHttpRequest) var xmlHttp = new XMLHttpRequest();
                else var xmlHttp = new ActiveXObject('Microsoft.XMLHTTP');

                xmlHttp.onreadystatechange = function(){
                    //alert(xmlHttp.readyState);
                    if(xmlHttp.readyState == 4 && xmlHttp.status == 200){
                        $('#login_msg').text(xmlHttp.responseText);
                        if(xmlHttp.responseText == ''){
                            window.location.reload();
                        }
                    }
                }

                xmlHttp.open('POST','php_solutions/login.php',true);
                xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                xmlHttp.send('login='+login+'&password='+md5(password));

            }
            else{
                if(login == ""){
                    loginInput.css("boxShadow","0px 0px 10px red");
                    loginInput.addClass("form_invalid");
                    loginInput.attr("placeholder","NEZADÁNO ŽÁDNÉ PŘIHLAŠOVACÍ JMÉNO!");
                }
                if(password == ""){
                    passwordInput.css("boxShadow","0px 0px 10px red");
                    passwordInput.addClass("form_invalid");
                    passwordInput.attr("placeholder","NEZADÁNO ŽÁDNÉ HESLO");
                }
                return;
            }
        });
    });

    //SCRIPT PRO ZPĚTNÝ NÁVRAT DEFAULTNÍCH STYLŮ INPUTŮ PŘIHLAŠOVACÍHO FORMULÁŘE
    $(document).ready(function(){
        $('#login').focus(function(){
            $(this).removeClass("form_invalid");
            $(this).css("box-shadow","0px 0px 10px #5BC0DE");
            $(this).attr("placeholder","ZADEJTE PŘIHLAŠOVACÍ JMÉNO");
        });
        $('#password').focus(function(){
            $(this).removeClass("form_invalid");
            $(this).css("box-shadow","0px 0px 10px #5BC0DE");
            $(this).attr("placeholder","ZADEJTE HESLO");
        });
        $('#login,#password').blur(function(){
            $(this).css("box-shadow","0px 0px 0px transparent");
        });
    });
    
<?php echo '</script'; ?>
>    <?php }
}
?>