<?php /* Smarty version 3.1.27, created on 2016-02-07 19:44:48
         compiled from "G:\Programy\EasyPHP-DevServer-14.1VC11\data\localweb\ian_photography\templates\admin_section.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:4856b790a0e91ae5_11322203%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0887e635d06116ec9594582b930b0b0dc7a0588f' => 
    array (
      0 => 'G:\\Programy\\EasyPHP-DevServer-14.1VC11\\data\\localweb\\ian_photography\\templates\\admin_section.tpl',
      1 => 1454870564,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '4856b790a0e91ae5_11322203',
  'variables' => 
  array (
    'backupFile' => 0,
    'galeryFiles' => 0,
    'galeryFile' => 0,
    'photosEditArray' => 0,
    'photo' => 0,
    'galeriesEditArray' => 0,
    'galery' => 0,
    'categoryFiles' => 0,
    'categoryFile' => 0,
    'articlesEditArray' => 0,
    'article' => 0,
    'categoriesEditArray' => 0,
    'category' => 0,
    'userContent' => 0,
    'openingLayerCurrentBg' => 0,
    'i' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_56b790a248e835_90403748',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_56b790a248e835_90403748')) {
function content_56b790a248e835_90403748 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '4856b790a0e91ae5_11322203';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title>IAN Photography</title>

    <!-- PŘIPOJENÍ CSS STYLŮ -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css_styles/general_styles.css"/>
    <link rel="stylesheet" type="text/css" href="css_styles/loading_screens.css"/>    
    <link rel="stylesheet" type="text/css" href="css_styles/header_styles.css"/>
    <link rel="stylesheet" type="text/css" href="css_styles/footer_styles.css"/>
    <link rel="stylesheet" type="text/css" href="css_styles/admin_section_styles.css"/>
    <link rel="stylesheet" type="text/css" href="css_styles/responsive_styles.css"/>
</head>



<body>



    <!-- DEFINICE LOADING SCREENU PRO LOADING DOM STRUKTURY DOKUMENTU -->
    <div class="cssloader-wrapper">

        <div class="cssload-loader">
            <div class="cssload-flipper">
                <div class="cssload-front"></div>
                <div class="cssload-back"></div>
            </div>
            <h4>Loading...</h4>
        </div>

    </div>
    
    
    <!-- DEFINICE LOADING SCREENU PRO ZPRACOVÁNÍ PHP APLIKACE V RÁMCI AJAXU -->
    <div id="floatingCirclesG-wrapper">
        <div id="floatingCirclesG">
            <div class="f_circleG" id="frotateG_01"></div>
            <div class="f_circleG" id="frotateG_02"></div>
            <div class="f_circleG" id="frotateG_03"></div>
            <div class="f_circleG" id="frotateG_04"></div>
            <div class="f_circleG" id="frotateG_05"></div>
            <div class="f_circleG" id="frotateG_06"></div>
            <div class="f_circleG" id="frotateG_07"></div>
            <div class="f_circleG" id="frotateG_08"></div>
        </div>
    </div>
    
    
    <!-- Výpis HTML struktury administračního rozhraní pouze v případě úspěšného přihlášení, viz. login.php -->
    <?php if (isset($_SESSION['logged']) && $_SESSION['logged'] == true) {?>

    <!-- DEFINICE WRAPPERU DOKUMENTU -->
    <div class="content-wrapper">


    <!-- DEFINICE WRAPPERU HLAVIČKY WEBU -->
    <header>

        <!-- DEFINICE UŽIVATESLKÉHO ADMINISTRAČNÍHO PANELU (pouze v případě úspěšného přihlášení uživatele -> $_SESSION['logged'] == true, viz. login.php) -->
        <div class='user_admin_bar'>
            <!-- Uvítací titulek uživateslkého panelu -> obsahuje uživatelské jméno, viz. login.php -->
            <p>Uživatel <?php echo $_SESSION['userData']['login'];?>
</p>
            <div class='full_resolution_links'>  
                <a href='php_solutions/logout.php?backupFile=../index.php'>ODHLÁSIT SE</a>
                <a href='<?php echo $_smarty_tpl->tpl_vars['backupFile']->value;?>
'>ZPĚT</a>  
            </div>
            <div class='dropdown'>
                <button class='btn btn-default dropdown-toggle' type='button' id='dropdownMenu1' data-toggle='dropdown' aria-haspopup='true' aria-expanded='true'>
                    MOŽNOSTI
                    <span class='glyphicon glyphicon-triangle-bottom'></span>
                    <span class='glyphicon glyphicon-triangle-top'></span>            
                </button>
                <ul class='dropdown-menu' aria-labelledby='dropdownMenu1'>
                    <li><a href='<?php echo $_smarty_tpl->tpl_vars['backupFile']->value;?>
'>ZPĚT</a></li>
                    <li><a href='php_solutions/logout.php?backupFile=../index.php'>ODHLÁSIT SE</a></li>
                </ul>
            </div>
        </div>

    </header>


    <!-- DEFINICE WRAPPERU ADMINISTRAČNÍ ČÁSTI -->    
    <div class='admin-section-wrapper'>

        <!-- DEFINICE NAVIGACE MEZI KATEGORIEMI UŽIVATELSKÝCH NÁSTROJŮ REDAKČNÍHO SYSTÉMU -->
        <nav class='rstools-categories-wrapper'>
            <!-- Navigace mezi kategoriemi nástrojů realizována za využití bootstrap pills -->
            <ul class="nav nav-pills nav-stacked" role="tablist">
                <!-- Bootstrap pills realizuje navigaci za využití záložek v dokumentu -> href="#zalozkaId" -->
                <li role="presentation" class="active"><a href="#photo-tools" aria-controls="photo-tools" role="tab" data-toggle="tab">NÁSTROJE SPRÁVY FOTOGALERIE</a></li>
                <li role="presentation"><a href="#blog-tools" aria-controls="blog-tools" role="tab" data-toggle="tab">NÁSTROJE SPRÁVY BLOGU</a></li>
                <li role="presentation"><a href="#user-content-tools" aria-controls="user-content-tools" role="tab" data-toggle="tab">NÁSTROJE UŽIVATELSKÉ EDITACE</a></li>
            </ul>
        </nav>

        <div class="tab-content tab-content-wrapper">
            <!-- DEFINICE KATEGORIE NÁSTROJE SPRÁVY FOTOGALERIE -->
            <div role="tabpanel" class="tab-pane active" id="photo-tools">
                <h2>NÁSTROJE SPRÁVY FOTOGALERIE</h2>
                <!-- Navigace mezi jednotlivými nástroji v rámci jedné kategorie realizováná za využití bootstrap tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <!-- Bootstrap tabs rovněž realizuje navigaci za využití záložek v dokumentu -->
                    <li role="presentation" class="active"><a href="#photo-add" aria-controls="home" role="tab" data-toggle="tab">PŘIDAT FOTOGRAFII</a></li>
                    <li role="presentation"><a href="#photo-edit" aria-controls="profile" role="tab" data-toggle="tab">EDITOVAT/ODEBRAT FOTOGRAFII</a></li>
                    <li role="presentation"><a href="#galery-add" aria-controls="settings" role="tab" data-toggle="tab">ZALOŽIT GALERII</a></li>
                    <li role="presentation"><a href="#galery-edit" aria-controls="settings" role="tab" data-toggle="tab">EDITOVAT/ODEBRAT GALERII</a></li>
                </ul>
                <div class="tab-content">
                    <!-- DEFINICE NÁSTROJE PRO PŘIDÁNÍ FOTOGRAFIE -->
                    <div role="tabpanel" class="tab-pane active" id="photo-add">
                        <h3>Přidat fotografii</h3>
                        <!-- DEFINICE FORMULÁŘE PRO PŘIDÁNÍ FOTOGRAFIE -> multipart/form-data -->
                        <form id="photo-add-form" role="form" method="post" action="php_solutions/redaction_system.php?tool=photoAdd&userId=<?php echo $_SESSION['userData']['id'];?>
" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="photo-add-title">Zadejte název fotografie</label>
                                <input class="form-control" type="text" name="photo-add-title" id="photo-add-title"/>
                                <label for="photo-add-galery">Zvolte galerii</label>
                                <select name="photo-add-galery" id="photo-add-galery" class="form-control">
                                    <!-- Jednotlivé galerie jako možnosti elementu select načítány z databáze, viz. database_load.php -->
                                    <?php
$_from = $_smarty_tpl->tpl_vars['galeryFiles']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['galeryFile'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['galeryFile']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['galeryFile']->value) {
$_smarty_tpl->tpl_vars['galeryFile']->_loop = true;
$foreach_galeryFile_Sav = $_smarty_tpl->tpl_vars['galeryFile'];
?>
                                    <option><?php echo $_smarty_tpl->tpl_vars['galeryFile']->value['galery_title'];?>
</option>
                                    <?php
$_smarty_tpl->tpl_vars['galeryFile'] = $foreach_galeryFile_Sav;
}
?>
                                </select>
                                <input id="photo-add-file" class="input-file" type="file" name="photo-add-file"/>
                                <label id="photo-add-label" class="btn btn-default" for="photo-add-file"><span class="glyphicon glyphicon-upload"></span><span>Vyberte soubor</span></label>
                                <input class="btn btn-lg btn-info" type="submit" value="Nahrát fotografii"/>
                            </div>
                        </form>
                        <!-- DEFINICE PROGRESS BARU -->
                        <div class="progress">
                            <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%"></div>
                        </div>
                        <!-- DEFINICE INFORMAČNÍHO MODALU PRO NÁSTROJ PŘIDÁNÍ FOTOGRAFIE -->
                        <div class="modal fade" id="photo-add-info-modal" tabindex="-1" role="dialog" aria-labelledby="photo-add-info-modal-label">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h3 class="modal-title" id="photo-add-info-modal-label">ZPRÁVA O STAVU UPLOADU</h3>
                                    </div>
                                    <div class="modal-footer">
                                        <h4></h4>
                                        <div class="image"></div>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">ZAVŘÍT</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- DEFINICE NÁSTROJE PRO EDITACI A ODSTANĚNÍ FOTOGRAFIE -->
                    <div role="tabpanel" class="tab-pane" id="photo-edit">
                        <h3>Editovat/odebrat fotografii</h3>
                        <!-- DEFINICE TABULKY OBSAHUJÍCÍ EDITOVATELNÉ FOTOGRAFIE -->
                        <table class="table table-hover photo-edit-table">
                            <!-- Jednotlivé záznamy fotografií načítány z databáze, viz. admin_section.php -->
                            <?php
$_from = $_smarty_tpl->tpl_vars['photosEditArray']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['photo'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['photo']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['photo']->value) {
$_smarty_tpl->tpl_vars['photo']->_loop = true;
$foreach_photo_Sav = $_smarty_tpl->tpl_vars['photo'];
?>
                            <tr id="photo-row<?php echo $_smarty_tpl->tpl_vars['photo']->value['id'];?>
">
                                <!-- WRAPPEREM KAŽDÉ FOTOGRAFIE BUŇKA DANÉ ŘÁDKY TABULKY -->
                                <td class="photo-edit-wrapper">
                                    <div class="photo-edit-thumbnail-underlay">
                                        <div class="photo-edit-thumbnail" style="background-image: url(<?php echo $_smarty_tpl->tpl_vars['photo']->value['photo_path'];?>
)"></div>
                                    </div>
                                    <div class="photo-edit-full-wrapper">
                                        <div class="photo-edit-full-underlay">
                                            <div class="photo-edit-full" style="background-image: url(<?php echo $_smarty_tpl->tpl_vars['photo']->value['photo_path'];?>
)"></div>
                                        </div>
                                        <h3><?php echo $_smarty_tpl->tpl_vars['photo']->value['photo_title'];?>
</h3>
                                        <p><?php echo $_smarty_tpl->tpl_vars['photo']->value['time_stamp'];?>
 | By: <?php echo $_smarty_tpl->tpl_vars['photo']->value['login'];?>
 | Galerie: <?php echo $_smarty_tpl->tpl_vars['photo']->value['galery_title'];?>
</p>
                                        <div class="photo-edit-buttons">
                                            <a class="btn btn-lg btn-info edit-button" data-toggle="modal" data-target="#photo-edit-modal<?php echo $_smarty_tpl->tpl_vars['photo']->value['id'];?>
">Upravit fotografii</a><button class="btn btn-lg btn-danger delete-button" data-toggle="modal" data-target="#photo-remove-modal<?php echo $_smarty_tpl->tpl_vars['photo']->value['id'];?>
"><span class="glyphicon glyphicon-remove"></span>Odebrat fotografii</button>
                                        </div>
                                    </div>
                                </td>
                                <!-- DEFINICE MODALU S FORMULÁŘEM PRO ODSTRANĚNÍ FOTOGRAFIE -->
                                <div class="modal fade photo-remove-modal" id="photo-remove-modal<?php echo $_smarty_tpl->tpl_vars['photo']->value['id'];?>
" tabindex="-1" role="dialog" aria-labelledby="photo-remove-modal<?php echo $_smarty_tpl->tpl_vars['photo']->value['id'];?>
-label">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="close"><span aria-hidden="true">&times;</span></button>
                                                <h3 class="modal-title" id="photo-remove-modal<?php echo $_smarty_tpl->tpl_vars['photo']->value['id'];?>
-label">Odstranit fotografii <?php echo $_smarty_tpl->tpl_vars['photo']->value['photo_title'];?>
 z <?php echo $_smarty_tpl->tpl_vars['photo']->value['time_stamp'];?>
?</h3>  
                                            </div>
                                            <div class="modal-footer">
                                                <!-- DEFINICE FORMULÁŘE PRO ODSTRANĚNÍ FOTOGRAFIE -->
                                                <form class="photo-remove-form" method="post" role="form" action="php_solutions/redaction_system.php?tool=photoRemove&id=<?php echo $_smarty_tpl->tpl_vars['photo']->value['id'];?>
&photoFile=<?php echo $_smarty_tpl->tpl_vars['photo']->value['photo_path'];?>
&photoTitle=<?php echo $_smarty_tpl->tpl_vars['photo']->value['photo_title'];?>
">
                                                      <button type="button" class="btn btn-default" data-dismiss="modal">ZPĚT</button>
                                                      <button type="submit" class="btn btn-danger">POTVRDIT</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- DEFINICE MODALU S FORMULÁŘEM PRO EDITACI FOTOGRAFIE -->
                                <div class="modal fade photo-edit-modal" id="photo-edit-modal<?php echo $_smarty_tpl->tpl_vars['photo']->value['id'];?>
" tabindex="-1" role="dialog" aria-labelledby="photo-edit-modal<?php echo $_smarty_tpl->tpl_vars['photo']->value['id'];?>
-label">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="close"><span aria-hidden="true">&times;</span></button>
                                                <h3 class="modal-title" id="photo-edit-modal<?php echo $_smarty_tpl->tpl_vars['photo']->value['id'];?>
-label">Editace fotografie <?php echo $_smarty_tpl->tpl_vars['photo']->value['photo_title'];?>
 z <?php echo $_smarty_tpl->tpl_vars['photo']->value['time_stamp'];?>
</h3>  
                                            </div>
                                            <div class="modal-footer">
                                                <!-- DEFINICE FORMULÁŘE PRO EDITACI FOTOGRAFIE -->
                                                <form class="photo-edit-form" method="post" role="form" action="php_solutions/redaction_system.php?tool=photoEdit&id=<?php echo $_smarty_tpl->tpl_vars['photo']->value['id'];?>
&timeStamp=<?php echo $_smarty_tpl->tpl_vars['photo']->value['time_stamp'];?>
&author=<?php echo $_smarty_tpl->tpl_vars['photo']->value['login'];?>
">
                                                    <div class="form-group">
                                                        <label for="photo<?php echo $_smarty_tpl->tpl_vars['photo']->value['id'];?>
-edit-title">Titulek fotografie</label>
                                                        <input class="form-control" id="photo<?php echo $_smarty_tpl->tpl_vars['photo']->value['id'];?>
-edit-title" name="photo<?php echo $_smarty_tpl->tpl_vars['photo']->value['id'];?>
-edit-title" type="text" value="<?php echo $_smarty_tpl->tpl_vars['photo']->value['photo_title'];?>
"/>
                                                        <label for="photo<?php echo $_smarty_tpl->tpl_vars['photo']->value['id'];?>
-edit-galery">Galerie</label>
                                                        <select class="form-control" id="photo<?php echo $_smarty_tpl->tpl_vars['photo']->value['id'];?>
-edit-galery" name="photo<?php echo $_smarty_tpl->tpl_vars['photo']->value['id'];?>
-edit-galery">
                                                            <!-- Výchozí položkou nabídky elementu select příslušná galerii dané fotografie -->
                                                            <option><?php echo $_smarty_tpl->tpl_vars['photo']->value['galery_title'];?>
</option>
                                                                <!-- Následující položky tvoří zbývající galerie -->
                                                            <?php
$_from = $_smarty_tpl->tpl_vars['galeryFiles']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['galeryFile'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['galeryFile']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['galeryFile']->value) {
$_smarty_tpl->tpl_vars['galeryFile']->_loop = true;
$foreach_galeryFile_Sav = $_smarty_tpl->tpl_vars['galeryFile'];
?>
                                                                <?php if ($_smarty_tpl->tpl_vars['galeryFile']->value['galery_title'] != $_smarty_tpl->tpl_vars['photo']->value['galery_title']) {?>
                                                            <option><?php echo $_smarty_tpl->tpl_vars['galeryFile']->value['galery_title'];?>
</option>
                                                                <?php }?>
                                                            <?php
$_smarty_tpl->tpl_vars['galeryFile'] = $foreach_galeryFile_Sav;
}
?>
                                                        </select>
                                                    </div>      
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">ZPĚT</button>
                                                    <button type="submit" class="btn btn-info">POTVRDIT</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </tr>
                            <?php
$_smarty_tpl->tpl_vars['photo'] = $foreach_photo_Sav;
}
?>
                        </table>
                        <!-- DEFINICE INFORMAČNÍHO MODALU O ODSTRANĚNÍ FOTOGRAFIE -->
                        <div class="modal fade" id="photo-remove-info-modal" tabindex="-1" role="dialog" aria-labelledby="photo-remove-info-modal-label">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h3 class="modal-title" id="photo-remove-info-modal-label">ZPRÁVA O STAVU ODEBRÁNÍ FOTOGRAFIE</h3>
                                    </div>
                                    <div class="modal-footer">
                                        <h4></h4>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">ZAVŘÍT</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- DEFINICE INFORMAČNÍHO MODALU O EDITACI FOTOGRAFIE -->
                        <div class="modal fade" id="photo-edit-info-modal" tabindex="-1" role="dialog" aria-labelledby="photo-edit-info-modal-label">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h3 class="modal-title" id="photo-edit-info-modal-label">ZPRÁVA O STAVU EDITACE FOTOGRAFIE</h3>
                                    </div>
                                    <div class="modal-footer">
                                        <h4></h4>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">ZAVŘÍT</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- DEFINICE NÁSTROJE PRO ZALOŽENÍ GALERIE -->
                    <div role="tabpanel" class="tab-pane" id="galery-add">
                        <h3>Založit galerii</h3>
                        <!-- DEFINICE FORMULÁŘE PRO ZALOŽENÍ GALERIE -->
                        <form id="galery-add-form" role="form" method="post" action="php_solutions/redaction_system.php?tool=galeryAdd&authorId=<?php echo $_SESSION['userData']['id'];?>
">
                            <div class="form-group">
                                <label for="galery-add-title">Zadejte název galerie</label>
                                <input class="form-control" type="text" name="galery-add-title" id="galery-add-title"/>
                                <input type="checkbox" name="galery-add-display" class="galery-display" id="galery-add-display" value="1"/>
                                <label class="display-label" for="galery-add-display">Zobrazit galerii v hlavní nabídce</label>
                            </div>
                            <input class="btn btn-lg btn-info" type="submit" value="Založit galerii"/>                          
                        </form>
                        <!-- DEFINICE MODALU PRO INFORMACE O ZALOŽENÍ GALERIE -->
                        <div class="modal fade" id="galery-add-info-modal" tabindex="-1" role="dialog" aria-labelledby="galery-add-info-modal-label">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h3 class="modal-title" id="galery-add-info-modal-label">ZPRÁVA O STAVU ZALOŽENÍ GALERIE</h3>
                                    </div>
                                    <div class="modal-footer">
                                        <h4></h4>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">ZAVŘÍT</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- DEFINICE NÁSTROJE PRO EDITACI A ODSTANĚNÍ GALERIE -->
                    <div role="tabpanel" class="tab-pane" id="galery-edit">
                        <h3>Editovat/odebrat galerii</h3>
                        <!-- DEFINICE TABULKY OBSAHUJÍCÍ EDITOVATELNÉ GALERIE -->
                        <table class="table table-hover galery-edit-table">
                            <!-- Jednotlivé záznamy galerií načítány z databáze, viz. admin_section.php -->
                        <?php
$_from = $_smarty_tpl->tpl_vars['galeriesEditArray']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['galery'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['galery']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['galery']->value) {
$_smarty_tpl->tpl_vars['galery']->_loop = true;
$foreach_galery_Sav = $_smarty_tpl->tpl_vars['galery'];
?>
                            <tr id="galery-row<?php echo $_smarty_tpl->tpl_vars['galery']->value['id'];?>
">
                                <td>
                                    <div class="galery-edit-description">
                                        <h3><?php echo $_smarty_tpl->tpl_vars['galery']->value['galery_title'];?>
</h3>
                                        <p><?php echo $_smarty_tpl->tpl_vars['galery']->value['time_stamp'];?>
 | By: <?php echo $_smarty_tpl->tpl_vars['galery']->value['login'];?>
</p>
                                    </div>
                                    <div class="galery-edit-buttons">
                                        <button class="btn btn-info edit-button" data-toggle="modal" data-target="#galery-edit-modal<?php echo $_smarty_tpl->tpl_vars['galery']->value['id'];?>
">Upravit galerii</button>
                                        <button class="btn btn-danger delete-button" data-toggle="modal" data-target="#galery-remove-modal<?php echo $_smarty_tpl->tpl_vars['galery']->value['id'];?>
"><span class="glyphicon glyphicon-remove"></span>Odebrat galerii</button>
                                    </div>
                                </td>
                                <!-- DEFINICE MODALU S FORMULÁŘEM PRO ODSTRANĚNÍ GALERIE -->
                                <div class="modal fade galery-remove-modal" id="galery-remove-modal<?php echo $_smarty_tpl->tpl_vars['galery']->value['id'];?>
" tabindex="-1" role="dialog" aria-labelledby="galery-remove-modal<?php echo $_smarty_tpl->tpl_vars['galery']->value['id'];?>
">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="close"><span aria-hidden="true">&times;</span></button>
                                                <h3 class="modal-title" id="galery-remove-modal<?php echo $_smarty_tpl->tpl_vars['galery']->value['id'];?>
-label">Odstranit galerii <?php echo $_smarty_tpl->tpl_vars['galery']->value['galery_title'];?>
 z <?php echo $_smarty_tpl->tpl_vars['galery']->value['time_stamp'];?>
?</h3>  
                                            </div>
                                            <div class="modal-footer">
                                                <form class="galery-remove-form" method="post" role="form" action="php_solutions/redaction_system.php?tool=galeryRemove&galeryId=<?php echo $_smarty_tpl->tpl_vars['galery']->value['id'];?>
&galeryTitle=<?php echo $_smarty_tpl->tpl_vars['galery']->value['galery_title'];?>
">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">ZPĚT</button>
                                                        <button type="submit" class="btn btn-danger">POTVRDIT</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- DEFINICE MODALU S FORMULÁŘEM PRO EDITACI GALERIE -->
                                <div class="modal fade galery-edit-modal" id="galery-edit-modal<?php echo $_smarty_tpl->tpl_vars['galery']->value['id'];?>
" tabindex="-1" role="dialog" aria-labelledby="galery-edit-modal<?php echo $_smarty_tpl->tpl_vars['galery']->value['id'];?>
">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="close"><span aria-hidden="true">&times;</span></button>
                                                <h3 class="modal-title" id="galery-edit-modal<?php echo $_smarty_tpl->tpl_vars['galery']->value['id'];?>
-label">Editace galerie <?php echo $_smarty_tpl->tpl_vars['galery']->value['galery_title'];?>
 z <?php echo $_smarty_tpl->tpl_vars['galery']->value['time_stamp'];?>
</h3>  
                                            </div>
                                            <div class="modal-footer">
                                                <form class="galery-edit-form" method="post" role="form" action="php_solutions/redaction_system.php?tool=galeryEdit&id=<?php echo $_smarty_tpl->tpl_vars['galery']->value['id'];?>
&timeStamp=<?php echo $_smarty_tpl->tpl_vars['galery']->value['time_stamp'];?>
&authorId=<?php echo $_smarty_tpl->tpl_vars['galery']->value['login'];?>
">
                                                    <div class="form-group">
                                                        <label for="galery<?php echo $_smarty_tpl->tpl_vars['galery']->value['id'];?>
-edit-title">Titulek galerie</label>
                                                        <input class="form-control" id="galery<?php echo $_smarty_tpl->tpl_vars['galery']->value['id'];?>
-edit-title" name="galery<?php echo $_smarty_tpl->tpl_vars['galery']->value['id'];?>
-edit-title" type="text" value="<?php echo $_smarty_tpl->tpl_vars['galery']->value['galery_title'];?>
"/>
                                                       <input type="checkbox" name="galery<?php echo $_smarty_tpl->tpl_vars['galery']->value['id'];?>
-edit-display" class="galery-display" id="galery<?php echo $_smarty_tpl->tpl_vars['galery']->value['id'];?>
-edit-display" value="1" <?php if ($_smarty_tpl->tpl_vars['galery']->value['display'] == 1) {?>checked<?php }?>/>
                                                       <label class="display-label" for="galery<?php echo $_smarty_tpl->tpl_vars['galery']->value['id'];?>
-edit-display">Zobrazit galerii v hlavní nabídce</label>
                                                    </div>      
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">ZPĚT</button>
                                                    <button type="submit" class="btn btn-info">POTVRDIT</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </tr>
                        <?php
$_smarty_tpl->tpl_vars['galery'] = $foreach_galery_Sav;
}
?>
                        </table>
                        <!-- DEFINICE INFORMAČNÍHO MODALU O ODSTRANĚNÍ GALERIE -->
                        <div class="modal fade" id="galery-remove-info-modal" tabindex="-1" role="dialog" aria-labelledby="galery-remove-info-modal-label">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h3 class="modal-title" id="galery-remove-info-modal-label">ZPRÁVA O STAVU ODEBRÁNÍ GALERIE</h3>
                                    </div>
                                    <div class="modal-footer">
                                        <h4></h4>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">ZAVŘÍT</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- DEFINICE INFORMAČNÍHO MODALU O EDITACI GALERIE -->
                        <div class="modal fade" id="galery-edit-info-modal" tabindex="-1" role="dialog" aria-labelledby="galery-edit-info-modal-label">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h3 class="modal-title" id="galery-edit-info-modal-label">ZPRÁVA O STAVU EDITACE GALERIE</h3>
                                    </div>
                                    <div class="modal-footer">
                                        <h4></h4>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">ZAVŘÍT</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>    
            </div>
            <!-- DEFINICE KATEGORIE NÁSTROJE SPRÁVY BLOGU -->  
            <div role="tabpanel" class="tab-pane" id="blog-tools">
                <h2>NÁSTROJE SPRÁVY BLOGU</h2>
                <ul class="nav nav-tabs" role="tablist">
                    <!-- Bootstrap tabs rovněž realizuje navigaci za využití záložek v dokumentu -->
                    <li role="presentation" class="active"><a href="#article-add" aria-controls="home" role="tab" data-toggle="tab">PŘIDAT ČLÁNEK</a></li>
                    <li role="presentation"><a href="#article-edit" aria-controls="profile" role="tab" data-toggle="tab">EDITOVAT/ODEBRAT ČLÁNEK</a></li>
                    <li role="presentation"><a href="#category-add" aria-controls="settings" role="tab" data-toggle="tab">ZALOŽIT KATEGORII ČLÁNKŮ</a></li>
                    <li role="presentation"><a href="#category-edit" aria-controls="settings" role="tab" data-toggle="tab">EDITOVAT/ODEBRAT KATEGORII ČLÁNKŮ</a></li>
                </ul>
                <div class="tab-content">
                    <!-- DEFINICE NÁSTROJE PRO VYTVOŘENÍ NOVÉHO ČLÁNKU -->
                    <div role="tabpanel" class="tab-pane active" id="article-add">
                        <h3>Vytvořit článek</h3>
                        <!-- DEFINICE FORMULÁŘE PRO VYTVOŘENÍ ČLÁNKU -->
                        <form id="article-add-form" role="form" method="post" action="php_solutions/redaction_system.php?tool=articleAdd&authorId=<?php echo $_SESSION['userData']['id'];?>
" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="article-add-title">Zadejte titulek článku</label>
                                <input class="form-control" type="text" name="article-add-title" id="article-add-title"/>
                                <label for="article-add-content">Vlastní obsah článku</label>
                                <textarea class="form-control" id="article-add-content" name="article-add-content"></textarea>
                                <label for="article-add-category">Zvolte kategorii</label>
                                <select name="article-add-category" id="article-add-category" class="form-control">
                                    <!-- Jednotlivé kategorie jako možnosti elementu select načítány z databáze, viz. database_load.php -->
                                    <?php
$_from = $_smarty_tpl->tpl_vars['categoryFiles']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['categoryFile'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['categoryFile']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['categoryFile']->value) {
$_smarty_tpl->tpl_vars['categoryFile']->_loop = true;
$foreach_categoryFile_Sav = $_smarty_tpl->tpl_vars['categoryFile'];
?>
                                    <option><?php echo $_smarty_tpl->tpl_vars['categoryFile']->value['category_title'];?>
</option>
                                    <?php
$_smarty_tpl->tpl_vars['categoryFile'] = $foreach_categoryFile_Sav;
}
?>
                                </select>
                                <input id="article-add-title-photo-file" class="input-file" type="file" name="article-add-title-photo-file"/>
                                <label class="article-add-title-photo-label1" for="article-add-title-photo-file">Zvolte titulní fotografii</label>
                                <label class="btn btn-default article-add-title-photo-label2" id="file-write-1" for="article-add-title-photo-file"><span class="glyphicon glyphicon-upload"></span><span>Vyberte soubor</span></label>
                                <input id="article-add-photos-files" class="input-file" type="file" name="article-add-photos-files[]" multiple/>
                                <label class="article-add-title-photo-label1" for="article-add-photos-file">Zvolte fotografie uvnitř článku</label>
                                <label class="btn btn-default article-add-title-photo-label2" id="file-write-2" for="article-add-photos-files"><span class="glyphicon glyphicon-upload"></span><span>Vyberte soubor(y)</span></label>
                                <input class="btn btn-lg btn-info" type="submit" value="Přidat článek"/>
                            </div>
                        </form>
                        <!-- DEFINICE INFORMAČNÍHO MODALU PRO NÁSTROJ PŘIDÁNÍ ČLÁNKU -->
                        <div class="modal fade" id="article-add-info-modal" tabindex="-1" role="dialog" aria-labelledby="article-add-info-modal-label">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h3 class="modal-title" id="article-add-info-modal-label">ZPRÁVA O STAVU UPLOADU</h3>
                                    </div>
                                    <div class="modal-footer">
                                        <h4></h4>
                                        <div class="image"></div>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">ZAVŘÍT</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- DEFINICE NÁSTROJE PRO EDITACI A ODEBRÁNÍ ČLÁNKU -->
                    <div role="tabpanel" class="tab-pane" id="article-edit">
                        <h3>Editovat/odebrat článek</h3>
                        <!-- DEFINICE TABULKY OBSAHUJÍCÍ EDITOVATELNÉ ČLÁNKY -->
                        <table class="table table-hover article-edit-table">
                            <!-- Jednotlivé záznamy článků načítány z databáze, viz. admin_section.php -->
                        <?php
$_from = $_smarty_tpl->tpl_vars['articlesEditArray']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['article'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['article']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['article']->value) {
$_smarty_tpl->tpl_vars['article']->_loop = true;
$foreach_article_Sav = $_smarty_tpl->tpl_vars['article'];
?>
                            <tr id="article-row<?php echo $_smarty_tpl->tpl_vars['article']->value['id'];?>
">
                                <td>
                                    <div class="article-edit-bg" style="background-image: url(<?php echo $_smarty_tpl->tpl_vars['article']->value['title_photo'];?>
)"></div>
                                    <div class="article-edit-description">
                                        <h3><?php echo $_smarty_tpl->tpl_vars['article']->value['article_title'];?>
</h3>
                                        <p><?php echo $_smarty_tpl->tpl_vars['article']->value['time_stamp'];?>
 | By: <?php echo $_smarty_tpl->tpl_vars['article']->value['login'];?>
 | Kategorie: <?php echo $_smarty_tpl->tpl_vars['article']->value['category_title'];?>
</p>
                                    </div>
                                    <div class="article-edit-buttons">
                                        <button class="btn btn-info edit-button" data-toggle="modal" data-target="#article-edit-modal<?php echo $_smarty_tpl->tpl_vars['article']->value['id'];?>
">Upravit článek</button>
                                        <button class="btn btn-danger delete-button" data-toggle="modal" data-target="#article-remove-modal<?php echo $_smarty_tpl->tpl_vars['article']->value['id'];?>
"><span class="glyphicon glyphicon-remove"></span>Odebrat článek</button>
                                    </div>
                                </td>
                                <!-- DEFINICE MODALU S FORMULÁŘEM PRO ODSTRANĚNÍ ČLÁNKU -->
                                <div class="modal fade article-remove-modal" id="article-remove-modal<?php echo $_smarty_tpl->tpl_vars['article']->value['id'];?>
" tabindex="-1" role="dialog" aria-labelledby="article-remove-modal<?php echo $_smarty_tpl->tpl_vars['article']->value['id'];?>
">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="close"><span aria-hidden="true">&times;</span></button>
                                                <h3 class="modal-title" id="article-remove-modal<?php echo $_smarty_tpl->tpl_vars['article']->value['id'];?>
-label">Odstranit článek <?php echo $_smarty_tpl->tpl_vars['article']->value['article_title'];?>
 z <?php echo $_smarty_tpl->tpl_vars['article']->value['time_stamp'];?>
?</h3>  
                                            </div>
                                            <div class="modal-footer">
                                                <form class="article-remove-form" method="post" role="form" action="php_solutions/redaction_system.php?tool=articleRemove&articleId=<?php echo $_smarty_tpl->tpl_vars['article']->value['id'];?>
&articleTitle=<?php echo $_smarty_tpl->tpl_vars['article']->value['article_title'];?>
&articleTitlePhoto=<?php echo $_smarty_tpl->tpl_vars['article']->value['title_photo'];?>
">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">ZPĚT</button>
                                                    <button type="submit" class="btn btn-danger">POTVRDIT</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- DEFINICE MODALU S FORMULÁŘEM PRO EDITACI ČLÁNKU -->
                                <div class="modal fade article-edit-modal" id="article-edit-modal<?php echo $_smarty_tpl->tpl_vars['article']->value['id'];?>
" tabindex="-1" role="dialog" aria-labelledby="article-edit-modal<?php echo $_smarty_tpl->tpl_vars['article']->value['id'];?>
">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="close"><span aria-hidden="true">&times;</span></button>
                                                <h3 class="modal-title" id="article-edit-modal<?php echo $_smarty_tpl->tpl_vars['article']->value['id'];?>
-label">Editace článku <?php echo $_smarty_tpl->tpl_vars['article']->value['article_title'];?>
 z <?php echo $_smarty_tpl->tpl_vars['article']->value['time_stamp'];?>
</h3>  
                                            </div>
                                            <div class="modal-footer">
                                                <form class="article-edit-form" method="post" role="form" action="php_solutions/redaction_system.php?tool=articleEdit&id=<?php echo $_smarty_tpl->tpl_vars['article']->value['id'];?>
&timeStamp=<?php echo $_smarty_tpl->tpl_vars['article']->value['time_stamp'];?>
&authorId=<?php echo $_smarty_tpl->tpl_vars['article']->value['login'];?>
">
                                                    <div class="form-group">
                                                        <label for="article<?php echo $_smarty_tpl->tpl_vars['article']->value['id'];?>
-edit-title">Titulek článku</label>
                                                        <input class="form-control" id="article<?php echo $_smarty_tpl->tpl_vars['article']->value['id'];?>
-edit-title" name="article<?php echo $_smarty_tpl->tpl_vars['article']->value['id'];?>
-edit-title" type="text" value="<?php echo $_smarty_tpl->tpl_vars['article']->value['article_title'];?>
"/>
                                                        <label for="article<?php echo $_smarty_tpl->tpl_vars['article']->value['id'];?>
-edit-content">Obsah článku</label>
                                                        <textarea class="form-control" id="article<?php echo $_smarty_tpl->tpl_vars['article']->value['id'];?>
-edit-content" name="article<?php echo $_smarty_tpl->tpl_vars['article']->value['id'];?>
-edit-content"><?php echo $_smarty_tpl->tpl_vars['article']->value['content'];?>
</textarea>
                                                        <label for="article<?php echo $_smarty_tpl->tpl_vars['article']->value['id'];?>
-edit-category">Kategorie článku</label>
                                                        <select class="form-control" id="article<?php echo $_smarty_tpl->tpl_vars['article']->value['id'];?>
-edit-category" name="article<?php echo $_smarty_tpl->tpl_vars['article']->value['id'];?>
-edit-category">    
                                                            <!-- Výchozí položkou nabídky elementu select příslušná kategorie daného článku -->
                                                            <option><?php echo $_smarty_tpl->tpl_vars['article']->value['category_title'];?>
</option>
                                                                <!-- Následující položky tvoří zbývající kategorie -->
                                                            <?php
$_from = $_smarty_tpl->tpl_vars['categoryFiles']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['categoryFile'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['categoryFile']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['categoryFile']->value) {
$_smarty_tpl->tpl_vars['categoryFile']->_loop = true;
$foreach_categoryFile_Sav = $_smarty_tpl->tpl_vars['categoryFile'];
?>
                                                                <?php if ($_smarty_tpl->tpl_vars['categoryFile']->value['category_title'] != $_smarty_tpl->tpl_vars['article']->value['category_title']) {?>
                                                            <option><?php echo $_smarty_tpl->tpl_vars['categoryFile']->value['category_title'];?>
</option>
                                                                <?php }?>
                                                            <?php
$_smarty_tpl->tpl_vars['categoryFile'] = $foreach_categoryFile_Sav;
}
?>
                                                        </select>
                                                    </div>
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">ZPĚT</button>
                                                    <button type="submit" class="btn btn-info">POTVRDIT</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </tr>
                        <?php
$_smarty_tpl->tpl_vars['article'] = $foreach_article_Sav;
}
?>
                        </table>
                        <!-- DEFINICE INFORMAČNÍHO MODALU O ODSTRANĚNÍ ČLÁNKU -->
                        <div class="modal fade" id="article-remove-info-modal" tabindex="-1" role="dialog" aria-labelledby="article-remove-info-modal-label">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h3 class="modal-title" id="article-remove-info-modal-label">ZPRÁVA O STAVU ODEBRÁNÍ ČLÁNKU</h3>
                                    </div>
                                    <div class="modal-footer">
                                        <h4></h4>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">ZAVŘÍT</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- DEFINICE INFORMAČNÍHO MODALU O EDITACI ČLÁNKU -->
                        <div class="modal fade" id="article-edit-info-modal" tabindex="-1" role="dialog" aria-labelledby="article-edit-info-modal-label">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h3 class="modal-title" id="article-edit-info-modal-label">ZPRÁVA O STAVU EDITACE ČLÁNKU</h3>
                                    </div>
                                    <div class="modal-footer">
                                        <h4></h4>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">ZAVŘÍT</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- DEFINICE NÁSTROJE PRO ZALOŽENÍ KATEGORIE ČLÁNKŮ -->
                    <div role="tabpanel" class="tab-pane" id="category-add">
                        <h3>Založit kategorii článků</h3>
                        <!-- DEFINICE FORMULÁŘE PRO ZALOŽENÍ KATEGORIE -->
                        <form id="category-add-form" role="form" method="post" action="php_solutions/redaction_system.php?tool=categoryAdd&authorId=<?php echo $_SESSION['userData']['id'];?>
">
                            <div class="form-group">
                                <label for="category-add-title">Zadejte název kategorie</label>
                                <input class="form-control" type="text" name="category-add-title" id="category-add-title"/>
                                <input type="checkbox" name="category-add-display" class="category-display" id="category-add-display" value="1"/>
                                <label class="display-label" for="category-add-display">Zobrazit kategorii v hlavní nabídce</label>
                            </div>
                            <input class="btn btn-lg btn-info" id="article-title-photo-file" type="submit" value="Založit kategorii"/>
                        </form>
                        <!-- DEFINICE MODALU PRO INFORMACE O ZALOŽENÍ KATEGORIE -->
                        <div class="modal fade" id="category-add-info-modal" tabindex="-1" role="dialog" aria-labelledby="category-add-info-modal-label">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h3 class="modal-title" id="category-add-info-modal-label">ZPRÁVA O STAVU ZALOŽENÍ KATEGORIE</h3>
                                    </div>
                                    <div class="modal-footer">
                                        <h4></h4>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">ZAVŘÍT</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- DEFINICE NÁSTROJE PRO EDITACI A ODEBRÁNÍ KATEGORIÍ ČLÁNKŮ -->
                    <div role="tabpanel" class="tab-pane" id="category-edit">
                        <h3>Editovat/odebrat kategorii článků</h3>
                        <!-- DEFINICE TABULKY OBSAHUJÍCÍ EDITOVATELNÉ KATEGORIE -->
                        <table class="table table-hover category-edit-table">
                            <!-- Jednotlivé záznamy kategorií načítány z databáze, viz. admin_section.php -->
                        <?php
$_from = $_smarty_tpl->tpl_vars['categoriesEditArray']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['category'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['category']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['category']->value) {
$_smarty_tpl->tpl_vars['category']->_loop = true;
$foreach_category_Sav = $_smarty_tpl->tpl_vars['category'];
?>
                            <tr id="category-row<?php echo $_smarty_tpl->tpl_vars['category']->value['id'];?>
">
                                <td>
                                    <div class="category-edit-description">
                                        <h3><?php echo $_smarty_tpl->tpl_vars['category']->value['category_title'];?>
</h3>
                                        <p><?php echo $_smarty_tpl->tpl_vars['category']->value['time_stamp'];?>
 | By: <?php echo $_smarty_tpl->tpl_vars['category']->value['login'];?>
</p>
                                    </div>
                                    <div class="category-edit-buttons">
                                        <button class="btn btn-info edit-button" data-toggle="modal" data-target="#category-edit-modal<?php echo $_smarty_tpl->tpl_vars['category']->value['id'];?>
">Upravit kategorii</button>
                                        <button class="btn btn-danger delete-button" data-toggle="modal" data-target="#category-remove-modal<?php echo $_smarty_tpl->tpl_vars['category']->value['id'];?>
"><span class="glyphicon glyphicon-remove"></span>Odebrat kategorii</button>
                                    </div>
                                </td>
                                <!-- DEFINICE MODALU S FORMULÁŘEM PRO ODSTRANĚNÍ KATEGORIE -->
                                <div class="modal fade category-remove-modal" id="category-remove-modal<?php echo $_smarty_tpl->tpl_vars['category']->value['id'];?>
" tabindex="-1" role="dialog" aria-labelledby="category-remove-modal<?php echo $_smarty_tpl->tpl_vars['category']->value['id'];?>
">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="close"><span aria-hidden="true">&times;</span></button>
                                                <h3 class="modal-title" id="category-remove-modal<?php echo $_smarty_tpl->tpl_vars['category']->value['id'];?>
-label">Odstranit kategorii <?php echo $_smarty_tpl->tpl_vars['category']->value['category_title'];?>
 z <?php echo $_smarty_tpl->tpl_vars['category']->value['time_stamp'];?>
?</h3>  
                                            </div>
                                            <div class="modal-footer">
                                                <form class="category-remove-form" method="post" role="form" action="php_solutions/redaction_system.php?tool=categoryRemove&categoryId=<?php echo $_smarty_tpl->tpl_vars['category']->value['id'];?>
&categoryTitle=<?php echo $_smarty_tpl->tpl_vars['category']->value['category_title'];?>
">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">ZPĚT</button>
                                                    <button type="submit" class="btn btn-danger">POTVRDIT</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- DEFINICE MODALU S FORMULÁŘEM PRO EDITACI KATEGORIE -->
                                <div class="modal fade category-edit-modal" id="category-edit-modal<?php echo $_smarty_tpl->tpl_vars['category']->value['id'];?>
" tabindex="-1" role="dialog" aria-labelledby="category-edit-modal<?php echo $_smarty_tpl->tpl_vars['category']->value['id'];?>
">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="close"><span aria-hidden="true">&times;</span></button>
                                                <h3 class="modal-title" id="category-edit-modal<?php echo $_smarty_tpl->tpl_vars['category']->value['id'];?>
-label">Editace kategorie <?php echo $_smarty_tpl->tpl_vars['category']->value['category_title'];?>
 z <?php echo $_smarty_tpl->tpl_vars['category']->value['time_stamp'];?>
</h3>  
                                            </div>
                                            <div class="modal-footer">
                                                <form class="category-edit-form" method="post" role="form" action="php_solutions/redaction_system.php?tool=categoryEdit&id=<?php echo $_smarty_tpl->tpl_vars['category']->value['id'];?>
&timeStamp=<?php echo $_smarty_tpl->tpl_vars['category']->value['time_stamp'];?>
&authorId=<?php echo $_smarty_tpl->tpl_vars['category']->value['login'];?>
">
                                                    <div class="form-group">
                                                        <label for="category<?php echo $_smarty_tpl->tpl_vars['category']->value['id'];?>
-edit-title">Titulek kategorie</label>
                                                        <input class="form-control" id="category<?php echo $_smarty_tpl->tpl_vars['category']->value['id'];?>
-edit-title" name="category<?php echo $_smarty_tpl->tpl_vars['category']->value['id'];?>
-edit-title" type="text" value="<?php echo $_smarty_tpl->tpl_vars['category']->value['category_title'];?>
"/>
                                                       <input type="checkbox" name="category<?php echo $_smarty_tpl->tpl_vars['category']->value['id'];?>
-edit-display" class="category-display" id="category<?php echo $_smarty_tpl->tpl_vars['category']->value['id'];?>
-edit-display" value="1" <?php if ($_smarty_tpl->tpl_vars['category']->value['display'] == 1) {?>checked<?php }?>/>
                                                       <label class="display-label" for="category<?php echo $_smarty_tpl->tpl_vars['category']->value['id'];?>
-edit-display">Zobrazit kategorii v hlavní nabídce</label>
                                                    </div>      
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">ZPĚT</button>
                                                    <button type="submit" class="btn btn-info">POTVRDIT</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </tr>
                        <?php
$_smarty_tpl->tpl_vars['category'] = $foreach_category_Sav;
}
?>
                        </table>
                        <!-- DEFINICE INFORMAČNÍHO MODALU O ODSTRANĚNÍ KATEGORIE -->
                        <div class="modal fade" id="category-remove-info-modal" tabindex="-1" role="dialog" aria-labelledby="category-remove-info-modal-label">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h3 class="modal-title" id="category-remove-info-modal-label">ZPRÁVA O STAVU ODEBRÁNÍ KATEGORIE</h3>
                                    </div>
                                    <div class="modal-footer">
                                        <h4></h4>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">ZAVŘÍT</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- DEFINICE INFORMAČNÍHO MODALU O EDITACI KATEGORIE -->
                        <div class="modal fade" id="category-edit-info-modal" tabindex="-1" role="dialog" aria-labelledby="category-edit-info-modal-label">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h3 class="modal-title" id="category-edit-info-modal-label">ZPRÁVA O STAVU EDITACE KATEGORIE</h3>
                                    </div>
                                    <div class="modal-footer">
                                        <h4></h4>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">ZAVŘÍT</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="user-content-tools">
                <h2>NÁSTROJE UŽIVATELSKÉ EDITACE</h2>
                <!-- Navigace mezi jednotlivými nástroji v rámci jedné kategorie realizována za využití bootstrap tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <!-- Bootstrap tabs rovněž realizuje navigaci za využití záložek v dokumentu -->
                    <li role="presentation" class="active"><a href="#main-menu-titles" aria-controls="main-menu-titles" role="tab" data-toggle="tab">EDITOVAT HLAVIČKU</a></li>
                    <li role="presentation"><a href="#opening-layer-modification" aria-controls="opening-layer-modification" role="tab" data-toggle="tab">EDITOVAT ÚVODNÍ VRSTVU</a></li>
                    <li role="presentation"><a href="#footer-user-variable-content" aria-controls="footer-user-variable-content" role="tab" data-toggle="tab">EDITOVAT PATIČKU</a></li>
                    <li role="presentation"><a href="#contacts-editation" aria-controls="contacts-editation" role="tab" data-toggle="tab">EDITOVAT KONTAKTY</a></li>
                </ul>
                <div class="tab-content">
                    <!-- DEFINICE NÁSTROJE PRO EDITACI POLOŽEK HLAVIČKY -->
                    <div role="tabpanel" class="tab-pane active" id="main-menu-titles">
                        <h3>Editovat hlavičku</h3>
                        <!-- DEFINICE FORMULÁŘE PRO EDITACI POLOŽEK HLAVIČKY -->
                        <form id="header-edit-form" role="form" method="post" action="php_solutions/redaction_system.php?tool=headerEdit">
                            <div class="form-group">
                                <label for="logo-title">Titulek loga webové stránky</label>
                                <input class="form-control" type="text" name="logo-title" id="logo-title" value="<?php echo $_smarty_tpl->tpl_vars['userContent']->value[0]['content'];?>
"/>
                                <label for="portfolio-title">Titulek portfolia</label>
                                <input class="form-control" type="text" name="portfolio-title" id="portfolio-title" value="<?php echo $_smarty_tpl->tpl_vars['userContent']->value[1]['content'];?>
"/>
                                <label for="galery-title">Titulek fotogalerie</label>
                                <input class="form-control" type="text" name="galery-title" id="galery-title" value="<?php echo $_smarty_tpl->tpl_vars['userContent']->value[2]['content'];?>
"/>
                                <label for="blog-title">Titulek blogu</label>
                                <input class="form-control" type="text" name="blog-title" id="blog-title" value="<?php echo $_smarty_tpl->tpl_vars['userContent']->value[3]['content'];?>
"/>
                                <label for="contacts-title">Titulek kontaktů</label>
                                <input class="form-control" type="text" name="contacts-title" id="contacts-title" value="<?php echo $_smarty_tpl->tpl_vars['userContent']->value[4]['content'];?>
"/>
                                <div class="header-edit-buttons">
                                    <input class="btn btn-lg btn-info" type="submit" value="Potvrdit"/>
                                    <input class="btn btn-lg btn-default" id="header-edit-default" type="button" value="Obnovit výchozí"/>
                                </div>
                            </div>
                        </form>
                        <!-- DEFINICE INFORMAČNÍHO MODALU PRO NÁSTROJ EDITACE POLOŽEK HLAVIČKY -->
                        <div class="modal fade" id="header-edit-info-modal" tabindex="-1" role="dialog" aria-labelledby="header-edit-info-modal-label">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h3 class="modal-title" id="header-edit-info-modal-label">ZPRÁVA O STAVU EDITACE HLAVIČKY</h3>
                                    </div>
                                    <div class="modal-footer">
                                        <h4></h4>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">ZAVŘÍT</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- DEFINICE NÁSTROJE PRO EDITACI ÚVODNÍ VRSTVY -->
                    <div role="tabpanel" class="tab-pane" id="opening-layer-modification">
                        <h3>Editovat úvodní vrstvu</h3>
                        <!-- DEFINICE FORMULÁŘE PRO EDITACI POLOŽEK HLAVIČKY -->
                        <form id="opening-layer-edit-form" role="form" method="post" action="php_solutions/redaction_system.php?tool=openingLayerEdit">
                            <div class="form-group">
                                <label for="opening-layer-title">Titulek úvodní vrstvy</label>
                                <input class="form-control" type="text" name="opening-layer-title" id="opening-layer-title" value="<?php echo $_smarty_tpl->tpl_vars['userContent']->value[5]['content'];?>
"/>
                                <label for="opening-layer-content">Článek úvodní vrstvy</label>
                                <textarea class="form-control" name="opening-layer-content" id="opening-layer-content"><?php echo $_smarty_tpl->tpl_vars['userContent']->value[6]['content'];?>
</textarea>
                                <label for="opening-layer-background">Pozadí uvítací vrstvy</label>
                                <div class="opening-layer-background-underlay">
                                    <div class="opening-layer-background" data-toggle="modal" data-target="#opening-layer-bg-edit-modal" style="background-image: url(<?php echo $_smarty_tpl->tpl_vars['openingLayerCurrentBg']->value[0]['bg_path'];?>
)"></div>
                                    <input name="opening-layer-background" id="opening-layer-background" value="<?php echo $_SESSION['userBackgrounds'][0]['bg_path'];?>
"/>
                                </div>
                                <div class="opening-layer-edit-button">
                                    <input class="btn btn-lg btn-info" type="submit" value="Potvrdit"/>
                                </div>
                            </div>
                        </form>
                        <!-- DEFINICE MODALU PRO EDITACI POZADÍ ÚVODNÍ VRSTVY -->
                        <div class="modal fade" id="opening-layer-bg-edit-modal" tabindex="-1" role="dialog" aria-labelledby="opening-layer-bg-edit-modal-label">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h3 class="modal-title" id="opening-layer-bg-edit-modal-label">EDITACE POZADÍ UVÍTACÍ VRSTVY</h3>
                                    </div>
                                    <div class="modal-footer">
                                        <label>Zvolit pozadí úvodní vrstvy</label>
                                        <?php
$_from = $_smarty_tpl->tpl_vars['photosEditArray']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['photo'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['photo']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['photo']->value) {
$_smarty_tpl->tpl_vars['photo']->_loop = true;
$foreach_photo_Sav = $_smarty_tpl->tpl_vars['photo'];
?>
                                            <div class="opening-layer-background-underlay">
                                                <div class="opening-layer-bg-edit-list" style="background-image: url(<?php echo $_smarty_tpl->tpl_vars['photo']->value['photo_path'];?>
)"></div>
                                                <p><?php echo $_smarty_tpl->tpl_vars['photo']->value['photo_path'];?>
</p>
                                            </div>
                                        <?php
$_smarty_tpl->tpl_vars['photo'] = $foreach_photo_Sav;
}
?>
                                    <div class="opening-layer-bg-edit-buttons">
                                        <button type="button" class="btn btn-info" data-dismiss="modal">POTVRDIT</button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">ZAVŘÍT</button>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- DEFINICE INFORMAČNÍHO MODALU PRO NÁSTROJ EDITACE ÚVODNÍ VRSTVY -->
                        <div class="modal fade" id="opening-layer-edit-info-modal" tabindex="-1" role="dialog" aria-labelledby="opening-layer-edit-info-modal-label">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h3 class="modal-title" id="opening-layer-edit-info-modal-label">ZPRÁVA O STAVU EDITACE ÚVODNÍ VRSTVY</h3>
                                    </div>
                                    <div class="modal-footer">
                                        <h4></h4>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">ZAVŘÍT</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- DEFINICE NÁSTROJE PRO EDITACI POLOŽEK PATIČKY -->
                    <div role="tabpanel" class="tab-pane" id="footer-user-variable-content">
                        <h3>Editovat patičku</h3>
                        <!-- DEFINICE FORMULÁŘE PRO EDITACI POLOŽEK PATIČKY -->
                        <form id="footer-edit-form" role="form" method="post" action="php_solutions/redaction_system.php?tool=footerEdit">
                            <div class="form-group">
                                <label for="first-section-title">Titulek prvního oddílu</label>
                                <input class="form-control" type="text" name="first-section-title" id="first-section-title" value="<?php echo $_smarty_tpl->tpl_vars['userContent']->value[7]['title'];?>
"/>
                                <label for="first-section-content">Obsah článku prvního oddílu</label>
                                <textarea class="form-control" id="firt-section-content" name="first-section-content"><?php echo $_smarty_tpl->tpl_vars['userContent']->value[7]['content'];?>
</textarea>
                                <label for="second-section-title">Titulek druhého oddílu</label>
                                <input class="form-control" type="text" name="second-section-title" id="second-section-title" value="<?php echo $_smarty_tpl->tpl_vars['userContent']->value[8]['title'];?>
"/>
                                <label for="second-section-content">Obsah článku druhého oddílu</label>
                                <textarea class="form-control" id="second-section-content" name="second-section-content"><?php echo $_smarty_tpl->tpl_vars['userContent']->value[8]['content'];?>
</textarea>
                                <label for="third-section-title">Titulek třetího oddílu</label>
                                <input class="form-control" type="text" name="third-section-title" id="third-section-title" value="<?php echo $_smarty_tpl->tpl_vars['userContent']->value[9]['title'];?>
"/>
                                <label for="third-section-content">Obsah článku třetího oddílu</label>
                                <textarea class="form-control" id="third-section-content" name="third-section-content"><?php echo $_smarty_tpl->tpl_vars['userContent']->value[9]['content'];?>
</textarea>
                                
                                <div class="footer-edit-buttons">
                                    <input class="btn btn-lg btn-info" type="submit" value="Potvrdit"/>
                                    <input class="btn btn-lg btn-default" id="footer-edit-default" type="button" value="Obnovit výchozí titulky"/>
                                </div>
                            </div>
                        </form>
                        <!-- DEFINICE INFORMAČNÍHO MODALU PRO NÁSTROJ EDITACE POLOŽEK PATIČKY -->
                        <div class="modal fade" id="footer-edit-info-modal" tabindex="-1" role="dialog" aria-labelledby="footer-edit-info-modal-label">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h3 class="modal-title" id="footer-edit-info-modal-label">ZPRÁVA O STAVU EDITACE PATIČKY</h3>
                                    </div>
                                    <div class="modal-footer">
                                        <h4></h4>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">ZAVŘÍT</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- DEFINICE NÁSTROJE PRO EDITACI KONTAKTŮ -->
                    <div role="tabpanel" class="tab-pane" id="contacts-editation">
                        <h3>Editovat kontakty</h3>
                        <!-- DEFINICE FORMULÁŘE PRO EDITACI POLOŽEK KONTAKTŮ -->
                        <form id="contacts-edit-form" role="form" method="post" action="php_solutions/redaction_system.php?tool=contactsEdit">
                            <div class="form-group">
                                <h4 class="contacts-section">Základní údaje</h4>
                                <?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['i']->step = 1;$_smarty_tpl->tpl_vars['i']->total = (int) ceil(($_smarty_tpl->tpl_vars['i']->step > 0 ? 13+1 - (10) : 10-(13)+1)/abs($_smarty_tpl->tpl_vars['i']->step));
if ($_smarty_tpl->tpl_vars['i']->total > 0) {
for ($_smarty_tpl->tpl_vars['i']->value = 10, $_smarty_tpl->tpl_vars['i']->iteration = 1;$_smarty_tpl->tpl_vars['i']->iteration <= $_smarty_tpl->tpl_vars['i']->total;$_smarty_tpl->tpl_vars['i']->value += $_smarty_tpl->tpl_vars['i']->step, $_smarty_tpl->tpl_vars['i']->iteration++) {
$_smarty_tpl->tpl_vars['i']->first = $_smarty_tpl->tpl_vars['i']->iteration == 1;$_smarty_tpl->tpl_vars['i']->last = $_smarty_tpl->tpl_vars['i']->iteration == $_smarty_tpl->tpl_vars['i']->total;?>
                                <label for="item-<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
-content"><?php echo $_smarty_tpl->tpl_vars['userContent']->value[$_smarty_tpl->tpl_vars['i']->value]['title'];?>
</label>
                                <input class="form-control" type="text" name="item-<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
-content" id="item-<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
-content" value="<?php echo $_smarty_tpl->tpl_vars['userContent']->value[$_smarty_tpl->tpl_vars['i']->value]['content'];?>
"/>
                                <?php }} ?>
                                <h4 class="contacts-section">Sociální sítě</h4>
                                <?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['i']->step = 1;$_smarty_tpl->tpl_vars['i']->total = (int) ceil(($_smarty_tpl->tpl_vars['i']->step > 0 ? 18+1 - (14) : 14-(18)+1)/abs($_smarty_tpl->tpl_vars['i']->step));
if ($_smarty_tpl->tpl_vars['i']->total > 0) {
for ($_smarty_tpl->tpl_vars['i']->value = 14, $_smarty_tpl->tpl_vars['i']->iteration = 1;$_smarty_tpl->tpl_vars['i']->iteration <= $_smarty_tpl->tpl_vars['i']->total;$_smarty_tpl->tpl_vars['i']->value += $_smarty_tpl->tpl_vars['i']->step, $_smarty_tpl->tpl_vars['i']->iteration++) {
$_smarty_tpl->tpl_vars['i']->first = $_smarty_tpl->tpl_vars['i']->iteration == 1;$_smarty_tpl->tpl_vars['i']->last = $_smarty_tpl->tpl_vars['i']->iteration == $_smarty_tpl->tpl_vars['i']->total;?>
                                <label for="item-<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
-content"><?php echo $_smarty_tpl->tpl_vars['userContent']->value[$_smarty_tpl->tpl_vars['i']->value]['title'];?>
</label>
                                <input class="form-control" type="text" name="item-<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
-content" id="item-<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
-content" value="<?php echo $_smarty_tpl->tpl_vars['userContent']->value[$_smarty_tpl->tpl_vars['i']->value]['content'];?>
"/>
                                <label for="item-<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
-link">Adresa profilu</label>
                                <input class="form-control" type="text" name="item-<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
-link" id="item-<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
-link" value="<?php echo $_smarty_tpl->tpl_vars['userContent']->value[$_smarty_tpl->tpl_vars['i']->value]['link'];?>
"/>
                                <?php }} ?>
                                <div class="footer-edit-buttons">
                                    <input class="btn btn-lg btn-info" type="submit" value="Potvrdit"/>
                                </div>
                            </div>
                        </form>
                        <!-- DEFINICE INFORMAČNÍHO MODALU PRO NÁSTROJ EDITACE KONTAKTŮ -->
                        <div class="modal fade" id="contacts-edit-info-modal" tabindex="-1" role="dialog" aria-labelledby="contacts-edit-info-modal-label">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h3 class="modal-title" id="contacts-edit-info-modal-label">ZPRÁVA O STAVU EDITACE KONTAKTŮ</h3>
                                    </div>
                                    <div class="modal-footer">
                                        <h4></h4>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">ZAVŘÍT</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <footer>
        <h3>CREATED BY &copy; PETR PONDĚLÍK 2016</h3>
    </footer>


    <!-- PŘIPOJENÍ JQUERY -->
    <?php echo '<script'; ?>
 src="https://code.jquery.com/jquery-2.1.1-rc2.min.js"><?php echo '</script'; ?>
>
        
    <!-- PŘIPOJENÍ PLUGINU PRO AJAX -->
    <?php echo '<script'; ?>
 src="http://malsup.github.com/jquery.form.js"><?php echo '</script'; ?>
>

    <!-- PŘIPOJENÍ BOOTSTRAP JS PLUGINŮ -->
    <?php echo '<script'; ?>
 src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"><?php echo '</script'; ?>
>

    <!-- PŘIPOJENÍ SCRIPTU OVLÁDAJÍCÍHO PRŮHLEDNOST HLAVNÍ NABÍDKY A NABÍDKU PRO MALÁ ROZLIŠENÍ-->
    <?php echo '<script'; ?>
 src="js_solutions/header_functions.js"><?php echo '</script'; ?>
>
    
    <!-- PŘIPOJENÍ SCRIPTU OVLÁDAJÍCÍHO AJAX UDÁLOSTI NA STRANĚ KLIENTA -->
    <?php echo '<script'; ?>
 src="js_solutions/redaction_system_ajax.js"><?php echo '</script'; ?>
>
        
    <!-- PŘIPOJENÍ SCRIPTU OVLÁDAJÍCÍHO DYNAMICKÉ UDÁLOSTI FRONTENDU -->
    <?php echo '<script'; ?>
 src="js_solutions/redaction_system_frontend_dynamic.js"><?php echo '</script'; ?>
>
    
    <!-- SCRIPT OVLÁDAJÍCÍ LOADING SCREEN PRO DOM STRUKTURU DOKUMENTU -->
    <?php echo '<script'; ?>
>
        $(window).load(function(){
            $(".cssloader-wrapper").remove();
            $("html").css("overflow","visible");
        });
    <?php echo '</script'; ?>
>

    <?php } else { ?>
        
    <p>PRO PŘÍSTUP DO ADMINISTRAČNÍHO ROZHRANÍ SE MUSÍTE PŘIHLÁSIT</p>

    <?php }?>



</body>
</html><?php }
}
?>