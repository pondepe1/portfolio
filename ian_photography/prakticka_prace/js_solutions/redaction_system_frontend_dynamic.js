$(document).ready(function(){
            
            //SCRIPT OVLÁDAJÍCÍ ZOBRAZOVÁNÍ NÁHLEDŮ FOTOGRAFIÍ V NÁSTROJE EDITACE FOTOGRAFIÍ
            $(".photo-edit-full-wrapper").hide();
            //alert($(".photoFullWrapper").css("display"));
            $(".photo-edit-wrapper").click(function(){
                $(this).find(".photo-edit-thumbnail-underlay").hide("slow");
                $(this).find(".photo-edit-full-wrapper").show("slow");
            });
            $(".photo-edit-wrapper").dblclick(function(){
                $(this).find(".photo-edit-thumbnail-underlay").show("slow");
                $(this).find(".photo-edit-full-wrapper").hide("slow");
            });
            
            //SCRIPT ZAJIŠŤUJÍCÍ ZÍSKÁNÍ A NÁSLEDNĚ VÝPIS NÁZVU ZVOLENÉ FOTOGRAFIE DO LABELU FILE INPUTU (nástroj přidání fotografie)
            $("#photo-add-file").change(function(){
                var fullFilePath = $(this).val();
                var splitedFilePath = fullFilePath.split("\\");
                $("#photo-add-label").text(splitedFilePath[2]);
            });
            
            //SCRIPT ZAJIŤUJÍCÍ ZÍSKÁNÍ A NÁSLEDNĚ VÝPIS NÁZVU TITULNÍ FOTOGRAFIE ČLÁNKU DO LABELU FILE INPUTU (nástroj přidání článku)
            $("#article-add-title-photo-file").change(function(){
                var fullFilePath = $(this).val();
                var splitedFilePath = fullFilePath.split("\\");
                $("#file-write-1").text(splitedFilePath[2]);
            });
            
            //SCRIPT ZAJIŠŤUJÍCÍ VÝPIS NÁZVU FOTOGRAFIE UVNITŘ ČLÁNKU (v případě 1) ČI POČTU FOTOGRAFIÍ UVNITŘ ČLÁNKU DO LABELU FILE INPUTU
            $("#article-add-photos-files").change(function(){
                if(this.files.length > 1){
                    var selectedFilesResult = this.files.length;
                    $("#file-write-2").text("Počet zvolených souborů: "+selectedFilesResult);
                }
                else{
                    var fullFilePath = $(this).val();
                    var splitedFilePath = fullFilePath.split("\\");
                    $("#file-write-2").text(splitedFilePath[2]);
                }
            });
            
            //SCRIPT VYKONÁVAJÍCÍ DOPLNĚNÍ DEFAULTNÍCH NÁZVŮ POLOŽEK HLAVIČKY WEBU
            $("#header-edit-default").click(function(){
                $("#logo-title").val("IAN PHOTOGRAPHY");
                $("#portfolio-title").val("PORTFOLIO");
                $("#galery-title").val("FOTO");
                $("#blog-title").val("BLOG");
                $("#contacts-title").val("KONTAKTY");
            });
            
            //SCRIPT VYKONÁVAJÍCÍ ZVÝRAZNĚNÍ OZNAČENÉHO POZADÍ ÚVODNÍ VRSTVY VE VÝBĚROVÉM SEZNAMU, PŘI POTVRZENÍ ZÁPIS RELATIVNÍ CESTY ZVOLENÉHO POZADÍ ÚVODNÍ VRSTVY DO PŘÍSLUŠNÉHO INPUTU A AKTUALIZACI ZOBRAZENÍ NÁHLEDU POZADÍ
            $(".modal-footer .opening-layer-background-underlay").click(function(){
                var openingLayerEditBg = "";
                $(".opening-layer-bg-edit-list").css("opacity",1);
                $(this).find(".opening-layer-bg-edit-list").css("opacity",0.8);
                openingLayerEditBg = $(this).find("p").text();
                //alert(openingLayerEditBg);
                if(openingLayerEditBg != ""){
                $(".opening-layer-bg-edit-buttons .btn-info").click(function(){
                    $("#opening-layer-background").val(openingLayerEditBg);
                    $(".opening-layer-background").css("background-image","url("+openingLayerEditBg+")")
                });
            }
            });
            
            //SCRIPT VYKONÁVAJÍCÍ DOPLNĚNÍ DEFAULTNÍCH TITULKŮ ODDÍLŮ PATIČKY
            $("#footer-edit-default").click(function(){
                $("#first-section-title").val("NĚCO MÁLO O MNĚ");
                $("#second-section-title").val("O MÉ ZÁLIBĚ");
                $("#third-section-title").val("KONTAKT");
            });
            
        });