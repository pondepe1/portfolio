/** APP: Ajax Image uploader with progress bar
    Website:packetcode.com
    Author: Krishna TEja G S
    Date: 29th April 2014
***/

$(function(){
	 
	 // function from the jquery form plugin
	 $('#photo-add-form').ajaxForm({
	 	beforeSend:function(){
	 		 $(".progress").show();
	 	},
	 	uploadProgress:function(event,position,total,percentComplete){
	 		$(".progress-bar").width(percentComplete+'%'); //dynamicaly change the progress bar width
	 		$(".progress-bar").text(percentComplete+'%'); // show the percentage number
	 	},
	 	success:function(){
        },
	 	complete:function(response){
            //document.write(response.responseText);
            if(response.responseText != "Nezvolena žádná fotografie." && response.responseText != "Nezadán název fotografie." && response.responseText != "Zvolený soubor není obrázkem." && response.responseText != "Soubor již existuje." && response.responseText != "Zadaný název fotografie již existuje." && response.responseText != "Zvolený soubor je příliš velký." && response.responseText != "Povolené formáty: GIF, JPEG, JPG, PNG" && response.responseText != "Během uploadu nastala chyba."){
                $(".image").html("<img src='"+response.responseText+"' width='100%'/>");
                $(".progress-bar").removeClass("progress-bar-danger");
                $(".progress-bar").addClass("progress-bar-success");
                $("#photo-add-info-modal").modal('show');
                $("#photo-add-info-modal").on("hide.bs.modal",function(){
                    $(".progress-bar").width("0%");
                    $(".progress-bar").text("");
                    window.location.reload();
                });
                $("#photo-add-info-modal h4").html("<span class='glyphicon glyphicon-ok'></span>Fotografiee úspěšně uploadována.");
                $("#photo-add-info-modal h4").css('color','green');
            }
            else{
                $(".progress-bar").removeClass("progress-bar-success");
                $(".progress-bar").addClass("progress-bar-danger");
                $("#photo-add-info-modal").modal('show');
                $("#photo-add-info-modal").on("hide.bs.modal",function(){
                    $(".progress-bar").width("0%");
                    $(".progress-bar").text("");
                });
                $("#photo-add-info-modal h4").html("<span class='glyphicon glyphicon-remove'></span>"+response.responseText);
                $("#photo-add-info-modal h4").css('color','red');
            }
	 	}
	 });

	 $(".photo-remove-form").ajaxForm({
        beforeSend: function(){
            $(".photo-remove-modal").modal("hide");
            $("#floatingCirclesG-wrapper").show();
        },
        uploadProgress: function(event,position,total,percentComplete){
        },
        success: function(){  
        },
        complete: function(response){
            var responseParts = response.responseText.split("|");
            $("#floatingCirclesG-wrapper").hide();
            $("#photo-remove-info-modal h4").html("<span class='glyphicon glyphicon-ok'></span>"+responseParts[1]);
            $("#photo-remove-info-modal h4").css("color","green");
            $("#photo-remove-info-modal").modal("show");
            $("#photo-remove-info-modal").on("hide.bs.modal",function(){
                $("#photo-row"+responseParts[0]).remove();
            });
        }   
     });
    
   $(".photo-edit-form").ajaxForm({
        beforeSend: function(){
            $(".photo-edit-modal").modal("hide");
            $("#floatingCirclesG-wrapper").show();
        },
        uploadProgress: function(event,position,total,percentComplete){
        },
        success: function(){
        },
        complete: function(response){
            $("#floatingCirclesG-wrapper").hide();
            $("#photo-edit-info-modal").modal("show");
            if(response.responseText != "Nezadán název fotografie." && response.responseText != "Zadaný název fotografie již existuje."){
                var responseParts = response.responseText.split("|");
                $("#photo-edit-info-modal h4").html("<span class='glyphicon glyphicon-ok'></span>Fotografie "+responseParts[1]+" úspěšně editována.");
                $("#photo-edit-info-modal h4").css("color","green");
                $("#photo-edit-info-modal").on("hide.bs.modal",function(){
                    $("#photo-row"+responseParts[0]+" h3").text(responseParts[1]);
                    $("#photo-row"+responseParts[0]+" p").text(responseParts[3]+" | By: "+responseParts[4]+" | Galerie: "+responseParts[2]);
                });
            }
            else{
                $("#photo-edit-info-modal h4").html("<span class='glyphicon glyphicon-remove'></span>"+response.responseText);
                $("#photo-edit-info-modal h4").css("color","red");
            }
        }
    });
    
    $("#galery-add-form").ajaxForm({
        beforeSend: function(){
            $("#floatingCirclesG-wrapper").show();
        },
        uploadProgress: function(event,position,total,percentComplete){
        },
        success: function(){
        },
        complete: function(response){
            $("#floatingCirclesG-wrapper").hide();
            if(response.responseText != "Nezadán název galerie" && response.responseText != "Zadaný název galerie již existuje" && response.responseText != "Chyba při vykonávání dotazu"){
                $("#galery-add-info-modal h4").html("<span class='glyphicon glyphicon-ok'></span>"+response.responseText);
                $("#galery-add-info-modal h4").css("color","green");
                $("#galery-add-info-modal").modal("show");
                $("#galery-add-info-modal").on("hide.bs.modal",function(){
                    window.location.reload();
                });
            }
            else{
                $("#galery-add-info-modal h4").html("<span class='glyphicon glyphicon-remove'></span>"+response.responseText);
                $("#galery-add-info-modal h4").css("color","red");
                $("#galery-add-info-modal").modal("show");
            }
        }
    });
    
    $(".galery-remove-form").ajaxForm({
       beforeSend: function(){
           $("#floatingCirclesG-wrapper").show();
       },
       uploadProgress: function(event,position,total,percentComplete){
       },
       success: function(){
       },
       complete: function(response){
           $("#floatingCirclesG-wrapper").hide();
           if(response.responseText != "Nepodařilo se odstranit fotografii z filesystému."){
               var responseParts = response.responseText.split("|");
               //document.write($responseParts[1]);
               $("#galery-remove-info-modal h4").html("<span class='glyphicon glyphicon-ok'></span>"+responseParts[0]);
               $("#galery-remove-info-modal h4").css("color","green");
               $(".galery-remove-modal").modal("hide");
               $("#galery-remove-info-modal").modal("show");
               $("#galery-remove-info-modal").on("hide.bs.modal",function(){
                   $("#galery-row"+responseParts[1]).remove();
               });
           }
           else{
                $("#galery-remove-info-modal h4").html("<span class='glyphicon glyphicon-remove'></span>"+response.responseText);
                $("#galery-remove-info-modal h4").css("color","red");
                $(".galery-remove-modal").modal("hide");
                $("#galery-remove-info-modal").modal("show");
            }
       }
    });
    
    $(".galery-edit-form").ajaxForm({
       beforeSend: function(){
           $(".galery-edit-modal").modal("hide");
           $("#floatingCirclesG-wrapper").show();
       },
       uploadProgress: function(event,position,total,percentComplete){
       },
       success: function(){
       },
       complete: function(response){
           $("#floatingCirclesG-wrapper").hide();
           $("#galery-edit-info-modal").modal("show");
           if(response.responseText != "Nezadán název galerie." && response.responseText != "Chyba při vykonávání dotazu." && response.responseText != "Zadaný název galerie již existuje."){
               var responseParts = response.responseText.split("|");
               //document.write(responseParts[1]);
               $("#galery-edit-info-modal h4").html("<span class='glyphicon glyphicon-ok'></span>Galerie "+responseParts[1]+" úspěšně editována");
               $("#galery-edit-info-modal h4").css("color","green");
               $("#galery-edit-info-modal").on("hide.bs.modal", function(){
                   $("#galery-row"+responseParts[0]+" h3").text(responseParts[1]);
               });
           }
           else{
               $("#galery-edit-info-modal h4").css("color","red");
               $("#galery-edit-info-modal h4").html("<span class='glyphicon glyphicon-remove'></span>"+response.responseText);
           }
       }
    });
    
    $("#article-add-form").ajaxForm({
        beforeSend: function(){
            $("#floatingCirclesG-wrapper").show();
        },
        uploadProgress: function(event,position,total,percentComplete){
        },
        success: function(){
        },
        complete: function(response){
            //alert(response.responseText);
            $("#floatingCirclesG-wrapper").hide();
            var responseParts = response.responseText.split("|");
            $("#article-add-info-modal").modal("show");
            if(responseParts[0] == 1){
                $("#article-add-info-modal h4").html("<span class='glyphicon glyphicon-ok'></span>Článek "+responseParts[1]+" úspěšně vytvořen");
                $("#article-add-info-modal h4").css("color","green");
                $("#article-add-info-modal").on("hide.bs.modal",function(){
                    window.location.reload();
                });
            }
            else{
                var responseHtml = "";
                for(var i=1; i<responseParts.length; i++){
                    if(i>1){
                        responseHtml += "<p class='multiple-info-msg'>"+responseParts[i]+"</p>";
                    }
                    else responseHtml += responseParts[i];
                }
                //alert(responseHtml);
                $("#article-add-info-modal h4").html("<span class='glyphicon glyphicon-remove'></span>"+responseHtml);
                $("#article-add-info-modal h4").css("color","red");
            }
        }
    });
    
    $(".article-remove-form").ajaxForm({
        beforeSend: function(){
            $("#floatingCirclesG-wrapper").show();
            $(".article-remove-modal").modal("hide");
        },
        uploadProgress: function(event,position,total,percentComplete){
        },
        success: function(){
        },
        complete: function(response){
            //alert(response.responseText);
            $("#floatingCirclesG-wrapper").hide();
            var responseParts = response.responseText.split("|");
            $("#article-remove-info-modal").modal("show");
            var responseHtml = "";
            for(var i=2; i<responseParts.length; i++){
                if(i>2){
                    responseHtml += "<p class='multiple-info-msg'>"+responseParts[i]+"</p>";
                }
                else responseHtml += responseParts[i];
            }
            //alert(responseHtml);
            if(responseParts[0] == 1){
                $("#article-remove-info-modal h4").css("color","green");
                $("#article-remove-info-modal h4").html("<span class='glyphicon glyphicon-ok'></span>"+responseHtml);
                $("#article-remove-info-modal").on("hide.bs.modal",function(){
                   $("#article-row"+responseParts[1]).remove();
                });
            }
            else{
                $("#article-remove-info-modal h4").css("color","red");
                $("#article-remove-info-modal h4").html("<span class='glyphicon glyphicon-remove'></span>"+responseHtml);
            }
        }
    });
    
    $(".article-edit-form").ajaxForm({
        beforeSend: function(){
            $("#floatingCirclesG-wrapper").show();
            $(".article-edit-modal").modal("hide");
        },
        uploadProgress: function(event,position,total,percentComplete){
        },
        success: function(){
        },
        complete: function(response){
            //alert(response.responseText);
            $("#floatingCirclesG-wrapper").hide();
            $("#article-edit-info-modal").modal("show");
            var responseParts = response.responseText.split("|");
            var responseHtml = "";
            //alert(responseParts[0]);
            if(responseParts[0] == 1){
                for(var i=6; i<responseParts.length; i++){
                    responseHtml += responseParts[i];
                }
                $("#article-edit-info-modal h4").html("<span class='glyphicon glyphicon-ok'></span>"+responseHtml);
                $("#article-edit-info-modal h4").css("color","green");
                $("#article-edit-info-modal").on("hide.bs.modal",function(){
                    $("#article-row"+responseParts[1]+" h3").text(responseParts[5]);
                    $("#article-row"+responseParts[1]+" p").text(responseParts[2]+" | By: "+responseParts[3]+" | Kategorie: "+responseParts[4]);
                });
            }
            else{
                for(var i=1; i<responseParts.length; i++){
                    responseHtml += responseParts[i];
                }
                $("#article-edit-info-modal h4").html("<span class='glyphicon glyphicon-remove'></span>"+responseHtml);
                $("#article-edit-info-modal h4").css("color","red");
            }
        }
    });
    
    $("#category-add-form").ajaxForm({
        beforeSend: function(){
            $("#floatingCirclesG-wrapper").show();
        },
        uploadProgress: function(event,position,total,percentComplete){
        },
        success: function(){
        },
        complete: function(response){
            $("#floatingCirclesG-wrapper").hide();
            if(response.responseText != "Nezadán název kategorie" && response.responseText != "Zadaný název kategorie již existuje" && response.responseText != "Chyba při vykonávání dotazu"){
                $("#category-add-info-modal h4").html("<span class='glyphicon glyphicon-ok'></span>"+response.responseText);
                $("#category-add-info-modal h4").css("color","green");
                $("#category-add-info-modal").modal("show");
                $("#category-add-info-modal").on("hide.bs.modal",function(){
                    window.location.reload();
                });
            }
            else{
                $("#category-add-info-modal h4").html("<span class='glyphicon glyphicon-remove'></span>"+response.responseText);
                $("#category-add-info-modal h4").css("color","red");
                $("#category-add-info-modal").modal("show");
            }
        }
    });
    
    $(".category-remove-form").ajaxForm({
        beforeSend: function(){
            $(".category-remove-modal").modal("hide");
            $("#floatingCirclesG-wrapper").show();
        },
        uploadProgress: function(event,position,total,percentComplete){
        },
        success: function(){
        },
        complete: function(response){
            //alert(response);
            $("#floatingCirclesG-wrapper").hide();
            $("#category-remove-info-modal").modal("show");
            var responseParts = response.responseText.split("|");
            //alert(responseParts[0]);
            var responseHtml = "";
            if(responseParts[0] == 1){
                for(var i=2; i<responseParts.length; i++){
                    responseHtml += responseParts[i];
                }
                $("#category-remove-info-modal h4").css("color","green");
                $("#category-remove-info-modal h4").html("<span class='glyphicon glyphicon-ok'></span>"+responseHtml);
                $("#category-remove-info-modal").on("hide.bs.modal", function(){
                   $("#category-row"+responseParts[1]).remove(); 
                });
            }
            else{
                for(var i=1; i<responseParts.length; i++){
                    responseHtml += responseParts[i];
                }
                $("#category-remove-info-modal h4").css("color","red");
                $("#category-remove-info-modal h4").html("<span class='glyphicon glyphicon-remove'></span>"+responseHtml);
            }
        }
    });
    
    $(".category-edit-form").ajaxForm({
       beforeSend: function(){
           $(".category-edit-modal").modal("hide");
           $("#floatingCirclesG-wrapper").show();
       },
       uploadProgress: function(event,position,total,percentComplete){
       },
       success: function(){
       },
       complete: function(response){
           $("#floatingCirclesG-wrapper").hide();
           $("#category-edit-info-modal").modal("show");
           if(response.responseText != "Nezadán název kategorie." && response.responseText != "Chyba při vykonávání dotazu." && response.responseText != "Zadaný název kategorie již existuje."){
               var responseParts = response.responseText.split("|");
               //document.write(responseParts[1]);
               $("#category-edit-info-modal h4").html("<span class='glyphicon glyphicon-ok'></span>Galerie "+responseParts[1]+" úspěšně editována");
               $("#category-edit-info-modal h4").css("color","green");
               $("#category-edit-info-modal").on("hide.bs.modal", function(){
                   $("#category-row"+responseParts[0]+" h3").text(responseParts[1]);
               });
           }
           else{
               $("#category-edit-info-modal h4").css("color","red");
               $("#category-edit-info-modal h4").html("<span class='glyphicon glyphicon-remove'></span>"+response.responseText);
           }
       }
    });
    
    $("#header-edit-form").ajaxForm({
        beforeSend: function(){
            $("#floatingCirclesG-wrapper").show();
        },
        complete: function(response){
            $("#floatingCirclesG-wrapper").hide();
            $("#header-edit-info-modal").modal("show");
            var responseParts = response.responseText.split("|");
            //alert(responseParts[0]);
            var responseHtml = "";
            for(var i=1; i<responseParts.length; i++){
                responseHtml += responseParts[i];
            }
            //alert(responseHtml);
            if(responseParts[0] == 1){
                $("#header-edit-info-modal h4").css("color","green");
                $("#header-edit-info-modal h4").html("<span class='glyphicon glyphicon-ok'></span>"+responseHtml);
            }
            else{
                $("#header-edit-info-modal h4").css("color","red");
                $("#header-edit-info-modal h4").html("<span class='glyphicon glyphicon-remove'></span>"+responseHtml);
            }
        }
    });
    
    $("#opening-layer-edit-form").ajaxForm({
        beforeSend: function(){
            $("#floatingCirclesG-wrapper").show();
        },
        complete: function(response){
            $("#floatingCirclesG-wrapper").hide();
            $("#opening-layer-edit-info-modal").modal("show");
            var responseParts = response.responseText.split("|");
            //alert(responseParts[0]);
            var responseHtml = "";
            for(var i=1; i<responseParts.length; i++){
                responseHtml += responseParts[i];
            }
            //alert(responseHtml);
            if(responseParts[0] == 1){
                $("#opening-layer-edit-info-modal h4").css("color","green");
                $("#opening-layer-edit-info-modal h4").html("<span class='glyphicon glyphicon-ok'></span>"+responseHtml);
            }
            else{
                $("#opening-layer-edit-info-modal h4").css("color","red");
                $("#opening-layer-edit-info-modal h4").html("<span class='glyphicon glyphicon-remove'></span>"+responseHtml);
            }
        }
    });
    
    $("#footer-edit-form").ajaxForm({
        beforeSend: function(){
            $("#floatingCirclesG-wrapper").show();
        },
        complete: function(response){
            $("#floatingCirclesG-wrapper").hide();
            $("#footer-edit-info-modal").modal("show");
            var responseParts = response.responseText.split("|");
            //alert(responseParts[0]);
            var responseHtml = "";
            for(var i=1; i<responseParts.length; i++){
                responseHtml += responseParts[i];
            }
            //alert(responseHtml);
            if(responseParts[0] == 1){
                $("#footer-edit-info-modal h4").css("color","green");
                $("#footer-edit-info-modal h4").html("<span class='glyphicon glyphicon-ok'></span>"+responseHtml);
            }
            else{
                $("#footer-edit-info-modal h4").css("color","red");
                $("#footer-edit-info-modal h4").html("<span class='glyphicon glyphicon-remove'></span>"+responseHtml);
            }
        }
    });
    
    $("#contacts-edit-form").ajaxForm({
       beforeSend: function(){
           $("#floatingCirclesG-wrapper").show();
       },
       complete: function(response){
           $("#floatingCirclesG-wrapper").hide();
           $("#contacts-edit-info-modal").modal("show");
           var responseParts = response.responseText.split("|");
           //alert(responseParts[0]);
           var responseHtml = "";
           for(var i=1; i<responseParts.length; i++){
               responseHtml += responseParts[i];
           }
           //alert(responseHtml);
           if(responseParts[0] == 1){
                $("#contacts-edit-info-modal h4").css("color","green");
                $("#contacts-edit-info-modal h4").html("<span class='glyphicon glyphicon-ok'></span>"+responseHtml);
            }
            else{
                $("#contacts-edit-info-modal h4").css("color","red");
                $("#contacts-edit-info-modal h4").html("<span class='glyphicon glyphicon-remove'></span>"+responseHtml);
            }
       }
    });
    
});