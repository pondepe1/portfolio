//SCRIPT PRO PŘIHLÁŠENÍ POMOCÍ TECHNOLOGIE AJAX
    $(document).ready(function(){
        $('#login_btn').click(function(){
            var login = $('#login').val(),
                password = $('#password').val(),
                loginInput = $('#login'),
                passwordInput = $('#password');

            if(login !== '' && password !== ''){
                if(window.XMLHttpRequest) var xmlHttp = new XMLHttpRequest();
                else var xmlHttp = new ActiveXObject('Microsoft.XMLHTTP');

                xmlHttp.onreadystatechange = function(){
                    //alert(xmlHttp.readyState);
                    if(xmlHttp.readyState == 4 && xmlHttp.status == 200){
                        $('#login_msg').text(xmlHttp.responseText);
                        if(xmlHttp.responseText == ''){
                            window.location.reload();
                        }
                    }
                }

                xmlHttp.open('POST','php_solutions/login.php',true);
                xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                xmlHttp.send('login='+login+'&password='+md5(password));

            }
            else{
                if(login == ""){
                    loginInput.css("boxShadow","0px 0px 10px red");
                    loginInput.addClass("form_invalid");
                    loginInput.attr("placeholder","NEZADÁNO ŽÁDNÉ PŘIHLAŠOVACÍ JMÉNO!");
                }
                if(password == ""){
                    passwordInput.css("boxShadow","0px 0px 10px red");
                    passwordInput.addClass("form_invalid");
                    passwordInput.attr("placeholder","NEZADÁNO ŽÁDNÉ HESLO");
                }
                return;
            }
        });
    });

    //SCRIPT PRO ZPĚTNÝ NÁVRAT DEFAULTNÍCH STYLŮ INPUTŮ PŘIHLAŠOVACÍHO FORMULÁŘE
    $(document).ready(function(){
        $('#login').focus(function(){
            $(this).removeClass("form_invalid");
            $(this).css("box-shadow","0px 0px 10px #5BC0DE");
            $(this).attr("placeholder","ZADEJTE PŘIHLAŠOVACÍ JMÉNO");
        });
        $('#password').focus(function(){
            $(this).removeClass("form_invalid");
            $(this).css("box-shadow","0px 0px 10px #5BC0DE");
            $(this).attr("placeholder","ZADEJTE HESLO");
        });
        $('#login,#password').blur(function(){
            $(this).css("box-shadow","0px 0px 0px transparent");
        });
    });