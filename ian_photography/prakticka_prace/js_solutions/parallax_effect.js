  //SCRIPT OVLÁDAJÍCÍ PARALLAX EFFECT NA ÚVODNÍ STRÁNCE WEBU
        var prlxLayer = document.getElementsByClassName('layer_underlay')[0];
        var prlxLayer2 = document.getElementsByClassName('layer_underlay')[2];
        var prlxLayer3 = document.getElementsByClassName('layer_underlay')[4]; //Získání požadovaných elementů stránky, jejich uložení do proměnných

        function defaultApply(){
            prlxLayer.style.top = 0 + 'px';
            prlxLayer2.style.top = 0 + 'px';
            prlxLayer3.style.top = 0 + 'px';
        }

        function parallax(){
            if(window.innerWidth > 991){ //Podmínka rozhodující o aktivaci scriptu na základě průběžného snímání šířky okna (problém s výpočty u méně výkonných zařízení)
            var yOffset = window.pageYOffset; //Snímání vzdálenosti scrollované pozice od horního okraje okna


            var prlxLayer2Offset = prlxLayer2.offsetTop;
            var prlxLayer3Offset = prlxLayer3.offsetTop; //Snímání vzdálenosti elementů od horního okraje okna

            prlxLayer.style.top = (yOffset * 0.48) + 'px';
            prlxLayer2.style.top = -(prlxLayer2Offset - yOffset)*0.4 + 'px';
            prlxLayer3.style.top = -(prlxLayer3Offset - yOffset)*0.2 + 'px'; //Zpomalení scrollu požadovaných elementů
          }
        }

        window.addEventListener('scroll',parallax);
        window.addEventListener('resize',defaultApply);  