$(document).ready(function(){
    
    //SCRIPT OVLÁDAJÍCÍ VYSUNUTÍ DROPDOWN NABÍDKY UŽIVATELSKÉHO PANELU PRO MALÁ ROZLIŠENÍ
    $('.glyphicon-triangle-top').hide();
    $('.dropdown').hover(function(){
        $(this).find('.dropdown-menu').first().stop(true, true).slideToggle(400);
        $(this).find('.glyphicon-triangle-bottom').toggle();
        $(this).find('.glyphicon-triangle-top').toggle();
    });
    
    //SCRIPT ZAJIŠŤUJÍCÍ PRŮHLEDNOST HLAVNÍ NABÍDKY
    function headerOpacity(){
        var pageOffset = window.pageYOffset,
            headerWrapper = $('header');
        if(pageOffset > 160){
            headerWrapper.css("opacity",0.96);
        }
        if(pageOffset < 160){
            headerWrapper.css("opacity",1);
        }
    }
    
    //SCRIPT OVLÁDAJÍCÍ VYSUNUTÍ NABÍDKY PRO MALÁ ROZLIŠENÍ
    $(function(){
        var pull 	= $('#pull'),
        menu 		= $('ul.main_menu_collapsed'),
        menuHeight	= menu.height();

        $(pull).click(function(e) {
            e.preventDefault();
            menu.slideToggle();
        });  
    });
    window.addEventListener('scroll',headerOpacity);
    
});