$(document).ready(function(){
    
   $("#contact_form").ajaxForm({
       beforeSend: function(){
           $("#floatingCirclesG-wrapper").show();
       },
       complete: function(response){
           
           //alert(response.responseText);
           $("#floatingCirclesG-wrapper").hide();
           $("#contact-form-info-modal").modal("show");
           var responseParts = response.responseText.split("|");
           //alert(responseParts[0]);
           
           var responseHtml = "";
           for(var i=1; i<responseParts.length; i++){
               responseHtml += responseParts[i];
           }
           //alert(responseHtml);
           
           if(responseParts[0] == 1){
               $("#contact-form-info-modal h4").css("color","green");
               $("#contact-form-info-modal h4").html("<span class='glyphicon glyphicon-ok'></span>"+responseHtml);
           }
           else{
               $("#contact-form-info-modal h4").css("color","red");
               $("#contact-form-info-modal h4").html("<span class='glyphicon glyphicon-remove'></span>"+responseHtml);
           }
           
       }
   });
    
});