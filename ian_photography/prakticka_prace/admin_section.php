<?php

require "libs/Smarty.class.php";
require "php_solutions/database_manipulation.php";
require "php_solutions/redaction_system.php";

$smarty = new Smarty();

$backupFile = $_GET['backupFile'];

if(isset($_GET['currentPage']) && !empty($_GET['currentPage'])){
    if(isset($_GET['articleId']) && !empty($_GET['articleId'])){
        $backupFile = $backupFile."?articleId=".$_GET['articleId']."&currentPage=".$_GET['currentPage'];
    }
    else $backupFile = $backupFile."?pageStart=".$_GET['currentPage'];
}

$openingLayerCurrentBg = $dbDataMan->getDbData($con,"SELECT bg_path FROM user_backgrounds");
//print_r($openingLayerCurrentBg);

$photosEditArray = $dbDataMan->getDbData($con,"SELECT photos.id,photos.time_stamp,photos.photo_title,photos.photo_path,users.login,galeries.galery_title
                                            FROM photos INNER JOIN users ON photos.author = users.id
                                            INNER JOIN galeries ON photos.galery = galeries.id
                                            ORDER BY photos.id DESC");
$galeriesEditArray = $dbDataMan->getDbData($con,"SELECT galeries.id,galeries.time_stamp,galeries.galery_title,galeries.display,users.login
                                                  FROM galeries INNER JOIN users ON galeries.author = users.id
                                                  ORDER BY galeries.id DESC");
$articlesEditArray = $dbDataMan->getDbData($con,"SELECT articles.id,articles.time_stamp,articles.article_title,articles.content,articles.title_photo,
                                                 categories.category_title,users.login
                                                 FROM articles INNER JOIN users ON articles.author = users.id
                                                 INNER JOIN categories ON articles.category = categories.id
                                                 ORDER BY articles.id DESC");
$categoriesEditArray = $dbDataMan->getDbData($con,"SELECT categories.id,categories.time_stamp,categories.category_title,categories.display,users.login
                                                   FROM categories INNER JOIN users ON categories.author = users.id
                                                   ORDER BY categories.id DESC");

$smarty->assign("backupFile",$backupFile);
$smarty->assign("openingLayerCurrentBg",$openingLayerCurrentBg);
$smarty->assign("photosEditArray",$photosEditArray);
$smarty->assign("galeriesEditArray",$galeriesEditArray);
$smarty->assign("articlesEditArray",$articlesEditArray);
$smarty->assign("categoriesEditArray",$categoriesEditArray);
$smarty->assign("galeryFiles",$galeryFiles);
$smarty->assign("categoryFiles",$categoryFiles);
$smarty->assign("userContent",$userContent);
$smarty->display("admin_section.tpl");